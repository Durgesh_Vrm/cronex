<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\FrontController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// GET ROUTE
Route::get('/',[FrontController::class, 'index']);
Route::get('about-us',[FrontController::class, 'about']);
Route::get('contact-us',[FrontController::class, 'contact']);
Route::get('service',[FrontController::class, 'Service']);
Route::get('private_policy',[FrontController::class, 'private_policy']);
Route::get('t&c',[FrontController::class, 'tc']);
Route::get('faq',[FrontController::class, 'faq']);




