<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\SettingController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\EmailController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// GET ROUTE



Route::get('cronex-login', function () {
    return redirect('login');
});

Auth::routes(['verify' => true]);


Route::middleware('auth')->group(function () {

Route::get('/dashboard', [HomeController::class, 'index'])->name('dashboard');

//Rols 
Route::get('user-permissions',[SettingController::class, 'user_permission']);

Route::get('user-rout',[SettingController::class, 'view_user_rout']);

Route::get('send-updates',[SettingController::class, 'view_send_updates']);




//Users
Route::get('user-rols',[UserController::class, 'view_user_rols'])->middleware('routepermisan');

Route::get('user-information/{id}',[UserController::class, 'user_information']);

Route::get('user-edit-information/{id}',[UserController::class, 'user_edit_information']);

Route::get('view-users',[UserController::class, 'view_user_all']);

Route::get('edit-delete-users',[UserController::class, 'edit_delete_users_all']);

Route::match(['get', 'post'], 'find-user',[UserController::class, 'view_user_info']);

Route::get('profile',[ProfileController::class, 'UserProfile']);

Route::get('profile/{name}',[ProfileController::class, 'UserProfile']);

Route::get('user-registration',[UserController::class, 'user_registration']);

Route::get('view-teams',[UserController::class, 'view_all_teams']);

Route::get('view-team-members',[UserController::class, 'view_team_members']);

Route::get('timeline',[UserController::class, 'view_timeline']);








//data 
Route::get('error-page',[SettingController::class, 'error_page']);


// location
Route::get('countries',[SettingController::class, 'view_countries']); //->middleware('userpermisan')
Route::get('states',[SettingController::class, 'view_states']);
Route::get('cities',[SettingController::class, 'view_cities']);

Route::post('select-cities-name',[SettingController::class, 'select_cities_name']);




});







// POST ROUTE

//login_verify
Route::post('authenticate', [LoginController::class, 'login_verify']);




// user
Route::post('registration',[UserController::class, 'registration']);
Route::post('change-user-role',[UserController::class, 'change_user_role']);
Route::post('change-user-role-name',[UserController::class, 'change_user_role_name']);
Route::post('change-route-permissions',[UserController::class, 'change_route_permission']);
Route::post('all-route-permissions',[UserController::class, 'all_route_permissions']);



Route::post('change-user-role-view-p',[UserController::class, 'change_user_role_view_p']);
Route::post('change-user-role-create-p',[UserController::class, 'change_user_role_create_p']);
Route::post('change-user-role-update-p',[UserController::class, 'change_user_role_update_p']);
Route::post('change-user-role-delete-p',[UserController::class, 'change_user_role_delete_p']);




Route::post('user-profile-uplode',[ProfileController::class, 'user_profile_uplode']);
Route::post('update-profile',[ProfileController::class, 'update_profile']);


Route::post('search-user',[UserController::class, 'search_user']);
Route::post('search-users',[UserController::class, 'search_users']);





Route::get('web-configuration-settings-value',[SettingController::class, 'web_configuration_settings_value']);
Route::get('web-configuration-settings-logo',[SettingController::class, 'web_configuration_settings_logo']);
Route::get('web-configuration-settings-banner',[SettingController::class, 'web_configuration_settings_banner']);
Route::get('web-configuration-settings-color',[SettingController::class, 'web_configuration_settings_color']);
Route::get('web-configuration-settings-loader',[SettingController::class, 'web_configuration_settings_loader']);

Route::post('create-configuration-settings-value',[SettingController::class, 'create_configuration_settings_value']);
Route::post('create-configuration-settings-logo',[SettingController::class, 'create_configuration_settings_logo']);
Route::post('create-configuration-settings-banner',[SettingController::class, 'create_configuration_settings_banner']);
Route::post('create-configuration-settings-color',[SettingController::class, 'create_configuration_settings_color']);
Route::post('create-configuration-settings-loader',[SettingController::class, 'create_configuration_settings_loader']);

Route::get('edit-configuration-settings-value/{id}',[SettingController::class, 'edit_configuration_settings_value']);
Route::get('edit-configuration-settings-logo/{id}',[SettingController::class, 'edit_configuration_settings_logo']);
Route::get('edit-configuration-settings-banner/{id}',[SettingController::class, 'edit_configuration_settings_banner']);
Route::get('edit-configuration-settings-color/{id}',[SettingController::class, 'edit_configuration_settings_color']);
Route::get('edit-configuration-settings-loader/{id}',[SettingController::class, 'edit_configuration_settings_loader']);

Route::post('update-configuration-settings-value',[SettingController::class, 'update_configuration_settings_value']);
Route::post('update-configuration-settings-logo',[SettingController::class, 'update_configuration_settings_logo']);
Route::post('update-configuration-settings-banner',[SettingController::class, 'update_configuration_settings_banner']);
Route::post('update-configuration-settings-color',[SettingController::class, 'update_configuration_settings_color']);
Route::post('update-configuration-settings-loader',[SettingController::class, 'update_configuration_settings_loader']);






Route::get('email-compose',[EmailController::class, 'view_email_compose']);
Route::get('mail-inbox',[EmailController::class, 'view_email_inbox']);
Route::get('mail-send',[EmailController::class, 'view_email_send']);
Route::get('mail-read/{id}',[EmailController::class, 'view_email_read']);




// theme view setting changes
Route::get('activate-theme-mode',[SettingController::class, 'ActivateThemeMode']);
Route::get('change-menu-style',[SettingController::class, 'ChangeMenuStyle']);
// two step verification on off
Route::post('change-type-tsv',[ProfileController::class, 'change_type_tsvs']);
// ajax capcha code
Route::post('get-captcha-code', [UserController::class, 'get_captcha_code']);
// ajax  search to email 
Route::post('Get-to-emails',[EmailController::class, 'get_to_emails']);
// ajax  search bcc email 
Route::post('Get-bcc-emails',[EmailController::class, 'get_bcc_emails']);
// ajax  search cc email 
Route::post('Get-cc-emails',[EmailController::class, 'get_cc_emails']);
// ajax on off profile show
Route::post('Change-show-profile',[ProfileController::class, 'change_show_profile']);
// ajax on off Activities Log
Route::post('Change-Activities-Log',[ProfileController::class, 'Change_Activities_Log']);
// ajax on off Notification
Route::post('Change-Notification-on',[ProfileController::class, 'change_notification_on']);
// ajax last update logout time update 
Route::post('user-logout-time',[ProfileController::class, 'user_log_time']);
// ajax get template
Route::post('get-email-templ',[EmailController::class, 'get_email_templ']);
//ajax get email in online email
Route::get('get-inbox-email',[EmailController::class, 'get_inbox_email']);




Route::get('create-content',[SettingController::class, 'create_content']);
Route::get('view-content',[SettingController::class, 'view_content']);
Route::get('edit-content/{id}',[SettingController::class, 'edit_content']);
Route::get('delete-content/{id}',[SettingController::class, 'deletecontent']);
Route::post('add-content',[SettingController::class, 'add_content']);
Route::post('update-content',[SettingController::class, 'update_content']);
 

Route::get('add-banner-logo',[SettingController::class, 'add_banner_logo']);
Route::get('view-banner-logo',[SettingController::class, 'view_banner_logo']);
Route::get('edit-bannerandlogo/{id}',[SettingController::class, 'edit_banner_logo']);
Route::get('delete-bannerandlogo/{id}',[SettingController::class, 'deletebannerlogo']);
Route::post('insert-banner-logo',[SettingController::class, 'insert_banner_logo']);
Route::post('update-banner-logo',[SettingController::class, 'update_banner_logo']);


Route::get('add-package',[SettingController::class, 'add_packaging']);
Route::get('view-package',[SettingController::class, 'view_packaging']);
Route::get('delete-package/{id}',[SettingController::class, 'delete_packaging']);
Route::get('edit-package/{id}',[SettingController::class, 'edit_packaging']);
Route::post('update-package',[SettingController::class, 'update_packaging']);
Route::post('insert-package',[SettingController::class, 'insert_package']);


Route::get('add-service-fee',[SettingController::class, 'add_service_fee']);
Route::get('view-service-fee',[SettingController::class, 'view_service_fee']);
Route::get('delete-service/{id}',[SettingController::class, 'delete_service_fee']);
Route::get('edit-service/{id}',[SettingController::class, 'edit_service_fee']);
Route::post('insert-service-fee',[SettingController::class, 'insert_service_fee']);
Route::post('update-service-fee',[SettingController::class, 'update_service_fee']);

Route::get('View-Center-Percentage',[SettingController::class, 'view_center_percentage']);
Route::post('update-center-percentage',[SettingController::class, 'update_center_percentage']);
