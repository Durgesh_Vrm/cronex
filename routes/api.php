<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\ApiController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::post('user-reg-code',[ApiController::class, 'user_reg_code']);

Route::post('user-registration',[ApiController::class, 'user_registration']);

Route::post('user-otp-verify',[ApiController::class, 'otp_verification']);

Route::post('add-address',[ApiController::class, 'add_address']);

Route::post('login',[ApiController::class, 'user_login']);

Route::post('login-otp-verify',[ApiController::class, 'login_otp_verification']);

Route::get('static-content',[ApiController::class, 'static_content']);

Route::get('static-content/{id}',[ApiController::class, 'static_content_id']);

Route::get('countries',[ApiController::class, 'countries']);

Route::get('countries/{id}',[ApiController::class, 'countries_id']);

Route::get('state',[ApiController::class, 'state']);

Route::get('state/{id}',[ApiController::class, 'state_id']);

Route::get('city',[ApiController::class, 'city']);

Route::get('city/{id}',[ApiController::class, 'city_id']);

Route::post('get-address',[ApiController::class, 'get_address']);

Route::post('get-user-address',[ApiController::class, 'get_address_id']);

Route::post('update-address',[ApiController::class, 'update_address']);

Route::post('delete-address',[ApiController::class, 'delete_address']);

Route::post('get-package',[ApiController::class, 'get_package']);

Route::post('get-package-id',[ApiController::class, 'get_package_id']);

Route::post('update-package',[ApiController::class, 'update_package']);

Route::post('delete-package',[ApiController::class, 'delete_package']);
Route::post('add-package',[ApiController::class, 'add_package']);
Route::get('get-service-fee',[ApiController::class, 'get_service_fee']);

Route::post('order-booking',[ApiController::class, 'order_booking']);

Route::post('get-near-hub',[ApiController::class, 'get_near_hub']);
Route::post('get-order-booking',[ApiController::class, 'get_order_booking']);
Route::post('getid-order-booking',[ApiController::class, 'getid_order_booking']);


