/**
 *
 * You can write your JS code here, DO NOT touch the default style file
 * because it will make it harder for you to update.
 * 
 */


//submit form confirmation
  function SubmitForm(id)
  {
    swal({
      title: "Are you sure to Submit the form?",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
        document.getElementById(id).submit();
      } 
    });
  }



//edit form confirmation
  function EditForm(id)
  {
    swal({
      title: "Are you sure you want to edit?",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
        document.getElementById(id).submit();
      } 
    });
  } 

  //edit form confirmation
  function DeleteForm(id)
  {
    swal({
      title: "Are you sure you want to Delete?",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
        document.getElementById(id).submit();
      } 
    });
  } 

$("#BCC").on("click", function() {
	if($("#IBCC").is(":visible")){ $("#IBCC").hide(); $('#BCC').removeClass('active'); $('#email_address_bcc').val(''); }
	else if($("#IBCC").is(":hidden")){ $("#IBCC").show(); $("#BCC").addClass('active'); }
	else { }    
});

$("#CC").on("click", function() {
	if($("#ICC").is(":visible")){ $("#ICC").hide();  $('#CC').removeClass('active');  $('#email_address_cc').val(''); }
	else if($("#ICC").is(":hidden")){ $("#ICC").show();  $("#CC").addClass('active'); }
	else { }
});





"use strict";

