/*
 ----------------------------------------------------------------------
 Preloader
 ----------------------------------------------------------------------
 */
$(window).on("load", function(e) {

    "use strict";

    $(".loader").delay(5000).fadeOut();
    $(".animationload").delay(5000).fadeOut("fast");

});
