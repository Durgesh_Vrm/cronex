<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <title><?php echo e(config('APP_NAME')); ?> - Lgoin</title>
  <!-- General CSS Files -->
  <link rel="stylesheet" href="<?php echo e(asset('assets/css/app.min.css')); ?>">
  <link rel="stylesheet" href="assets/bundles/izitoast/css/iziToast.min.css">

  <!-- Template CSS -->
  <link rel="stylesheet" href="<?php echo e(asset('assets/css/style.css')); ?>">
  <link rel="stylesheet" href="<?php echo e(asset('assets/css/components.css')); ?>">

  <!-- Custom style CSS -->
  <link rel="stylesheet" href="<?php echo e(asset('assets/css/custom.css')); ?>">
  <link rel='shortcut icon' type='image/x-icon' href="<?php echo e(asset('assets/img/favicon.ico')); ?>" />
  <style type="text/css">
    #banner {
  background: url('<?php echo e(asset('uplode/config/banner/'.config('LOGIN_BANNER'))); ?>');
  background-repeat: no-repeat;
  background-size: cover;
  background-attachment: fixed;
  background-position: center; 
     }

.container-login100::before {
    content: "";
    display: block;
    position: absolute;
    z-index: -1;
    width: 100%;
    height: 100%;
    top: 0;
    left: 0;
    background: rgba(93,84,240,0.5);
    background: -webkit-linear-gradient(left, rgba(0,168,255,0.5), rgba(185,0,255,0.5));
    background: -o-linear-gradient(left, rgba(0,168,255,0.5), rgba(185,0,255,0.5));
    background: -moz-linear-gradient(left, rgba(0,168,255,0.5), rgba(185,0,255,0.5));
    background: linear-gradient(left, rgba(0,168,255,0.5), rgba(185,0,255,0.5));
    pointer-events: none;
}

  .card {
    margin-top: 30%;
    background-color: #fff;
    border-radius: 10px;
    border: none;
    position: relative;
    margin-bottom: 30px;
    box-shadow: 0 0.46875rem 2.1875rem rgba(90,97,105,0.1), 0 0.9375rem 1.40625rem rgba(90,97,105,0.1), 0 0.25rem 0.53125rem rgba(90,97,105,0.12), 0 0.125rem 0.1875rem rgba(90,97,105,0.1);
}
  </style>
</head>

<body  class="container-login100" id="banner">
  <div class="loader"></div>
  <div id="app">
    <section class="section">
      <div class="container mt-5">
        <div class="row">
          <div class="col-12 col-sm-8 offset-sm-2 col-md-6 offset-md-3 col-lg-6 offset-lg-3 col-xl-4 offset-xl-4">
            <div class="card card-primary">
              <div class="card-header">
                <h4>Two Factor Authentication</h4>
              </div>
              <div class="card-body">
                <p class="text-muted">Enter Your OTP</p>
                <form>

                  <div class="form-group">
                    <label for="password">New One Time Password</label>
                    <input id="otp" type="text" class="form-control pwstrength" data-indicator="pwindicator"
                      name="otp" tabindex="2" autocomplete="new-otp" pattern=".{6,6}" required title="6 Number">
                    <div id="pwindicator" class="pwindicator">
                      <div class="bar"></div>
                      <div class="label"></div>
                      <?php $__errorArgs = ['otp'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                          <span class="invalid-feedback" role="alert">
                              <strong><?php echo e($message); ?></strong>
                          </span>
                      <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                    </div>
                  </div>

                  <div class="form-group">
                    <input id="email" type="hidden" class="form-control <?php $__errorArgs = ['email'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" name="email" tabindex="1" value="<?php echo e(session()->get('USER_NAME')); ?>" autocomplete="email" required autofocus>
                    <?php $__errorArgs = ['email'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                      <span class="invalid-feedback" role="alert">
                        <strong><?php echo e($message); ?></strong>
                      </span>
                    <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                  </div>
                  <div class="form-group">
                    <input id="password" type="hidden" name="password" tabindex="2" value="<?php echo e(session()->get('USER_PASS')); ?>" class="form-control <?php $__errorArgs = ['password'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" required autocomplete="current-password">
                  </div>



                  <div class="form-group text-center">
                    <button type="button" onclick="TwoFactorAuthentication(this.id)" id="TWOFA"  class="btn btn-success btn-md" tabindex="4">
                      <i class="fas fa-lock-open"></i> Submit
                    </button>
                    <button type="button" onClick="window.location.reload();" class="btn btn-warning btn-md" tabindex="4">
                      <i class="fas fa-redo"></i> Resend OTP
                    </button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  <!-- General JS Scripts -->
  <script src="<?php echo e(asset('assets/js/app.min.js')); ?>"></script>
  <!-- JS Libraies -->
  <!-- Page Specific JS File -->
  <!-- Template JS File -->
  <script src="<?php echo e(asset('assets/js/scripts.js')); ?>"></script>
  <!-- Custom JS File -->
  <script src="<?php echo e(asset('assets/js/custom.js')); ?>"></script>
    <!-- JS Libraies -->
  <script src="assets/bundles/izitoast/js/iziToast.min.js"></script>
  <!-- Page Specific JS File -->
  <script src="assets/js/page/toastr.js"></script>


  <script type="text/javascript">
    function TwoFactorAuthentication(data) {
      var email = $('#email').val();
      var password = $('#password').val();
      var otp = $('#otp').val();
      
       $.ajax({
       type:"POST",
       url:"<?php echo e(url('/authenticate')); ?>",
       data:{"_token": "<?php echo e(csrf_token()); ?>", "password": password, "email": email, "otp": otp},
       success:function(result)
       { 
          console.log(result);
          if(result == 'error'){
            var count = sessionStorage.getItem("count"); 
          if(count == 1)
                {  sessionStorage.removeItem('count'); 
                   window.location.href='<?php echo e(url("login")); ?>'; 
                }

           else if(count == 2)
                {  sessionStorage.setItem("count", '1'); }

          else if (count == null)
                { sessionStorage.setItem("count", '2'); }
              
          }
          else{
              window.location.href='<?php echo e(url("home")); ?>'; 
          } 
         
            iziToast.error({
              title: 'Error!',
              message: 'This OTP is wrong  '+ sessionStorage.getItem("count") +' attempt remaining',
              position: 'topCenter'
            });

       },
       error:function(result) {

          $.each(result.responseJSON.errors,function(field_name,error){
          $(document).find('[name='+field_name+']').after('<span class="text-strong text-danger">' +error+ '</span>')
                        })
       }
      });
    }
  </script>
</body>
</html><?php /**PATH C:\wamp64\www\laravel9\resources\views/auth/passwords/two-factor.blade.php ENDPATH**/ ?>