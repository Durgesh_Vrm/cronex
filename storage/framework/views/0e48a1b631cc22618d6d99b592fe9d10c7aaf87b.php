
<?php $__env->startSection('content'); ?>
<style>
/* width */
::-webkit-scrollbar {
  width: 3px;
}
/* Track */
::-webkit-scrollbar-track {
  background: #f1f1f1; 
}
/* Handle */
::-webkit-scrollbar-thumb {
  background: #888; 
}
/* Handle on hover */
::-webkit-scrollbar-thumb:hover {
  background: #555; 
}
</style>
<?php $color_arrar = array('col-red','col-orange','col-blue-grey','col-yellow','col-pink','col-grey','col-blue','col-green','col-purple','col-cyan','col-lime','col-teal','col-black');?>
      <!-- Main Content -->
      <div class="main-content">
        <section class="section">
          <ul class="breadcrumb breadcrumb-style ">
            <li class="breadcrumb-item">
              <h4 class="page-title m-b-0">Inbox</h4>
            </li>
            <li class="breadcrumb-item">
              <a href="<?php echo e(url('dashboard')); ?>">
                <i data-feather="home"></i></a>
            </li>
            <li class="breadcrumb-item">Email</li>
            <li class="breadcrumb-item">Inbox</li>
          </ul>
          <div class="section-body">
            <div class="row">
              <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                <div class="card">
                  <div class="body">
                    <div id="mail-nav">
                     <a href="<?php echo e(url('email-compose')); ?>"> <button type="button" class="btn btn-danger waves-effect btn-compose m-b-15">COMPOSE</button></a>
                      <ul class="" id="mail-folders">
                        <li>
                          <a href="<?php echo e(url('mail-inbox')); ?>" title="Inbox">Inbox (<?php echo e($data['count']); ?>)
                          </a>
                        </li>
                        <li>
                          <a href="<?php echo e(url('mail-send')); ?>" title="Sent">Sent</a>
                        </li>
                      </ul>
                      <h5 class="b-b p-10 text-strong">Templates</h5>
                      <ul class="" id="mail-labels" style="height: 441px; overflow-y: scroll;">
                        <?php $__currentLoopData = $data['all']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <li>
                          <a href="javascript:;" id="<?php echo e($value->id); ?>" onclick="get_temp(this.id)">
                            <i class="material-icons <?php echo e($color_arrar[$key]); ?>">local_offer</i><?php echo e($value->subject); ?></a>
                        </li>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>    
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                <div class="card">
                  <div class="boxs mail_listing">
                    <div class="inbox-center table-responsive">
                      <table class="table table-hover">
                        <thead>
                          <tr>
                            <th class="pl-3">
                              <div class="pretty p-default p-curve p-thick">
                                <input type="checkbox" />
                                <div class="state p-warning">
                                  <label></label>
                                </div>
                              </div>
                            </th>
                            <th colspan="3">
                              <div class="inbox-header">
                                <div class="mail-option">
                                  <div class="email-btn-group m-l-15">
                                    <a href="<?php echo e(url()->previous()); ?>" class="col-dark-gray waves-effect m-r-20" title="back"
                                      data-toggle="tooltip">
                                      <i class="material-icons">keyboard_return</i>
                                    </a>
                                    <a href="<?php echo e(url('get-inbox-email')); ?>" class="col-dark-gray waves-effect m-r-20" title="Refresh"
                                      data-toggle="tooltip">
                                      <i class="material-icons">autorenew</i>
                                    </a>
                                    <a href="#" class="col-dark-gray waves-effect m-r-20" title="Delete"
                                      data-toggle="tooltip">
                                      <i class="material-icons">delete</i>
                                    </a>
                                  </div>
                                </div>
                              </div>
                            </th>
                            <th class="hidden-xs" colspan="2">
                              <div class="pull-right">
                                <div class="email-btn-group m-l-15">
                                  <a href="<?php echo e($data['emails']->previousPageUrl()); ?>" class="col-dark-gray waves-effect m-r-20" title="previous"
                                    data-toggle="tooltip">
                                    <i class="material-icons">navigate_before</i>
                                  </a>
                                  <a href="<?php echo e($data['emails']->nextPageUrl()); ?>" class="col-dark-gray waves-effect m-r-20" title="next"
                                    data-toggle="tooltip">
                                    <i class="material-icons">navigate_next</i>
                                  </a>
                                </div>
                              </div>
                            </th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php $__currentLoopData = $data['emails']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                          <tr class="unread">
                            <td class="tbl-checkbox">
                              <div class="pretty p-default p-curve p-thick">
                                <input type="checkbox" />
                                <div class="state p-warning">
                                  <label></label>
                                </div>
                              </div>
                            </td>
                            <td class="hidden-xs">
                              <i class="material-icons">star_border</i>
                            </td>
                            <?php  $name = explode("<",$value->from); ?>
                            <td class="hidden-xs"><?php echo e($name[0]); ?></td>
                            <td class="max-texts">
                              <a href="#"><?php echo e(substr($value->subject,0,30)); ?>...</a>
                            </td>
                            <td class="hidden-xs">
                              <i class="material-icons">attach_file</i>
                            </td>
                            <td class="text-right"><?php echo e(substr($value->date,0,25)); ?></td>
                          </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tbody>
                      </table>
                    </div>
                    <div class="row">
                      <div class="col-sm-7 ">
                        <?php  $num = explode("=",$data['emails']->previousPageUrl()); ?>
                        <p class="p-15">Showing <?php if(isset($num[1])): ?> <?php echo e($num[1]+1); ?> - <?php echo e(count($data['emails'])*($num[1]+1)); ?> <?php else: ?> 1 - <?php echo e(count($data['emails'])); ?> <?php endif; ?> of <?php echo e($data['count']); ?></p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\wamp64\www\cronex\resources\views/Email/email-inbox.blade.php ENDPATH**/ ?>