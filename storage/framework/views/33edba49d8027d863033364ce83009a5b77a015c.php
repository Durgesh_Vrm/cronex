
<?php $__env->startSection('content'); ?>
  <!-- Main Content -->
  <div class="main-content">
    <section class="section">
      <ul class="breadcrumb breadcrumb-style ">
        <li class="breadcrumb-item">
          <a href="<?php echo e(url('dashboard')); ?>">
            <i data-feather="home"></i></a>
        </li>
        <li class="breadcrumb-item">Setting</li>
        <li class="breadcrumb-item active">Website & App Content</li>
        <li class="breadcrumb-item active">Create Content</li>
      </ul>
      <div class="section-body">
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="card">
              <div class="boxs mail_listing">
              <form class="composeForm" action="<?php echo e(url('update-content')); ?>" method="post" id="form">
                <?php echo csrf_field(); ?>
                <input type="hidden" id="iid" name="id" required="" value="<?php echo e($data->id); ?>">
                <div class="row">
                  <div class="col-lg-6">
                      <div class="form-group">
                        <div class="form-line">
                          <select name="for" id="for" class="form-control form-control-sm custom-select <?php $__errorArgs = ['for'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>">
                            <option value="">Select For</option>
                            <option value="1" <?php echo e($data->sc_for == 'Website'? 'selected':''); ?>>Website</option>
                            <option value="2" <?php echo e($data->sc_for == 'Android'? 'selected':''); ?>>Android</option>
                            <option value="3" <?php echo e($data->sc_for == 'Both'? 'selected':''); ?>>Both</option>
                          </select>
                          <?php $__errorArgs = ['for'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                              <div class="invalid-feedback"><?php echo e($message); ?></div>
                          <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                        </div>
                      </div>
                  </div>
                  <div class="col-lg-6">
                      <div class="form-group">
                        <div class="form-line">
                          <select name="type" id="type" class="form-control form-control-sm custom-select <?php $__errorArgs = ['type'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>">
                            <option value="">Select Type</option>
                            <option value="1" <?php echo e($data->sc_type == 'Home Slider'? 'selected':''); ?>>Home Slider</option>
                            <option value="2" <?php echo e($data->sc_type == 'About Us'? 'selected':''); ?>>About Us</option>
                            <option value="3" <?php echo e($data->sc_type == 'Our Best Services'? 'selected':''); ?>>Our Best Services</option>
                            <option value="4" <?php echo e($data->sc_type == 'What We Offer'? 'selected':''); ?>>What We Offer</option>
                            <option value="5" <?php echo e($data->sc_type == 'Why Choose Us'? 'selected':''); ?>>Why Choose Us</option>
                            <option value="6" <?php echo e($data->sc_type == 'FAQ'? 'selected':''); ?>>FAQ</option>
                            <option value="7" <?php echo e($data->sc_type == 'Our Policies'? 'selected':''); ?>>Our Policies</option>
                            <option value="8" <?php echo e($data->sc_type == 'Terms & Conditions'? 'selected':''); ?>>Terms & Conditions</option>
                            <option value="9" <?php echo e($data->sc_type == 'Footer Content'? 'selected':''); ?>>Footer Content</option>
                          </select>
                          <?php $__errorArgs = ['type'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                              <div class="invalid-feedback"><?php echo e($message); ?></div>
                          <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                        </div>
                      </div>
                  </div>
                  <div class="col-lg-6">
                      <div class="form-group">
                        <div class="form-line">
                          <input type="text" id="name" name="name" value="<?php echo e($data->sc_name); ?>" class="form-control <?php $__errorArgs = ['name'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" placeholder="name" autocomplete="off" readonly="readonly">
                        <?php $__errorArgs = ['name'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                            <div class="invalid-feedback"><?php echo e($message); ?></div>
                        <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                        </div>
                      </div>
                  </div>
                  <div class="col-lg-6">
                      <div class="form-group">
                        <div class="form-line">
                          <input type="text" id="description" name="description" value="<?php echo e($data->sc_desc); ?>" class="form-control <?php $__errorArgs = ['description'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" placeholder="description" autocomplete="off">
                        <?php $__errorArgs = ['description'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                            <div class="invalid-feedback"><?php echo e($message); ?></div>
                        <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                        </div>
                      </div>
                  </div>
                  <div class="col-lg-12">
                      <div class="form-group">
                        <div class="form-line">
                          <input type="text" id="title" name="title" value="<?php echo e($data->sc_title); ?>" class="form-control <?php $__errorArgs = ['title'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" placeholder="title" autocomplete="off">
                        <?php $__errorArgs = ['title'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                            <div class="invalid-feedback"><?php echo e($message); ?></div>
                        <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                        </div>
                      </div>
                  </div>
                  <div class="col-lg-12">
                      <textarea class="summernote-simple" id="content" name="content"><?php echo e($data->sc_value); ?></textarea>
                        <?php $__errorArgs = ['content'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                            <div class="invalid-feedback"><?php echo e($message); ?></div>
                        <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                  </div>
                  <div class="col-lg-12">
                    <div class="m-l-25 m-b-20">
                      <button type="button" class="btn btn-info btn-border-radius waves-effect" onclick="SubmitForm('form');">Send</button>
                      <button type="reset" class="btn btn-danger btn-border-radius waves-effect">Reset</button>
                    </div>
                  </div>
                </div>
              </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\wamp64\www\cronex\resources\views/Master/Content/edit-content.blade.php ENDPATH**/ ?>