

<?php $__env->startSection('content'); ?>
<!--header end -->
<div class="ed_slider_form_section">
<!--Slider start-->
	<section class="ed_mainslider">
		<article class="content">
			<div class="rev_slider_wrapper">			
				<!-- START REVOLUTION SLIDER 5.0 auto mode -->
				<div id="rev_slider_4_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-alias="classicslider1" style="margin:0px auto;background-color:transparent;padding:0px;margin-top:0px;margin-bottom:0px;">
				<div id="rev_slider" class="rev_slider "  data-version="5.0">
					<ul>	
						<!-- SLIDE  -->
						<li data-transition="boxfade">
							
							<!-- MAIN IMAGE -->
							<img src="<?php if(isset($data['Home Slider_home'])): ?><?php echo e(url('uplode/BannerAndLogo/'.$data['Home Slider_home'])); ?> <?php endif; ?>"  alt="banner">
							<div class="ed_course_single_image_overlay"></div>

							<!-- LAYER NR. 1 -->
								<div class="tp-caption NotGeneric-Title   tp-resizeme rs-parallaxlevel-0" 

									 data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
									data-y="['top','top','top','top']" data-voffset="['220','2205','195','135']" 
									
									
									
									data-width="none"
									data-height="none"
									data-whitespace="nowrap"
									data-transform_idle="o:1;"
						 
									 data-transform_in="x:-50px;skX:100px;opacity:0;s:2000;e:Power4.easeInOut;" 
									 data-transform_out="s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" 
									data-start="1510" 
									data-splitin="none" 
									data-splitout="none" 
									data-responsive_offset="on" 

									data-elementdelay="0.05" 
									
									style="z-index: 5; white-space: nowrap; font-size: 60px; color:#fff; font-weight:500; font-family: 'Trirong', serif;"><?php if(isset($data['HOME_HEAD_SLIDER-1-1_C'])): ?> <?php echo $data['HOME_HEAD_SLIDER-1-1_C']; ?><?php endif; ?>
								</div>
								<div class="tp-caption NotGeneric-Title   tp-resizeme rs-parallaxlevel-0" 
						
									data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
									data-y="['top','top','top','top']" data-voffset="['300','235','210','200']" 
									
									data-width="none"
									data-height="none"
									data-whitespace="nowrap"
									data-transform_idle="o:1;"
						 
									 data-transform_in="x:-50px;skX:100px;opacity:0;s:2000;e:Power4.easeInOut;" 
									 data-transform_out="s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" 
									data-start="1810" 
									data-splitin="none" 
									data-splitout="none" 
									data-responsive_offset="on" 

									data-elementdelay="0.05" 
									
									style="z-index: 5; white-space: nowrap; font-size: 60px; color:#fff; font-weight:400; font-family: 'Trirong', serif;"><?php if(isset($data['HOME_HEAD_SLIDER-1-2_C'])): ?> <?php echo $data['HOME_HEAD_SLIDER-1-2_C']; ?><?php endif; ?>
								</div>

								<!-- LAYER NR. 2 -->
								<div class="tp-caption NotGeneric-CallToAction ed_btn ed_green tp-resizeme rs-parallaxlevel-0" 
									 							 
									 data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
									 data-y="['top','top','top','top']" data-voffset="['430','306','776','651']" 
									 
									data-whitespace="nowrap"
									data-transform_idle="o:1;"
						 
									 data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;" 
									 data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" 
									 data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;" 
									 data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" 
									data-start="2000" 
									data-splitin="none" 
									data-splitout="none" 
									data-responsive_offset="on" 
	
									style="z-index: 7; white-space: nowrap;font-family: 'Trirong', serif; cursor: pointer;">see more
								</div>
								
						</li>
						<li data-transition="fadefromtop">
							
							<!-- MAIN IMAGE -->
							<img src="<?php if(isset($data['Home Slider_Logo & Banner'])): ?><?php echo e(url('uplode/BannerAndLogo/'.$data['Home Slider_Logo & Banner'])); ?> <?php endif; ?>"  alt="">
							<div class="ed_course_single_image_overlay"></div>

							<!-- LAYER NR. 1 -->
								<div class="tp-caption NotGeneric-Title   tp-resizeme rs-parallaxlevel-0" 

									 data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
									data-y="['top','top','top','top']" data-voffset="['220','2205','195','135']" 
									
									
									
									data-width="none"
									data-height="none"
									data-whitespace="nowrap"
									data-transform_idle="o:1;"
						 
									 data-transform_in="x:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;" 
									 data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;" 
									 data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" 
									data-start="1510" 
									data-splitin="none" 
									data-splitout="none" 
									data-responsive_offset="on" 

									data-elementdelay="0.05" 
									
									style="z-index: 5; white-space: nowrap; font-size: 60px; color:#fff; font-weight:400; font-family: 'Trirong', serif;"><?php if(isset($data['HOME_HEAD_SLIDER-2-1_C'])): ?> <?php echo $data['HOME_HEAD_SLIDER-2-1_C']; ?><?php endif; ?>
								</div>
								<div class="tp-caption NotGeneric-Title   tp-resizeme rs-parallaxlevel-0" 
						
									data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
									data-y="['top','top','top','top']" data-voffset="['300','235','210','200']" 
									
									data-width="none"
									data-height="none"
									data-whitespace="nowrap"
									data-transform_idle="o:1;"
						 
									 data-transform_in="x:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;" 
									 data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;" 
									 data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" 
									data-start="1810" 
									data-splitin="none" 
									data-splitout="none" 
									data-responsive_offset="on" 

									data-elementdelay="0.05" 
									
									style="z-index: 5; white-space: nowrap; font-size: 60px; color:#fff; font-weight:400; font-family: 'Trirong', serif;"><?php if(isset($data['HOME_HEAD_SLIDER-2-2_C'])): ?> <?php echo $data['HOME_HEAD_SLIDER-2-2_C']; ?><?php endif; ?>
								</div>

								<!-- LAYER NR. 2 -->
								<div class="tp-caption NotGeneric-CallToAction ed_btn ed_green tp-resizeme rs-parallaxlevel-0" 
								 
									 
									 data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
									 data-y="['top','top','top','top']" data-voffset="['430','306','776','651']" 
									 
									data-whitespace="nowrap"
									data-transform_idle="o:1;"
						 
									 data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;" 
									 data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" 
									 data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;" 
									 data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" 
									data-start="1500" 
									data-splitin="none" 
									data-splitout="none" 
									data-responsive_offset="on" 
	
									style="z-index: 7; white-space: nowrap; font-family: 'Trirong', serif; cursor: pointer;">see more
								</div>
								
						</li>
						
						<!-- SLIDE  -->
						
					</ul>				
				</div><!-- END REVOLUTION SLIDER -->
				</div><!-- END  -->
			</div><!-- END REVOLUTION SLIDER WRAPPER -->		
		</article>
	</section>
<!--Slider end-->
<!--Slider form start-->
<div class="ed_form_box style_2 ed_bottompadder60">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="ed_search_form">
					<form class="form-inline">
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<p>Look up the status of your shipment.</p>
						</div>
						<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
							<div class="form-group">
								<input type="text" placeholder="Enter Track Id..." class="form-control" id="course">
							</div>
						</div>
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
							<div class="form-group">
								<button type="button" class="btn ed_btn pull-right ed_orange">track</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<!--Slider form end-->
<div class="cs_top_services_wrapper">
               <div class="container">
                  <div class="row wow fadeIn bounceInUp" data-wow-duration="1.3s" style="visibility: visible; animation-duration: 1.3s; animation-name: bounceInUp;">
                     <div class="col-lg-2 col-md-4 col-sm-6 col-12">
                        <div class="cs_top_services_box">
                           <div class="cs_top_services_icon">
                              <div class="cs_top_services_icon">
                                 <p>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="60" height="40" viewBox="0 0 60 40">
                                       <path d="M479.053,1069.25a5.62,5.62,0,0,0-3.909,1.57,5.291,5.291,0,0,0-1.638,3.8,5.21,5.21,0,0,0,1.638,3.8,5.607,5.607,0,0,0,3.909,1.58A5.376,5.376,0,1,0,479.053,1069.25Zm0,8.36a2.987,2.987,0,1,1,0-5.97A2.985,2.985,0,1,1,479.053,1077.61Zm1.417-26.27a1.215,1.215,0,0,0-.818-0.31h-6.268a1.213,1.213,0,0,0-1.222,1.19v9.8a1.206,1.206,0,0,0,1.222,1.19h9.945a1.206,1.206,0,0,0,1.222-1.19v-6.56a1.173,1.173,0,0,0-.4-0.88Zm1.637,9.48h-7.5v-7.41h4.569l2.932,2.58v4.83Zm-32.889,8.43a5.624,5.624,0,0,0-3.91,1.57,5.294,5.294,0,0,0-1.637,3.8,5.213,5.213,0,0,0,1.637,3.8,5.611,5.611,0,0,0,3.91,1.58A5.376,5.376,0,1,0,449.218,1069.25Zm0,8.36a2.987,2.987,0,1,1,0-5.97A2.985,2.985,0,1,1,449.218,1077.61Zm-8.149-6.89h-2.456v-3.18a1.222,1.222,0,0,0-2.444,0v4.37a1.215,1.215,0,0,0,1.222,1.2h3.678A1.2,1.2,0,1,0,441.069,1070.72Zm6.194-6.51a1.214,1.214,0,0,0-1.222-1.2H431.222a1.2,1.2,0,1,0,0,2.39h14.819A1.206,1.206,0,0,0,447.263,1064.21Zm-13.561-4.32,14.82,0.09a1.225,1.225,0,0,0,1.234-1.19,1.191,1.191,0,0,0-1.21-1.2l-14.82-.09h-0.012a1.206,1.206,0,0,0-1.222,1.19A1.191,1.191,0,0,0,433.7,1059.89Zm2.492-5.42h14.82a1.2,1.2,0,1,0,0-2.39h-14.82A1.2,1.2,0,1,0,436.194,1054.47Zm53.354-1.61-8.748-7.09a1.211,1.211,0,0,0-.782-0.27H469.731v-4.3a1.215,1.215,0,0,0-1.221-1.2H437.391a1.215,1.215,0,0,0-1.222,1.2v8.74a1.222,1.222,0,0,0,2.444,0v-7.55H467.3v28.33H457.282a1.2,1.2,0,1,0,0,2.39H472.2a1.2,1.2,0,1,0,0-2.39h-2.456v-22.83h9.848l7.965,6.45-0.085,16.35H486.2a1.2,1.2,0,1,0,0,2.39h2.48a1.2,1.2,0,0,0,1.222-1.18l0.1-18.11A1.233,1.233,0,0,0,489.548,1052.86Z" transform="translate(-430 -1040)"></path>
                                    </svg>
                                 </p>
                              </div>
                              <div class="cs_top_services_icon">
                                 <p>Export <br> Logistics</p>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-lg-2 col-md-4 col-sm-6 col-12">
                        <div class="cs_top_services_box">
                           <div class="cs_top_services_icon">
                              <div class="cs_top_services_icon">
                                 <p>
                                    <svg viewBox="0 0 50 50" width="50" height="50">
                                       <path d="M36.27 23.5C29.25 23.5 23.54 29.22 23.54 36.25C23.54 43.28 29.25 49 36.27 49C43.29 49 49 43.28 49 36.25C49 29.22 43.29 23.5 36.27 23.5ZM36.27 47.04C30.33 47.04 25.5 42.2 25.5 36.25C25.5 30.3 30.33 25.46 36.27 25.46C42.21 25.46 47.04 30.3 47.04 36.25C47.04 42.2 42.21 47.04 36.27 47.04ZM41.45 35.56L37.25 39.76L37.25 30.36C37.25 29.82 36.81 29.38 36.27 29.38C35.73 29.38 35.29 29.82 35.29 30.36L35.29 39.77L31.09 35.56C30.71 35.17 30.08 35.17 29.7 35.56C29.32 35.94 29.32 36.56 29.7 36.94L35.58 42.83C35.77 43.02 36.02 43.11 36.27 43.11C36.52 43.11 36.77 43.02 36.96 42.83L42.84 36.94C43.22 36.56 43.22 35.94 42.84 35.56C42.45 35.17 41.83 35.17 41.45 35.56ZM39.21 11.17L39.21 18.6C39.21 19.14 39.65 19.58 40.19 19.58C40.73 19.58 41.17 19.14 41.17 18.6L41.17 9.77C41.17 9.72 41.16 9.68 41.16 9.63C41.15 9.62 41.15 9.61 41.15 9.6C41.14 9.56 41.13 9.51 41.12 9.47C41.12 9.47 41.12 9.47 41.12 9.47C41.11 9.43 41.09 9.39 41.07 9.35C41.07 9.34 41.06 9.33 41.06 9.33C41.04 9.29 41.02 9.25 40.99 9.21C40.99 9.21 40.99 9.2 40.98 9.2C40.96 9.17 40.93 9.13 40.9 9.1C40.9 9.1 40.9 9.09 40.89 9.09C40.86 9.06 40.83 9.03 40.79 9C40.79 9 40.78 8.99 40.78 8.99C40.74 8.96 40.71 8.94 40.67 8.91C40.66 8.91 40.66 8.91 40.66 8.91C40.62 8.89 40.58 8.87 40.54 8.85C40.53 8.85 40.53 8.85 40.53 8.85L21.92 1.98C21.7 1.9 21.46 1.9 21.24 1.98L2.64 8.85C2.64 8.85 2.63 8.85 2.63 8.85C2.59 8.87 2.54 8.89 2.5 8.91C2.5 8.91 2.5 8.91 2.5 8.91C2.46 8.94 2.42 8.96 2.39 8.99C2.38 8.99 2.38 9 2.37 9C2.34 9.03 2.3 9.06 2.27 9.09C2.27 9.09 2.27 9.1 2.26 9.1C2.23 9.13 2.21 9.17 2.18 9.2C2.18 9.2 2.18 9.21 2.17 9.21C2.15 9.25 2.13 9.29 2.11 9.33C2.1 9.33 2.1 9.34 2.1 9.35C2.08 9.39 2.06 9.43 2.05 9.47C2.05 9.47 2.05 9.47 2.05 9.47C2.03 9.51 2.02 9.56 2.01 9.6C2.01 9.61 2.01 9.62 2.01 9.63C2 9.68 2 9.72 2 9.77L2 33.31C2 33.72 2.26 34.08 2.64 34.23L18.54 40.09C18.65 40.13 18.77 40.15 18.88 40.15C19.28 40.15 19.65 39.91 19.8 39.51C19.99 39 19.73 38.44 19.22 38.25L3.96 32.62L3.96 11.17L20.6 17.32L20.6 26.5C20.6 27.04 21.04 27.48 21.58 27.48C22.12 27.48 22.56 27.04 22.56 26.5L22.56 17.32L39.21 11.17ZM21.58 3.95L37.36 9.77L21.58 15.59L5.81 9.77L21.58 3.95Z"></path>
                                    </svg>
                                 </p>
                              </div>
                              <div class="cs_top_services_icon">
                                 <p>Import <br> Logistics</p>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-lg-2 col-md-4 col-sm-6 col-12">
                        <div class="cs_top_services_box">
                           <div class="cs_top_services_icon">
                              <div class="cs_top_services_icon">
                                 <p>
                                    <svg viewBox="0 0 48 53" width="48" height="53">
                                       <path d="M6.06 28.85C5.61 28.85 5.25 29.21 5.25 29.66L5.25 32.91C5.25 33.36 5.61 33.73 6.06 33.73C6.5 33.73 6.87 33.36 6.87 32.91L6.87 29.66C6.87 29.21 6.5 28.85 6.06 28.85ZM9.3 30.47C8.85 30.47 8.49 30.84 8.49 31.29L8.49 34.54C8.49 34.99 8.85 35.35 9.3 35.35C9.75 35.35 10.11 34.99 10.11 34.54L10.11 31.29C10.11 30.84 9.75 30.47 9.3 30.47ZM38.89 34.84C38.51 34.6 38.01 34.71 37.77 35.09C37.54 35.45 37.63 35.93 37.98 36.19C40.21 37.69 40.8 40.72 39.3 42.96C37.8 45.2 34.78 45.79 32.55 44.28C30.32 42.78 29.73 39.74 31.23 37.51C32.13 36.16 33.64 35.35 35.26 35.35C35.71 35.35 36.07 34.99 36.07 34.54C36.07 34.09 35.71 33.73 35.26 33.73C31.68 33.73 28.77 36.64 28.77 40.24C28.77 43.83 31.68 46.74 35.26 46.74C38.85 46.74 41.75 43.83 41.75 40.23C41.75 38.07 40.68 36.05 38.89 34.84ZM44.62 37.53C43.83 34.8 41.9 32.55 39.32 31.37L39.32 13.39C39.32 13.1 39.17 12.83 38.92 12.69L21.07 2.11C20.82 1.96 20.5 1.96 20.25 2.11L2.4 12.69C2.38 12.7 2.37 12.71 2.35 12.72C2.34 12.74 2.32 12.75 2.31 12.76C2.23 12.82 2.17 12.88 2.13 12.96C2.13 12.97 2.12 12.97 2.11 12.97L2.11 12.98C2.07 13.06 2.04 13.15 2.02 13.25C2.02 13.27 2.01 13.29 2.01 13.32C2.01 13.34 2 13.36 2 13.39L2 34.54C2 34.83 2.15 35.09 2.4 35.24L20.25 45.82C20.27 45.83 20.29 45.84 20.31 45.84C20.33 45.86 20.36 45.86 20.38 45.87C20.56 45.95 20.76 45.95 20.94 45.87C20.96 45.86 20.98 45.86 21.01 45.84C21.03 45.83 21.05 45.83 21.07 45.82L25.92 42.95C27.41 48.12 32.8 51.11 37.96 49.61C43.13 48.12 46.11 42.71 44.62 37.53ZM20.66 3.76L24.88 6.26L18.62 9.96C18.24 10.19 18.11 10.69 18.34 11.08C18.57 11.47 19.06 11.59 19.45 11.37L26.47 7.21L30.69 9.71L14.44 19.33L10.22 16.83L17.02 12.81C17.4 12.58 17.53 12.08 17.3 11.69C17.07 11.3 16.58 11.18 16.19 11.4L8.63 15.89L4.41 13.39L20.66 3.76ZM13.63 20.74L13.63 26.18L9.44 23.69L9.44 18.26L13.63 20.74ZM25.85 37.72C25.63 38.54 25.52 39.39 25.53 40.23C25.53 40.57 25.54 40.92 25.58 41.25L21.47 43.69L21.47 41.05C21.47 40.6 21.11 40.23 20.66 40.23C20.21 40.23 19.85 40.6 19.85 41.05L19.85 43.69L3.62 34.08L3.62 14.81L7.81 17.3L7.81 24.16C7.81 24.44 7.97 24.71 8.21 24.86L14.03 28.3C14.15 28.38 14.29 28.41 14.44 28.41C14.89 28.41 15.25 28.05 15.25 27.6L15.25 21.7L19.85 24.43L19.85 37.79C19.85 38.24 20.21 38.61 20.66 38.61C21.11 38.61 21.47 38.24 21.47 37.79L21.47 24.43L36.17 15.72C36.56 15.49 36.69 14.99 36.46 14.6C36.23 14.21 35.73 14.09 35.35 14.31L20.66 23.02L16.04 20.28L32.29 10.65L37.7 13.85L37.7 30.8C32.52 29.43 27.21 32.53 25.85 37.72ZM35.26 48.37C30.78 48.37 27.15 44.73 27.15 40.23C27.15 35.74 30.78 32.1 35.26 32.1C39.74 32.1 43.37 35.74 43.37 40.23C43.37 44.73 39.74 48.37 35.26 48.37ZM33.4 40.47C33.08 40.16 32.57 40.17 32.25 40.49C31.95 40.81 31.95 41.31 32.25 41.62L33.47 42.84C33.79 43.16 34.3 43.16 34.62 42.84L38.27 39.18C38.58 38.86 38.57 38.34 38.25 38.03C37.94 37.73 37.44 37.73 37.12 38.03L34.05 41.12L33.4 40.47Z"></path>
                                    </svg>
                                 </p>
                              </div>
                              <div class="cs_top_services_icon">
                                 <p>Product <br> Safety</p>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-lg-2 col-md-4 col-sm-6 col-12">
                        <div class="cs_top_services_box">
                           <div class="cs_top_services_icon">
                              <div class="cs_top_services_icon">
                                 <p>
                                    <svg viewBox="0 0 51 51" width="51" height="51">
                                       <path class="shp0" d="M37.25 39.57C37.68 39.57 38.03 39.21 38.03 38.77L38.03 37.17C38.03 36.73 37.68 36.37 37.25 36.37C36.82 36.37 36.47 36.73 36.47 37.17L36.47 38.77C36.47 39.21 36.82 39.57 37.25 39.57ZM33.33 38.77L33.33 40.37C33.33 40.81 33.68 41.17 34.12 41.17C34.55 41.17 34.9 40.81 34.9 40.37L34.9 38.77C34.9 38.33 34.55 37.97 34.12 37.97C33.68 37.97 33.33 38.33 33.33 38.77ZM22.17 22.51C22.48 22.2 22.48 21.69 22.17 21.38C22.17 21.38 22.17 21.38 22.17 21.38C21.87 21.07 21.37 21.07 21.07 21.38C20.76 21.69 20.76 22.2 21.07 22.51C21.37 22.82 21.87 22.82 22.17 22.51ZM28.82 13.46C28.52 13.78 28.52 14.28 28.82 14.6C28.82 14.6 28.82 14.6 28.82 14.6C29.13 14.91 29.62 14.91 29.93 14.6C30.24 14.28 30.24 13.78 29.93 13.46C29.62 13.15 29.13 13.15 28.82 13.46ZM22.17 14.6L22.17 14.6C22.48 14.28 22.48 13.78 22.17 13.46C21.87 13.15 21.37 13.15 21.07 13.46C20.76 13.78 20.76 14.28 21.07 14.6C21.37 14.91 21.87 14.91 22.17 14.6ZM28.82 21.38C28.82 21.38 28.82 21.38 28.82 21.38C28.52 21.69 28.52 22.2 28.82 22.51C29.13 22.82 29.62 22.82 29.93 22.51C30.24 22.2 30.24 21.69 29.93 21.38C29.62 21.07 29.13 21.07 28.82 21.38ZM19.23 17.99C19.23 18.43 19.58 18.79 20.01 18.79C20.45 18.79 20.8 18.43 20.8 17.99C20.8 17.55 20.45 17.19 20.01 17.19C19.58 17.19 19.23 17.55 19.23 17.99ZM31.77 17.99C31.77 17.55 31.41 17.19 30.98 17.19C30.55 17.19 30.2 17.55 30.2 17.99C30.2 18.43 30.55 18.79 30.98 18.79C31.41 18.79 31.77 18.43 31.77 17.99ZM25.5 13.19C25.93 13.19 26.28 12.83 26.28 12.39C26.28 11.95 25.93 11.59 25.5 11.59C25.07 11.59 24.72 11.95 24.72 12.39C24.72 12.83 25.07 13.19 25.5 13.19ZM25.5 24.38C25.93 24.38 26.28 24.02 26.28 23.58C26.28 23.14 25.93 22.78 25.5 22.78C25.07 22.78 24.72 23.14 24.72 23.58C24.72 24.02 25.07 24.38 25.5 24.38ZM26.51 20.15C26.66 20.3 26.86 20.39 27.07 20.39C27.5 20.38 27.85 20.03 27.85 19.59C27.85 19.37 27.77 19.17 27.62 19.02L26.28 17.66L26.28 15.59C26.28 15.15 25.93 14.79 25.5 14.79C25.07 14.79 24.72 15.15 24.72 15.59L24.72 17.99C24.72 18.2 24.8 18.4 24.94 18.55L26.51 20.15ZM25.5 9.19C25.93 9.19 26.28 8.84 26.28 8.39L26.28 4.4C26.28 3.96 25.93 3.6 25.5 3.6C25.07 3.6 24.72 3.96 24.72 4.4L24.72 8.39C24.72 8.84 25.07 9.19 25.5 9.19ZM22.36 4.4C22.8 4.4 23.15 4.04 23.15 3.6L23.15 2.8C23.15 2.36 22.8 2 22.36 2C21.93 2 21.58 2.36 21.58 2.8L21.58 3.6C21.58 4.04 21.93 4.4 22.36 4.4ZM48.61 26.72L41.95 22.78L48.61 18.85C48.85 18.7 49 18.44 49 18.16C49 17.87 48.85 17.61 48.61 17.46L45.47 15.61C45.09 15.4 44.62 15.55 44.41 15.94C44.22 16.31 44.34 16.77 44.69 17L46.65 18.16L40.38 21.86L35.38 18.9C35.22 18.81 35.04 18.78 34.86 18.82C34.88 18.54 34.9 18.26 34.9 17.98C34.91 15.37 33.86 12.87 32.01 11.06L33.33 10.29L39.42 13.88C39.79 14.12 40.27 14.01 40.5 13.64C40.73 13.26 40.63 12.77 40.26 12.53C40.24 12.52 40.22 12.51 40.2 12.5L33.72 8.67C33.48 8.53 33.18 8.53 32.94 8.67L30.7 9.99C30.29 9.72 29.86 9.47 29.42 9.26L29.42 8.39C29.42 7.95 29.06 7.6 28.63 7.6C28.2 7.6 27.85 7.95 27.85 8.39L27.85 9.79C27.85 10.12 28.05 10.42 28.35 10.54C29 10.8 29.62 11.15 30.18 11.58L30.19 11.59C33.66 14.24 34.36 19.25 31.77 22.78C29.18 26.31 24.27 27.03 20.8 24.39C18.83 22.88 17.66 20.51 17.66 17.99C17.67 14.86 19.45 12.03 22.23 10.72C22.37 10.65 22.51 10.59 22.65 10.54C22.95 10.42 23.15 10.12 23.15 9.79L23.15 6C23.15 5.55 22.8 5.2 22.36 5.2C21.93 5.2 21.58 5.55 21.58 6L21.58 9.26L21.58 9.26C21.13 9.47 20.71 9.72 20.3 9.99L18.06 8.67C17.81 8.53 17.52 8.53 17.27 8.67L2.39 17.46C2.01 17.69 1.89 18.18 2.1 18.56C2.17 18.68 2.27 18.78 2.39 18.85L9.05 22.78L2.39 26.72C2.01 26.94 1.89 27.43 2.1 27.81C2.17 27.93 2.27 28.03 2.39 28.1L9.83 32.5L9.83 40.37C9.83 40.66 9.98 40.92 10.22 41.06L25.11 49.85C25.12 49.87 25.15 49.87 25.16 49.88C25.18 49.89 25.21 49.9 25.23 49.91C25.4 49.98 25.6 49.98 25.77 49.91C25.79 49.9 25.81 49.89 25.83 49.88C25.86 49.87 25.87 49.87 25.89 49.85L40.78 41.06C41.02 40.92 41.17 40.65 41.17 40.37L41.17 32.5L48.61 28.1C48.98 27.88 49.11 27.39 48.89 27.01C48.82 26.89 48.73 26.79 48.61 26.72ZM4.35 18.16L17.66 10.29L19 11.07C17.15 12.88 16.1 15.38 16.1 17.99C16.1 18.27 16.12 18.55 16.14 18.82C16.06 18.8 15.99 18.8 15.91 18.8C15.81 18.82 15.71 18.85 15.61 18.91L10.61 21.86L4.35 18.16ZM4.35 27.41L10.61 23.71L18.34 28.27C18.71 28.51 19.19 28.4 19.42 28.03C19.65 27.65 19.55 27.16 19.18 26.92C19.16 26.91 19.14 26.9 19.12 26.89L12.18 22.78L16.39 20.3C17.63 25.43 22.72 28.57 27.75 27.3C31.14 26.45 33.78 23.75 34.61 20.3L38.82 22.78L25.5 30.65L21.83 28.49C21.45 28.28 20.97 28.43 20.77 28.82C20.59 29.19 20.7 29.65 21.05 29.87L23.94 31.58L17.67 35.28L4.35 27.41ZM39.6 39.91L26.28 47.78L26.28 45.97C26.28 45.52 25.93 45.17 25.5 45.17C25.07 45.17 24.72 45.52 24.72 45.97L24.72 47.78L11.4 39.91L11.4 33.42L17.27 36.9C17.52 37.04 17.81 37.04 18.06 36.9L24.72 32.96L24.72 42.77C24.72 43.21 25.07 43.57 25.5 43.57C25.93 43.57 26.28 43.21 26.28 42.77L26.28 32.96L32.94 36.9C33.18 37.04 33.48 37.04 33.72 36.9L39.6 33.42L39.6 39.91ZM33.33 35.28L27.07 31.58L40.38 23.71L46.65 27.41L33.33 35.28ZM41.56 15.14L42.34 15.61C42.72 15.81 43.2 15.66 43.4 15.27C43.59 14.9 43.47 14.45 43.12 14.22L42.34 13.76C41.96 13.56 41.48 13.71 41.28 14.1C41.1 14.47 41.21 14.92 41.56 15.14ZM28.63 6C29.06 6 29.42 5.64 29.42 5.2L29.42 3.6C29.42 3.16 29.06 2.8 28.63 2.8C28.2 2.8 27.85 3.16 27.85 3.6L27.85 5.2C27.85 5.64 28.2 6 28.63 6Z"></path>
                                    </svg>
                                 </p>
                              </div>
                              <div class="cs_top_services_icon">
                                 <p>Fastest <br> Delivery</p>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-lg-2 col-md-4 col-sm-6 col-12">
                        <div class="cs_top_services_box">
                           <div class="cs_top_services_icon">
                              <div class="cs_top_services_icon">
                                 <p>
                                    <svg viewBox="0 0 47 48" width="47" height="48">
                                       <path class="shp0" d="M38.82 39.64C38.46 40.01 38.46 40.6 38.82 40.97C39 41.15 39.23 41.25 39.47 41.25C39.7 41.25 39.94 41.15 40.12 40.97L40.12 40.96C40.48 40.6 40.48 40.01 40.12 39.64C39.76 39.28 39.18 39.28 38.82 39.64ZM46.95 22.48C46.59 16.65 44.17 11.16 40.12 7.03C35.68 2.5 29.78 0 23.5 0C17.52 0 11.87 2.27 7.52 6.41L6.52 5.47C6.25 5.22 5.86 5.16 5.53 5.31C5.19 5.46 4.98 5.79 4.98 6.16L4.98 13.17C4.98 13.69 5.39 14.11 5.9 14.11L12.76 14.11C13.13 14.11 13.46 13.89 13.6 13.54C13.75 13.2 13.68 12.8 13.43 12.53L11.81 10.78C18.65 4.47 29.24 4.67 35.83 11.4C42.63 18.35 42.63 29.65 35.83 36.6C32.54 39.96 28.16 41.81 23.5 41.81C18.84 41.81 14.46 39.96 11.17 36.6C7.87 33.23 6.06 28.76 6.06 24C6.06 23.48 5.65 23.06 5.14 23.06L0.92 23.06C0.41 23.06 0 23.48 0 24C0 30.41 2.44 36.44 6.88 40.97C10.9 45.08 16.24 47.56 21.91 47.94C22.44 47.98 22.97 48 23.5 48C28.56 48 33.51 46.33 37.56 43.23C37.97 42.92 38.05 42.33 37.75 41.92C37.45 41.5 36.87 41.42 36.46 41.73C27.91 48.27 15.75 47.37 8.18 39.64C4.31 35.69 2.08 30.5 1.85 24.94L4.24 24.94C4.47 29.84 6.44 34.42 9.87 37.92C17.38 45.6 29.62 45.6 37.13 37.92C44.65 30.25 44.65 17.75 37.13 10.08C29.62 2.4 17.38 2.4 9.87 10.08C9.52 10.44 9.51 11.01 9.85 11.38L10.63 12.23L6.82 12.23L6.82 8.29L6.91 8.38C7.27 8.72 7.83 8.71 8.18 8.36C12.27 4.18 17.71 1.87 23.5 1.87C29.29 1.87 34.73 4.18 38.82 8.36C46.46 16.16 47.29 28.65 40.74 37.41C40.43 37.82 40.51 38.4 40.91 38.72C41.31 39.03 41.89 38.95 42.2 38.54C45.62 33.96 47.31 28.26 46.95 22.48ZM10.77 27.27C10.45 27.5 10.31 27.93 10.43 28.32C10.55 28.7 10.9 28.97 11.3 28.97L16.65 28.97C17.16 28.97 17.57 28.55 17.57 28.03C17.57 27.51 17.16 27.09 16.65 27.09L13.87 27.09C13.99 26.98 14.12 26.86 14.24 26.74C15.98 25.03 16.82 23.59 16.82 22.32C16.82 20.35 15.53 19.03 13.6 19.03C11.83 19.03 10.38 20.5 10.38 22.32C10.38 22.83 10.8 23.25 11.3 23.25C11.81 23.25 12.22 22.83 12.22 22.32C12.22 21.54 12.84 20.91 13.6 20.91C14.98 20.91 14.98 21.97 14.98 22.32C14.98 23.62 12.52 26 10.77 27.27ZM21.72 20.43C21.97 19.98 21.81 19.41 21.37 19.15C20.93 18.9 20.37 19.05 20.12 19.5L17.28 24.61C17.11 24.9 17.12 25.26 17.28 25.55C17.44 25.84 17.75 26.02 18.07 26.02L20.92 26.02L20.92 28.03C20.92 28.55 21.33 28.97 21.84 28.97C22.34 28.97 22.75 28.55 22.75 28.03L22.75 26.02L23.38 26.02C23.89 26.02 24.3 25.6 24.3 25.08C24.3 24.56 23.89 24.14 23.38 24.14L22.75 24.14L22.75 23.87C22.75 23.36 22.34 22.94 21.84 22.94C21.33 22.94 20.92 23.36 20.92 23.87L20.92 24.14L19.65 24.14L21.72 20.43ZM25.78 28.97C26.15 28.97 26.5 28.74 26.64 28.37L29.68 20.3C29.86 19.82 29.62 19.28 29.15 19.09C28.68 18.91 28.15 19.15 27.96 19.63L24.93 27.69C24.74 28.18 24.98 28.72 25.45 28.91C25.56 28.95 25.67 28.97 25.78 28.97ZM32.44 28.91C32.55 28.95 32.65 28.97 32.76 28.97C33.13 28.97 33.48 28.74 33.62 28.36L36.56 20.3C36.66 20.01 36.62 19.69 36.45 19.43C36.28 19.18 36 19.03 35.7 19.03L31.84 19.03C31.34 19.03 30.92 19.45 30.92 19.97C30.92 20.49 31.34 20.91 31.84 20.91L34.38 20.91L31.9 27.7C31.72 28.19 31.97 28.73 32.44 28.91Z"></path>
                                    </svg>
                                 </p>
                              </div>
                              <div class="cs_top_services_icon">
                                 <p>24 X 7 <br> Service</p>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-lg-2 col-md-4 col-sm-6 col-12">
                        <div class="cs_top_services_box">
                           <div class="cs_top_services_icon">
                              <div class="cs_top_services_icon">
                                 <p>
                                    <svg viewBox="0 0 48 44" width="48" height="44">
                                       <path class="shp0" d="M44.81 21.18L44.81 20.7C44.81 15.83 43.07 11.09 39.91 7.36C36.78 3.68 32.45 1.18 27.72 0.33C27.21 0.24 26.72 0.58 26.63 1.08C26.54 1.59 26.88 2.07 27.39 2.17C36.4 3.79 42.94 11.58 42.94 20.7L42.94 20.98L41.06 20.98L41.06 20.7C41.06 11.34 33.41 3.73 24 3.73C14.59 3.73 6.94 11.34 6.94 20.7L6.94 20.98L5.06 20.98L5.06 20.7C5.06 11.46 11.7 3.65 20.85 2.12C21.36 2.04 21.7 1.56 21.62 1.05C21.53 0.54 21.05 0.2 20.54 0.29C15.75 1.09 11.36 3.57 8.18 7.27C4.96 11.01 3.19 15.78 3.19 20.7L3.19 21.18C1.34 21.75 0 23.46 0 25.47L0 31.77C0 34.25 2.03 36.27 4.52 36.27L7.16 36.27C7.65 37.08 8.54 37.62 9.56 37.62C11.11 37.62 12.37 36.36 12.37 34.82L12.37 22.42C12.37 20.88 11.11 19.63 9.56 19.63C9.31 19.63 9.07 19.66 8.85 19.72C9.35 11.84 15.96 5.59 24 5.59C32.04 5.59 38.65 11.84 39.15 19.72C38.93 19.66 38.69 19.63 38.44 19.63C36.89 19.63 35.62 20.88 35.62 22.42L35.62 24.75C35.62 25.26 36.04 25.68 36.56 25.68C37.08 25.68 37.5 25.26 37.5 24.75L37.5 22.42C37.5 21.91 37.92 21.49 38.44 21.49C38.95 21.49 39.37 21.91 39.37 22.42L39.37 34.5L39.37 34.82C39.37 35.34 38.95 35.75 38.44 35.75C37.92 35.75 37.5 35.34 37.5 34.82L37.5 32.67C37.5 32.16 37.08 31.74 36.56 31.74C36.04 31.74 35.62 32.16 35.62 32.67L35.62 34.82C35.62 36.36 36.89 37.62 38.44 37.62C38.77 37.62 39.08 37.56 39.37 37.46L39.37 38.07C39.37 39.08 38.55 39.9 37.53 39.9L28.93 39.9C28.53 38.6 27.32 37.66 25.89 37.66L22.14 37.66C20.38 37.66 18.95 39.08 18.95 40.83C18.95 42.58 20.38 44 22.14 44L25.89 44C27.32 44 28.53 43.06 28.93 41.76L37.53 41.76C39.58 41.76 41.25 40.1 41.25 38.07L41.25 36.27L43.48 36.27C45.97 36.27 48 34.25 48 31.77L48 25.48C48 23.46 46.66 21.75 44.81 21.18ZM6.75 34.4L4.52 34.4C3.06 34.4 1.87 33.22 1.87 31.77L1.87 25.47C1.87 24.02 3.06 22.84 4.52 22.84L6.75 22.84L6.75 34.4ZM9.56 21.49C10.08 21.49 10.5 21.91 10.5 22.42L10.5 34.82C10.5 35.34 10.08 35.75 9.56 35.75C9.05 35.75 8.62 35.34 8.62 34.82L8.62 22.42C8.62 21.91 9.05 21.49 9.56 21.49ZM25.89 42.13L22.14 42.13C21.41 42.13 20.82 41.55 20.82 40.83C20.82 40.11 21.41 39.52 22.14 39.52L25.89 39.52C26.61 39.52 27.2 40.11 27.2 40.83C27.2 41.55 26.61 42.13 25.89 42.13ZM46.12 31.77C46.12 33.22 44.94 34.4 43.48 34.4L41.25 34.4L41.25 22.84L43.48 22.84C44.94 22.84 46.12 24.02 46.12 25.48L46.12 31.77ZM24 1.86L24.07 1.86C24.08 1.86 24.08 1.86 24.08 1.86C24.59 1.86 25.01 1.45 25.02 0.94C25.02 0.42 24.6 0 24.08 0L24 0C23.48 0 23.06 0.42 23.06 0.93C23.06 1.45 23.48 1.86 24 1.86ZM36.56 29.55C36.81 29.55 37.05 29.45 37.23 29.28C37.4 29.11 37.5 28.87 37.5 28.62C37.5 28.38 37.4 28.14 37.23 27.96C37.05 27.79 36.81 27.69 36.56 27.69C36.32 27.69 36.07 27.79 35.9 27.96C35.73 28.14 35.62 28.38 35.62 28.62C35.62 28.87 35.73 29.11 35.9 29.28C36.07 29.45 36.32 29.55 36.56 29.55ZM23.26 33.04C23.44 33.26 23.7 33.39 23.99 33.39C23.99 33.39 23.99 33.39 23.99 33.39C24.27 33.39 24.54 33.27 24.71 33.05L26.91 30.41L29.45 30.41C31.82 30.41 33.75 28.5 33.75 26.14L33.75 20.69C33.75 18.33 31.82 16.41 29.45 16.41L18.55 16.41C16.18 16.41 14.25 18.33 14.25 20.69L14.25 26.14C14.25 28.5 16.18 30.41 18.55 30.41L21.14 30.41L23.26 33.04ZM16.12 26.14L16.12 20.69C16.12 19.36 17.21 18.28 18.55 18.28L29.45 18.28C30.79 18.28 31.87 19.36 31.87 20.69L31.87 26.14C31.87 27.47 30.79 28.55 29.45 28.55L26.47 28.55C26.19 28.55 25.92 28.67 25.75 28.89L24 30.98L22.33 28.9C22.15 28.68 21.88 28.55 21.59 28.55L18.55 28.55C17.21 28.55 16.12 27.47 16.12 26.14ZM19.89 22.95C19.64 22.95 19.4 23.05 19.22 23.22C19.05 23.39 18.95 23.63 18.95 23.88C18.95 24.12 19.05 24.36 19.22 24.54C19.4 24.71 19.64 24.81 19.89 24.81C20.13 24.81 20.37 24.71 20.55 24.54C20.72 24.36 20.82 24.12 20.82 23.88C20.82 23.63 20.72 23.39 20.55 23.22C20.37 23.05 20.13 22.95 19.89 22.95ZM24.01 24.81C24.26 24.81 24.5 24.71 24.67 24.54C24.85 24.36 24.95 24.12 24.95 23.88C24.95 23.63 24.85 23.39 24.67 23.22C24.5 23.05 24.26 22.95 24.01 22.95C23.76 22.95 23.52 23.05 23.35 23.22C23.17 23.39 23.07 23.63 23.07 23.88C23.07 24.12 23.17 24.36 23.35 24.54C23.52 24.71 23.76 24.81 24.01 24.81ZM28.14 24.81C28.38 24.81 28.62 24.71 28.8 24.54C28.97 24.36 29.07 24.12 29.07 23.88C29.07 23.63 28.97 23.39 28.8 23.22C28.62 23.05 28.38 22.95 28.14 22.95C27.89 22.95 27.65 23.05 27.47 23.22C27.3 23.39 27.2 23.63 27.2 23.88C27.2 24.13 27.3 24.36 27.47 24.54C27.65 24.71 27.89 24.81 28.14 24.81Z"></path>
                                    </svg>
                                 </p>
                              </div>
                              <div class="cs_top_services_icon">
                                 <p>Help <br> Center</p>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            
</div>
<!--Our expertise section one start -->
<div class="ed_transprentbg">
	<div class="container">
		<div class="row">
			<div class="col-lg-6 col-md-6 col-sm-12">
				<div class="ed_video_section_discription_img">
					<img  src="<?php if(isset($data['About Us Banner_About'])): ?><?php echo e(url('uplode/BannerAndLogo/'.$data['About Us Banner_About'])); ?> <?php endif; ?>" style="cursor:pointer"  alt="1" />
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-12">
				<div class="ed_video_section_discription ed_toppadder90">
					<h4><?php if(isset($data['ABOUT_T'])): ?> <?php echo e($data['ABOUT_T']); ?> <?php endif; ?></h4>
					<p><?php if(isset($data['ABOUT_T'])): ?> <?php echo Str::limit($data['ABOUT_C'], 620, ' ...'); ?> <?php endif; ?></p>
					<span><a href="<?php echo e(url('about-us')); ?>" class="btn ed_btn ed_orange">know more</a></span>
				</div>
			</div>
		</div>
    </div><!-- /.container -->
</div>
<!--Our expertise section one end -->
<!--skill section start -->
<div class="ed_graysection ed_toppadder90 ed_bottompadder60">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="ed_heading_top ed_bottompadder50">
					<h3>what we offer</h3>
				</div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
				<div class="skill_section">
					<h4><?php if(isset($data['WHAT_WE_OFFER-1_T'])): ?> <?php echo e($data['WHAT_WE_OFFER-1_T']); ?> <?php endif; ?></h4>
					<p><?php if(isset($data['WHAT_WE_OFFER-1_C'])): ?> <?php echo $data['WHAT_WE_OFFER-1_C']; ?> <?php endif; ?></p>
					<span><i class="fa fa-handshake-o" aria-hidden="true"></i></span>
				</div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
				<div class="skill_section">
					<h4><?php if(isset($data['WHAT_WE_OFFER-2_T'])): ?> <?php echo e($data['WHAT_WE_OFFER-2_T']); ?> <?php endif; ?></h4>
					<p><?php if(isset($data['WHAT_WE_OFFER-2_C'])): ?> <?php echo $data['WHAT_WE_OFFER-2_C']; ?> <?php endif; ?></p>
					<span><i class="fa fa-users" aria-hidden="true"></i></span>
				</div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
				<div class="skill_section">
					<h4><?php if(isset($data['WHAT_WE_OFFER-3_T'])): ?> <?php echo e($data['WHAT_WE_OFFER-3_T']); ?> <?php endif; ?></h4>
					<p><?php if(isset($data['WHAT_WE_OFFER-3_C'])): ?> <?php echo $data['WHAT_WE_OFFER-3_C']; ?> <?php endif; ?></p>
					<span><i class="fa fa-gift" aria-hidden="true"></i></span>
				</div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
				<div class="skill_section">
					<h4><?php if(isset($data['WHAT_WE_OFFER-4_T'])): ?> <?php echo e($data['WHAT_WE_OFFER-4_T']); ?> <?php endif; ?></h4>
					<p><?php if(isset($data['WHAT_WE_OFFER-4_C'])): ?> <?php echo $data['WHAT_WE_OFFER-4_C']; ?> <?php endif; ?></p>
					<span><i class="fa fa-money" aria-hidden="true"></i></span>
				</div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
				<div class="skill_section">
					<h4><?php if(isset($data['WHAT_WE_OFFER-5_T'])): ?> <?php echo e($data['WHAT_WE_OFFER-5_T']); ?> <?php endif; ?></h4>
					<p><?php if(isset($data['WHAT_WE_OFFER-5_C'])): ?> <?php echo $data['WHAT_WE_OFFER-5_C']; ?> <?php endif; ?></p>
					<span><i class="fa fa-child" aria-hidden="true"></i></span>
				</div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
				<div class="skill_section">
					<h4><?php if(isset($data['WHAT_WE_OFFER-6_T'])): ?> <?php echo e($data['WHAT_WE_OFFER-6_T']); ?> <?php endif; ?></h4>
					<p><?php if(isset($data['WHAT_WE_OFFER-6_C'])): ?> <?php echo $data['WHAT_WE_OFFER-6_C']; ?> <?php endif; ?></p>
					<span><i class="fa fa-tags" aria-hidden="true"></i></span>
				</div>
			</div>
        </div>
	</div>
</div>
<!--skill section end -->
<!--chart section start -->
<div class="ed_transprentbg ed_toppadder100">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="ed_counter_wrapper">
					<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
						<div class="ed_chart_ratio">
							<i class="fa fa-bus" aria-hidden="true"></i>
							<h4><?php if(isset($data['WHAT_WE_OFFER-7_T'])): ?> <?php echo e($data['WHAT_WE_OFFER-7_T']); ?> <?php endif; ?></h4>
							<p><?php if(isset($data['WHAT_WE_OFFER-7_C'])): ?> <?php echo $data['WHAT_WE_OFFER-7_C']; ?> <?php endif; ?></p>
						</div>
					</div>
					<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
						<div class="ed_chart_ratio">
							<i class="fa fa-plane" aria-hidden="true"></i>
							<h4><?php if(isset($data['WHAT_WE_OFFER-8_T'])): ?> <?php echo e($data['WHAT_WE_OFFER-8_T']); ?> <?php endif; ?></h4>
							<p><?php if(isset($data['WHAT_WE_OFFER-8_C'])): ?> <?php echo $data['WHAT_WE_OFFER-8_C']; ?> <?php endif; ?></p>
						</div>
					</div>
					<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
						<div class="ed_chart_ratio">
							<i class="fa fa-ship" aria-hidden="true"></i>
							<h4><?php if(isset($data['WHAT_WE_OFFER-9_T'])): ?> <?php echo e($data['WHAT_WE_OFFER-9_T']); ?> <?php endif; ?></h4>
							<p><?php if(isset($data['WHAT_WE_OFFER-9_C'])): ?> <?php echo $data['WHAT_WE_OFFER-9_C']; ?> <?php endif; ?></p>
						</div>
					</div>
				</div>
			</div>
        </div>
	</div>
</div>
<!-- chart Section end -->
<!-- Services start -->
<div class="ed_transprentbg ed_toppadder90 ed_bottompadder60">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="ed_heading_top ed_bottompadder50">
					<h3>our best services</h3>
				</div>
			</div>
			<div class="ed_mostrecomeded_course_slider">
				<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
					<div class="ed_mostrecomeded_course style_2">
						<div class="ed_item_img">
							<img src="<?php if(isset($data['Our Best Services_Ground Transport'])): ?><?php echo e(url('uplode/BannerAndLogo/'.$data['Our Best Services_Ground Transport'])); ?> <?php endif; ?>" alt="item1" class="img-responsive">
						</div>
						<div class="ed_item_description ed_most_recomended_data">
							<h4><a href="#"><?php if(isset($data['OUR_BEST_SERVICES-1_T'])): ?><?php echo e($data['OUR_BEST_SERVICES-1_T']); ?> <?php endif; ?></a></h4>
							<p><?php if(isset($data['OUR_BEST_SERVICES-1_C'])): ?> <?php echo $data['OUR_BEST_SERVICES-1_C']; ?> <?php endif; ?></p>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
					<div class="ed_mostrecomeded_course style_2">
						<div class="ed_item_img">
							<img  src="<?php if(isset($data['Our Best Services_Warehousing'])): ?><?php echo e(url('uplode/BannerAndLogo/'.$data['Our Best Services_Warehousing'])); ?> <?php endif; ?>" alt="item1" class="img-responsive">
						</div>
						<div class="ed_item_description ed_most_recomended_data">
							<h4><a href="#"><?php if(isset($data['OUR_BEST_SERVICES-2_T'])): ?><?php echo e($data['OUR_BEST_SERVICES-2_T']); ?> <?php endif; ?></a></h4>
							<p><?php if(isset($data['OUR_BEST_SERVICES-2_C'])): ?> <?php echo $data['OUR_BEST_SERVICES-2_C']; ?> <?php endif; ?></p>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
					<div class="ed_mostrecomeded_course style_2">
						<div class="ed_item_img">
							<img  src="<?php if(isset($data['Our Best Services_Packaging & Storage'])): ?><?php echo e(url('uplode/BannerAndLogo/'.$data['Our Best Services_Packaging & Storage'])); ?> <?php endif; ?>" alt="item1" class="img-responsive">
						</div>
						<div class="ed_item_description ed_most_recomended_data">
							<h4><a href="#"><?php if(isset($data['OUR_BEST_SERVICES-3_T'])): ?><?php echo e($data['OUR_BEST_SERVICES-3_T']); ?> <?php endif; ?></a></h4>
							<p><?php if(isset($data['OUR_BEST_SERVICES-3_C'])): ?> <?php echo $data['OUR_BEST_SERVICES-3_C']; ?> <?php endif; ?></p>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
					<div class="ed_mostrecomeded_course style_2">
						<div class="ed_item_img">
							<img src="<?php if(isset($data['Our Best Services_Door-To-Door Delivery'])): ?><?php echo e(url('uplode/BannerAndLogo/'.$data['Our Best Services_Door-To-Door Delivery'])); ?> <?php endif; ?>" alt="item1" class="img-responsive">
						</div>
						<div class="ed_item_description ed_most_recomended_data">
							<h4><a href="#"><?php if(isset($data['OUR_BEST_SERVICES-4_T'])): ?><?php echo e($data['OUR_BEST_SERVICES-4_T']); ?> <?php endif; ?></a></h4>
							<p><?php if(isset($data['OUR_BEST_SERVICES-4_C'])): ?> <?php echo $data['OUR_BEST_SERVICES-4_C']; ?> <?php endif; ?></p>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
					<div class="ed_mostrecomeded_course style_2">
						<div class="ed_item_img">
							<img  src="<?php if(isset($data['Our Best Services_Cargo'])): ?><?php echo e(url('uplode/BannerAndLogo/'.$data['Our Best Services_Cargo'])); ?> <?php endif; ?>" alt="item1" class="img-responsive">
						</div>
						<div class="ed_item_description ed_most_recomended_data">
							<h4><a href="#"><?php if(isset($data['OUR_BEST_SERVICES-5_T'])): ?><?php echo e($data['OUR_BEST_SERVICES-5_T']); ?> <?php endif; ?></a></h4>
							<p><?php if(isset($data['OUR_BEST_SERVICES-5_C'])): ?> <?php echo $data['OUR_BEST_SERVICES-5_C']; ?> <?php endif; ?></p>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
					<div class="ed_mostrecomeded_course style_2">
						<div class="ed_item_img">
							<img  src="<?php if(isset($data['Our Best Services_Logistic'])): ?><?php echo e(url('uplode/BannerAndLogo/'.$data['Our Best Services_Logistic'])); ?> <?php endif; ?>" alt="item1" class="img-responsive">
						</div>
						<div class="ed_item_description ed_most_recomended_data">
							<h4><a href="#"><?php if(isset($data['OUR_BEST_SERVICES-6_T'])): ?><?php echo e($data['OUR_BEST_SERVICES-6_T']); ?> <?php endif; ?></a></h4>
							<p><?php if(isset($data['OUR_BEST_SERVICES-6_C'])): ?> <?php echo $data['OUR_BEST_SERVICES-6_C']; ?> <?php endif; ?></p>
						</div>
					</div>
				</div>
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center ed_toppadder30">
					<a href="<?php echo e(url('service')); ?>" class="btn ed_btn ed_orange">view  more</a>
				</div>
			</div>
		</div>
    </div><!-- /.container -->
</div>
<!-- Services end -->
<!--Timer Section three start -->
<div class="ed_timer_section ed_toppadder90 ed_bottompadder60">
<div class="ed_img_overlay"></div>
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="ed_heading_top ed_bottompadder50">
					<h3>Why Choose Us</h3>
				</div>
			</div>
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="ed_counter_wrapper">
					<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
						<div class="ed_counter">
							<h2 class="timer" data-from="0" data-to="<?php if(isset($data['WHY_CHOOSE_US-1_T'])): ?> <?php echo $data['WHY_CHOOSE_US-1_T']; ?> <?php endif; ?>" data-speed="5000"></h2>
							<h4><?php if(isset($data['WHY_CHOOSE_US-1_C'])): ?> <?php echo $data['WHY_CHOOSE_US-1_C']; ?> <?php endif; ?></h4>
						</div>
					</div>
					<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
						<div class="ed_counter">
							<h2 class="timer" data-from="0" data-to="<?php if(isset($data['WHY_CHOOSE_US-2_T'])): ?> <?php echo $data['WHY_CHOOSE_US-2_T']; ?> <?php endif; ?>" data-speed="5000"></h2>
							<h4><?php if(isset($data['WHY_CHOOSE_US-2_C'])): ?> <?php echo $data['WHY_CHOOSE_US-2_C']; ?> <?php endif; ?></h4>
						</div>
					</div>
					<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
						<div class="ed_counter">
							<h2 class="timer" data-from="0" data-to="<?php if(isset($data['WHY_CHOOSE_US-3_T'])): ?> <?php echo $data['WHY_CHOOSE_US-3_T']; ?> <?php endif; ?>" data-speed="5000"></h2>
							<h4><?php if(isset($data['WHY_CHOOSE_US-3_C'])): ?> <?php echo $data['WHY_CHOOSE_US-3_C']; ?> <?php endif; ?></h4>
						</div>
					</div>
					<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
						<div class="ed_counter">
							<h2 class="timer" data-from="0" data-to="<?php if(isset($data['WHY_CHOOSE_US-4_T'])): ?> <?php echo $data['WHY_CHOOSE_US-4_T']; ?> <?php endif; ?>" data-speed="5000"></h2>
							<h4><?php if(isset($data['WHY_CHOOSE_US-4_C'])): ?> <?php echo $data['WHY_CHOOSE_US-4_C']; ?> <?php endif; ?></h4>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!--Timer Section three end -->

<!-- Testimonial start -->
<div class="ed_graysection ed_toppadder90 ed_bottompadder90">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ed_bottompadder80">
				<div class="ed_heading_top">
					<h3>what our client say</h3>
				</div>
			</div>
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="ed_latest_news_slider">
					<div id="owl-demo2" class="owl-carousel owl-theme">
						<div class="item">
							<div class="ed_item_description">
								<img src="front/images/content/t1.jpg" alt="1" title="1"/>
								<h4>Carolyn Ayala</h4>
								<span>CEO</span>
								<p>Just in case there is anyone looking for it, new expertise to our knowledge base to make you happy as well.</p>
							</div>
						</div>
						<div class="item">
							<div class="ed_item_description">
								<img src="front/images/content/t2.jpg" alt="1" title="1"/>
								<h4>Ronnie Parker</h4>
								<span>manager</span>
								<p>Just in case there is anyone looking for it, new expertise to our knowledge base to make you happy as well.</p>
							</div>
						</div>
						<div class="item">
							<div class="ed_item_description">
								<img src="front/images/content/t3.jpg" alt="1" title="1"/>
								<h4>Kim Hiatt</h4>
								<span>director</span>
								<p>Just in case there is anyone looking for it, new expertise to our knowledge base to make you happy as well.</p>
							</div>
						</div>
						<div class="item">
							<div class="ed_item_description">
								<img src="front/images/content/t4.jpg" alt="1" title="1"/>
								<h4>Michael Garza</h4>
								<span>Employee</span>
								<p>Just in case there is anyone looking for it, new expertise to our knowledge base to make you happy as well.</p>
							</div>
						</div>
						<div class="item">
							<div class="ed_item_description">
								<img src="front/images/content/t5.jpg" alt="1" title="1"/>
								<h4>Mary J. Cole</h4>
								<span>receptionist</span>
								<p>Just in case there is anyone looking for it, new expertise to our knowledge base to make you happy as well.</p>
							</div>
						</div> 
					</div>
				</div>
			</div>
		</div>
    </div><!-- /.container -->
</div>
<!--Testimonial end -->
<!--client section start -->
<div class="ed_transprentbg ed_toppadder90 ed_bottompadder90">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="ed_heading_top ed_bottompadder50">
					<h3>our partners</h3>
				</div>
			</div>
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="ed_clientslider">
					<div id="owl-demo5" class="owl-carousel owl-theme">
						<?php if(isset($data['Our Partners'])): ?>
						<?php $__currentLoopData = $data['Our Partners']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
						<div class="item">
							<a href="#">
								<img src="<?php echo e(url('uplode/BannerAndLogo/'.$value)); ?>" alt="Partner Img" />
							</a>
						</div>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
    </div><!-- /.container -->
</div>
<!--client section end -->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('front.layout', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\cronex\resources\views/front/index.blade.php ENDPATH**/ ?>