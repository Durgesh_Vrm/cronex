<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <title><?php echo e(config('APP_NAME')); ?> - Lgoin</title>
  <!-- General CSS Files -->
  <link rel="stylesheet" href="<?php echo e(asset('assets/css/app.min.css')); ?>">
  <link rel="stylesheet" href="<?php echo e(asset('assets/bundles/bootstrap-social/bootstrap-social.css')); ?>">
  <!-- Template CSS -->
  <link rel="stylesheet" href="<?php echo e(asset('assets/css/style.css')); ?>">
  <link rel="stylesheet" href="<?php echo e(asset('assets/css/components.css')); ?>">
  <!-- Custom style CSS -->
  <link rel="stylesheet" href="<?php echo e(asset('assets/css/custom.css')); ?>">
  <link rel='shortcut icon' type='image/x-icon' href="<?php echo e(asset('assets/img/favicon.ico')); ?>" />


  <script src="<?php echo e(asset('assets/bundles/sweetalert/sweetalert.min.js')); ?>"></script>
    <!-- Page Specific JS File -->
  <script src="<?php echo e(asset('assets/js/page/sweetalert.js')); ?>"></script>
  <style type="text/css">
    #banner {
  background: url('<?php echo e(asset('uplode/config/banner/'.config('LOGIN_BANNER'))); ?>');
  background-repeat: no-repeat;
  background-size: cover;
  background-attachment: fixed;
  background-position: center; 
     }

.container-login100::before {
    content: "";
    display: block;
    position: absolute;
    z-index: -1;
    width: 100%;
    height: 100%;
    top: 0;
    left: 0;
    background: rgba(93,84,240,0.5);
    background: -webkit-linear-gradient(left, rgba(0,168,255,0.5), rgba(185,0,255,0.5));
    background: -o-linear-gradient(left, rgba(0,168,255,0.5), rgba(185,0,255,0.5));
    background: -moz-linear-gradient(left, rgba(0,168,255,0.5), rgba(185,0,255,0.5));
    background: linear-gradient(left, rgba(0,168,255,0.5), rgba(185,0,255,0.5));
    pointer-events: none;
}
 a.socialme{
    background-color: #3B5998; 
    border-color: transparent !important;
    color: #fff; 
    box-shadow: 0 2px 6px #8b9dc3;
  } 
  a.sociall{
    background-color: #0077B5; 
    border-color: transparent !important;
    color: #fff; 
    box-shadow: 0 2px 6px #8b9dc3;
  }  a.sociali{
     background: #d6249f;
    border-color: transparent !important;
    color: #fff; 
    box-shadow: 0 2px 6px #8b9dc3;
  }  a.socialt{
    background-color: #55acee;
    border-color: transparent !important;
    color: #fff; 
    box-shadow: 0 2px 6px #8b9dc3;
  }
  .fab
  {font-size:20px; padding-top: 5px; padding-bottom: 5px;}
  .card {
    margin-top: 30%;
    background-color: #fff;
    border-radius: 10px;
    border: none;
    position: relative;
    margin-bottom: 30px;
    box-shadow: 0 0.46875rem 2.1875rem rgba(90,97,105,0.1), 0 0.9375rem 1.40625rem rgba(90,97,105,0.1), 0 0.25rem 0.53125rem rgba(90,97,105,0.12), 0 0.125rem 0.1875rem rgba(90,97,105,0.1);
}
  </style>
}
</head>

<body class="container-login100" id="banner">
  <div class="loader"></div>
  <div id="app">
    <section class="section">
      <div class="container mt-5">
        <div class="row">
          <div class="col-12 col-sm-8 offset-sm-2 col-md-6 offset-md-3 col-lg-6 offset-lg-3 col-xl-4 offset-xl-4">
            <div class="card card-primary">
              <div class="card-header">
                <h4>Login</h4>
              </div>
              <div class="card-body">
                <form method="POST" action="<?php echo e(route('login')); ?>" class="needs-validation" novalidate="">
                  <?php echo csrf_field(); ?>
                  <div class="form-group">
                    <label for="email">E-Mail Address</label>
                    <input id="email" type="email" class="form-control <?php $__errorArgs = ['email'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" name="email" tabindex="1" value="<?php echo e(old('email')); ?>" autocomplete="email" required autofocus>
                    <div class="invalid-feedback">
                      Please fill in your email
                    </div>
                    <?php $__errorArgs = ['email'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                      <span class="invalid-feedback" role="alert">
                        <strong><?php echo e($message); ?></strong>
                      </span>
                    <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                  </div>
                  <div class="form-group">
                    <div class="d-block">
                      <label for="password" class="control-label">Password</label>
                      <div class="float-right">
                      <?php if(Route::has('password.request')): ?>
                        <a href="<?php echo e(route('password.request')); ?>" class="text-small">
                          <?php echo e(__('Forgot Your Password?')); ?>

                        </a>
                      <?php endif; ?>
                      </div>
                      <?php $__errorArgs = ['password'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                        <span class="invalid-feedback" role="alert">
                          <strong><?php echo e($message); ?></strong>
                        </span>
                      <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                    </div>
                    <input id="password" type="password" name="password" tabindex="2" class="form-control <?php $__errorArgs = ['password'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" required autocomplete="current-password">
                    <div class="invalid-feedback">
                      please fill in your password
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="custom-control custom-checkbox">
                      <input type="checkbox" name="remember" class="custom-control-input" tabindex="3" id="remember" <?php echo e(old('remember') ? 'checked' : ''); ?>>
                      <label class="custom-control-label" for="remember">Remember Me</label>
                    </div>
                  </div>
                  <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-lg btn-block" tabindex="4">
                     <i class="fas fa-sign-in-alt"></i> Login
                    </button>
                  </div>
                </form>
                <div class="text-center mt-4 mb-3">
                  <div class="text-job text-muted">Join With Social</div>
                </div>
                <div class="row sm-gutters ">
                  <div class="col-3">
                    <a  class="btn btn-block socialme">
                      <i class="fab fa-facebook"></i>
                    </a>
                  </div>
                  <div class="col-3">
                    <a class="btn btn-block socialt">
                      <span class="fab fa-twitter"></span>
                    </a>
                  </div> 
                  <div class="col-3">
                    <a class="btn btn-block sociali">
                      <span class="fab fa-instagram"></span>
                    </a>
                  </div>
                  <div class="col-3">
                    <a class="btn btn-block sociall">
                      <span class="fab fa-linkedin-in"></span>
                    </a>
                  </div>
                </div>
              </div>
            </div>
            <div class="mt-5 text-muted text-center">
              Developed By? <a target="_blank" href="www.goto.com">Durgesh Verma</a>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  <!-- General JS Scripts -->
  <script src="<?php echo e(asset('assets/js/app.min.js')); ?>"></script>
  <!-- JS Libraies -->
  <!-- Template JS File -->
  <script src="<?php echo e(asset('assets/js/scripts.js')); ?>"></script>
  <!-- Custom JS File -->
  <script src="<?php echo e(asset('assets/js/custom.js')); ?>"></script>

  <?php if(session('error')): ?>
  <script type="text/javascript">
    swal({
          title: "<?php echo e(session('error')); ?>",
          icon: "error",
          button: "OK",
        });
  </script>
  <?php endif; ?> 

  <script type="text/javascript">
  $( document ).ready(function() {
    localStorage.removeItem("firstTime");
  });

  </script>
</body>

</html><?php /**PATH C:\wamp64\www\cronex\resources\views/auth/login.blade.php ENDPATH**/ ?>