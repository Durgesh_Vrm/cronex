
<?php $__env->startSection('content'); ?>

<!--Breadcrumb start-->
<div class="ed_pagetitle">
<div class="ed_img_overlay"></div>
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12">
				<div class="page_title">
					<h2>About Us</h2>
				</div>
			</div>
			<div class="col-lg-12 col-md-12 col-sm-12">
				<ul class="breadcrumb">
					<li><a href="<?php echo e(url('/')); ?>">home</a></li>
					<li>//</li>
					<li><a href="<?php echo e(url('about-us')); ?>">About us</a></li>
				</ul>
			</div>
		</div>
	</div>
</div>
<!--Breadcrumb end-->
<!--Our expertise section one start -->
<div class="ed_transprentbg">
	<div class="container">
		<div class="row">
			<div class="col-lg-6 col-md-6 col-sm-12">
				<div class="ed_video_section_discription_img">
					<img src="front/images/content/about_dummy1.jpg" style="cursor:pointer"  alt="1" />
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-12">
				<div class="ed_video_section_discription ed_toppadder90">
					<h4><?php if(isset($data['ABOUT_T'])): ?> <?php echo e($data['ABOUT_T']); ?> <?php endif; ?></h4>
					<p><?php if(isset($data['ABOUT_T'])): ?> <?php echo $data['ABOUT_C']; ?> <?php endif; ?></p>
				</div>
			</div>
		</div>
    </div><!-- /.container -->
</div>
<!--Our expertise section one end -->
<!--skill section start -->
<div class="ed_graysection ed_toppadder90 ed_bottompadder60">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="ed_heading_top ed_bottompadder50">
					<h3>what we offer</h3>
				</div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
				<div class="skill_section">
					<h4><?php if(isset($data['WHAT_WE_OFFER-1_T'])): ?> <?php echo e($data['WHAT_WE_OFFER-1_T']); ?> <?php endif; ?></h4>
					<p><?php if(isset($data['WHAT_WE_OFFER-1_C'])): ?> <?php echo $data['WHAT_WE_OFFER-1_C']; ?> <?php endif; ?></p>
					<span><i class="fa fa-handshake-o" aria-hidden="true"></i></span>
				</div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
				<div class="skill_section">
					<h4><?php if(isset($data['WHAT_WE_OFFER-2_T'])): ?> <?php echo e($data['WHAT_WE_OFFER-2_T']); ?> <?php endif; ?></h4>
					<p><?php if(isset($data['WHAT_WE_OFFER-2_C'])): ?> <?php echo $data['WHAT_WE_OFFER-2_C']; ?> <?php endif; ?></p>
					<span><i class="fa fa-users" aria-hidden="true"></i></span>
				</div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
				<div class="skill_section">
					<h4><?php if(isset($data['WHAT_WE_OFFER-3_T'])): ?> <?php echo e($data['WHAT_WE_OFFER-3_T']); ?> <?php endif; ?></h4>
					<p><?php if(isset($data['WHAT_WE_OFFER-3_C'])): ?> <?php echo $data['WHAT_WE_OFFER-3_C']; ?> <?php endif; ?></p>
					<span><i class="fa fa-gift" aria-hidden="true"></i></span>
				</div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
				<div class="skill_section">
					<h4><?php if(isset($data['WHAT_WE_OFFER-4_T'])): ?> <?php echo e($data['WHAT_WE_OFFER-4_T']); ?> <?php endif; ?></h4>
					<p><?php if(isset($data['WHAT_WE_OFFER-4_C'])): ?> <?php echo $data['WHAT_WE_OFFER-4_C']; ?> <?php endif; ?></p>
					<span><i class="fa fa-money" aria-hidden="true"></i></span>
				</div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
				<div class="skill_section">
					<h4><?php if(isset($data['WHAT_WE_OFFER-5_T'])): ?> <?php echo e($data['WHAT_WE_OFFER-5_T']); ?> <?php endif; ?></h4>
					<p><?php if(isset($data['WHAT_WE_OFFER-5_C'])): ?> <?php echo $data['WHAT_WE_OFFER-5_C']; ?> <?php endif; ?></p>
					<span><i class="fa fa-child" aria-hidden="true"></i></span>
				</div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
				<div class="skill_section">
					<h4><?php if(isset($data['WHAT_WE_OFFER-6_T'])): ?> <?php echo e($data['WHAT_WE_OFFER-6_T']); ?> <?php endif; ?></h4>
					<p><?php if(isset($data['WHAT_WE_OFFER-6_C'])): ?> <?php echo $data['WHAT_WE_OFFER-6_C']; ?> <?php endif; ?></p>
					<span><i class="fa fa-tags" aria-hidden="true"></i></span>
				</div>
			</div>
        </div>
	</div>
</div>
<!--skill section end -->
<!--chart section start -->
<div class="ed_transprentbg ed_toppadder100">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="ed_counter_wrapper">
					<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
						<div class="ed_chart_ratio">
							<i class="fa fa-bus" aria-hidden="true"></i>
							<h4><?php if(isset($data['WHAT_WE_OFFER-7_T'])): ?> <?php echo e($data['WHAT_WE_OFFER-7_T']); ?> <?php endif; ?></h4>
							<p><?php if(isset($data['WHAT_WE_OFFER-7_C'])): ?> <?php echo $data['WHAT_WE_OFFER-7_C']; ?> <?php endif; ?></p>
						</div>
					</div>
					<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
						<div class="ed_chart_ratio">
							<i class="fa fa-plane" aria-hidden="true"></i>
							<h4><?php if(isset($data['WHAT_WE_OFFER-8_T'])): ?> <?php echo e($data['WHAT_WE_OFFER-8_T']); ?> <?php endif; ?></h4>
							<p><?php if(isset($data['WHAT_WE_OFFER-8_C'])): ?> <?php echo $data['WHAT_WE_OFFER-8_C']; ?> <?php endif; ?></p>
						</div>
					</div>
					<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
						<div class="ed_chart_ratio">
							<i class="fa fa-ship" aria-hidden="true"></i>
							<h4><?php if(isset($data['WHAT_WE_OFFER-9_T'])): ?> <?php echo e($data['WHAT_WE_OFFER-9_T']); ?> <?php endif; ?></h4>
							<p><?php if(isset($data['WHAT_WE_OFFER-9_C'])): ?> <?php echo $data['WHAT_WE_OFFER-9_C']; ?> <?php endif; ?></p>
						</div>
					</div>
				</div>
			</div>
        </div>
	</div>
</div>
<!-- chart Section end -->
<!-- Services start -->
<div class="ed_transprentbg ed_toppadder90 ed_bottompadder60">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="ed_heading_top ed_bottompadder50">
					<h3>our best services</h3>
				</div>
			</div>
			<div class="ed_mostrecomeded_course_slider">
				<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
					<div class="ed_mostrecomeded_course style_2">
						<div class="ed_item_img">
							<img src="<?php if(isset($data['Our Best Services_Ground Transport'])): ?><?php echo e(url('uplode/BannerAndLogo/'.$data['Our Best Services_Ground Transport'])); ?> <?php endif; ?>" alt="item1" class="img-responsive">
						</div>
						<div class="ed_item_description ed_most_recomended_data">
							<h4><a href="#"><?php if(isset($data['OUR_BEST_SERVICES-1_T'])): ?><?php echo e($data['OUR_BEST_SERVICES-1_T']); ?> <?php endif; ?></a></h4>
							<p><?php if(isset($data['OUR_BEST_SERVICES-1_C'])): ?> <?php echo $data['OUR_BEST_SERVICES-1_C']; ?> <?php endif; ?></p>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
					<div class="ed_mostrecomeded_course style_2">
						<div class="ed_item_img">
							<img  src="<?php if(isset($data['Our Best Services_Warehousing'])): ?><?php echo e(url('uplode/BannerAndLogo/'.$data['Our Best Services_Warehousing'])); ?> <?php endif; ?>" alt="item1" class="img-responsive">
						</div>
						<div class="ed_item_description ed_most_recomended_data">
							<h4><a href="#"><?php if(isset($data['OUR_BEST_SERVICES-2_T'])): ?><?php echo e($data['OUR_BEST_SERVICES-2_T']); ?> <?php endif; ?></a></h4>
							<p><?php if(isset($data['OUR_BEST_SERVICES-2_C'])): ?> <?php echo $data['OUR_BEST_SERVICES-2_C']; ?> <?php endif; ?></p>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
					<div class="ed_mostrecomeded_course style_2">
						<div class="ed_item_img">
							<img  src="<?php if(isset($data['Our Best Services_Packaging & Storage'])): ?><?php echo e(url('uplode/BannerAndLogo/'.$data['Our Best Services_Packaging & Storage'])); ?> <?php endif; ?>" alt="item1" class="img-responsive">
						</div>
						<div class="ed_item_description ed_most_recomended_data">
							<h4><a href="#"><?php if(isset($data['OUR_BEST_SERVICES-3_T'])): ?><?php echo e($data['OUR_BEST_SERVICES-3_T']); ?> <?php endif; ?></a></h4>
							<p><?php if(isset($data['OUR_BEST_SERVICES-3_C'])): ?> <?php echo $data['OUR_BEST_SERVICES-3_C']; ?> <?php endif; ?></p>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
					<div class="ed_mostrecomeded_course style_2">
						<div class="ed_item_img">
							<img src="<?php if(isset($data['Our Best Services_Door-To-Door Delivery'])): ?><?php echo e(url('uplode/BannerAndLogo/'.$data['Our Best Services_Door-To-Door Delivery'])); ?> <?php endif; ?>" alt="item1" class="img-responsive">
						</div>
						<div class="ed_item_description ed_most_recomended_data">
							<h4><a href="#"><?php if(isset($data['OUR_BEST_SERVICES-4_T'])): ?><?php echo e($data['OUR_BEST_SERVICES-4_T']); ?> <?php endif; ?></a></h4>
							<p><?php if(isset($data['OUR_BEST_SERVICES-4_C'])): ?> <?php echo $data['OUR_BEST_SERVICES-4_C']; ?> <?php endif; ?></p>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
					<div class="ed_mostrecomeded_course style_2">
						<div class="ed_item_img">
							<img  src="<?php if(isset($data['Our Best Services_Cargo'])): ?><?php echo e(url('uplode/BannerAndLogo/'.$data['Our Best Services_Cargo'])); ?> <?php endif; ?>" alt="item1" class="img-responsive">
						</div>
						<div class="ed_item_description ed_most_recomended_data">
							<h4><a href="#"><?php if(isset($data['OUR_BEST_SERVICES-5_T'])): ?><?php echo e($data['OUR_BEST_SERVICES-5_T']); ?> <?php endif; ?></a></h4>
							<p><?php if(isset($data['OUR_BEST_SERVICES-5_C'])): ?> <?php echo $data['OUR_BEST_SERVICES-5_C']; ?> <?php endif; ?></p>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
					<div class="ed_mostrecomeded_course style_2">
						<div class="ed_item_img">
							<img  src="<?php if(isset($data['Our Best Services_Logistic'])): ?><?php echo e(url('uplode/BannerAndLogo/'.$data['Our Best Services_Logistic'])); ?> <?php endif; ?>" alt="item1" class="img-responsive">
						</div>
						<div class="ed_item_description ed_most_recomended_data">
							<h4><a href="#"><?php if(isset($data['OUR_BEST_SERVICES-6_T'])): ?><?php echo e($data['OUR_BEST_SERVICES-6_T']); ?> <?php endif; ?></a></h4>
							<p><?php if(isset($data['OUR_BEST_SERVICES-6_C'])): ?> <?php echo $data['OUR_BEST_SERVICES-6_C']; ?> <?php endif; ?></p>
						</div>
					</div>
				</div>
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center ed_toppadder30">
					<a href="<?php echo e(url('service')); ?>" class="btn ed_btn ed_orange">view  more</a>
				</div>
			</div>
		</div>
    </div><!-- /.container -->
</div>
<!-- Services end -->
<!--Timer Section three start -->
<div class="ed_timer_section ed_toppadder90 ed_bottompadder60">
<div class="ed_img_overlay"></div>
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="ed_heading_top ed_bottompadder50">
					<h3>Why Choose Us</h3>
				</div>
			</div>
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="ed_counter_wrapper">
					<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
						<div class="ed_counter">
							<h2 class="timer" data-from="0" data-to="<?php if(isset($data['WHY_CHOOSE_US-1_T'])): ?> <?php echo $data['WHY_CHOOSE_US-1_T']; ?> <?php endif; ?>" data-speed="5000"></h2>
							<h4><?php if(isset($data['WHY_CHOOSE_US-1_C'])): ?> <?php echo $data['WHY_CHOOSE_US-1_C']; ?> <?php endif; ?></h4>
						</div>
					</div>
					<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
						<div class="ed_counter">
							<h2 class="timer" data-from="0" data-to="<?php if(isset($data['WHY_CHOOSE_US-2_T'])): ?> <?php echo $data['WHY_CHOOSE_US-2_T']; ?> <?php endif; ?>" data-speed="5000"></h2>
							<h4><?php if(isset($data['WHY_CHOOSE_US-2_C'])): ?> <?php echo $data['WHY_CHOOSE_US-2_C']; ?> <?php endif; ?></h4>
						</div>
					</div>
					<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
						<div class="ed_counter">
							<h2 class="timer" data-from="0" data-to="<?php if(isset($data['WHY_CHOOSE_US-3_T'])): ?> <?php echo $data['WHY_CHOOSE_US-3_T']; ?> <?php endif; ?>" data-speed="5000"></h2>
							<h4><?php if(isset($data['WHY_CHOOSE_US-3_C'])): ?> <?php echo $data['WHY_CHOOSE_US-3_C']; ?> <?php endif; ?></h4>
						</div>
					</div>
					<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
						<div class="ed_counter">
							<h2 class="timer" data-from="0" data-to="<?php if(isset($data['WHY_CHOOSE_US-4_T'])): ?> <?php echo $data['WHY_CHOOSE_US-4_T']; ?> <?php endif; ?>" data-speed="5000"></h2>
							<h4><?php if(isset($data['WHY_CHOOSE_US-4_C'])): ?> <?php echo $data['WHY_CHOOSE_US-4_C']; ?> <?php endif; ?></h4>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!--Timer Section three end -->

<!-- Testimonial start -->
<div class="ed_graysection ed_toppadder90 ed_bottompadder90">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ed_bottompadder80">
				<div class="ed_heading_top">
					<h3>what our client say</h3>
				</div>
			</div>
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="ed_latest_news_slider">
					<div id="owl-demo2" class="owl-carousel owl-theme">
						<div class="item">
							<div class="ed_item_description">
								<img src="front/images/content/t1.jpg" alt="1" title="1"/>
								<h4>Carolyn Ayala</h4>
								<span>CEO</span>
								<p>Just in case there is anyone looking for it, new expertise to our knowledge base to make you happy as well.</p>
							</div>
						</div>
						<div class="item">
							<div class="ed_item_description">
								<img src="front/images/content/t2.jpg" alt="1" title="1"/>
								<h4>Ronnie Parker</h4>
								<span>manager</span>
								<p>Just in case there is anyone looking for it, new expertise to our knowledge base to make you happy as well.</p>
							</div>
						</div>
						<div class="item">
							<div class="ed_item_description">
								<img src="front/images/content/t3.jpg" alt="1" title="1"/>
								<h4>Kim Hiatt</h4>
								<span>director</span>
								<p>Just in case there is anyone looking for it, new expertise to our knowledge base to make you happy as well.</p>
							</div>
						</div>
						<div class="item">
							<div class="ed_item_description">
								<img src="front/images/content/t4.jpg" alt="1" title="1"/>
								<h4>Michael Garza</h4>
								<span>Employee</span>
								<p>Just in case there is anyone looking for it, new expertise to our knowledge base to make you happy as well.</p>
							</div>
						</div>
						<div class="item">
							<div class="ed_item_description">
								<img src="front/images/content/t5.jpg" alt="1" title="1"/>
								<h4>Mary J. Cole</h4>
								<span>receptionist</span>
								<p>Just in case there is anyone looking for it, new expertise to our knowledge base to make you happy as well.</p>
							</div>
						</div> 
					</div>
				</div>
			</div>
		</div>
    </div><!-- /.container -->
</div>
<!--Testimonial end -->
<!--client section start -->
<div class="ed_transprentbg ed_toppadder90 ed_bottompadder90">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="ed_heading_top ed_bottompadder50">
					<h3>our partners</h3>
				</div>
			</div>
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="ed_clientslider">
					<div id="owl-demo5" class="owl-carousel owl-theme">
						<?php if(isset($data['Our Partners'])): ?>
						<?php $__currentLoopData = $data['Our Partners']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
						<div class="item">
							<a href="#">
								<img src="<?php echo e(url('uplode/BannerAndLogo/'.$value)); ?>" alt="Partner Img" />
							</a>
						</div>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
    </div><!-- /.container -->
</div>
<!--client section end -->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('front.layout', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\wamp64\www\cronex\resources\views/front/about.blade.php ENDPATH**/ ?>