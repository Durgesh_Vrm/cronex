<?php
        if(Auth::user()->role === 'ADMIN469785')
        {
            $rdata = DB::table('user_route')->select('id', 'route', 'Administrator')->where('Administrator', 'Yes')->get();
            if($rdata != '[]'){ $url = []; foreach ($rdata as $value){ $url[] = $value->route; }}
            else{ $url = []; }
        }        
        elseif(Auth::user()->role === 'USER58428D3')
        {
            $rdata = DB::table('user_route')->select('id', 'route')->where('USER58428D3', 'Yes')->get();
            if($rdata != '[]'){ $url = []; foreach ($rdata as $value){ $url[] = $value->route; }}
            else{ $url = []; }
        }        
        elseif(Auth::user()->role === 'EDITOR32238')
        {
            $rdata = DB::table('user_route')->select('id', 'route', 'Editor')->where('Editor', 'Yes')->get();
            if($rdata != '[]'){ $url = []; foreach ($rdata as $value){ $url[] = $value->route; }}
            else{ $url = []; }
        }        
        elseif(Auth::user()->role === 'MEMBER55228')
        {
            $rdata = DB::table('user_route')->select('id', 'route', 'Member')->where('Member', 'Yes')->get();
            if($rdata != '[]'){ $url = []; foreach ($rdata as $value){ $url[] = $value->route; }}
            else{ $url = []; }
        }        
        elseif(Auth::user()->role === 'EMPLOY15472')
        {
            $rdata = DB::table('user_route')->select('id', 'route', 'Employee')->where('Employee', 'Yes')->get();
            if($rdata != '[]'){ $url = []; foreach ($rdata as $value){ $url[] = $value->route; }}
            else{ $url = []; }
        }        
        elseif(Auth::user()->role === 'MANAGER0739')
        {
            $rdata = DB::table('user_route')->select('id', 'route', 'Manager')->where('Manager', 'Yes')->get();
            if($rdata != '[]'){ $url = []; foreach ($rdata as $value){ $url[] = $value->route; }}
            else{ $url = []; }
        }
        elseif (Auth::user()->role === 'SUPPERADMINC60C3421A4D4B2152B430') 
        {
            $url = [true];
        }
        else
        {
            $url = [];
        } 
?>

 <div class="main-sidebar sidebar-style-2">
        <aside id="sidebar-wrapper">
          <div class="sidebar-brand">
            <a href="<?php echo e(URL('dashboard')); ?>"> <img style="height: 45px;" alt="<?php echo e(config('LOGO')); ?>" src="<?php echo e(asset('uplode/config/logo/'.config('LOGO'))); ?>" class="header-logo" />
            </a>
          </div>
          <div class="sidebar-user">
            <div class="sidebar-user-picture">
              <?php $Name = explode(' ', Auth::user()->name, 2); ?>
              <?php if(!Auth::user()->image): ?> 
              <figure class="avatar mr-2 avatar-xl  text-white" data-initial="<?php if(isset($Name[1])){ $N = explode($Name[0], 1); echo substr($Name[0],0,1); echo substr($Name[1],0,1); }else{ echo substr($Name[0],0,1); } ?>"></figure>
              <?php else: ?> 
              <img alt="image" src="<?php echo e(asset('uplode/users/'.Auth::user()->image)); ?>">
              <?php endif; ?>
            </div>
            <div class="sidebar-user-details">
              <div class="user-name"><?php echo e(ucwords(Auth::user()->name)); ?></div>
              <div class="user-role"><?php echo e(ucfirst(Auth::user()->email)); ?></div>
            </div>
          </div>
          <ul class="sidebar-menu">
            <li class="menu-header">Main</li>
            <li class="active"><a class="nav-link" href="blank.html"><i data-feather="airplay"></i><span>Dashboard </span></a></li>
            <li class="dropdown">
              <a href="#" class="menu-toggle nav-link has-dropdown"><i data-feather="mail"></i><span>Email</span></a>
              <ul class="dropdown-menu">
                <li><a class="nav-link" href="<?php echo e(url('mail-inbox')); ?>">Inbox</a></li>
                <li><a class="nav-link" href="<?php echo e(url('email-compose')); ?>">Compose</a></li>
                <li><a class="nav-link" href="<?php echo e(url('mail-send')); ?>">Send</a></li>
              </ul>
            </li>
            <li class="dropdown">
              <a href="#" class="menu-toggle nav-link has-dropdown"><i data-feather="mail"></i><span>SMS</span></a>
              <ul class="dropdown-menu">
                <li><a class="nav-link" href="<?php echo e(url('email-compose')); ?>">Compose</a></li>
                <li><a class="nav-link" href="<?php echo e(url('mail-send')); ?>">Send</a></li>
              </ul>
            </li>

            <li class="dropdown">
              <a href="#" class="menu-toggle nav-link has-dropdown"><i
                  data-feather="shopping-bag"></i><span>Booking</span></a>
              <ul class="dropdown-menu">
                <li><a class="nav-link" href="<?php echo e(url('dashboard')); ?>">Today</a></li>
                <li><a class="nav-link" href="<?php echo e(url('dashboard')); ?>">Active</a></li>
                <li><a class="nav-link" href="<?php echo e(url('dashboard')); ?>">Pending</a></li>
                <li><a class="nav-link" href="<?php echo e(url('dashboard')); ?>">Close</a></li>
              </ul>
            </li>
            <li class="dropdown">
              <a href="#" class="menu-toggle nav-link has-dropdown"><i
                  data-feather="shopping-bag"></i><span>Booking Assign</span></a>
              <ul class="dropdown-menu">
                <li><a class="nav-link" href="<?php echo e(url('dashboard')); ?>">Assign Vender</a></li>
                <li><a class="nav-link" href="<?php echo e(url('dashboard')); ?>">Assign Driver</a></li>
                <li><a class="nav-link" href="<?php echo e(url('dashboard')); ?>">Close Booking</a></li>
              </ul>
            </li>
            <?php if(array_intersect(array('view-users','find-user','user-registration',true), $url)): ?>
            <li class="menu-header">Users</li>
            <li class="dropdown">
              <a href="#" class="menu-toggle nav-link has-dropdown"><i data-feather="layout"></i><span>Users</span></a>
              <ul class="dropdown-menu">
              <?php if(array_intersect(array('user-registration',true), $url)): ?>
                <li><a class="nav-link" href="<?php echo e(URL('user-registration')); ?>">User Registration</a></li>
              <?php endif; ?>
              <?php if(array_intersect(array('view-users',true), $url)): ?>
                <li><a class="nav-link" href="<?php echo e(URL('view-users')); ?>">View Users</a></li>
              <?php endif; ?>
              <?php if(array_intersect(array('edit-delete-users',true), $url)): ?>
                <li><a class="nav-link" href="<?php echo e(URL('edit-delete-users')); ?>">Edit & Del Users</a></li>
              <?php endif; ?>
              <?php if(array_intersect(array('find-user',true), $url)): ?>
                <li><a class="nav-link" href="<?php echo e(URL('find-user')); ?>">Find User</a></li>
              <?php endif; ?>
              </ul>
            </li>
            <?php endif; ?>
            <li class="dropdown">
              <a href="#" class="menu-toggle nav-link has-dropdown"><i data-feather="grid"></i><span>Team</span></a>
              <ul class="dropdown-menu">
                <li><a class="nav-link" href="<?php echo e(url('view-teams')); ?>">View Team</a></li>
                <li><a class="nav-link" href="<?php echo e(url('view-team-members')); ?>">View Team Member</a></li>
              </ul>
            </li>
            <li class="dropdown">
              <a href="#" class="menu-toggle nav-link has-dropdown"><i
                  data-feather="pie-chart"></i><span>Commission</span></a>
              <ul class="dropdown-menu">
                <li><a class="nav-link" href="chart-amchart.html">View Commission</a></li>
              </ul>
            </li>
            <li class="menu-header">Setting</li>
            <li class="dropdown">
              <a href="#" class="menu-toggle nav-link has-dropdown"><i data-feather="flag"></i><span>Margin</span></a>
              <ul class="dropdown-menu">
                <li><a href="<?php echo e(url('View-Center-Percentage')); ?>">Set Percentage</a></li>
              </ul>
            </li>
            <li class="dropdown">
              <a href="#" class="menu-toggle nav-link has-dropdown"><i data-feather="command"></i><span>Packages</span></a>
              <ul class="dropdown-menu">
                <li>
                  <a href="{url{('add-package')}}">Add Package</a>
                  <a href="{url{('view-package')}}">View Package</a>
                </li>
              </ul>
            </li>
            <li class="dropdown">
              <a href="#" class="menu-toggle nav-link has-dropdown"><i data-feather="briefcase"></i><span>Services Fee</span></a>
              <ul class="dropdown-menu">
                <li>
                    <a href="<?php echo e(url('add-service-fee')); ?>">Add Serivce Fee</a>
                    <a href="<?php echo e(url('view-service-fee')); ?>">View Service Fee</a>
                </li>
              </ul>
            </li>

            <?php if(array_intersect(array('countries','states','cities',true), $url)): ?>
            <li class="dropdown">
              <a href="#" class="menu-toggle nav-link has-dropdown"><i data-feather="sliders"></i><span>Setting</span></a>
              <ul class="dropdown-menu">
                <li class="dropdown">
                  <a href="#" class="has-dropdown">Location</a>
                  <ul class="dropdown-menu">
           <!--          <?php if(array_intersect(array('countries','states','cities',true), $url)): ?> -->
                    <li><a href="<?php echo e(URL('countries')); ?>"> Countries</a></li>
<!--                     <?php endif; ?>
                    <?php if(array_intersect(array('countries','states','cities',true), $url)): ?> -->
                    <li><a href="<?php echo e(URL('states')); ?>"> states</a></li>
                   <!--  <?php endif; ?>
                    <?php if(array_intersect(array('countries','states','cities',true), $url)): ?> -->
                    <li><a href="<?php echo e(URL('cities')); ?>"> Cities</a></li>
                 <!--    <?php endif; ?> -->
                  </ul>
                </li>
                <li class="dropdown">
                  <a href="#" class="has-dropdown">Logo & Banner</a>
                  <ul class="dropdown-menu">
                    <li class="dropdown">
                      <a href="add-banner-logo">Add Logo & Banner</a>
                      <a href="view-banner-logo">View Logo & Banner</a>
                    </li>
                  </ul>
                </li>
                <li class="dropdown">
                  <a href="#" class="has-dropdown">Website & App Content</a>
                  <ul class="dropdown-menu">
                    <li class="dropdown">
                      <a href="create-content">Add Content</a>
                      <a href="view-content">View Content</a>
                    </li>
                  </ul>
                </li>
                <li><a href="<?php echo e(URL('web-configuration-settings-value')); ?>"> App Configuration</a></li>
              </ul>
            </li>
            <?php endif; ?>

          </ul>
        </aside>
      </div><?php /**PATH C:\xampp\htdocs\cronex\resources\views////layouts/sidebar.blade.php ENDPATH**/ ?>