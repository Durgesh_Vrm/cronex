
<?php $__env->startSection('content'); ?>
      <!-- Main Content -->
      <div class="main-content">
        <section class="section">
          <ul class="breadcrumb breadcrumb-style ">
            <li class="breadcrumb-item">
              <a href="index.html">
                <i data-feather="home"></i></a>
            </li>
            <li class="breadcrumb-item">Setting</li>
            <li class="breadcrumb-item">User Permissions</li>
          </ul>
          <div class="section-body">
            <div class="row">
              <div class="col-12 col-sm-12 col-lg-12">
                <div class="card">
                          <div class="card-header">
                            <h4>User Permissions</h4>
                          </div>
                          <div class="card-body">
                              <div class="row">
                              <table class="table table-striped table-hover table-responsive"  id="tableExport" style="width:100%;">
                                <thead>
                                  <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Route Name</th>
                                    <?php $__currentLoopData = $data['role']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <th scope="col"><?php echo e($value->role_name); ?></th>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                  </tr>
                                </thead>
                                <tbody>
                                  <tr>
                                    <th >0</th>
                                    <th >Select All Routes</th>
                                    <?php $__currentLoopData = $data['role']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <?php $ids = $value->role_id; ?>
                                    <th>
                                        <div class="preview text-success"  onclick="route_all_permissions(this.id)" value="<?php echo e(Crypt::encrypt($value->role_id)); ?>" id="customChecks<?php echo e($value->id); ?>"><i class="material-icons">done_all</i> <span class="icon-name"></span></div>
                                    </th>
                                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                  </tr>
                                  <?php $sn=0; ?>
                                  <?php $__currentLoopData = $data['all']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $values): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                  <tr>
                                    <th scope="row"><?php echo e(++$sn); ?></th>
                                    <th><?php echo e($values->route); ?></th>
                                  <?php $__currentLoopData = $data['role']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                  <?php if($value->role_id != $values): ?>
                                    <?php $ids = $value->role_id; ?>
                                    <td>
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" onclick="route_permissions(this.id)" permission="<?php echo e($values->$ids); ?>" data="<?php echo e($values->id); ?>" value="<?php echo e(Crypt::encrypt($value->role_id)); ?>" id="customCheck<?php echo e($value->id); ?><?php echo e($values->id); ?>" <?php echo e($values->$ids == 'Yes'? 'checked':''); ?> >
                                        <label class="custom-control-label" for="customCheck<?php echo e($value->id); ?><?php echo e($values->id); ?>"></label>
                                      </div>
                                    </td>
                                  <?php endif; ?>
                                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                  </tr>
                                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </tbody>
                              </table>

                            </div>
                          </div>
                </div>
              </div>
            </div>

          </div>
        </section>
      </div>


<script type="text/javascript">
      
  function route_permissions(id) {
     var roleID = $('#'+id).val();
     var routeID = $('#'+id).attr('data');
     var permissionID = $('#'+id).attr('permission');

        swal({
          title: "Are you sure you want to change user role",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {
              $.ajax({
               type:"POST",
               url:"<?php echo e(url('/change-route-permissions')); ?>",
               data:{"_token": "<?php echo e(csrf_token()); ?>", "roleID": roleID, "routeID": routeID, "permissionID": permissionID},
               success:function(result)
               {  
                  swal({
                        title: "Successfully change role",
                        icon: "success",
                        button: "OK",
                      }).then((willDelete) => { if (willDelete) { location.reload();  }});      
               }
            });
          } 
        });
  }

  function route_all_permissions(id) {
     var roleID = $('#'+id).attr('value');

        swal({
          title: "Are you sure you want to change user role",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {
              $.ajax({
               type:"POST",
               url:"<?php echo e(url('/all-route-permissions')); ?>",
               data:{"_token": "<?php echo e(csrf_token()); ?>", "roleID": roleID},
               success:function(result)
               {  
                  swal({
                        title: "Successfully change role",
                        icon: "success",
                        button: "OK",
                      }).then((willDelete) => { if (willDelete) { location.reload();  }});      
               }
            });
          } 
        });
  }
   
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\wamp64\www\laravel9\resources\views/rol/view-rout.blade.php ENDPATH**/ ?>