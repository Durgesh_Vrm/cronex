
<?php $__env->startSection('content'); ?>
<style type="text/css">
  .card-header.note-toolbar .dropdown-menu {
    min-width: 150px; 
}
i.fab{
  color: #ffffff;
}
</style>

      <!-- Main Content -->
      <div class="main-content">
        <section class="section">
          <ul class="breadcrumb breadcrumb-style ">
            <li class="breadcrumb-item">
              <a href="index.html">
                <i data-feather="home"></i></a>
            </li>
            <li class="breadcrumb-item">Profile</li>
          </ul>
          <div class="section-body">
            <div class="row mt-sm-4">
              <div class="col-12 col-md-12 col-lg-4">
                <div class="card author-box">
                  <div class="card-body">
                    <div class="author-box-center">
                      <img alt="image" id="user-image" src="<?php echo e(asset('uplode/users/'.Auth::user()->image)); ?>" class="rounded-circle author-box-picture img-thumbnail img-responsive">
                      <div class="clearfix"></div>
                      <div class="author-box-name">
                        <a href="#"><?php echo e(ucwords(Auth::user()->name)); ?></a>
                      </div>
                      <div class="author-box-job"><b><?php echo e($data['Rol']); ?></b></div>
                    </div>
                    <div class="text-center">
                      <div class="mb-2 mt-3">
                        <div class="text-small font-weight-bold">Social Media</div>
                      </div>
                      <a href="<?php echo e(Auth::user()->facebook); ?>" class="btn btn-social-icon mr-1 btn-facebook">
                        <i class="fab fa-facebook-f"></i>
                      </a>
                      <a href="<?php echo e(Auth::user()->twitter); ?>" class="btn btn-social-icon mr-1 btn-twitter">
                        <i class="fab fa-twitter"></i>
                      </a>
                      <a href="<?php echo e(Auth::user()->linkedin); ?>" class="btn btn-social-icon mr-1 btn-github">
                        <i class="fab fa-linkedin"></i>
                      </a>
                      <a href="<?php echo e(Auth::user()->instagram); ?>" class="btn btn-social-icon mr-1 btn-instagram">
                        <i class="fab fa-instagram"></i>
                      </a>
                      <div class="w-100 d-sm-none"></div>
                    </div>
                  </div>
                </div>
                <div class="card">
                  <div class="card-header">
                    <h4>Personal Details</h4>
                  </div>
                  <div class="card-body gallery">
                    <div class="py-4 ">
                      <p class="clearfix">
                        <span class="float-left">
                          Gender
                        </span>
                        <span class="float-right text-muted">
                          <?php echo e(Auth::user()->gander); ?>

                        </span>
                      </p>
                      <p class="clearfix">
                        <span class="float-left">
                          Registration Number
                        </span>
                        <span class="float-right text-muted">
                          <?php echo e(Auth::user()->reg_number); ?>

                        </span>
                      </p>
                      <p class="clearfix">
                        <span class="float-left">
                          Birthday
                        </span>
                        <span class="float-right text-muted">
                          <?php echo e(Auth::user()->dob); ?>

                        </span>
                      </p>
                      <p class="clearfix">
                        <span class="float-left">
                          Phone No.
                        </span>
                        <span class="float-right text-muted">
                          +91 <?php echo e(Auth::user()->phone_no); ?>

                        </span>
                      </p>
                      <p class="clearfix">
                        <span class="float-left">
                          Mail
                        </span>
                        <span class="float-right text-muted">
                          <?php echo e(Auth::user()->email); ?>

                        </span>
                      </p>
                      <p class="clearfix">
                        <span class="float-left">
                          Aadhar Card
                        </span>
                        <span class="float-right text-muted">
                         <a style="width: 170px;  height: 20px;  text-align: right;" class="gallery-item  text-muted" data-title="Image 1" href="<?php echo e(asset('uplode/aadhar_card/'.$data['kyc']->aadhaar_proof_img)); ?>" title="Image 1"><?php echo e($data['kyc']->aadhaar_proof_no); ?> </a>
                        </span>
                      </p>
                      <p class="clearfix">
                        <span class="float-left">
                          Pan Card
                        </span>
                        <span class="float-right text-muted">
                         <a style="width: 170px;  height: 20px;  text-align: right;" class="gallery-item  text-muted" data-title="Image 1" href="<?php echo e(asset('uplode/pan/'.$data['kyc']->pan_img)); ?>" title="Image 1"><?php echo e($data['kyc']->pan_no); ?> </a>
                        </span>
                      </p>
                      <p class="clearfix">
                        <span class="float-left">
                          Other Id Card
                        </span>
                        <span class="float-right text-muted">
                          <a style="width: 170px; height: 20px; text-align: right;" class="gallery-item  text-muted" data-title="Image 1" href="<?php echo e(asset('uplode/idcard/'.$data['kyc']->id_proof_img)); ?>" title="Image 1"><?php echo e($data['kyc']->id_proof_no); ?> </a>
                        </span>
                      </p>
                    </div>
                  </div>
                </div>
                <div class="card">
                  <div class="card-header">
                    <h4>Address</h4>
                  </div>
                  <div class="card-body">
                    <ul class="list-unstyled user-progress list-unstyled-border list-unstyled-noborder">
                      <?php $__currentLoopData = $data['address']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                      <li class="media">
                        <div class="media-body">
                          <div class="media-title"><i class="fas <?php if($value->type == 'Home'): ?> <?php echo e('fa-home'); ?> <?php elseif($value->type == 'Office'): ?> <?php echo e('fa-hotel'); ?> <?php else: ?> <?php echo e('fa-map-marked-alt'); ?> <?php endif; ?> fa-md"></i></div>
                        </div>
                        <div class="p-l-30">
                          <?php echo e($value->address. $value->street.' '. $value->city.' '. $value->country.' '. $value->zip); ?>

                        </div>
                      </li>
                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </ul>
                  </div>
                </div>
              </div>
              <div class="col-12 col-md-12 col-lg-8">
                <div class="card">
                  <div class="padding-20">
                    <ul class="nav nav-tabs" id="myTab2" role="tablist">
                      <li class="nav-item">
                        <a class="nav-link <?php if($name == 'Setting'): ?> <?php else: ?> active <?php endif; ?>" id="home-tab2" data-toggle="tab" href="#about" role="tab"
                          aria-selected="true">About</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" id="profile-tab2" data-toggle="tab" href="#settings" role="tab"
                          aria-selected="false">Profile Setting</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" id="document-tab2" data-toggle="tab" href="#document" role="tab"
                          aria-selected="false">Document</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" id="profile-image-tab2" data-toggle="tab" href="#profile-image" role="tab"
                          aria-selected="false">Profile Image</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" id="address-tab2" data-toggle="tab" href="#address" role="tab"
                          aria-selected="false">Address</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" id="education-tab2" data-toggle="tab" href="#education" role="tab"
                          aria-selected="false">Education</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" id="experience-tab2" data-toggle="tab" href="#experience" role="tab"
                          aria-selected="false">Experience</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link <?php if($name == 'Setting'): ?> active <?php else: ?> <?php endif; ?>" id="setting-tab2" data-toggle="tab" href="#setting" role="tab"
                          aria-selected="false">Setting</a>
                      </li>
                    </ul>
                    <div class="tab-content tab-bordered" id="myTab3Content">
                      <div class="tab-pane fade <?php if($name == 'Setting'): ?> <?php else: ?> show active table-responsive <?php endif; ?>" id="about" role="tabpanel" aria-labelledby="home-tab2">
                        <div class="row">
                          <div class="col-md-3 col-6 b-r">
                            <strong>Father's Name</strong>
                            <br>
                            <p class="text-muted"><?php echo e(ucwords(Auth::user()->father_name)); ?></p>
                          </div>
                          <div class="col-md-3 col-6 b-r">
                            <strong>Mother's Name</strong>
                            <br>
                            <p class="text-muted"><?php echo e(Auth::user()->mother_name); ?></p>
                          </div>
                          <div class="col-md-3 col-6 b-r">
                            <strong>Mobile</strong>
                            <br>
                            <p class="text-muted"><?php echo e(Auth::user()->other_phone_no); ?></p>
                          </div>
                          <div class="col-md-3 col-6">
                            <strong>Location</strong>
                            <br>
                            <p class="text-muted">India</p>
                          </div>
                        </div>
                        <div class="section-title">Bio</div>
                        <p class="m-t-30"><?php echo Auth::user()->bio; ?></p>
                        <div class="section-title">Education</div>
                        <table class="table table-sm">
                          <tr>
                            <th>Education</th>
                            <th>Duration</th>
                            <th>%</th>
                            <th>Passing Year</th>
                            <th>School/College</th>
                          </tr>
                          <?php $__currentLoopData = $data['qualification']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                          <tr>
                            <td><?php echo e($value->qulification); ?></td>
                            <td><?php echo e($value->duration); ?></td>
                            <td><?php echo e($value->percentage); ?></td>
                            <td><?php echo e($value->pass_year); ?></td>
                            <td><?php echo e($value->collage); ?></td>
                            <td><?php echo e($value->desc); ?></td>
                          </tr>
                          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </table>
                        <div class="section-title">Experience</div>
                        <table class="table table-sm">
                          <tr>
                            <th>Company</th>
                            <th>Role</th>
                            <th>Duration</th>
                            <th>Job Note</th>
                            <th>Date Of Employment</th>
                          </tr>
                          <?php $__currentLoopData = $data['experience']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                          <tr>
                            <td><?php echo e($value->company); ?></td>
                            <td><?php echo e($value->role); ?></td>
                            <td><?php echo e($value->time); ?> Year</td>
                            <td><?php echo e($value->job_note); ?></td>
                            <td><?php echo e($value->date_of_employment); ?></td>
                          </tr>
                          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </table>
                      </div>
                      <div class="tab-pane fade" id="settings" role="tabpanel" aria-labelledby="profile-tab2">
                        <form method="post" action="<?php echo e(URL('update-profile')); ?>" class="needs-validation" id="form_pro">
                          <?php echo csrf_field(); ?>
                          <div class="card-header">
                            <h4>Edit Profile</h4>
                          </div>
                          <div class="card-body">
                            <div class="row">
                              <div class="form-group col-md-6 col-12">
                                <label>Name</label>
                                <input type="text" class="form-control <?php $__errorArgs = ['name'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" name="name" value="<?php echo e(Auth::user()->name); ?>" required>
                                <?php $__errorArgs = ['name'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                    <div class="invalid-feedback"><?php echo e($message); ?></div>
                                <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                              </div>

                              <div class="form-group col-md-6 col-12">
                                <label>Father's Name</label>
                                <input type="text" class="form-control <?php $__errorArgs = ['father_name'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" name="father_name" value="<?php echo e(Auth::user()->father_name); ?>">
                                <?php $__errorArgs = ['father_name'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                    <div class="invalid-feedback"><?php echo e($message); ?></div>
                                <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                              </div>

                              <div class="form-group col-md-6 col-12">
                                <label>Mother's Name</label>
                                <input type="text" class="form-control <?php $__errorArgs = ['mother_name'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" name="mother_name" value="<?php echo e(Auth::user()->mother_name); ?>">
                                <?php $__errorArgs = ['mother_name'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                    <div class="invalid-feedback"><?php echo e($message); ?></div>
                                <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                              </div>
                              <div class="form-group col-md-6 col-12">
                                <label>Gender</label>
                                    <div class="selectgroup w-100">
                                      <label class="selectgroup-item">
                                        <input type="radio" name="gender" value="1" class="selectgroup-input-radio <?php $__errorArgs = ['gender'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" <?php echo e(Auth::user()->gander == 'Male'? 'checked':''); ?> required>
                                        <span class="selectgroup-button">Male</span>
                                      </label>
                                      <label class="selectgroup-item">
                                        <input type="radio" name="gender" value="2" class="selectgroup-input-radio <?php $__errorArgs = ['gender'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" <?php echo e(Auth::user()->gander == 'Female'? 'checked':''); ?>>
                                        <span class="selectgroup-button">Female</span>
                                      </label>
                                      <label class="selectgroup-item">
                                        <input type="radio" name="gender" value="3" class="selectgroup-input-radio <?php $__errorArgs = ['gender'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" <?php echo e(Auth::user()->gander == 'Other'? 'checked':''); ?>>
                                        <span class="selectgroup-button" >Other</span>
                                      </label>
                                    </div>
                                <?php $__errorArgs = ['gender'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                    <div class="invalid-feedback"><?php echo e($message); ?></div>
                                <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                              </div>
                              <div class="form-group col-md-6 col-12">
                                <label>Birthdate</label>
                                <input type="date" class="form-control <?php $__errorArgs = ['birthdate'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" name="birthdate" value="<?php echo e(Auth::user()->dob); ?>" required>
                                <?php $__errorArgs = ['birthdate'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                    <div class="invalid-feedback"><?php echo e($message); ?></div>
                                <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                              </div>
                            </div>
                            <div class="row">
                              <div class="form-group col-md-6 col-12">
                                <label>Phone</label>
                                <input type="tel" class="form-control <?php $__errorArgs = ['phone_number'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" name="phone_number" value="<?php echo e(Auth::user()->phone_no); ?>">
                                <?php $__errorArgs = ['phone_number'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                    <div class="invalid-feedback"><?php echo e($message); ?></div>
                                <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                              </div>
                              <div class="form-group col-md-6 col-12">
                                <label>Other Phone</label>
                                <input type="tel" class="form-control <?php $__errorArgs = ['other_phone_number'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" name="other_phone_number" value="<?php echo e(Auth::user()->other_phone_no); ?>">
                              </div>
                                <?php $__errorArgs = ['other_phone_number'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                    <div class="invalid-feedback"><?php echo e($message); ?></div>
                                <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                            </div>
                            <div class="row">
                              <div class="form-group col-md-6 col-12">
                                <label>Facebook</label>
                                <input type="text" class="form-control <?php $__errorArgs = ['facebook'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" name="facebook" value="<?php echo e(Auth::user()->facebook); ?>">
                                <?php $__errorArgs = ['facebook'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                    <div class="invalid-feedback"><?php echo e($message); ?></div>
                                <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                              </div>
                              <div class="form-group col-md-6 col-12">
                                <label>Twitter</label>
                                <input type="tel" class="form-control <?php $__errorArgs = ['twitter'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" name="twitter" value="<?php echo e(Auth::user()->twitter); ?>">
                                <?php $__errorArgs = ['twitter'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                    <div class="invalid-feedback"><?php echo e($message); ?></div>
                                <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                              </div>
                              <div class="form-group col-md-6 col-12">
                                <label>Instagram</label>
                                <input type="tel" class="form-control <?php $__errorArgs = ['instagram'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" name="instagram" value="<?php echo e(Auth::user()->instagram); ?>">
                                <?php $__errorArgs = ['instagram'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                    <div class="invalid-feedback"><?php echo e($message); ?></div>
                                <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                              </div>
                              <div class="form-group col-md-6 col-12">
                                <label>Linkedin</label>
                                <input type="tel" class="form-control <?php $__errorArgs = ['linkedin'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" name="linkedin" value="<?php echo e(Auth::user()->linkedin); ?>">
                                <?php $__errorArgs = ['linkedin'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                    <div class="invalid-feedback"><?php echo e($message); ?></div>
                                <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                              </div>
                            </div>
                            <div class="row">
                              <div class="form-group col-12" >
                                <label>Bio</label>
                                <textarea class="summernote-simple <?php $__errorArgs = ['bio'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" name="bio"><?php echo e(Auth::user()->bio); ?></textarea>
                                <?php $__errorArgs = ['bio'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                    <div class="invalid-feedback"><?php echo e($message); ?></div>
                                <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                              </div>
                            </div>
                            <div class="row">
                              <div class="form-group mb-0 col-12">
                                <div class="custom-control custom-checkbox">
                                  <input type="checkbox" name="remember" class="custom-control-input <?php $__errorArgs = ['remember'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" id="newsletter" checked disabled>
                                  <label class="custom-control-label" for="newsletter">I agree with <a href="">App_Name</a> <a href="">Terms Conditions</a>  and <a href="">Privacy Policy</a> </label>
                                  <div class="text-muted form-text">
                                    You will get new information about products, offers and promotions
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="card-footer text-right">
                            <button class="btn btn-primary" onclick="SubmitForm('form_pro');" type="button"><i class="fas fa-save"></i> Save Changes</button>
                          </div>
                        </form>
                      </div>   
                      <div class="tab-pane fade" id="document" role="tabpanel" aria-labelledby="document-tab2">
                        <form method="post" class="needs-validation" id="form_Docum">
                          <div class="card-header">
                            <h4>Edit Document</h4>
                          </div>
                          <div class="card-body" id="aniimated-thumbnials">
                            <div class="row">
                              <div class="form-group col-md-3 col-12">
                                <div class="list-unstyled clearfix">
                                  <?php if($data['kyc']->aadhaar_proof_veryfy == 'Yes'): ?>
                                  <div class="verify-green badge-outline col-green"><i class="far fa-check-circle"> </i> Verify</div>
                                  <?php else: ?>
                                  <div class="verify-red badge-outline col-red"><i class="far fa-times-circle"> </i> <?php echo e($data['kyc']->aadhaar_proof_veryfy); ?> Verify</div>
                                  <?php endif; ?>
                                    <a href="<?php echo e(asset('uplode/aadhar_card/'.$data['kyc']->aadhaar_proof_img)); ?>" data-sub-html="Demo Description" style="margin-top: -14px;">
                                      <img style="width: 150px;" class="img-responsive thumbnail img-thumbnail img-responsive" src="<?php echo e(asset('uplode/aadhar_card/'.$data['kyc']->aadhaar_proof_img)); ?>" alt="">
                                    </a>
                                </div>
                              </div>
                              <div class="form-group col-md-5 col-12">
                                <label>Aadhar Card Number</label>
                                <input type="number" class="form-control" value="<?php echo e($data['kyc']->aadhaar_proof_no); ?>" <?php echo e($data['kyc']->aadhaar_proof_veryfy == 'Yes'? 'disabled':''); ?> placeholder="*******">
                                <div class="invalid-feedback">
                                  Please fill in the email
                                </div>
                              </div>
                              <div class="form-group col-md-4 col-12">
                                <label>Aadhar Card Front</label>
                                <div class="custom-file">
                                <input type="file" class="custom-file-input" id="customAadhar" <?php echo e($data['kyc']->aadhaar_proof_veryfy == 'Yes'? 'disabled':''); ?>>
                                <label class="custom-file-label" for="customAadhar">Choose Aadhar Card</label>
                              </div>
                              </div>
                            </div> 
                            <div class="row">
                              <div class="form-group col-md-3 col-12">
                                <div class="list-unstyled clearfix" >
                                  <?php if($data['kyc']->aadhaar_proof_veryfy == 'Yes'): ?>
                                  <div class="verify-green badge-outline col-green"><i class="far fa-check-circle"> </i> Verify</div>
                                  <?php else: ?>
                                  <div class="verify-red badge-outline col-red"><i class="far fa-times-circle"> </i> <?php echo e($data['kyc']->aadhaar_proof_veryfy); ?> Verify</div>
                                  <?php endif; ?>
                                    <a href="<?php echo e(asset('uplode/aadhar_card/'.$data['kyc']->aadhaar_proof_img_back)); ?>" data-sub-html="Demo Description" style="margin-top: -14px;">
                                      <img style="width: 150px;" class="img-responsive thumbnail img-thumbnail img-responsive" src="<?php echo e(asset('uplode/aadhar_card/'.$data['kyc']->aadhaar_proof_img_back)); ?>" alt="">
                                    </a>
                                </div>
                              </div>
                              <div class="form-group col-md-5 col-12">
                                <label>VID Number</label>
                                <input type="number" class="form-control" value="<?php echo e($data['kyc']->aadhaar_proof_vid); ?>" placeholder="" <?php echo e($data['kyc']->aadhaar_proof_veryfy == 'Yes'? 'disabled':''); ?>>
                                <div class="invalid-feedback">
                                  Please fill in the email
                                </div>
                              </div>
                              <div class="form-group col-md-4 col-12">
                                <label>Aadhar Card Back</label>
                                <div class="custom-file">
                                <input type="file" class="custom-file-input" id="customAa" <?php echo e($data['kyc']->aadhaar_proof_veryfy == 'Yes'? 'disabled':''); ?>>
                                <label class="custom-file-label" for="customAa">Choose Aadhar Card</label>
                              </div>
                              </div>
                            </div> 
                            <div class="row">
                              <div class="form-group col-md-3 col-12">
                                <div class="list-unstyled clearfix" >
                                  <?php if($data['kyc']->pan_veryfy == 'Yes'): ?>
                                  <div class="verify-green badge-outline col-green"><i class="far fa-check-circle"> </i> Verify</div>
                                  <?php else: ?>
                                  <div class="verify-red badge-outline col-red"><i class="far fa-times-circle"> </i> <?php echo e($data['kyc']->pan_veryfy); ?> Verify</div>
                                  <?php endif; ?>
                                    <a href="<?php echo e(asset('uplode/pan/'.$data['kyc']->pan_img)); ?>" data-sub-html="Demo Description" style="margin-top: -14px;">
                                      <img style="width: 150px;" class="img-responsive thumbnail img-thumbnail img-responsive" src="<?php echo e(asset('uplode/pan/'.$data['kyc']->pan_img)); ?>" alt="">
                                    </a>
                                </div>
                              </div>
                              <div class="form-group col-md-5 col-12">
                                <label>Pan Card Number</label>
                                <input type="text" class="form-control" value="<?php echo e($data['kyc']->pan_no); ?>" placeholder="" <?php echo e($data['kyc']->pan_veryfy == 'Yes'? 'disabled':''); ?>>
                                <div class="invalid-feedback">
                                  Please fill in the email
                                </div>
                              </div>
                              <div class="form-group col-md-4 col-12">
                                <label>Pan Card</label>
                                <div class="custom-file">
                                <input type="file" class="custom-file-input" id="customPan"  <?php echo e($data['kyc']->pan_veryfy == 'Yes'? 'disabled':''); ?>>
                                <label class="custom-file-label" for="customPan">Choose Pan Card</label>
                              </div>
                              </div>
                            </div> 
                            <div class="row">
                              <div class="form-group col-md-3 col-12">
                                <div class="list-unstyled clearfix" >
                                  <?php if($data['kyc']->id_proof_veryfy == 'Yes'): ?>
                                  <div class="verify-green badge-outline col-green"><i class="far fa-check-circle"> </i> Verify</div>
                                  <?php else: ?>
                                  <div class="verify-red badge-outline col-red"><i class="far fa-times-circle"> </i> <?php echo e($data['kyc']->id_proof_veryfy); ?> Verify</div>
                                  <?php endif; ?>
                                    <a href="<?php echo e(asset('uplode/idcard/'.$data['kyc']->id_proof_img)); ?>" data-sub-html="Demo Description" style="margin-top: -14px;">
                                      <img style="width: 150px;" class="img-responsive thumbnail img-thumbnail img-responsive" src="<?php echo e(asset('uplode/idcard/'.$data['kyc']->id_proof_img)); ?>" alt="">
                                    </a>
                                </div>
                              </div>
                              <div class="form-group col-md-5 col-12">
                                <label>Choose ID Type And Enter ID No</label>
                                <div class="input-group">
                                <select class="custom-select" id="inputGroupSelect05">
                                  <option <?php echo e($data['kyc']->id_proof_veryfy == 'Yes'? 'disabled':''); ?>>Choose...</option>

                                  <option value="1" <?php echo e($data['kyc']->id_proof_type == "Indian passport"? 'selected':''); ?> <?php echo e($data['kyc']->id_proof_veryfy == 'Yes'? 'disabled':''); ?>>Indian passport</option>

                                  <option value="2" <?php echo e($data['kyc']->id_proof_type == "Bank/ Kisan/ Post Office Passbooks"? 'selected':''); ?> <?php echo e($data['kyc']->id_proof_veryfy == 'Yes'? 'disabled':''); ?>>Bank/ Kisan/ Post Office Passbooks</option>

                                  <option value="3" <?php echo e($data['kyc']->id_proof_type == "Gas Connection Bill"? 'selected':''); ?> <?php echo e($data['kyc']->id_proof_veryfy == 'Yes'? 'disabled':''); ?>>Gas Connection Bill</option>

                                  <option value="4" <?php echo e($data['kyc']->id_proof_type == "Driving license"? 'selected':''); ?> <?php echo e($data['kyc']->id_proof_veryfy == 'Yes'? 'disabled':''); ?>>Driving license</option>

                                  <option value="5" <?php echo e($data['kyc']->id_proof_type == "Service Identity Card issued by State/Central Government"? 'selected':''); ?> <?php echo e($data['kyc']->id_proof_veryfy == 'Yes'? 'disabled':''); ?>>Service Identity Card issued by State/Central Government</option>

                                  <option value="6" <?php echo e($data['kyc']->id_proof_type == "Electoral Photo Identity Card (EPIC)"? 'selected':''); ?> <?php echo e($data['kyc']->id_proof_veryfy == 'Yes'? 'disabled':''); ?>>Electoral Photo Identity Card (EPIC)</option>
                                </select>
                                <input type="text" value="<?php echo e($data['kyc']->id_proof_no); ?>" class="form-control" <?php echo e($data['kyc']->id_proof_veryfy == 'Yes'? 'disabled':''); ?>>
                              </div>
                              </div>
                              <div class="form-group col-md-4 col-12">
                                <label>Id Card</label>
                                <div class="custom-file">
                                <input type="file" class="custom-file-input" id="customCard" <?php echo e($data['kyc']->id_proof_veryfy == 'Yes'? 'disabled':''); ?>>
                                <label class="custom-file-label" for="customCard">Choose ID Card</label>
                              </div>
                              </div>
                            </div>
                          </div>
                          <div class="card-footer text-right">
                            <button class="btn btn-primary" type="button" onclick="SubmitForm('form_Docum');"><i class="fas fa-save"></i> Save Changes</button>
                          </div>
                        </form>
                      </div>

                      <div class="tab-pane fade" id="profile-image" role="tabpanel" aria-labelledby="profile-image-tab2">
                        <form method="post" action="<?php echo e(URL('user-profile-uplode')); ?>" class="needs-validation" enctype="multipart/form-data" id="form_uplode">
                          <?php echo csrf_field(); ?>
                          <div class="card-header">
                            <h4>Edit Profile</h4>
                          </div>
                          <div class="card-body">
                              <div class="row">
                                <div class="col-sm-2 col-lg-4 col-mb-2 mb-md-0"></div>
                                <div class="col-sm-8 col-lg-4 col-mb-6 mb-md-0">
                                <div class="avatar-item">
                                  <img alt="image" src="<?php echo e(asset('uplode/users/'.Auth::user()->image)); ?>" class="img-fluid img-thumbnail img-responsive" data-toggle="tooltip" title="<?php echo e(Auth::user()->nmae); ?>" data-original-title="<?php echo e(Auth::user()->nmae); ?>">
                                </div>
                              </div>
                              <div class="col-md-4 col-lg-4"></div>
                              <div class="col-md-4 col-lg-4"></div>
                              <div class="form-group col-sm-12 col-md-4 col-lg-4">
                                <div class="custom-file">
                                <input type="file" class="custom-file-input" name="profile-photo" id="customFiles">
                                <label class="custom-file-label" for="customFiles">Choose Image</label>
                              </div>
                              </div>
                            </div> 
                          </div>
                          <div class="card-footer text-right">
                            <button class="btn btn-primary" type="button" onclick="SubmitForm('form_uplode');"><i class="fas fa-save"></i> Save Changes</button>
                          </div>
                        </form>
                      </div> 

                      <div class="tab-pane fade" id="address" role="tabpanel" aria-labelledby="address-tab2">
                        <form method="post" class="needs-validation">
                          <div class="card-header">
                            <h4>Manage Address</h4>
                          </div>
                          <div class="card-body">
                            <ul class="list-unstyled user-progress list-unstyled-border list-unstyled-noborder">
                              <?php $__currentLoopData = $data['address']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                              <li class="media">
                                <div>
                                  <div class="media-title"><i class="fas <?php if($value->type == 'Home'): ?> <?php echo e('fa-home'); ?> <?php elseif($value->type == 'Office'): ?> <?php echo e('fa-hotel'); ?> <?php else: ?> <?php echo e('fa-map-marked-alt'); ?> <?php endif; ?> fa-md"></i></div>
                                </div>
                                <div class="p-l-30 text-left">
                                  <?php echo e($value->address. $value->street.' '. $value->city.' '. $value->country.' '. $value->zip); ?> &emsp;&emsp;
                                <a href=""> <i class="far fa-edit text-info"></i></a>
                                </div>
                              </li>
                              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </ul>
                          </div>
                          <div class="card-footer text-right">
                            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#exampleModal"><i class="fas fa-plus" ></i> New Address</button>
                          </div>
                        </form>
                      </div> 

                      <div class="tab-pane fade" id="education" role="tabpanel" aria-labelledby="education-tab2">
                        <form method="post" class="needs-validation">
                          <div class="card-header">
                            <h4>Manage Education Info</h4>
                          </div>
                          <div class="card-body">
                            <ul class="list-unstyled user-progress list-unstyled-border list-unstyled-noborder">
                              <?php $__currentLoopData = $data['address']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                              <li class="media">
                                <div>
                                  <div class="media-title"><i class="fas <?php if($value->type == 'Home'): ?> <?php echo e('fa-home'); ?> <?php elseif($value->type == 'Office'): ?> <?php echo e('fa-hotel'); ?> <?php else: ?> <?php echo e('fa-map-marked-alt'); ?> <?php endif; ?> fa-md"></i></div>
                                </div>
                                <div class="p-l-30 text-left">
                                  <?php echo e($value->address. $value->street.' '. $value->city.' '. $value->country.' '. $value->zip); ?> &emsp;&emsp;
                                <a href=""> <i class="far fa-edit text-info"></i></a>
                                </div>
                              </li>
                              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </ul>
                          </div>
                          <div class="card-footer text-right">
                            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#educationModal"><i class="fas fa-plus" ></i> New Address</button>
                          </div>
                        </form>
                      </div> 

                      <div class="tab-pane fade" id="experience" role="tabpanel" aria-labelledby="experience-tab2">
                        <form method="post" class="needs-validation">
                          <div class="card-header">
                            <h4>Manage Address</h4>
                          </div>
                          <div class="card-body">
                            <ul class="list-unstyled user-progress list-unstyled-border list-unstyled-noborder">
                              <?php $__currentLoopData = $data['address']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                              <li class="media">
                                <div>
                                  <div class="media-title"><i class="fas <?php if($value->type == 'Home'): ?> <?php echo e('fa-home'); ?> <?php elseif($value->type == 'Office'): ?> <?php echo e('fa-hotel'); ?> <?php else: ?> <?php echo e('fa-map-marked-alt'); ?> <?php endif; ?> fa-md"></i></div>
                                </div>
                                <div class="p-l-30 text-left">
                                  <?php echo e($value->address. $value->street.' '. $value->city.' '. $value->country.' '. $value->zip); ?> &emsp;&emsp;
                                <a href=""> <i class="far fa-edit text-info"></i></a>
                                </div>
                              </li>
                              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </ul>
                          </div>
                          <div class="card-footer text-right">
                            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#experienceModal"><i class="fas fa-plus" ></i> New Address</button>
                          </div>
                        </form>
                      </div>

                      <div class="tab-pane fade  <?php if($name == 'Setting'): ?> show active table-responsive <?php else: ?> <?php endif; ?>" id="setting" role="tabpanel" aria-labelledby="setting-tab2">
                        <form method="post" class="needs-validation" id="form_Manage">
                          <div class="card-header">
                            <h4>Manage Setting</h4>
                          </div>
                          <div class="card-body">
                            <div class="row">
                              <div class="form-group col-md-6 col-lg-6 col-sm-12">
                                <div class="pretty p-switch p-fill">
                                  <input type="checkbox" id="ShowProfileEveryone" <?php echo e(Auth::user()->show_profile_everyone == 'Yes'? 'checked':''); ?>>
                                  <div class="state p-success">
                                    <label><i class="fas fa-eye"> </i> Show Profile Everyone</label>
                                  </div>
                                </div>
                              </div>
                              <div class="form-group col-md-6 col-lg-6 col-sm-12">
                                <div class="pretty p-switch p-fill">
                                  <input type="checkbox"  id="ActivitiesLog" <?php echo e(Auth::user()->activities_recorder == 'Yes'? 'checked':''); ?>>
                                  <div class="state p-success">
                                    <label><i class="fas fa-eye"> </i> Activities Log</label>
                                  </div>
                                </div>
                              </div>
                              <div class="form-group col-md-6 col-lg-6 col-sm-12">
                                <div class="pretty p-switch p-fill">
                                  <input type="checkbox" name="two_step_verification" id="two_step_verification" <?php echo e($data['TSV'] == 'Yes' ? 'checked':''); ?>>
                                  <div class="state p-success">
                                    <label><i class="fa fa-lock" aria-hidden="true"></i> Two Step Verification</label>
                                  </div>
                                </div>
                              </div>
                              <div class="form-group col-md-6 col-lg-6 col-sm-12">
                                <div class="pretty p-switch p-fill">
                                  <input type="checkbox" id="Notification" <?php echo e(Auth::user()->notification == 'Yes'? 'checked':''); ?>>
                                  <div class="state p-success">
                                    <label> <i class="fa fa-bell" aria-hidden="true"></i> Notification </label>
                                  </div>
                                </div>
                              </div>
                           
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>

        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="formModal" aria-hidden="true" style="display: none;">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="formModal">Add New Address</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>
              <div class="modal-body">
                <form action="" method="post" id="form_Address">
                            <div class="row">
                            <div class="form-group col-md-12 col-12">
                              <div class="form-group">
                              <label class="form-label">Icon input</label>
                              <div class="selectgroup selectgroup-pills">
                                <label class="selectgroup-item">
                                  <input type="radio" name="icon-input" value="1" class="selectgroup-input">
                                  <span class="selectgroup-button selectgroup-button-icon"><i class="fas fa-home"></i></span>
                                </label>
                                <label class="selectgroup-item">
                                  <input type="radio" name="icon-input" value="3" class="selectgroup-input">
                                  <span class="selectgroup-button selectgroup-button-icon"><i class="fas fa-hotel"></i></span>
                                </label>
                                <label class="selectgroup-item">
                                  <input type="radio" name="icon-input" value="4" class="selectgroup-input">
                                  <span class="selectgroup-button selectgroup-button-icon"><i class="fas fa-map-marked-alt"></i></span>
                                </label>
                              </div>
                            </div>
                                <label>Address</label>
                                <input type="text" class="form-control" value="<?php echo e(Auth::user()->name); ?>">
                                <div class="invalid-feedback">
                                  Please fill in the name
                                </div>
                              </div>
                              <div class="form-group col-md-12 col-12">
                                <label>Street Address</label>
                                <input type="text" class="form-control" value="<?php echo e(Auth::user()->father_name); ?>">
                                <div class="invalid-feedback">
                                  Please fill in the father's name
                                </div>
                              </div>
                              <div class="form-group col-md-6 col-12">
                                <label>City</label>
                                <select class="form-control">
                                  <option>Please Select</option>
                                </select>
                                <div class="invalid-feedback">
                                  Please fill in the mother's name
                                </div>
                              </div>
                              <div class="form-group col-md-6 col-12">
                                <label>State</label>
                                <select class="form-control">
                                  <option>Please Select</option>
                                </select>
                                <div class="invalid-feedback">
                                  Please fill in the birthdate name
                                </div>
                              </div>
                              <div class="form-group col-md-6 col-12">
                                <label>Postal / Zip Code</label>
                                <input type="text" class="form-control" value="<?php echo e(Auth::user()->mother_name); ?>">
                                <div class="invalid-feedback">
                                  Please fill in the mother's name
                                </div>
                              </div>
                              <div class="form-group col-md-6 col-12">
                                <label>Country</label>
                                <select class="form-control">
                                  <option>Please Select</option>
                                </select>
                                <div class="invalid-feedback">
                                  Please fill in the birthdate name
                                </div>
                              </div>
                            </div>
                  <button class="btn btn-primary" type="button" onclick="SubmitForm('form_Address');"> <i class="fas fa-save"></i> Save Changes</button>
                  <button type="button" class="btn btn-danger" data-dismiss="modal" aria-label="Close">
                  <i class="fas fa-times"> </i> Cancel</button>
                </form>
              </div>
            </div>
          </div>
        </div>

        <div class="modal fade" id="educationModal" tabindex="-1" role="dialog" aria-labelledby="formModal" aria-hidden="true" style="display: none;">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="formModal">Add New Education Info</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>
              <div class="modal-body">
                <form action="" method="post" id="form_Education">
                            <div class="row">
                            <div class="form-group col-md-12 col-12">
                              <div class="form-group">
                              <label class="form-label">Icon input</label>
                              <div class="selectgroup selectgroup-pills">
                                <label class="selectgroup-item">
                                  <input type="radio" name="icon-input" value="1" class="selectgroup-input">
                                  <span class="selectgroup-button selectgroup-button-icon"><i class="fas fa-home"></i></span>
                                </label>
                                <label class="selectgroup-item">
                                  <input type="radio" name="icon-input" value="3" class="selectgroup-input">
                                  <span class="selectgroup-button selectgroup-button-icon"><i class="fas fa-hotel"></i></span>
                                </label>
                                <label class="selectgroup-item">
                                  <input type="radio" name="icon-input" value="4" class="selectgroup-input">
                                  <span class="selectgroup-button selectgroup-button-icon"><i class="fas fa-map-marked-alt"></i></span>
                                </label>
                              </div>
                            </div>
                                <label>Address</label>
                                <input type="text" class="form-control" value="<?php echo e(Auth::user()->name); ?>">
                                <div class="invalid-feedback">
                                  Please fill in the name
                                </div>
                              </div>
                              <div class="form-group col-md-12 col-12">
                                <label>Street Address</label>
                                <input type="text" class="form-control" value="<?php echo e(Auth::user()->father_name); ?>">
                                <div class="invalid-feedback">
                                  Please fill in the father's name
                                </div>
                              </div>
                              <div class="form-group col-md-6 col-12">
                                <label>City</label>
                                <select class="form-control">
                                  <option>Please Select</option>
                                </select>
                                <div class="invalid-feedback">
                                  Please fill in the mother's name
                                </div>
                              </div>
                              <div class="form-group col-md-6 col-12">
                                <label>State</label>
                                <select class="form-control">
                                  <option>Please Select</option>
                                </select>
                                <div class="invalid-feedback">
                                  Please fill in the birthdate name
                                </div>
                              </div>
                              <div class="form-group col-md-6 col-12">
                                <label>Postal / Zip Code</label>
                                <input type="text" class="form-control" value="<?php echo e(Auth::user()->mother_name); ?>">
                                <div class="invalid-feedback">
                                  Please fill in the mother's name
                                </div>
                              </div>
                              <div class="form-group col-md-6 col-12">
                                <label>Country</label>
                                <select class="form-control">
                                  <option>Please Select</option>
                                </select>
                                <div class="invalid-feedback">
                                  Please fill in the birthdate name
                                </div>
                              </div>
                            </div>
                  <button class="btn btn-primary" type="button" onclick="SubmitForm('form_Education');"> <i class="fas fa-save"></i> Save Changes</button>
                  <button type="button" class="btn btn-danger" data-dismiss="modal" aria-label="Close">
                  <i class="fas fa-times"> </i> Cancel</button>
                </form>
              </div>
            </div>
          </div>
        </div>

        <div class="modal fade" id="experienceModal" tabindex="-1" role="dialog" aria-labelledby="formModal" aria-hidden="true" style="display: none;">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="formModal">Add New Experience Info</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>
              <div class="modal-body">
                <form action="" method="post" id="form">
                            <div class="row">
                            <div class="form-group col-md-12 col-12">
                              <div class="form-group">
                              <label class="form-label">Icon input</label>
                              <div class="selectgroup selectgroup-pills">
                                <label class="selectgroup-item">
                                  <input type="radio" name="icon-input" value="1" class="selectgroup-input">
                                  <span class="selectgroup-button selectgroup-button-icon"><i class="fas fa-home"></i></span>
                                </label>
                                <label class="selectgroup-item">
                                  <input type="radio" name="icon-input" value="3" class="selectgroup-input">
                                  <span class="selectgroup-button selectgroup-button-icon"><i class="fas fa-hotel"></i></span>
                                </label>
                                <label class="selectgroup-item">
                                  <input type="radio" name="icon-input" value="4" class="selectgroup-input">
                                  <span class="selectgroup-button selectgroup-button-icon"><i class="fas fa-map-marked-alt"></i></span>
                                </label>
                              </div>
                            </div>
                                <label>Address</label>
                                <input type="text" class="form-control" value="<?php echo e(Auth::user()->name); ?>">
                                <div class="invalid-feedback">
                                  Please fill in the name
                                </div>
                              </div>
                              <div class="form-group col-md-12 col-12">
                                <label>Street Address</label>
                                <input type="text" class="form-control" value="<?php echo e(Auth::user()->father_name); ?>">
                                <div class="invalid-feedback">
                                  Please fill in the father's name
                                </div>
                              </div>
                              <div class="form-group col-md-6 col-12">
                                <label>City</label>
                                <select class="form-control">
                                  <option>Please Select</option>
                                </select>
                                <div class="invalid-feedback">
                                  Please fill in the mother's name
                                </div>
                              </div>
                              <div class="form-group col-md-6 col-12">
                                <label>State</label>
                                <select class="form-control">
                                  <option>Please Select</option>
                                </select>
                                <div class="invalid-feedback">
                                  Please fill in the birthdate name
                                </div>
                              </div>
                              <div class="form-group col-md-6 col-12">
                                <label>Postal / Zip Code</label>
                                <input type="text" class="form-control" value="<?php echo e(Auth::user()->mother_name); ?>">
                                <div class="invalid-feedback">
                                  Please fill in the mother's name
                                </div>
                              </div>
                              <div class="form-group col-md-6 col-12">
                                <label>Country</label>
                                <select class="form-control">
                                  <option>Please Select</option>
                                </select>
                                <div class="invalid-feedback">
                                  Please fill in the birthdate name
                                </div>
                              </div>
                            </div>
                  <button class="btn btn-primary" type="button" onclick="SubmitForm('form');"> <i class="fas fa-save"></i> Save Changes</button>
                  <button type="button" class="btn btn-danger" data-dismiss="modal" aria-label="Close">
                  <i class="fas fa-times"> </i> Cancel</button>
                </form>
              </div>
            </div>
          </div>
        </div>

<script type="text/javascript">

$("#ShowProfileEveryone").on("change", function() {
  var show_p = $("#"+this.id).is(":checked");
  $.ajax({
       type:"POST",
       url:"<?php echo e(url('Change-show-profile')); ?>",
       data:{"_token": "<?php echo e(csrf_token()); ?>", "id":show_p }, 
       success:function(result)
       {  

       }
    });
});



$("#ActivitiesLog").on("change", function() {
  var show_p = $("#"+this.id).is(":checked");
  $.ajax({
       type:"POST",
       url:"<?php echo e(url('Change-Activities-Log')); ?>",
       data:{"_token": "<?php echo e(csrf_token()); ?>", "id":show_p }, 
       success:function(result)
       {    
       }
    });
});

$("#two_step_verification").on("change", function() {
    var id = $('#'+this.id).is(":checked");
  $.ajax({
       type:"POST",
       url:"<?php echo e(url('/change-type-tsv')); ?>",
       data:{"_token": "<?php echo e(csrf_token()); ?>", "id": id}, 
       success:function(result)
       {     
       }
    });
});


$("#Notification").on("change", function() {
  var show_p = $("#"+this.id).is(":checked");
  $.ajax({
       type:"POST",
       url:"<?php echo e(url('Change-Notification-on')); ?>",
       data:{"_token": "<?php echo e(csrf_token()); ?>", "id":show_p }, 
       success:function(result)
       {     
       }
    });
});
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\wamp64\www\laravel9\resources\views/Users/profile-page.blade.php ENDPATH**/ ?>