
<?php $__env->startSection('content'); ?>
<!--Breadcrumb start-->
<div class="ed_pagetitle">
<div class="ed_img_overlay"></div>
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12">
				<div class="page_title">
					<h2>Our services</h2>
				</div>
			</div>
			<div class="col-lg-12 col-md-12 col-sm-12">
				<ul class="breadcrumb">
					<li><a href="<?php echo e(url('/')); ?>">home</a></li>
					<li>//</li>
					<li><a href="<?php echo e(url('service')); ?>">Our services</a></li>
				</ul>
			</div>
		</div>
	</div>
</div>
<!--Breadcrumb end-->
<!-- Services start -->
<div class="ed_transprentbg ed_toppadder90 ed_bottompadder60">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="ed_heading_top ed_bottompadder50">
					<h3>our best services</h3>
				</div>
			</div>
			<div class="ed_mostrecomeded_course_slider">
				<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
					<div class="ed_mostrecomeded_course style_2">
						<div class="ed_item_img">
							<img src="<?php if(isset($data['Our Best Services_Ground Transport'])): ?><?php echo e(url('uplode/BannerAndLogo/'.$data['Our Best Services_Ground Transport'])); ?> <?php endif; ?>" alt="item1" class="img-responsive">
						</div>
						<div class="ed_item_description ed_most_recomended_data">
							<h4><a href="#"><?php if(isset($data['OUR_BEST_SERVICES-1_T'])): ?><?php echo e($data['OUR_BEST_SERVICES-1_T']); ?> <?php endif; ?></a></h4>
							<p><?php if(isset($data['OUR_BEST_SERVICES-1_C'])): ?> <?php echo $data['OUR_BEST_SERVICES-1_C']; ?> <?php endif; ?></p>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
					<div class="ed_mostrecomeded_course style_2">
						<div class="ed_item_img">
							<img  src="<?php if(isset($data['Our Best Services_Warehousing'])): ?><?php echo e(url('uplode/BannerAndLogo/'.$data['Our Best Services_Warehousing'])); ?> <?php endif; ?>" alt="item1" class="img-responsive">
						</div>
						<div class="ed_item_description ed_most_recomended_data">
							<h4><a href="#"><?php if(isset($data['OUR_BEST_SERVICES-2_T'])): ?><?php echo e($data['OUR_BEST_SERVICES-2_T']); ?> <?php endif; ?></a></h4>
							<p><?php if(isset($data['OUR_BEST_SERVICES-2_C'])): ?> <?php echo $data['OUR_BEST_SERVICES-2_C']; ?> <?php endif; ?></p>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
					<div class="ed_mostrecomeded_course style_2">
						<div class="ed_item_img">
							<img  src="<?php if(isset($data['Our Best Services_Packaging & Storage'])): ?><?php echo e(url('uplode/BannerAndLogo/'.$data['Our Best Services_Packaging & Storage'])); ?> <?php endif; ?>" alt="item1" class="img-responsive">
						</div>
						<div class="ed_item_description ed_most_recomended_data">
							<h4><a href="#"><?php if(isset($data['OUR_BEST_SERVICES-3_T'])): ?><?php echo e($data['OUR_BEST_SERVICES-3_T']); ?> <?php endif; ?></a></h4>
							<p><?php if(isset($data['OUR_BEST_SERVICES-3_C'])): ?> <?php echo $data['OUR_BEST_SERVICES-3_C']; ?> <?php endif; ?></p>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
					<div class="ed_mostrecomeded_course style_2">
						<div class="ed_item_img">
							<img src="<?php if(isset($data['Our Best Services_Door-To-Door Delivery'])): ?><?php echo e(url('uplode/BannerAndLogo/'.$data['Our Best Services_Door-To-Door Delivery'])); ?> <?php endif; ?>" alt="item1" class="img-responsive">
						</div>
						<div class="ed_item_description ed_most_recomended_data">
							<h4><a href="#"><?php if(isset($data['OUR_BEST_SERVICES-4_T'])): ?><?php echo e($data['OUR_BEST_SERVICES-4_T']); ?> <?php endif; ?></a></h4>
							<p><?php if(isset($data['OUR_BEST_SERVICES-4_C'])): ?> <?php echo $data['OUR_BEST_SERVICES-4_C']; ?> <?php endif; ?></p>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
					<div class="ed_mostrecomeded_course style_2">
						<div class="ed_item_img">
							<img  src="<?php if(isset($data['Our Best Services_Cargo'])): ?><?php echo e(url('uplode/BannerAndLogo/'.$data['Our Best Services_Cargo'])); ?> <?php endif; ?>" alt="item1" class="img-responsive">
						</div>
						<div class="ed_item_description ed_most_recomended_data">
							<h4><a href="#"><?php if(isset($data['OUR_BEST_SERVICES-5_T'])): ?><?php echo e($data['OUR_BEST_SERVICES-5_T']); ?> <?php endif; ?></a></h4>
							<p><?php if(isset($data['OUR_BEST_SERVICES-5_C'])): ?> <?php echo $data['OUR_BEST_SERVICES-5_C']; ?> <?php endif; ?></p>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
					<div class="ed_mostrecomeded_course style_2">
						<div class="ed_item_img">
							<img  src="<?php if(isset($data['Our Best Services_Logistic'])): ?><?php echo e(url('uplode/BannerAndLogo/'.$data['Our Best Services_Logistic'])); ?> <?php endif; ?>" alt="item1" class="img-responsive">
						</div>
						<div class="ed_item_description ed_most_recomended_data">
							<h4><a href="#"><?php if(isset($data['OUR_BEST_SERVICES-6_T'])): ?><?php echo e($data['OUR_BEST_SERVICES-6_T']); ?> <?php endif; ?></a></h4>
							<p><?php if(isset($data['OUR_BEST_SERVICES-6_C'])): ?> <?php echo $data['OUR_BEST_SERVICES-6_C']; ?> <?php endif; ?></p>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
					<div class="ed_mostrecomeded_course style_2">
						<div class="ed_item_img">
							<img src="<?php if(isset($data['Our Best Services_Air Shipping'])): ?><?php echo e(url('uplode/BannerAndLogo/'.$data['Our Best Services_Air Shipping'])); ?> <?php endif; ?>" alt="item1" class="img-responsive">
						</div>
						<div class="ed_item_description ed_most_recomended_data">
							<h4><a href="#"><?php if(isset($data['OUR_BEST_SERVICES-7_T'])): ?><?php echo e($data['OUR_BEST_SERVICES-7_T']); ?> <?php endif; ?></a></h4>
							<p><?php if(isset($data['OUR_BEST_SERVICES-7_C'])): ?> <?php echo $data['OUR_BEST_SERVICES-7_C']; ?> <?php endif; ?></p>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
					<div class="ed_mostrecomeded_course style_2">
						<div class="ed_item_img">
							<img src="<?php if(isset($data['Our Best Services_Ship Delivery'])): ?><?php echo e(url('uplode/BannerAndLogo/'.$data['Our Best Services_Ship Delivery'])); ?> <?php endif; ?>" alt="item1" class="img-responsive">
						</div>
						<div class="ed_item_description ed_most_recomended_data">
							<h4><a href="#"><?php if(isset($data['OUR_BEST_SERVICES-8_T'])): ?><?php echo e($data['OUR_BEST_SERVICES-8_T']); ?> <?php endif; ?></a></h4>
							<p><?php if(isset($data['OUR_BEST_SERVICES-8_C'])): ?> <?php echo $data['OUR_BEST_SERVICES-8_C']; ?> <?php endif; ?></p>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
					<div class="ed_mostrecomeded_course style_2">
						<div class="ed_item_img">
							<img src="<?php if(isset($data['Our Best Services_Passenger Transport'])): ?><?php echo e(url('uplode/BannerAndLogo/'.$data['Our Best Services_Passenger Transport'])); ?> <?php endif; ?>" alt="item1" class="img-responsive">
						</div>
						<div class="ed_item_description ed_most_recomended_data">
							<h4><a href="#"><?php if(isset($data['OUR_BEST_SERVICES-9_T'])): ?><?php echo e($data['OUR_BEST_SERVICES-9_T']); ?> <?php endif; ?></a></h4>
							<p><?php if(isset($data['OUR_BEST_SERVICES-9_C'])): ?> <?php echo $data['OUR_BEST_SERVICES-9_C']; ?> <?php endif; ?></p>
						</div>
					</div>
				</div>
			</div>
		</div>
    </div><!-- /.container -->
</div>
<!-- Services end -->
<!-- Services start -->
<div class="ed_transprentbg ed_toppadder90 ed_bottompadder60">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="ed_heading_top ed_bottompadder50">
					<h3><?php if(isset($data['OUR_BEST_SERVICES-10_T'])): ?><?php echo e($data['OUR_BEST_SERVICES-10_T']); ?> <?php endif; ?></h3>
				</div>
						<div class="ed_item_description ed_most_recomended_data">
							<p><?php if(isset($data['OUR_BEST_SERVICES-10_C'])): ?><?php echo $data['OUR_BEST_SERVICES-10_C']; ?> <?php endif; ?></p>
						</div>
			</div>
		</div>
    </div><!-- /.container -->
</div>
<!-- Services end -->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('front.layout', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\cronex\resources\views/front/services.blade.php ENDPATH**/ ?>