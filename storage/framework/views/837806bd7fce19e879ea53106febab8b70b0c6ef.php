
<?php $__env->startSection('content'); ?>
  
      <!-- Main Content -->
      <div class="main-content">
        <section class="section">
          <ul class="breadcrumb breadcrumb-style ">
            <!-- <li class="breadcrumb-item">
              <h4 class="page-title m-b-0">Setting</h4>
            </li> -->
            <li class="breadcrumb-item">
              <a href="index.html">
                <i data-feather="home"></i></a>
            </li>
            <li class="breadcrumb-item">Setting</li>
            <li class="breadcrumb-item">Center Percentage</li>
            <li class="breadcrumb-item">View Percentage</li>
          </ul>
          <div class="section-body">
            

            <div class="row">
              <div class="col-12">
                <div class="card">
                  <div class="card-header">
                    <h4>Export Table</h4>
                  </div>
                  <div class="card-body">
                    <div class="table-responsive">
                      <table class="table table-striped table-hover" id="tableExport" style="width:100%;">
                        <thead>
                          <tr>
                            <th>ID</th>
                            <th>Role</th>
                            <th>Margin Value</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody>
                        <?php $sn=0; ?>
                          <?php if(isset($data)): ?>
                          <?php $__currentLoopData = $data['data']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                          <tr>
                            <td><?php echo e(++$sn); ?></td>
                            <td><?php echo e($value->role); ?></td>
                            <form action="<?php echo e(url('update-center-percentage')); ?>" method="post">
                            <?php echo csrf_field(); ?>
                            <input type="hidden" value="<?php echo e($value->id); ?>" name="id" />
                            <td><input type="text" id="marginvalue" value="<?php echo e($value->margin); ?>" name="marginvalue" class="form-control <?php $__errorArgs = ['marginvalue'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" placeholder="Margin Value" required="" autocomplete="off"></td>
                            <td>
                              <div class="buttons">
                                <button class="btn btn-success btn-lg"> Update</button>
                                <!-- <a href="" class="btn btn-success btn-sm"><i class="far fa-edit"></i></a> -->
                                </div>
                            </td>
                            </form>
                          </tr>
                          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                          <?php endif; ?>

                          
                          
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      
      </div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\wamp64\www\cronex\resources\views/Master/CenterPercentage/view-center-percentage.blade.php ENDPATH**/ ?>