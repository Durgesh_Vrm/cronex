
<?php $__env->startSection('content'); ?>
      <!-- Main Content -->
      <div class="main-content">
        <section class="section">
          <ul class="breadcrumb breadcrumb-style ">
            <li class="breadcrumb-item">
              <a href="<?php echo e(url('home')); ?>">
                <i data-feather="home"></i></a>
            </li>
            <li class="breadcrumb-item">Setting</li>
            <li class="breadcrumb-item">User Role & Permissions</li>
          </ul>
          <div class="section-body">
            <div class="row">
              <div class="col-12 col-sm-12 col-lg-12">
                <div class="card">
                          <div class="card-header">
                            <h4>User Role & Permissions</h4>
                          </div>
                          <div class="card-body">
                              <div class="row">
                              <table class="table table-sm">
                                <thead>
                                  <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Role</th>
                                    <th scope="col">View</th>
                                    <th scope="col">Create</th>
                                    <th scope="col">Update</th>
                                    <th scope="col">Delete</th>
                                    <th scope="col">Action</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <?php $sn=0; ?> 
                                  <?php $__currentLoopData = $data['all']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                  <tr>
                                    <th scope="row"><?php echo e(++$sn); ?></th>
                                    <th scope="row"><span  id="a_div<?php echo e($value->id); ?>"> <?php echo e($value->role_name); ?> </span>
                              
                                      <input  class="form-control form-control-sm col-sm-8"  style="display: none" type="text" name="role_name<?php echo e($value->id); ?>" id="role_name<?php echo e($value->id); ?>" value="<?php echo e($value->role_name); ?>" required> 
                                    </th>
                                    <td>
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" <?php if($value->role_name == 'Administrator' || $value->role_name == 'User'): ?> disabled <?php else: ?> onclick="change_view_role(<?php echo e($value->id); ?>)" <?php endif; ?> value="<?php echo e($value->view); ?>" id="View<?php echo e($value->id); ?>" <?php echo e($value->view == 'Yes' ? 'checked':''); ?>>
                                        <label class="custom-control-label" for="View<?php echo e($value->id); ?>"></label>
                                      </div>
                                    </td>
                                    <td>
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" <?php if($value->role_name == 'Administrator' || $value->role_name == 'User'): ?> disabled <?php else: ?> onclick="change_create_role(<?php echo e($value->id); ?>)" <?php endif; ?> value="<?php echo e($value->create); ?>" id="Create<?php echo e($value->id); ?>" <?php echo e($value->create == 'Yes' ? 'checked':''); ?>>
                                        <label class="custom-control-label" for="Create<?php echo e($value->id); ?>"></label>
                                      </div>
                                    </td>
                                    <td>
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" <?php if($value->role_name == 'Administrator' || $value->role_name == 'User'): ?> disabled <?php else: ?> onclick="change_update_role(<?php echo e($value->id); ?>)" <?php endif; ?> value="<?php echo e($value->update); ?>" id="Update<?php echo e($value->id); ?>" <?php echo e($value->update == 'Yes' ? 'checked':''); ?>>
                                        <label class="custom-control-label" for="Update<?php echo e($value->id); ?>"></label>
                                      </div>
                                    </td>
                                    <td>
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" <?php if($value->role_name == 'Administrator' || $value->role_name == 'User'): ?> disabled <?php else: ?> onclick="change_delete_role(<?php echo e($value->id); ?>)" <?php endif; ?> value="<?php echo e($value->delete); ?>" id="Delete<?php echo e($value->id); ?>" <?php echo e($value->delete == 'Yes' ? 'checked':''); ?>>
                                        <label class="custom-control-label" for="Delete<?php echo e($value->id); ?>"></label>
                                      </div>
                                    </td>
                                    <td>
                                        <button class="btn btn-primary btn-sm" type="button" id="bt<?php echo e($value->id); ?>"  <?php if($value->role_name == 'Administrator' || $value->role_name == 'User'): ?> disabled <?php else: ?>  onclick="EditRole('<?php echo e($value->id); ?>');"  <?php endif; ?>><i class="fas fa-edit"></i> Edit </button>
                                        <button class="btn btn-success btn-sm" style="display: none"  onclick="Save('<?php echo e($value->id); ?>');" id="btn<?php echo e($value->id); ?>" type="button"><i class="fas fa-save"></i>  Save </button>
                                        <button class="btn btn-danger btn-sm" style="display: none"  onclick="CHidde('<?php echo e($value->id); ?>');" id="btns<?php echo e($value->id); ?>" type="button"><i class="fas fa-times"></i>  Cancel </button>
                                    </td>
                                  </tr>
                                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </tbody>
                              </table>
                            </div>
                          </div>
                </div>
              </div>
            </div>

          </div>
        </section>
      </div>


<script type="text/javascript">
  //edit form confirmation
  function EditRole(id)
  {
    swal({
      title: "Are you sure you want to edit?",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
        
        $('#a_div'+id).hide();
        $('#role_name'+id).show("slow");        
        $('#bt'+id).hide();
        $('#btn'+id).show("slow");
        $('#btns'+id).show("slow");
      } 
    });
  }



  function CHidde(id) {
        $('#a_div'+id).show("slow");
        $('#role_name'+id).hide();        
        $('#bt'+id).show("slow");
        $('#btn'+id).hide();
        $('#btns'+id).hide();
  }



  function Save(id)
  {
      var nam = $('#role_name'+id).val();

        $.ajax({
         type:"POST",
         url:"<?php echo e(url('/change-user-role-name')); ?>",
         data:{"_token": "<?php echo e(csrf_token()); ?>", "id": id, "name": nam},
         success:function(result)
         { 
              location.reload();
         }
        });      
  }

  



  function change_view_role(id) {
    var valu = $('#View'+id).val();
      $.ajax({
         type:"POST",
         url:"<?php echo e(url('/change-user-role-view-p')); ?>",
         data:{"_token": "<?php echo e(csrf_token()); ?>", "id": id, "valu": valu},
         success:function(result)
         { 
              location.reload();
         }
      }); 
  }

  function change_create_role(id) {
    var valu = $('#Create'+id).val();
      $.ajax({
         type:"POST",
         url:"<?php echo e(url('/change-user-role-create-p')); ?>",
         data:{"_token": "<?php echo e(csrf_token()); ?>", "id": id, "valu": valu},
         success:function(result)
         { 
              location.reload();
         }
      }); 
  }

  function change_update_role(id) {
    var valu = $('#Update'+id).val();
      $.ajax({
         type:"POST",
         url:"<?php echo e(url('/change-user-role-update-p')); ?>",
         data:{"_token": "<?php echo e(csrf_token()); ?>", "id": id, "valu": valu},
         success:function(result)
         { 
              location.reload();
         }
      }); 
  }

  function change_delete_role(id) {
    var valu = $('#Delete'+id).val();
      $.ajax({
         type:"POST",
         url:"<?php echo e(url('/change-user-role-delete-p')); ?>",
         data:{"_token": "<?php echo e(csrf_token()); ?>", "id": id, "valu": valu},
         success:function(result)
         { 
              location.reload();
         }
      }); 
  }

  function show_message() {
    swal({
      title: "Are you sure you want to edit?",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
  }
   
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\wamp64\www\cronex\resources\views/rol/view-rols.blade.php ENDPATH**/ ?>