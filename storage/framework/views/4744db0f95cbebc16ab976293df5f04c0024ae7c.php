
<?php $__env->startSection('content'); ?>
  <style type="text/css">
select {
  /* for Firefox */
  -moz-appearance: none;
  /* for Chrome */
  -webkit-appearance: none;
}
/* For IE10 */
select::-ms-expand {
  display: none;
}
.no-scroll::-webkit-scrollbar {display:none;}
.no-scroll::-moz-scrollbar {display:none;}
.no-scroll::-o-scrollbar {display:none;}
.no-scroll::-google-ms-scrollbar {display:none;}
.no-scroll::-khtml-scrollbar {display:none;}
  </style>
      <!-- Main Content -->
      <div class="main-content">
        <section class="section">
          <ul class="breadcrumb breadcrumb-style ">
            <li class="breadcrumb-item">
              <h4 class="page-title m-b-0">Export Tables</h4>
            </li>
            <li class="breadcrumb-item">
              <a href="index.html">
                <i data-feather="home"></i></a>
            </li>
            <li class="breadcrumb-item">Tables</li>
            <li class="breadcrumb-item">Export Tables</li>
          </ul>
          <div class="section-body">
            <div class="row">
              <div class="col-12">
                <div class="card">
                  <div class="card-header">
                    <h4>Export Table</h4>
                  </div>
                  <div class="card-body">
                    <div class="table-responsive">
                      <table class="table table-striped table-hover" id="tableExport" style="width:100%;">
                        <thead>
                          <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Change Rol</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php $__currentLoopData = $data['users']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                          <tr>
                            <td><?php echo e($value->reg_number); ?></td>
                            <td><?php echo e($value->name); ?></td>
                            <td><?php echo e($value->email); ?></td>
                            <td>
                            <?php if($value->role != 'SUPPERADMINC60C3421A4D4B2152B430'): ?>
                                <select class="form-control-sm btn-info no-scroll" id="<?php echo e($value->reg_number); ?>" name="role_nmae<?php echo e($value->reg_number); ?>" onchange="change_role(this.id)">
                                  <?php $__currentLoopData = $data['role']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $values): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                  <option value="<?php echo e(Crypt::encrypt($values->role_id)); ?>" <?php echo e($value->role == $values->role_id? 'selected':''); ?>><?php echo e($values->role_name); ?></option>
                                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                  <option value="other">Other</option>
                                </select>
                              <?php endif; ?>
                            </td>
                          </tr>
                          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>

  <script type="text/javascript">
    
  function change_role(id) {
     var roleID = $('#'+id).val();
        swal({
          title: "Are you sure you want to change user role",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {
              $.ajax({
               type:"POST",
               url:"<?php echo e(url('/change-user-role')); ?>",
               data:{"_token": "<?php echo e(csrf_token()); ?>", "roleID": roleID, "id": id},
               success:function(result)
               {  
                  swal({
                        title: "Successfully change role",
                        icon: "success",
                        button: "OK",
                      }).then((willDelete) => { if (willDelete) { location.reload();  }});      
               }
            });
          } 
        });
  }
  </script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\wamp64\www\laravel9\resources\views/Users/role-user-info.blade.php ENDPATH**/ ?>