
<?php $__env->startSection('content'); ?>
      <!-- Main Content -->
      <div class="main-content">
        <section class="section">
          <ul class="breadcrumb breadcrumb-style ">
            <li class="breadcrumb-item">
              <h4 class="page-title m-b-0">Read</h4>
            </li>
            <li class="breadcrumb-item">
              <a href="<?php echo e(url('dashboard')); ?>">
                <i data-feather="home"></i></a>
            </li>
            <li class="breadcrumb-item">Email</li>
            <li class="breadcrumb-item">Read</li>
          </ul>
          <div class="section-body">
            <div class="row">
              <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                <div class="card">
                  <div class="body">
                    <div id="mail-nav">
                     <a href="<?php echo e(url('email-compose')); ?>"> <button type="button" class="btn btn-danger waves-effect btn-compose m-b-15">COMPOSE</button></a>
                      <ul class="" id="mail-folders">
                        <li>
                          <a href="<?php echo e(url('mail-inbox')); ?>" title="Inbox">Inbox (10)
                          </a>
                        </li>
                        <li>
                          <a href="<?php echo e(url('mail-read')); ?>" title="Sent">Sent</a>
                        </li>
                      </ul>
                      <h5 class="b-b p-10 text-strong">Labels</h5>
                      <ul class="" id="mail-labels">
                        <li>
                          <a href="javascript:;">
                            <i class="material-icons col-red">local_offer</i>Family</a>
                        </li>
                        <li>
                          <a href="javascript:;">
                            <i class="material-icons col-blue">local_offer</i>Work</a>
                        </li>
                        <li>
                          <a href="javascript:;">
                            <i class="material-icons col-orange">local_offer</i>Shop</a>
                        </li>
                        <li>
                          <a href="javascript:;">
                            <i class="material-icons col-cyan">local_offer</i>Themeforest</a>
                        </li>
                        <li>
                          <a href="javascript:;">
                            <i class="material-icons col-blue-grey">local_offer</i>Google</a>
                        </li>
                      </ul>
                      <h5 class="b-b p-10 text-strong">Online</h5>
                      <ul class="online-user" id="online-offline">
                        <li><a href="javascript:;"> <img alt="image" src="assets/img/users/user-2.png"
                              class="rounded-circle" width="35" data-toggle="tooltip" title="Sachin Pandit">
                            Sachin Pandit
                          </a></li>
                        <li><a href="javascript:;"> <img alt="image" src="assets/img/users/user-1.png"
                              class="rounded-circle" width="35" data-toggle="tooltip" title="Sarah Smith">
                            Sarah Smith
                          </a></li>
                        <li><a href="javascript:;"> <img alt="image" src="assets/img/users/user-3.png"
                              class="rounded-circle" width="35" data-toggle="tooltip" title="Airi Satou">
                            Airi Satou
                          </a></li>
                        <li><a href="javascript:;"> <img alt="image" src="assets/img/users/user-4.png"
                              class="rounded-circle" width="35" data-toggle="tooltip" title="Angelica Ramos ">
                            Angelica Ramos
                          </a></li>
                        <li><a href="javascript:;"> <img alt="image" src="assets/img/users/user-5.png"
                              class="rounded-circle" width="35" data-toggle="tooltip" title="Cara Stevens">
                            Cara Stevens
                          </a></li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                <div class="card">
                  <div class="boxs mail_listing">
                    <div class="inbox-body no-pad">
                      <section class="mail-list">
                        <div class="mail-sender">
                          <div class="mail-heading">
                            <h4 class="vew-mail-header">
                              <b>Hi Dear, How are you?</b>
                            </h4>
                          </div>
                          <hr>
                          <div class="media">
                            <a href="#" class="table-img m-r-15">
                              <img alt="image" src="assets/img/users/user-2.png" class="rounded-circle" width="35"
                                data-toggle="tooltip" title="Sachin Pandit">
                            </a>
                            <div class="media-body">
                              <span class="date pull-right">4:15AM 04 April 2017</span>
                              <h5 class="col-black">Sarah Smith</h5>
                              <small class="text-muted">From: sarah@example.com</small>
                            </div>
                          </div>
                        </div>
                        <div class="view-mail p-t-20">
                          <p>Donec ultrices faucibus rutrum. Phasellus sodales vulputate urna, vel
                            accumsan augue
                            egestas ac. Donec vitae leo at sem lobortis porttitor eu consequat risus.
                            Mauris
                            sed congue orci. Donec ultrices faucibus rutrum. Phasellus sodales
                            vulputate urna,
                            vel accumsan augue egestas ac. Donec vitae leo at sem lobortis porttitor eu
                            consequat
                            risus. Mauris sed congue orci. Donec ultrices faucibus rutrum. Phasellus
                            sodales
                            vulputate urna, vel accumsan augue egestas ac. Donec vitae leo at sem
                            lobortis porttitor
                            eu consequat risus. Mauris sed congue orci.</p>
                          <p>
                            Porttitor eu consequat risus.
                            <a href="#">Mauris sed congue orci. Donec ultrices faucibus rutrum</a>.
                            Phasellus sodales vulputate
                            urna, vel accumsan augue egestas ac. Donec vitae leo at sem lobortis
                            porttitor eu
                            consequat risus. Mauris sed congue orci. Donec ultrices faucibus rutrum.
                            Phasellus
                            sodales vulputate urna, vel accumsan augue egestas ac. Donec vitae leo at
                            sem lobortis
                            porttitor eu consequat risus. Mauris sed congue orci.
                          </p>
                          <p>
                            Sodales
                            <a href="#">vulputate urna, vel accumsan augue egestas ac</a>. Donec vitae
                            leo at sem lobortis
                            porttitor eu consequat risus. Mauris sed congue orci. Donec ultrices
                            faucibus rutrum.
                            Phasellus sodales vulputate urna, vel accumsan augue egestas ac. Donec
                            vitae leo
                            at sem lobortis porttitor eu consequat risus. Mauris sed congue orci.
                          </p>
                        </div>
                        <div class="attachment-mail">
                          <p>
                            <span>
                              <i class="fa fa-paperclip"></i> 3 attachments — </span>
                            <a href="#">Download all attachments</a> |
                            <a href="#">View all images</a>
                          </p>
                          <div class="row">
                            <div class="col-md-2">
                              <a href="#">
                                <img class="img-thumbnail img-responsive" alt="attachment"
                                  src="assets/img/users/user-1.png">
                              </a>
                              <a class="name" href="#"> IMG_001.png
                                <span>20KB</span>
                              </a>
                            </div>
                            <div class="col-md-2">
                              <a href="#">
                                <img class="img-thumbnail img-responsive" alt="attachment"
                                  src="assets/img/users/user-3.png">
                              </a>
                              <a class="name" href="#"> IMG_002.png
                                <span>22KB</span>
                              </a>
                            </div>
                            <div class="col-md-2">
                              <a href="#">
                                <img class="img-thumbnail img-responsive" alt="attachment"
                                  src="assets/img/users/user-4.png">
                              </a>
                              <a class="name" href="#"> IMG_003.png
                                <span>28KB</span>
                              </a>
                            </div>
                          </div>
                        </div>
                        <div class="replyBox m-t-20">
                          <p class="p-b-20">click here to
                            <a href="#">Reply</a> or
                            <a href="#">Forward</a>
                          </p>
                        </div>
                      </section>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\wamp64\www\laravel9\resources\views/Email/email-read.blade.php ENDPATH**/ ?>