
<?php $__env->startSection('content'); ?>
<!--Breadcrumb start-->
<div class="ed_pagetitle">
<div class="ed_img_overlay"></div>
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12">
				<div class="page_title">
					<h2>Hello, how can we help?</h2>
				</div>
			</div>
			<div class="col-lg-12 col-md-12 col-sm-12">
				<ul class="breadcrumb">
					<li><a href="<?php echo e(url('/')); ?>">home</a></li>
					<li>//</li>
					<li><a href="<?php echo e(url('faq')); ?>">FAQ</a></li>
				</ul>
			</div>
		</div>
	</div>
</div>
<!--Breadcrumb end-->
<!--FAQ content start-->
<div class="ed_transprentbg ed_toppadder90 ed_bottompadder60">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12">
				<div class="ed_faq_section">
					<ul>
						<li>
							<?php if(isset($data['FAQ'])): ?>
							<?php $__currentLoopData = $data['FAQ']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<?php if($key % 2 == 0): ?>
							<div class="ed_faq_que">
								<h3><?php echo e($value['FAQ_T']); ?></h3>
								<p><?php echo $value['FAQ_T']; ?></p>
							</div>
							<?php endif; ?>
							<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
							<?php endif; ?>

						</li>
						<li>
							<?php if(isset($data['FAQ'])): ?>
							<?php $__currentLoopData = $data['FAQ']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<?php if($key % 2 == 1): ?>
							<div class="ed_faq_que">
								<h3><?php echo e($value['FAQ_T']); ?></h3>
								<p><?php echo $value['FAQ_T']; ?></p>
							</div>
							<?php endif; ?>
							<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
							<?php endif; ?>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>  
</div>
<!--FAQ content end-->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('front.layout', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\wamp64\www\cronex\resources\views/front/faq.blade.php ENDPATH**/ ?>