<!DOCTYPE html>
<html lang="en">
<!-- Begin Head -->
<head>
<meta charset="utf-8" />
<title><?php echo e(config('APP_NAME')); ?></title>
<meta content="width=device-width, initial-scale=1.0" name="viewport" />
<meta name="description"  content="Transporting"/>
<meta name="keywords" content="Transporting, html template" />
<meta name="author"  content="Kamleshyadav"/>
<meta name="MobileOptimized" content="320" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<!--srart theme style -->
<link href="front/css/main.css" rel="stylesheet" type="text/css"/>
<!-- end theme style -->
<!-- favicon links -->
<link rel="shortcut icon" type="image/png" href="<?php echo e(asset('uplode/config/favicon/'.config('FAVICON'))); ?>" />
<style type="text/css">
.ed_pagetitle {
    float: left;
    width: 100%;
    background-color: #272727;
    position: relative;
    background-attachment: fixed;
    background-size: cover;
    color: #fff;
    padding: 100px 0px;
    background-image: url(front/images/content/track.png);
    background-position: 100% 100%;
    background-repeat: no-repeat;
}
</style>
</head>
<body>
<!--Page main section start-->
<div id="pro_wrapper">
<!--Header start-->
<header id="ed_header_wrapper">
	<div class="ed_header_top">
		<div class="container">
			<div class="row">
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
					<div class="pro_call">
						<p>call now <i class="fa fa-volume-control-phone" aria-hidden="true"></i> 1800-2202-0909</p>
					</div>
				</div>
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
					<div class="ed_info_wrapper">
						<ul>
							<li><a href="#" data-toggle="tooltip" data-placement="bottom" title="Facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
							<li><a href="#" data-toggle="tooltip" data-placement="bottom" title="Google+"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
							<li><a href="#" data-toggle="tooltip" data-placement="bottom" title="Twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
							<li><a href="#" data-toggle="tooltip" data-placement="bottom" title="Linkedin"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
							<li><a href="#" data-toggle="tooltip" data-placement="bottom" title="Whatsapp"><i class="fa fa-whatsapp" aria-hidden="true"></i></a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="ed_header_bottom">
		<div class="container">
			<div class="row">
				<div class="col-lg-3 col-md-3 col-sm-3">
					<div class="pro_logo"> <a href="<?php echo e(url('/')); ?>"><img style="height: 55px;" src="<?php echo e(asset('uplode/config/logo/'.config('LOGO'))); ?>" alt="logo" /></a> </div>
				</div>
				<div class="col-lg-9 col-md-9 col-sm-9">
					<div class="pro_menu_toggle navbar-toggle" data-toggle="collapse" data-target="#ed_menu">Menu <i class="fa fa-bars"></i>
					</div>
					<div class="pro_menu">
						<ul class="collapse navbar-collapse" id="ed_menu">
							<li><a href="<?php echo e(url('/')); ?>">Home</a></li>
							<li><a href="<?php echo e(url('about-us')); ?>">about us</a></li>
							<li><a href="<?php echo e(url('service')); ?>">Services</a></li>
							<li><a href="<?php echo e(url('faq')); ?>">FAQ</a></li>
							<li><a href="<?php echo e(url('contact-us')); ?>">Contact</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
    </div>
</header>

<?php echo $__env->yieldContent('content'); ?>

<!--Newsletter Section six start-->
<div class="ed_newsletter_section ed_toppadder90 ed_bottompadder90">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12">
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="ed_newsletter_section_heading">
							<h4>Let us inform you about everything important directly.</h4>
						</div>
					</div>
					<div class="col-lg-6 col-md-6 col-sm-8 col-xs-12 col-lg-offset-3 col-md-offset-3 col-sm-offset-2 col-xs-offset-0">
						<div class="row">
							<div class="ed_newsletter_section_form">
								<form class="form">
									<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
										<input class="form-control" type="text" placeholder="Newsletter Email" />
									</div>
									<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
										<button class="btn ed_btn ed_green">confirm</button>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
        </div>
	</div>
</div>
<!--Newsletter Section six end-->
<!--Footer Top section start-->
<div class="ed_footer_wrapper ed_toppadder60 ed_bottompadder30">
	<div class="ed_footer_top">
		<div class="container">
			<div class="row">
				<div class="col-lg-4 col-md-4 col-sm-12">
					<div class="widget text-widget">
						<p><a href="<?php echo e(url('/')); ?>"><img style="height: 50px;" src="<?php echo e(asset('uplode/config/logo/'.config('FOOTER_LOGO'))); ?>" alt="Footer Logo" /></a></p>
						<p>Transportation is the center of the world! It is the glue of our daily lives. When it goes well, we don't see it. When it goes wrong, it negatively colors our day, makes us feel angry and impotent.</p>
					</div>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-12">
					<div class="widget text-widget">
						<h4 class="widget-title">find us</h4>
						<p><i class="fa fa-envelope" aria-hidden="true"></i><a href="#">info@cronex.com</a><br> <a href="#">public@cronex.co.in</a></p>
						<p><i class="fa fa-phone" aria-hidden="true"></i> 1800-0000-0000</p>
					</div>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-12">
					<div class="widget text-widget">
						<h4 class="widget-title">Address</h4>
						<p><i class="fa fa-home" aria-hidden="true"></i> 141 N Prospect Ave, Bergenfield, NJ, 07621</p>
						<div class="ed_sociallink">
						<ul>
							<li><a href="#" data-toggle="tooltip" data-placement="bottom" title="Facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
							<li><a href="#" data-toggle="tooltip" data-placement="bottom" title="Google+"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
							<li><a href="#" data-toggle="tooltip" data-placement="bottom" title="Twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
							<li><a href="#" data-toggle="tooltip" data-placement="bottom" title="Linkedin"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
							<li><a href="#" data-toggle="tooltip" data-placement="bottom" title="Whatsapp"><i class="fa fa-whatsapp" aria-hidden="true"></i></a></li>
						</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!--Footer Top section end-->
<!--Footer Bottom section start-->
<div class="ed_footer_bottom">
	<div class="container">
		<div class="col-lg-12 col-md-12 col-sm-12">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12">
				<div class="ed_copy_right">
					<p>&copy; Copyright 2018, All Rights Reserved, <a href="#">cronex</a> | 
						<a href="<?php echo e(url('private_policy')); ?>">Private Policy</a> | <a href="<?php echo e(url('t&c')); ?>">Terms</a></p>
				</div>
			</div>
		</div>
		</div>
	</div>
</div>
<!--Footer Bottom section end-->
</div>
<!--Page main section end-->
<!--main js file start--> 
<script type="text/javascript" src="front/js/jquery-1.12.2.js"></script> 
<script type="text/javascript" src="front/js/bootstrap.js"></script> 
<script type="text/javascript" src="front/js/modernizr.js"></script> 
<script type="text/javascript" src="front/js/owl.carousel.js"></script>
<script type="text/javascript" src="front/js/smooth-scroll.js"></script> 
<script type="text/javascript" src="front/js/jquery.magnific-popup.js"></script> 
<script type="text/javascript" src="front/js/plugins/revel/jquery.themepunch.tools.min.js"></script>
<script type="text/javascript" src="front/js/plugins/revel/jquery.themepunch.revolution.min.js"></script>
<script type="text/javascript" src="front/js/plugins/revel/revolution.extension.actions.min.js"></script>
<script type="text/javascript" src="front/js/plugins/revel/revolution.extension.carousel.min.js"></script>
<script type="text/javascript" src="front/js/plugins/revel/revolution.extension.kenburn.min.js"></script>
<script type="text/javascript" src="front/js/plugins/revel/revolution.extension.layeranimation.min.js"></script>
<script type="text/javascript" src="front/js/plugins/revel/revolution.extension.navigation.min.js"></script>
<script type="text/javascript" src="front/js/plugins/revel/revolution.extension.parallax.min.js"></script>
<script type="text/javascript" src="front/js/plugins/revel/revolution.extension.slideanims.min.js"></script>
<script type="text/javascript" src="front/js/plugins/revel/revolution.extension.video.min.js"></script>
<script type="text/javascript" src="front/js/plugins/countto/jquery.countTo.js"></script>
<script type="text/javascript" src="front/js/plugins/countto/jquery.appear.js"></script>
<script type="text/javascript" src="front/js/custom.js"></script> 
<!--main js file end-->
</body>

</html><?php /**PATH C:\xampp\htdocs\cronex\resources\views/front/layout.blade.php ENDPATH**/ ?>