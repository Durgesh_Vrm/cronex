
<?php $__env->startSection('content'); ?>

<!--Breadcrumb start-->
<div class="ed_pagetitle">
<div class="ed_img_overlay"></div>
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12">
				<div class="page_title">
					<h2>Contact Us</h2>
				</div>
			</div>
			<div class="col-lg-12 col-md-12 col-sm-12">
				<ul class="breadcrumb">
					<li><a href="<?php echo e(url('/')); ?>">home</a></li>
					<li>//</li>
					<li><a href="<?php echo e(url('contact-us')); ?>">Contact us</a></li>
				</ul>
			</div>
		</div>
	</div>
</div>
<!--Breadcrumb end-->
<!--Section fourteen Contact form start-->
<div class="ed_transprentbg ed_toppadder90 ed_bottompadder90">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="ed_heading_top ed_bottompadder50">
					<h3>contact info</h3>
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6">
				<div class="ed_contact_form">
					<div class="form-group">
						<input type="text" id="uname" class="form-control"  placeholder="Your Name">
					</div>
					<div class="form-group">
						<input type="email" id="umail" class="form-control"  placeholder="Your Email">
					</div>
					<div class="form-group">
						<input type="text" id="sub" class="form-control"  placeholder="Subject">
					</div>
					<div class="form-group">
						<textarea id="msg" class="form-control" rows="4" placeholder="Message"></textarea>
					</div>
					<button id="ed_submit" class="btn ed_btn ed_orange pull-right">send</button>
					<p id="err"></p>
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6">
				<div class="ed_event_single_address_info">
					<h4 class="">We will give you our best</h4>
					<p class="ed_bottompadder40 ed_toppadder10">You can always reach us via following contact details. We will give our best to reach you as possible.</p>
					<p>Phone: <span>0000-0000-0000</span></p>
					<p>Email: <a href="#">info@cronex.com</a></p>
					<p>Website: <a href="#">http://www.cronex.co.in</a></p>
				</div>
			</div>
		</div>
	</div>
</div>
<!--Section fourteen Contact form start-->
<!--Section fifteen Contact form start-->
<div class="ed_event_single_contact_address ed_toppadder90">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="ed_heading_top ed_bottompadder50">
					<h3>Find here</h3>
				</div>
			</div>
		</div>
	</div>
	<div class="ed_event_single_address_map">
		<div id="map"><iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d28441.484860654928!2d81.31538413955077!3d26.992677800000006!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sin!4v1621021557128!5m2!1sen!2sin" width="100%" height="100%" style="border:0;" allowfullscreen="" loading="lazy"></iframe></div>
	</div>
</div>
<!--Section fifteen Contact form start-->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('front.layout', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\wamp64\www\cronex\resources\views/front/contact.blade.php ENDPATH**/ ?>