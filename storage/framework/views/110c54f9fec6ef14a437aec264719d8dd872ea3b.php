<!DOCTYPE html>
<html lang="<?php echo e(str_replace('_', '-', app()->getLocale())); ?>">
<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <!-- CSRF Token -->
  <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
  <title><?php echo e(config('APP_NAME')); ?></title>
  <!-- General CSS Files -->
  <link rel="stylesheet" href="<?php echo e(asset('assets/css/app.min.css')); ?>">
  <link rel="stylesheet" href="<?php echo e(asset('assets/css/style.css')); ?>">
  <!-- Template CSS -->
  <link rel="stylesheet" href="<?php echo e(asset('assets/bundles/summernote/summernote-bs4.css')); ?>">
  <link rel="stylesheet" href="<?php echo e(asset('assets/bundles/datatables/datatables.min.css')); ?>">
  <link rel="stylesheet" href="<?php echo e(asset('assets/bundles/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css')); ?>">
  <link rel="stylesheet" href="<?php echo e(asset('assets/bundles/izitoast/css/iziToast.min.css')); ?>">
  <link rel="stylesheet" href="<?php echo e(asset('assets/bundles/chocolat/dist/css/chocolat.css')); ?>">
  <link rel="stylesheet" href="<?php echo e(asset('assets/bundles/lightgallery/dist/css/lightgallery.css')); ?>">
  <link rel="stylesheet" href="<?php echo e(asset('assets/bundles/pretty-checkbox/pretty-checkbox.min.css')); ?>">

  <link rel="stylesheet" href="<?php echo e(asset('assets/bundles/jqvmap/dist/jqvmap.min.css')); ?>">
  <link rel="stylesheet" href="<?php echo e(asset('assets/bundles/select2/dist/css/select2.min.css')); ?>">

  <link rel="stylesheet" href="<?php echo e(asset('assets/bundles/bootstrap-social/bootstrap-social.css')); ?>">

  <link rel="stylesheet" href="<?php echo e(asset('assets/css/components.css')); ?>">

  <link rel="stylesheet" href="<?php echo e(asset('assets/bundles/summernote/summernote-bs4.css')); ?>">
  <link rel="stylesheet" href="<?php echo e(asset('assets/css/custom.css')); ?>">
  <link rel='shortcut icon' type='image/x-icon' href="<?php echo e(asset('uplode/config/favicon/'.config('FAVICON'))); ?>" />


  <!-- General JS Scripts -->
  <script src="<?php echo e(asset('assets/js/app.min.js')); ?>"></script>
<style type="text/css">
    body.layout-3 .navbar.navbar-secondary {
    box-shadow: 0 4px 25px 0 rgb(0 0 0 / 10%);
    background-color: <?php if(config('SIDEBAR_COLOR') != null): ?> <?php echo e(config('SIDEBAR_COLOR')); ?> <?php else: ?> <?php if(Cache()->get('mode') == 'DarkTheme'): ?> #353c48 <?php else: ?> #fff <?php endif; ?> <?php endif; ?>;
    top: 70px;
    padding: 0;
    z-index: 889;
}

.theme-white .navbar {
    background-color: <?php if(config('HEADERBAR_COLOR') != null): ?> <?php echo e(config('HEADERBAR_COLOR')); ?> <?php else: ?> <?php if(Cache()->get('mode') == 'DarkTheme'): ?> #353c48 <?php else: ?> #e3eaef; <?php endif; ?> <?php endif; ?>;
    box-shadow: 0 0 10px 1px rgb(68 102 242 / 5%);
}

.main-sidebar {
    box-shadow: 0 4px 25px 0 rgb(0 0 0 / 10%);
    position: fixed;
    top: 0;
    height: 100%;
    width: 250px;
    background-color: <?php if(config('SIDEBAR_COLOR') != null): ?> <?php echo e(config('SIDEBAR_COLOR')); ?> <?php else: ?> <?php if(Cache()->get('mode') == 'DarkTheme'): ?> #353c48 <?php else: ?> #fff <?php endif; ?> <?php endif; ?>;
    z-index: 880;
    left: 0;
}

.loader {
    position: fixed;
    left: 0px;
    top: 0px;
    width: 100%;
    height: 100%;
    z-index: 9999;
    background: url("<?php echo e(asset('/uplode/loder/'.config('LOADER'))); ?>") 50% 50% no-repeat #f9f9f9;
    opacity: 1;
}
  </style>
</head>
  <?php if(auth()->guard()->guest()): ?>
  <?php else: ?>
<body class="<?php if(Cache::get('mode') == 'DarkTheme'): ?> dark dark-sidebar theme-black <?php else: ?> light light-sidebar theme-white <?php endif; ?> <?php if(Cache::get('menustyle') == 'TopMenuStyle'): ?> layout-3 <?php else: ?> <?php endif; ?>">
  <div class="loader"></div>
  <div id="app">
    <div class="main-wrapper <?php if(Cache::get('menustyle') == 'TopMenuStyle'): ?> container-fluid <?php else: ?> main-wrapper-1 <?php endif; ?> ">
    <div class="navbar-bg"></div>
    <!-- Navebar -->
    <?php echo $__env->make('../layouts/navebar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <!-- End Navebar -->

    <!--  Memu List  -->
    <?php if(Cache::get('menustyle') == 'TopMenuStyle'): ?> 
      <?php echo $__env->make('../layouts/navebar-menu', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <?php else: ?> 
      <?php echo $__env->make('../layouts/sidebar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <?php endif; ?>

  <!--   <?php echo $__env->make('../layouts/settingbar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?> -->
    <!--  End  Memu List  -->

    <!-- Main Content -->
    <?php echo $__env->yieldContent('content'); ?>
    <!-- End Main Content -->

      <footer class="main-footer">
        <div class="footer-left">
          Copyright &copy; <?php echo e(date('Y')); ?> <div class="bullet"></div> Developed By <a href="<?php echo e(URL('/')); ?>"><?php echo e(config('APP_NAME')); ?></a>
        </div>
        <div class="footer-right">
        </div>
      </footer>
    </div>
  </div>
</body>
  <?php endif; ?>
  <!-- JS Libraies -->
  <script src="<?php echo e(asset('assets/bundles/datatables/datatables.min.js')); ?>"></script>
  <script src="<?php echo e(asset('assets/bundles/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js')); ?>"></script>
  <script src="<?php echo e(asset('assets/bundles/jquery-ui/jquery-ui.min.js')); ?>"></script>

  <script src="<?php echo e(asset('assets/js/page/datatables.js')); ?>"></script>
  <script src="<?php echo e(asset('assets/bundles/izitoast/js/iziToast.min.js')); ?>"></script>
  <script src="<?php echo e(asset('assets/bundles/apexcharts/apexcharts.min.js')); ?>"></script>
  <script src="<?php echo e(asset('assets/js/page/toastr.js')); ?>"></script>
   <!-- JS Libraies -->
  <script src="<?php echo e(asset('assets/bundles/summernote/summernote-bs4.js')); ?>"></script>
  <script src="<?php echo e(asset('assets/bundles/chocolat/dist/js/jquery.chocolat.min.js')); ?>"></script>
  <script src="<?php echo e(asset('assets/js/page/gallery1.js')); ?>"></script>
  <script src="<?php echo e(asset('assets/bundles/lightgallery/dist/js/lightgallery-all.js')); ?>"></script>
  <!-- Page Specific JS File -->
  <script src="<?php echo e(asset('assets/js/page/light-gallery.js')); ?>"></script>
  <script src="<?php echo e(asset('assets/bundles/amcharts4/core.js')); ?>"></script>
  <script src="<?php echo e(asset('assets/bundles/amcharts4/charts.js')); ?>"></script>
  <script src="<?php echo e(asset('assets/bundles/amcharts4/animated.js')); ?>"></script>
  <script src="<?php echo e(asset('assets/bundles/jqvmap/dist/jquery.vmap.min.js')); ?>"></script>
  <script src="<?php echo e(asset('assets/bundles/jqvmap/dist/maps/jquery.vmap.world.js')); ?>"></script>
  <script src="<?php echo e(asset('assets/bundles/sweetalert/sweetalert.min.js')); ?>"></script>
  <script src="<?php echo e(asset('assets/bundles/select2/dist/js/select2.full.min.js')); ?>"></script>
  <!-- Page Specific JS File -->
  <script src="<?php echo e(asset('assets/js/page/sweetalert.js')); ?>"></script>
  <!-- Page Specific JS File -->
  <script src="<?php echo e(asset('assets/js/page/index.js')); ?>"></script>
  <!-- Template JS File -->
  <script src="<?php echo e(asset('assets/js/scripts.js')); ?>"></script>
  <!-- Custom JS File -->
  <script src="<?php echo e(asset('assets/js/custom.js')); ?>"></script>
  
  <script src="<?php echo e(asset('assets/bundles/summernote/summernote-bs4.js')); ?>"></script>

  <?php if(session('success')): ?>
  <script type="text/javascript">
    swal({
          title: "<?php echo e(session('success')); ?>",
          icon: "success",
          button: "OK",
        });
  </script>
  <?php endif; ?>  
  <?php if(session('warning')): ?>
  <script type="text/javascript">
    swal({
          title: "<?php echo e(session('warning')); ?>",
          icon: "warning",
          button: "OK",
        });
  </script>
  <?php endif; ?>  
  <?php if(session('error')): ?>
  <script type="text/javascript">
    swal({
          title: "<?php echo e(session('error')); ?>",
          icon: "error",
          button: "OK",
        });
  </script>
  <?php endif; ?> 

  <script type="text/javascript">

  $(document).ready(function() {
    log_time();
    //$("#country").select2();
  });



  function log_time(){
      $.ajax({
       type:"POST",
       url:"<?php echo e(url('/user-logout-time')); ?>",
       data:{"_token": "<?php echo e(csrf_token()); ?>"},
       success:function(result)
       {       
          setTimeout(log_time,2000);
       }
    });
    }



if(localStorage.getItem("firstTime")==null){
    show_welcome_message();
   localStorage.setItem("firstTime","done");
}


<?php if(Auth::user()): ?>
function show_welcome_message() {
  var userID = "<?php echo e(Auth::user()->name); ?>";
  var userIMAGE = "<?php echo e(Auth::user()->image); ?>";
  var userNAME = userID.toUpperCase();
  var d = new Date();
  var hoursCOUNT = d.getHours();
<?php endif; ?>

  if(hoursCOUNT >= 0 && hoursCOUNT < 18)
  {
    iziToast.show({
        id: 'haduken',
        theme: 'dark',
        icon: 'far fa-smile-beam',
        title: 'HI, '+userNAME+' GOOD DAY',
        displayMode: 2,
        message: 'Welcome '+'<?php echo e(config('APP_NAME')); ?>',
        position: 'topCenter',
        transitionIn: 'flipInX',
        transitionOut: 'flipOutX',
        progressBarColor: 'rgb(0, 255, 184)',
        image: 'uplode/users/'+userIMAGE,
        imageWidth: 70,
        layout: 2,
        onClosing: function(){
            console.info('onClosing');
        },
        onClosed: function(instance, toast, closedBy){
            console.info('Closed | closedBy: ' + closedBy);
        },
        iconColor: 'rgb(0, 255, 184)'
    });

  }
  else
  {
    iziToast.show({
        id: 'haduken',
        theme: 'dark',
        icon: 'far fa-smile-beam',
        title: 'HI, '+userNAME+' GOOD EVENING',
        displayMode: 2,
        message: 'Welcome '+'<?php echo e(config('APP_NAME')); ?>',
        position: 'topCenter',
        transitionIn: 'flipInX',
        transitionOut: 'flipOutX',
        progressBarColor: 'rgb(0, 255, 184)',
        image: 'uplode/users/'+userIMAGE,
        imageWidth: 70,
        layout: 2,
        onClosing: function(){
            console.info('onClosing');
        },
        onClosed: function(instance, toast, closedBy){
            console.info('Closed | closedBy: ' + closedBy);
        },
        iconColor: 'rgb(0, 255, 184)'
    });
  }
  };



$('#state').on('change', function() {
    var state = ( this.value );
    
    var countryID = $('option:selected',$('#country')).val();

    if(countryID == '')
     {
        swal({
          title: "Please Select Country",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        });

        $('option:selected',$('#state')).prop("selected", false);
     }
     else if(countryID != '101')
     {
        swal({
          title: "You're Not Indian",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        });
      $("#state option[value='other']").attr('selected', true);

     }
     else{
    if(this.value){
    $.ajax({
       type:"POST",
       url:"<?php echo e(url('/select-cities-name')); ?>",
       data:{"_token": "<?php echo e(csrf_token()); ?>", "state": state},
       success:function(result)
       {  
          $('#city').html(result);      
       }
    });
  }} });


$('#city').on('change', function() {
  var cityID = ( this.value );
       var stateID = $('option:selected',$('#state')).val();
  //alert( stateID );
     if(stateID == '')
     {
        swal({
          title: "Please Select State",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        });
        $('option:selected',$('#city')).prop("selected", false);
     }
    if(stateID == 'other')
    {
      $("#city option[value='other']").attr('selected', true);
    }

});





/*
document.onkeydown = function(e) {
  if(event.keyCode == 123) {
     return false;
  }
  if(e.ctrlKey && e.shiftKey && e.keyCode == 'I'.charCodeAt(0)) {
     return false;
  }
  if(e.ctrlKey && e.shiftKey && e.keyCode == 'C'.charCodeAt(0)) {
     return false;
  }
  if(e.ctrlKey && e.shiftKey && e.keyCode == 'J'.charCodeAt(0)) {
     return false;
  }
  if(e.ctrlKey && e.keyCode == 'U'.charCodeAt(0)) {
     return false;
  }
}
*/
/*$(document).bind("contextmenu",function(e){
  return false;
    });
*/
  </script>

<!-- <script src="../node_modules/devtools-detect/index.js"></script>
<script type="module">
  if(window.devtools.isOpen)
  {
    window.location.href="<?php echo e(url('error-page')); ?>";
  }
  window.addEventListener('devtoolschange', event => {
    if(event.detail.isOpen)
    {
      window.location.href="<?php echo e(url('error-page')); ?>";
    }
  });
</script> -->
</html><?php /**PATH C:\wamp64\www\laravel9\resources\views/layouts/app.blade.php ENDPATH**/ ?>