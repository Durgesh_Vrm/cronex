<?php
        if(Auth::user()->role === 'ADMIN469785')
        {
            $rdata = DB::table('user_route')->select('id', 'route', 'Administrator')->where('Administrator', 'Yes')->get();
            if($rdata != '[]'){ $url = []; foreach ($rdata as $value){ $url[] = $value->route; }}
            else{ $url = []; }
        }        
        elseif(Auth::user()->role === 'USER58428D3')
        {
            $rdata = DB::table('user_route')->select('id', 'route')->where('USER58428D3', 'Yes')->get();
            if($rdata != '[]'){ $url = []; foreach ($rdata as $value){ $url[] = $value->route; }}
            else{ $url = []; }
        }        
        elseif(Auth::user()->role === 'EDITOR32238')
        {
            $rdata = DB::table('user_route')->select('id', 'route', 'Editor')->where('Editor', 'Yes')->get();
            if($rdata != '[]'){ $url = []; foreach ($rdata as $value){ $url[] = $value->route; }}
            else{ $url = []; }
        }        
        elseif(Auth::user()->role === 'MEMBER55228')
        {
            $rdata = DB::table('user_route')->select('id', 'route', 'Member')->where('Member', 'Yes')->get();
            if($rdata != '[]'){ $url = []; foreach ($rdata as $value){ $url[] = $value->route; }}
            else{ $url = []; }
        }        
        elseif(Auth::user()->role === 'EMPLOY15472')
        {
            $rdata = DB::table('user_route')->select('id', 'route', 'Employee')->where('Employee', 'Yes')->get();
            if($rdata != '[]'){ $url = []; foreach ($rdata as $value){ $url[] = $value->route; }}
            else{ $url = []; }
        }        
        elseif(Auth::user()->role === 'MANAGER0739')
        {
            $rdata = DB::table('user_route')->select('id', 'route', 'Manager')->where('Manager', 'Yes')->get();
            if($rdata != '[]'){ $url = []; foreach ($rdata as $value){ $url[] = $value->route; }}
            else{ $url = []; }
        }
        elseif (Auth::user()->role === 'SUPPERADMINC60C3421A4D4B2152B430') 
        {
            $url = [true];
        }
        else
        {
            $url = [];
        } 
?>

 <div class="main-sidebar sidebar-style-2">
        <aside id="sidebar-wrapper">
          <div class="sidebar-brand">
            <a href="<?php echo e(URL('dashboard')); ?>"> <img alt="<?php echo e(config('LOGO')); ?>" src="<?php echo e(asset('uplode/config/logo/'.config('LOGO'))); ?>" class="header-logo" /> <span
                class="logo-name"><?php echo e(config('APP_NAME')); ?></span>
            </a>
          </div>
          <div class="sidebar-user">
            <div class="sidebar-user-picture">
              <?php $Name = explode(' ', Auth::user()->name, 2); ?>
              <?php if(!Auth::user()->image): ?> 
              <figure class="avatar mr-2 avatar-xl  text-white" data-initial="<?php if(isset($Name[1])){ $N = explode($Name[0], 1); echo substr($Name[0],0,1); echo substr($Name[1],0,1); }else{ echo substr($Name[0],0,1); } ?>"></figure>
              <?php else: ?> 
              <img alt="image" src="<?php echo e(asset('uplode/users/'.Auth::user()->image)); ?>">
              <?php endif; ?>
            </div>
            <div class="sidebar-user-details">
              <div class="user-name"><?php echo e(ucwords(Auth::user()->name)); ?></div>
              <div class="user-role"><?php echo e(ucfirst(Auth::user()->email)); ?></div>
            </div>
          </div>
          <ul class="sidebar-menu">
            <li class="menu-header">Main</li>
            <li class="active"><a class="nav-link" href="blank.html"><i data-feather="airplay"></i><span>Dashboard </span></a></li>
            <li class="dropdown">
              <a href="#" class="menu-toggle nav-link has-dropdown"><i data-feather="box"></i><span>Widgets</span></a>
              <ul class="dropdown-menu">
                <li><a class="nav-link" href="widget-chart.html">Chart Widgets</a></li>
                <li><a class="nav-link" href="widget-data.html">Data Widgets</a></li>
              </ul>
            </li>
            <li class="dropdown">
              <a href="#" class="menu-toggle nav-link has-dropdown"><i data-feather="command"></i><span>Apps</span></a>
              <ul class="dropdown-menu">
                <li><a class="nav-link" href="chat.html">Chat</a></li>
                <li><a class="nav-link" href="portfolio.html">Portfolio</a></li>
                <li><a class="nav-link" href="blog.html">Blog</a></li>
                <li><a class="nav-link" href="calendar.html">Calendar</a></li>
                <li><a class="nav-link" href="drag-drop.html">Drag & Drop</a></li>
              </ul>
            </li>
            <li class="dropdown">
              <a href="#" class="menu-toggle nav-link has-dropdown"><i data-feather="mail"></i><span>Email</span></a>
              <ul class="dropdown-menu">
                <li><a class="nav-link" href="email-inbox.html">Inbox</a></li>
                <li><a class="nav-link" href="email-compose.html">Compose</a></li>
                <li><a class="nav-link" href="email-read.html">read</a></li>
              </ul>
            </li>
            <li class="dropdown">
              <a href="#" class="menu-toggle nav-link has-dropdown"><i data-feather="mail"></i><span>SMS</span></a>
              <ul class="dropdown-menu">
                <li><a class="nav-link" href="email-inbox.html">Inbox</a></li>
                <li><a class="nav-link" href="email-compose.html">Compose</a></li>
                <li><a class="nav-link" href="email-read.html">read</a></li>
              </ul>
            </li>
            <li class="menu-header">UI Elements</li>
            <li class="dropdown">
              <a href="#" class="menu-toggle nav-link has-dropdown"><i data-feather="briefcase"></i><span>Basic
                  Components</span></a>
              <ul class="dropdown-menu">
                <li><a class="nav-link" href="alert.html">Alert</a></li>
                <li><a class="nav-link" href="badge.html">Badge</a></li>
                <li><a class="nav-link" href="breadcrumb.html">Breadcrumb</a></li>
                <li><a class="nav-link" href="buttons.html">Buttons</a></li>
                <li><a class="nav-link" href="collapse.html">Collapse</a></li>
                <li><a class="nav-link" href="dropdown.html">Dropdown</a></li>
                <li><a class="nav-link" href="checkbox-and-radio.html">Checkbox &amp; Radios</a></li>
                <li><a class="nav-link" href="list-group.html">List Group</a></li>
                <li><a class="nav-link" href="media-object.html">Media Object</a></li>
                <li><a class="nav-link" href="navbar.html">Navbar</a></li>
                <li><a class="nav-link" href="pagination.html">Pagination</a></li>
                <li><a class="nav-link" href="popover.html">Popover</a></li>
                <li><a class="nav-link" href="progress.html">Progress</a></li>
                <li><a class="nav-link" href="tooltip.html">Tooltip</a></li>
                <li><a class="nav-link" href="flags.html">Flag</a></li>
                <li><a class="nav-link" href="typography.html">Typography</a></li>
              </ul>
            </li>
            <li class="dropdown">
              <a href="#" class="menu-toggle nav-link has-dropdown"><i
                  data-feather="shopping-bag"></i><span>Advanced</span></a>
              <ul class="dropdown-menu">
                <li><a class="nav-link" href="avatar.html">Avatar</a></li>
                <li><a class="nav-link" href="card.html">Card</a></li>
                <li><a class="nav-link" href="modal.html">Modal</a></li>
                <li><a class="nav-link" href="sweet-alert.html">Sweet Alert</a></li>
                <li><a class="nav-link" href="toastr.html">Toastr</a></li>
                <li><a class="nav-link" href="empty-state.html">Empty State</a></li>
                <li><a class="nav-link" href="multiple-upload.html">Multiple Upload</a></li>
                <li><a class="nav-link" href="pricing.html">Pricing</a></li>
                <li><a class="nav-link" href="tabs.html">Tab</a></li>
              </ul>
            </li>
            <li><a class="nav-link" href="blank.html"><i data-feather="file"></i><span>Blank Page</span></a></li>
            <?php if(array_intersect(array('view-users','find-user','user-registration',true), $url)): ?>
            <li class="menu-header">Users</li>
            <li class="dropdown">
              <a href="#" class="menu-toggle nav-link has-dropdown"><i data-feather="layout"></i><span>Users</span></a>
              <ul class="dropdown-menu">
              <?php if(array_intersect(array('user-registration',true), $url)): ?>
                <li><a class="nav-link" href="<?php echo e(URL('user-registration')); ?>">User Registration</a></li>
              <?php endif; ?>
              <?php if(array_intersect(array('view-users',true), $url)): ?>
                <li><a class="nav-link" href="<?php echo e(URL('view-users')); ?>">View Users</a></li>
              <?php endif; ?>
              <?php if(array_intersect(array('edit-delete-users',true), $url)): ?>
                <li><a class="nav-link" href="<?php echo e(URL('edit-delete-users')); ?>">Edit & Del Users</a></li>
              <?php endif; ?>
              <?php if(array_intersect(array('find-user',true), $url)): ?>
                <li><a class="nav-link" href="<?php echo e(URL('find-user')); ?>">Find User</a></li>
              <?php endif; ?>
              </ul>
            </li>
            <?php endif; ?>
            <li class="dropdown">
              <a href="#" class="menu-toggle nav-link has-dropdown"><i data-feather="grid"></i><span>Team</span></a>
              <ul class="dropdown-menu">
                <li><a class="nav-link" href="<?php echo e(url('view-teams')); ?>">View Team</a></li>
                <li><a class="nav-link" href="<?php echo e(url('view-team-members')); ?>">View Team Member</a></li>
              </ul>
            </li>
            <li class="dropdown">
              <a href="#" class="menu-toggle nav-link has-dropdown"><i
                  data-feather="pie-chart"></i><span>Charts</span></a>
              <ul class="dropdown-menu">
                <li><a class="nav-link" href="chart-amchart.html">amChart</a></li>
                <li><a class="nav-link" href="chart-apexchart.html">apexchart</a></li>
                <li><a class="nav-link" href="chart-echart.html">eChart</a></li>
                <li><a class="nav-link" href="chart-chartjs.html">Chartjs</a></li>
                <li><a class="nav-link" href="chart-sparkline.html">Sparkline</a></li>
                <li><a class="nav-link" href="chart-morris.html">Morris</a></li>
              </ul>
            </li>
            <li class="dropdown">
              <a href="#" class="menu-toggle nav-link has-dropdown"><i data-feather="feather"></i><span>Icons</span></a>
              <ul class="dropdown-menu">
                <li><a class="nav-link" href="icon-font-awesome.html">Font Awesome</a></li>
                <li><a class="nav-link" href="icon-material.html">Material Design</a></li>
                <li><a class="nav-link" href="icon-ionicons.html">Ion Icons</a></li>
                <li><a class="nav-link" href="icon-feather.html">Feather Icons</a></li>
                <li><a class="nav-link" href="icon-weather-icon.html">Weather Icon</a></li>
              </ul>
            </li>
            <li class="menu-header">Media</li>
            <li class="dropdown">
              <a href="#" class="menu-toggle nav-link has-dropdown"><i data-feather="image"></i><span>Gallery</span></a>
              <ul class="dropdown-menu">
                <li><a class="nav-link" href="light-gallery.html">Light Gallery</a></li>
                <li><a href="gallery1.html">Gallery 2</a></li>
              </ul>
            </li>
            <li class="dropdown">
              <a href="#" class="menu-toggle nav-link has-dropdown"><i data-feather="flag"></i><span>Sliders</span></a>
              <ul class="dropdown-menu">
                <li><a href="carousel.html">Bootstrap Carousel.html</a></li>
                <li><a class="nav-link" href="owl-carousel.html">Owl Carousel</a></li>
              </ul>
            </li>
            <?php if(array_intersect(array('countries','states','cities',true), $url)): ?>
            <li class="dropdown">
              <a href="#" class="menu-toggle nav-link has-dropdown"><i data-feather="sliders"></i><span>Setting</span></a>
              <ul class="dropdown-menu">
                <li><a href="carousel.html"> Translate</a></li>
                <li class="dropdown">
                  <a href="#" class="has-dropdown">Location</a>
                  <ul class="dropdown-menu">
                    <?php if(array_intersect(array('countries','states','cities',true), $url)): ?>
                    <li><a href="<?php echo e(URL('countries')); ?>"> Countries</a></li>
                    <?php endif; ?>
                    <?php if(array_intersect(array('countries','states','cities',true), $url)): ?>
                    <li><a href="<?php echo e(URL('states')); ?>"> states</a></li>
                    <?php endif; ?>
                    <?php if(array_intersect(array('countries','states','cities',true), $url)): ?>
                    <li><a href="<?php echo e(URL('cities')); ?>"> Cities</a></li>
                    <?php endif; ?>
                  </ul>
                </li>
                <li><a href="carousel.html"> App Logo & Banner</a></li>
                <li><a href="<?php echo e(URL('web-configuration-settings-value')); ?>"> App Configuration</a></li>
                <li><a href="carousel.html"> Google Map</a></li>
              </ul>
            </li>
            <?php endif; ?>
            <?php if(array_intersect(array('user-permissions','user-rols',true), $url)): ?>
            <li class="dropdown">
              <a href="#" class="menu-toggle nav-link has-dropdown"><i data-feather="sliders"></i><span>Master</span></a>
              <ul class="dropdown-menu">
                <?php if(in_array(true, $url)): ?>
                <li><a class="nav-link" href="<?php echo e(URL('user-permissions')); ?>">User Permission</a></li>
                <?php endif; ?>
                <?php if(in_array(true, $url)): ?>
                <li><a class="nav-link" href="<?php echo e(URL('user-rols')); ?>">User Role</a></li>
                <?php endif; ?>
                <?php if(in_array(true, $url)): ?>
                <li><a class="nav-link" href="<?php echo e(URL('user-rout')); ?>">User Route</a></li>
                <?php endif; ?>
                <?php if(in_array(true, $url)): ?>
                <li><a class="nav-link" href="<?php echo e(URL('send-updates')); ?>">Send Update & Notif</a></li>
                <?php endif; ?>
              </ul>
            </li>
            <?php endif; ?>
            <li class="menu-header">Maps</li>
            <li class="dropdown">
              <a href="#" class="menu-toggle nav-link has-dropdown"><i data-feather="map"></i><span>Google
                  Maps</span></a>
              <ul class="dropdown-menu">
                <li><a href="gmaps-advanced-route.html">Advanced Route</a></li>
                <li><a href="gmaps-draggable-marker.html">Draggable Marker</a></li>
                <li><a href="gmaps-geocoding.html">Geocoding</a></li>
                <li><a href="gmaps-geolocation.html">Geolocation</a></li>
                <li><a href="gmaps-marker.html">Marker</a></li>
                <li><a href="gmaps-multiple-marker.html">Multiple Marker</a></li>
                <li><a href="gmaps-route.html">Route</a></li>
                <li><a href="gmaps-simple.html">Simple</a></li>
              </ul>
            </li>
            <li><a class="nav-link" href="vector-map.html"><i data-feather="map-pin"></i><span>Vector
                  Map</span></a></li>
            <li class="dropdown">
              <a href="#" class="menu-toggle nav-link has-dropdown"><i
                  data-feather="alert-triangle"></i><span>Errors</span></a>
              <ul class="dropdown-menu">
                <li><a class="nav-link" href="errors-503.html">503</a></li>
                <li><a class="nav-link" href="errors-403.html">403</a></li>
                <li><a class="nav-link" href="errors-404.html">404</a></li>
                <li><a class="nav-link" href="errors-500.html">500</a></li>
              </ul>
            </li>           
            <li class="dropdown">
              <a href="#" class="menu-toggle nav-link has-dropdown"><i data-feather="anchor"></i><span>Other
                  Pages</span></a>
              <ul class="dropdown-menu">
                <li><a class="nav-link" href="create-post.html">Create Post</a></li>
                <li><a class="nav-link" href="posts.html">Posts</a></li>
                <li><a class="nav-link" href="profile.html">Profile</a></li>
                <li><a class="nav-link" href="contact.html">Contact</a></li>
                <li><a class="nav-link" href="invoice.html">Invoice</a></li>
              </ul>
            </li>
            <li class="dropdown">
              <a href="#" class="menu-toggle nav-link has-dropdown"><i
                  data-feather="chevrons-down"></i><span>Multilevel</span></a>
              <ul class="dropdown-menu">
                <li><a href="#">Menu 1</a></li>
                <li class="dropdown">
                  <a href="#" class="has-dropdown">Menu 2</a>
                  <ul class="dropdown-menu">
                    <li><a href="#">Child Menu 1</a></li>
                    <li class="dropdown">
                      <a href="#" class="has-dropdown">Child Menu 2</a>
                      <ul class="dropdown-menu">
                        <li><a href="#">Child Menu 1</a></li>
                        <li><a href="#">Child Menu 2</a></li>
                      </ul>
                    </li>
                    <li><a href="#"> Child Menu 3</a></li>
                  </ul>
                </li>
              </ul>
            </li>
          </ul>
        </aside>
      </div><?php /**PATH C:\wamp64\www\laravel9\resources\views////layouts/sidebar.blade.php ENDPATH**/ ?>