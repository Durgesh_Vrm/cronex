
<?php $__env->startSection('content'); ?>
      <!-- Main Content -->
      <div class="main-content">
        <section class="section">
          <ul class="breadcrumb breadcrumb-style ">
            <li class="breadcrumb-item">
              <a href="index.html">
                <i data-feather="home"></i></a>
            </li>
            <li class="breadcrumb-item">Setting</li>
            <li class="breadcrumb-item">Software Configuration</li>
          </ul>
          <div class="section-body">
            <div class="row">
              <div class="col-12 col-sm-12 col-lg-12">
                <div class="card">
                    <form method="post" action="<?php echo e(URL('create-configuration-settings-value')); ?>" class="needs-validation" enctype="multipart/form-data" id="form">
                          <?php echo csrf_field(); ?>
                          <div class="card-header">
                            <h4>Software Configuration</h4>
                          </div>
                          <div class="card-body">
                              <div class="row">
                              <div class="form-group col-sm-12 col-md-2 col-lg-2">
                                  <label>Config Type</label>
                                  <select name="type" id="type" onchange="change_type(this.id)" class="form-control form-control-sm custom-select <?php $__errorArgs = ['type'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" style="height: calc(1.5em + .5rem + 2px); padding: .25rem .5rem; font-size: .875rem; line-height: 1.5; border-radius: .2rem;">
                                    <option value="1" selected>Value</option>
                                    <option value="2">Logo</option>
                                    <option value="3">Banner</option>
                                    <option value="4">Color</option>
                                    <option value="5">Loader</option>
                                  </select>
                                  <?php $__errorArgs = ['type'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                      <div class="invalid-feedback"><?php echo e($message); ?></div>
                                  <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                              </div>
                              <div class="form-group col-sm-12 col-md-2 col-lg-2">
                                  <label>Config For</label>
                                  <select name="config_for" id="config_for" class="form-control form-control-sm custom-select <?php $__errorArgs = ['config_for'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" style="height: calc(1.5em + .5rem + 2px); padding: .25rem .5rem; font-size: .875rem; line-height: 1.5; border-radius: .2rem;">
                                    <option value="4">Both</option>
                                    <option value="2">Website</option>
                                    <option value="1">Admin Panel</option>
                                    <option value="3">Android</option>
                                  </select>
                                  <?php $__errorArgs = ['config_for'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                      <div class="invalid-feedback"><?php echo e($message); ?></div>
                                  <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                              </div>
                              <div class="form-group col-sm-12 col-md-2 col-lg-2">
                                  <label>Config Variable</label>
                                  <input type="text" name="config_variable" id="config_variable" class="form-control form-control-sm <?php $__errorArgs = ['config_variable'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>">
                                  <?php $__errorArgs = ['config_variable'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                      <div class="invalid-feedback"><?php echo e($message); ?></div>
                                  <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                              </div>
                              <div class="form-group col-sm-12 col-md-3 col-lg-3" id="convalue">
                                  <label>Config Value</label>
                                  <input type="text"  class="form-control form-control-sm <?php $__errorArgs = ['config_value'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" name="config_value" id="config_value">
                                  <?php $__errorArgs = ['config_value'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                      <div class="invalid-feedback"><?php echo e($message); ?></div>
                                  <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                              </div>
                              <div class="form-group col-sm-12 col-md-3 col-lg-3">
                                  <label>Config Description</label>
                                  <input type="text" class="form-control form-control-sm <?php $__errorArgs = ['config_desc'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" name="config_desc" id="config_desc">
                                  <?php $__errorArgs = ['config_desc'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                      <div class="invalid-feedback"><?php echo e($message); ?></div>
                                  <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                              </div>
                            </div> 
                          </div>
                          <div class="card-footer text-center">
                            <button class="btn btn-primary" type="button" onclick="SubmitForm('form');"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-save"><path d="M19 21H5a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h11l5 5v11a2 2 0 0 1-2 2z"></path><polyline points="17 21 17 13 7 13 7 21"></polyline><polyline points="7 3 7 8 15 8"></polyline></svg> Update Configuration</button> 
                            <a class="btn btn-danger" href=""><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-refresh-cw"><polyline points="23 4 23 10 17 10"></polyline><polyline points="1 20 1 14 7 14"></polyline><path d="M3.51 9a9 9 0 0 1 14.85-3.36L23 10M1 14l4.64 4.36A9 9 0 0 0 20.49 15"></path></svg> Reset</a>
                          </div>
                        </form>
                </div>
              </div>
            </div>
             <div class="row">
              <div class="col-12">
                <div class="card">
                  <div class="card-header">
                    <h4>Basic DataTables</h4>
                  </div>
                  <div class="card-body">
                    <div class="table-responsive">
                      <table class="table table-striped" id="table-1">
                        <thead>
                          <tr>
                            <th class="text-center">
                              #
                            </th>
                            <th>For</th>
                            <th>Key</th>
                            <th>Value</th>
                            <th>Description</th>
                            <th>Update Date</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php $sn = 0; ?>
                          <?php $__currentLoopData = $data['config']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                          <tr>
                            <td>
                              <?php echo e(++$sn); ?>

                            </td>
                            <td><?php echo e($value->for); ?></td>
                            <td><?php echo e($value->name); ?></td>
                            <td><?php $img = substr($value->value, strpos($value->value, ".")); 
                                     $col = substr($value->value,0,1);
                                if($img == '.png' || $img == '.jpeg' || $img == '.jpg')
                                { 
                                 if(mb_substr($value->value, 0, 2) != 'BN')
                                 {
                                ?>
                                  <img alt="image" src="<?php echo e(asset('uplode/config/logo/'.$value->value)); ?>" width="40">
                                <?php }else{ ?> 
                                  <img alt="image" src="<?php echo e(asset('uplode/config/banner/'.$value->value)); ?>" width="60">
                                <?php
                                }} 
                                elseif($img == '.gif')
                                { 
                                ?>
                                  <img alt="image" src="<?php echo e(asset('uplode/loder/'.$value->value)); ?>" width="40">
                                <?php
                                }
                                elseif($col == '#')
                                {
                                ?> 
                                  <div class="badge" style="background: linear-gradient(to right,<?php echo e($value->value); ?>, #f2f2f2) !important; color: #fff;"><?php echo e($value->value); ?></div>
                                <?php
                                }
                                else
                                {
                                  echo $value->value;
                                }
                                ?>

                            </td>
                            <td><?php echo e($value->desc); ?></td>
                            <td><?php echo e($value->updated_at); ?></td>
                            <td>
                              <div class="buttons">
                              <?php $img = substr($value->value, strpos($value->value, ".")); 
                                     $col = substr($value->value,0,1);
                                if($img == '.png' || $img == '.jpeg' || $img == '.jpg')
                                { 
                                 if(mb_substr($value->value, 0, 2) != 'BN')
                                 {
                                ?>
                                <a href="<?php echo e(URL('edit-configuration-settings-logo/'.Crypt::encrypt($value->id))); ?>" class="btn btn-outline-info btn-sm"><svg xmlns="http://www.w3.org/2000/svg" width="15" height="15" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-edit"><path d="M11 4H4a2 2 0 0 0-2 2v14a2 2 0 0 0 2 2h14a2 2 0 0 0 2-2v-7"></path><path d="M18.5 2.5a2.121 2.121 0 0 1 3 3L12 15l-4 1 1-4 9.5-9.5z"></path></svg></a>
                                <?php }else{ ?> 
                                <a href="<?php echo e(URL('edit-configuration-settings-banner/'.Crypt::encrypt($value->id))); ?>" class="btn btn-outline-info btn-sm"><svg xmlns="http://www.w3.org/2000/svg" width="15" height="15" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-edit"><path d="M11 4H4a2 2 0 0 0-2 2v14a2 2 0 0 0 2 2h14a2 2 0 0 0 2-2v-7"></path><path d="M18.5 2.5a2.121 2.121 0 0 1 3 3L12 15l-4 1 1-4 9.5-9.5z"></path></svg></a>
                                <?php
                                }}
                                elseif($img == '.gif')
                                { 
                                ?>
                                <a href="<?php echo e(URL('edit-configuration-settings-loader/'.Crypt::encrypt($value->id))); ?>" class="btn btn-outline-info btn-sm"><svg xmlns="http://www.w3.org/2000/svg" width="15" height="15" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-edit"><path d="M11 4H4a2 2 0 0 0-2 2v14a2 2 0 0 0 2 2h14a2 2 0 0 0 2-2v-7"></path><path d="M18.5 2.5a2.121 2.121 0 0 1 3 3L12 15l-4 1 1-4 9.5-9.5z"></path></svg></a>
                                <?php 
                                } 
                                elseif($col == '#')
                                {
                                ?> 
                                <a href="<?php echo e(URL('edit-configuration-settings-color/'.Crypt::encrypt($value->id))); ?>" class="btn btn-outline-info btn-sm"><svg xmlns="http://www.w3.org/2000/svg" width="15" height="15" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-edit"><path d="M11 4H4a2 2 0 0 0-2 2v14a2 2 0 0 0 2 2h14a2 2 0 0 0 2-2v-7"></path><path d="M18.5 2.5a2.121 2.121 0 0 1 3 3L12 15l-4 1 1-4 9.5-9.5z"></path></svg></a>
                                <?php
                                }
                                else
                                {
                                ?>  
                                <a href="<?php echo e(URL('edit-configuration-settings-value/'.Crypt::encrypt($value->id))); ?>" class="btn btn-outline-info btn-sm"><svg xmlns="http://www.w3.org/2000/svg" width="15" height="15" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-edit"><path d="M11 4H4a2 2 0 0 0-2 2v14a2 2 0 0 0 2 2h14a2 2 0 0 0 2-2v-7"></path><path d="M18.5 2.5a2.121 2.121 0 0 1 3 3L12 15l-4 1 1-4 9.5-9.5z"></path></svg></a>
                                <?php
                                }
                                ?>
                                <a href="<?php echo e(URL('delete-configuration-settings/'.Crypt::encrypt($value->value))); ?>" class="btn btn-sm btn-outline-danger"><svg xmlns="http://www.w3.org/2000/svg" width="15" height="15" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash-2"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path><line x1="10" y1="11" x2="10" y2="17"></line><line x1="14" y1="11" x2="14" y2="17"></line></svg></a>
                                </div>
                              </td>
                          </tr>
                          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
<script type="text/javascript">
    function change_type(id)
  {
    var id = $('#'+id).val();

    if(id)
    {
      if(id == 1){  
        window.location = "<?php echo e(URL('web-configuration-settings-value')); ?>";
      }
      if(id == 2){  
        window.location = "<?php echo e(URL('web-configuration-settings-logo')); ?>";
      }
      if(id == 3){  
        window.location = "<?php echo e(URL('web-configuration-settings-banner')); ?>";
      }
      if(id == 4){ 
        window.location = "<?php echo e(URL('web-configuration-settings-color')); ?>";
      }
      if(id == 5){ 
        window.location = "<?php echo e(URL('web-configuration-settings-loader')); ?>";
      }
    }
  }
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\wamp64\www\cronex\resources\views/Setting/config-setting.blade.php ENDPATH**/ ?>