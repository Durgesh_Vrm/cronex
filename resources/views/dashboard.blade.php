@extends('layouts.app')

@section('content')
      <div class="main-content">
        <section class="section">
          <ul class="breadcrumb breadcrumb-style ">
            <li class="breadcrumb-item">
              <a href="index.html">
                <i data-feather="home"></i></a>
            </li>
            <li class="breadcrumb-item active">Dashboard</li>
          </ul>

          <div class="row">
            <div class="col-xl-3 col-lg-6">
              <div class="card l-bg-cherry">
                <div class="card-statistic-3 p-4">
                  <div class="card-icon card-icon-large"><i class="fas fa-shopping-cart"></i></div>
                  <div class="mb-4">
                    <h5 class="card-title mb-0">New Orders</h5>
                  </div>
                  <div class="row align-items-center mb-2 d-flex">
                    <div class="col-8">
                      <h2 class="d-flex align-items-center mb-0">
                        3,243
                      </h2>
                    </div>
                    <div class="col-4 text-right">
                      <span>12.5% <i class="fa fa-arrow-up"></i></span>
                    </div>
                  </div>
                  <div class="progress mt-1 " data-height="8">
                    <div class="progress-bar l-bg-cyan" role="progressbar" data-width="25%" aria-valuenow="25"
                      aria-valuemin="0" aria-valuemax="100"></div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-xl-3 col-lg-6">
              <div class="card l-bg-blue-dark">
                <div class="card-statistic-3 p-4">
                  <div class="card-icon card-icon-large"><i class="fas fa-users"></i></div>
                  <div class="mb-4">
                    <h5 class="card-title mb-0">Customers</h5>
                  </div>
                  <div class="row align-items-center mb-2 d-flex">
                    <div class="col-8">
                      <h2 class="d-flex align-items-center mb-0">
                        15.07k
                      </h2>
                    </div>
                    <div class="col-4 text-right">
                      <span>9.23% <i class="fa fa-arrow-up"></i></span>
                    </div>
                  </div>
                  <div class="progress mt-1 " data-height="8">
                    <div class="progress-bar l-bg-green" role="progressbar" data-width="25%" aria-valuenow="25"
                      aria-valuemin="0" aria-valuemax="100"></div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-xl-3 col-lg-6">
              <div class="card l-bg-green-dark">
                <div class="card-statistic-3 p-4">
                  <div class="card-icon card-icon-large"><i class="fas fa-ticket-alt"></i></div>
                  <div class="mb-4">
                    <h5 class="card-title mb-0">Ticket Resolved</h5>
                  </div>
                  <div class="row align-items-center mb-2 d-flex">
                    <div class="col-8">
                      <h2 class="d-flex align-items-center mb-0">
                        578
                      </h2>
                    </div>
                    <div class="col-4 text-right">
                      <span>10% <i class="fa fa-arrow-up"></i></span>
                    </div>
                  </div>
                  <div class="progress mt-1 " data-height="8">
                    <div class="progress-bar l-bg-orange" role="progressbar" data-width="25%" aria-valuenow="25"
                      aria-valuemin="0" aria-valuemax="100"></div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-xl-3 col-lg-6">
              <div class="card l-bg-orange-dark">
                <div class="card-statistic-3 p-4">
                  <div class="card-icon card-icon-large"><i class="fas fa-dollar-sign"></i></div>
                  <div class="mb-4">
                    <h5 class="card-title mb-0">Revenue Today</h5>
                  </div>
                  <div class="row align-items-center mb-2 d-flex">
                    <div class="col-8">
                      <h2 class="d-flex align-items-center mb-0">
                        $11.61k
                      </h2>
                    </div>
                    <div class="col-4 text-right">
                      <span>2.5% <i class="fa fa-arrow-up"></i></span>
                    </div>
                  </div>
                  <div class="progress mt-1 " data-height="8">
                    <div class="progress-bar l-bg-cyan" role="progressbar" data-width="25%" aria-valuenow="25"
                      aria-valuemin="0" aria-valuemax="100"></div>
                  </div>
                </div>
              </div>
            </div>
          </div>          

          <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-12">
              <div class="card card-statistic-2">
                <div class="card-icon shadow-primary bg-purple">
                  <i class="fas fa-shopping-cart"></i>
                </div>
                <div class="card-wrap float-right">
                  <div class="card-header">
                    <h4>New Orders</h4>
                  </div>
                  <div class="card-body">
                    3,243
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-12">
              <div class="card card-statistic-2">
                <div class="card-icon shadow-primary bg-orange">
                  <i class="fas fa-users"></i>
                </div>
                <div class="card-wrap float-right">
                  <div class="card-header">
                    <h4>Customers</h4>
                  </div>
                  <div class="card-body">
                    15.07k
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-12">
              <div class="card card-statistic-2">
                <div class="card-icon shadow-primary bg-green">
                  <i class="fas fa-ticket-alt"></i>
                </div>
                <div class="card-wrap float-right">
                  <div class="card-header">
                    <h4>Ticket Resolved</h4>
                  </div>
                  <div class="card-body">
                    578
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-12">
              <div class="card card-statistic-2">
                <div class="card-icon shadow-primary bg-blue">
                  <i class="fas fa-dollar-sign"></i>
                </div>
                <div class="card-wrap float-right">
                  <div class="card-header">
                    <h4>Revenue Today</h4>
                  </div>
                  <div class="card-body">
                    $11.61k
                  </div>
                </div>
              </div>
            </div>
          </div>
          
          <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-12">
              <div class="card card-statistic-2">
                <img style="padding: 5px;  position: inherit;" src="{{asset('assets/img/banner/2.png')}}">
                <div class="card-wrap float-right">
                  <div class="card-header">
                    <h4>New Orders</h4>
                  </div>
                  <div class="card-body">
                    3,243
                  </div>
                </div>
              </div>
            </div>            
            <div class="col-lg-3 col-md-3 col-sm-12">
              <div class="card card-statistic-2">
                <img style="padding: 5px;  position: inherit;" src="{{asset('assets/img/banner/3.png')}}">
                <div class="card-wrap float-right">
                  <div class="card-header">
                    <h4>New Orders</h4>
                  </div>
                  <div class="card-body">
                    3,243
                  </div>
                </div>
              </div>
            </div>            
            <div class="col-lg-3 col-md-3 col-sm-12">
              <div class="card card-statistic-2">
                <img style="padding: 5px; position: inherit;" src="{{asset('assets/img/banner/4.png')}}">
                <div class="card-wrap float-right">
                  <div class="card-header">
                    <h4>New Orders</h4>
                  </div>
                  <div class="card-body">
                    3,243
                  </div>
                </div>
              </div>
            </div>            
            <div class="col-lg-3 col-md-3 col-sm-12">
              <div class="card card-statistic-2">
                <img style="padding: 5px;  position: inherit;" src="{{asset('assets/img/banner/1.png')}}">
                <div class="card-wrap float-right">
                  <div class="card-header">
                    <h4>New Orders</h4>
                  </div>
                  <div class="card-body">
                    3,243
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-12 col-sm-12 col-lg-8">
              <div class="card">
                <div class="card-header">
                  <h4>Chart</h4>
                </div>
                <div class="card-body">
                  <div id="schart2"></div>
                  <div class="row">
                    <div class="col-4">
                      <p class="text-muted font-15 text-truncate">Target</p>
                      <h5>
                        <i class="fas fa-arrow-circle-up col-green m-r-5"></i>$15.3k
                      </h5>
                    </div>
                    <div class="col-4">
                      <p class="text-muted font-15 text-truncate">Last
                        week</p>
                      <h5>
                        <i class="fas fa-arrow-circle-down col-red m-r-5"></i>$2.8k
                      </h5>
                    </div>
                    <div class="col-4">
                      <p class="text-muted text-truncate">Last
                        Month</p>
                      <h5>
                        <i class="fas fa-arrow-circle-up col-green m-r-5"></i>$12.5k
                      </h5>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            
            <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4">
              <div class="card">
                <div class="card-header">
                  <h4>Team</h4>
                </div>
                <div class="card-body">
                  <div class="media-list position-relative">
                    <div class="table-responsive" id="project-team-scroll">
                      <table class="table table-hover table-xl mb-0">
                        <thead>
                          <tr>
                            <th>Project Name</th>
                            <th>Employees</th>
                            <th>Cost</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td class="text-truncate">Project X</td>
                            <td class="text-truncate">
                              <ul class="list-unstyled order-list m-b-0">
                                <li class="team-member team-member-sm"><img class="rounded-circle"
                                    src="assets/img/users/user-8.png" alt="user" data-toggle="tooltip" title=""
                                    data-original-title="Wildan Ahdian"></li>
                                <li class="team-member team-member-sm"><img class="rounded-circle"
                                    src="assets/img/users/user-9.png" alt="user" data-toggle="tooltip" title=""
                                    data-original-title="John Deo"></li>
                                <li class="team-member team-member-sm"><img class="rounded-circle"
                                    src="assets/img/users/user-10.png" alt="user" data-toggle="tooltip" title=""
                                    data-original-title="Sarah Smith"></li>
                                <li class="avatar avatar-sm"><span class="badge badge-primary">+3</span></li>
                              </ul>
                            </td>
                            <td class="text-truncate">$8999</td>
                          </tr>
                          <tr>
                            <td class="text-truncate">Project AB2</td>
                            <td class="text-truncate">
                              <ul class="list-unstyled order-list m-b-0">
                                <li class="team-member team-member-sm"><img class="rounded-circle"
                                    src="assets/img/users/user-1.png" alt="user" data-toggle="tooltip" title=""
                                    data-original-title="Wildan Ahdian"></li>
                                <li class="team-member team-member-sm"><img class="rounded-circle"
                                    src="assets/img/users/user-3.png" alt="user" data-toggle="tooltip" title=""
                                    data-original-title="John Deo"></li>
                                <li class="team-member team-member-sm"><img class="rounded-circle"
                                    src="assets/img/users/user-2.png" alt="user" data-toggle="tooltip" title=""
                                    data-original-title="Sarah Smith"></li>
                                <li class="avatar avatar-sm"><span class="badge badge-primary">+1</span></li>
                              </ul>
                            </td>
                            <td class="text-truncate">$5550</td>
                          </tr>
                          <tr>
                            <td class="text-truncate">Project DS3</td>
                            <td class="text-truncate">
                              <ul class="list-unstyled order-list m-b-0">
                                <li class="team-member team-member-sm"><img class="rounded-circle"
                                    src="assets/img/users/user-5.png" alt="user" data-toggle="tooltip" title=""
                                    data-original-title="Wildan Ahdian"></li>
                                <li class="team-member team-member-sm"><img class="rounded-circle"
                                    src="assets/img/users/user-9.png" alt="user" data-toggle="tooltip" title=""
                                    data-original-title="Sarah Smith"></li>
                                <li class="avatar avatar-sm"><span class="badge badge-primary">+4</span></li>
                              </ul>
                            </td>
                            <td class="text-truncate">$9000</td>
                          </tr>
                          <tr>
                            <td class="text-truncate">Project XCD</td>
                            <td class="text-truncate">
                              <ul class="list-unstyled order-list m-b-0">
                                <li class="team-member team-member-sm"><img class="rounded-circle"
                                    src="assets/img/users/user-8.png" alt="user" data-toggle="tooltip" title=""
                                    data-original-title="Wildan Ahdian"></li>
                                <li class="team-member team-member-sm"><img class="rounded-circle"
                                    src="assets/img/users/user-3.png" alt="user" data-toggle="tooltip" title=""
                                    data-original-title="John Deo"></li>
                                <li class="team-member team-member-sm"><img class="rounded-circle"
                                    src="assets/img/users/user-5.png" alt="user" data-toggle="tooltip" title=""
                                    data-original-title="Sarah Smith"></li>
                                <li class="avatar avatar-sm"><span class="badge badge-primary">+2</span></li>
                              </ul>
                            </td>
                            <td class="text-truncate">$7500</td>
                          </tr>
                          <tr>
                            <td class="text-truncate">Project Z2</td>
                            <td class="text-truncate">
                              <ul class="list-unstyled order-list m-b-0">
                                <li class="team-member team-member-sm"><img class="rounded-circle"
                                    src="assets/img/users/user-8.png" alt="user" data-toggle="tooltip" title=""
                                    data-original-title="Wildan Ahdian"></li>
                                <li class="team-member team-member-sm"><img class="rounded-circle"
                                    src="assets/img/users/user-10.png" alt="user" data-toggle="tooltip" title=""
                                    data-original-title="Sarah Smith"></li>
                                <li class="avatar avatar-sm"><span class="badge badge-primary">+3</span></li>
                              </ul>
                            </td>
                            <td class="text-truncate">$8500</td>
                          </tr>
                          <tr>
                            <td class="text-truncate">Project GTe</td>
                            <td class="text-truncate">
                              <ul class="list-unstyled order-list m-b-0">
                                <li class="team-member team-member-sm"><img class="rounded-circle"
                                    src="assets/img/users/user-3.png" alt="user" data-toggle="tooltip" title=""
                                    data-original-title="Wildan Ahdian"></li>
                                <li class="team-member team-member-sm"><img class="rounded-circle"
                                    src="assets/img/users/user-5.png" alt="user" data-toggle="tooltip" title=""
                                    data-original-title="Sarah Smith"></li>
                                <li class="avatar avatar-sm"><span class="badge badge-primary">+3</span></li>
                              </ul>
                            </td>
                            <td class="text-truncate">$8500</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
              <div class="card">
                <div class="card-header">
                  <h4>Today Booking List</h4>
                </div>
                <div class="card-body">
                  <div class="table-responsive" id="proTeamScroll">
                    <table class="table table-striped">
                      <thead>
                        <tr>
                          <th>Cust.</th>
                          <th>Project</th>
                          <th>Assign Date</th>
                          <th>Team</th>
                          <th>Priority</th>
                          <th>Status</th>
                          <th>Edit</th>
                        </tr>
                      </thead>
                      <tr>
                        <td class="table-img"><img src="assets/img/users/user-8.png" alt="">
                        </td>
                        <td>
                          <h6 class="mb-0 font-13">Wordpress Website</h6>
                          <p class="m-0 font-12">
                            Assigned to<span class="col-green font-weight-bold"> Airi Satou</span>
                          </p>
                        </td>
                        <td>20-02-2018</td>
                        <td class="text-truncate">
                          <ul class="list-unstyled order-list m-b-0">
                            <li class="team-member team-member-sm"><img class="rounded-circle"
                                src="assets/img/users/user-8.png" alt="user" data-toggle="tooltip" title=""
                                data-original-title="Wildan Ahdian"></li>
                            <li class="team-member team-member-sm"><img class="rounded-circle"
                                src="assets/img/users/user-9.png" alt="user" data-toggle="tooltip" title=""
                                data-original-title="John Deo"></li>
                            <li class="team-member team-member-sm"><img class="rounded-circle"
                                src="assets/img/users/user-10.png" alt="user" data-toggle="tooltip" title=""
                                data-original-title="Sarah Smith"></li>
                            <li class="avatar avatar-sm"><span class="badge badge-primary">+4</span></li>
                          </ul>
                        </td>
                        <td>
                          <div class="badge-outline col-red">High</div>
                        </td>
                        <td class="align-middle">
                          <div class="progress-text">50%</div>
                          <div class="progress" data-height="6">
                            <div class="progress-bar bg-success" data-width="50%"></div>
                          </div>
                        </td>
                        <td>
                          <a data-toggle="tooltip" title="" data-original-title="Edit"><i
                              class="fas fa-pencil-alt"></i></a>
                          <a data-toggle="tooltip" title="" data-original-title="Delete"><i
                              class="far fa-trash-alt"></i></a>
                        </td>
                      </tr>
                  

                    </table>
                  </div>
                </div>
              </div>
            </div>

          </div>

      

          <div style="display:none" id="lineChart"></div>
          
          <div  style="display:none"  id="barChart"></div>
   
          
        </section>
       
      </div>
      @endsection