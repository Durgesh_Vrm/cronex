@extends('layouts.app')
@section('content')
<style type="text/css">
  .card-header.note-toolbar .dropdown-menu {
    min-width: 150px; 
}
i.fab{
  color: #ffffff;
}
</style>

      <!-- Main Content -->
      <div class="main-content">
        <section class="section">
          <ul class="breadcrumb breadcrumb-style ">
            <li class="breadcrumb-item">
              <a href="index.html">
                <i data-feather="home"></i></a>
            </li>
            <li class="breadcrumb-item">Profile</li>
          </ul>
          <div class="section-body">
            <div class="row mt-sm-4">
              <div class="col-12 col-md-12 col-lg-4">
                <div class="card author-box">
                  <div class="card-body">
                    <div class="author-box-center">
                      <img alt="image" id="user-image" src="{{asset('uplode/users/'.Auth::user()->image)}}" class="rounded-circle author-box-picture img-thumbnail img-responsive">
                      <div class="clearfix"></div>
                      <div class="author-box-name">
                        <a href="#">{{ucwords(Auth::user()->name)}}</a>
                      </div>
                      <div class="author-box-job"><b>{{$data['Rol']}}</b></div>
                    </div>
                    <div class="text-center">
                      <div class="mb-2 mt-3">
                        <div class="text-small font-weight-bold">Social Media</div>
                      </div>
                      <a href="{{Auth::user()->facebook}}" class="btn btn-social-icon mr-1 btn-facebook">
                        <i class="fab fa-facebook-f"></i>
                      </a>
                      <a href="{{Auth::user()->twitter}}" class="btn btn-social-icon mr-1 btn-twitter">
                        <i class="fab fa-twitter"></i>
                      </a>
                      <a href="{{Auth::user()->linkedin}}" class="btn btn-social-icon mr-1 btn-github">
                        <i class="fab fa-linkedin"></i>
                      </a>
                      <a href="{{Auth::user()->instagram}}" class="btn btn-social-icon mr-1 btn-instagram">
                        <i class="fab fa-instagram"></i>
                      </a>
                      <div class="w-100 d-sm-none"></div>
                    </div>
                  </div>
                </div>
                <div class="card">
                  <div class="card-header">
                    <h4>Personal Details</h4>
                  </div>
                  <div class="card-body gallery">
                    <div class="py-4 ">
                      <p class="clearfix">
                        <span class="float-left">
                          Gender
                        </span>
                        <span class="float-right text-muted">
                          {{Auth::user()->gander}}
                        </span>
                      </p>
                      <p class="clearfix">
                        <span class="float-left">
                          Registration Number
                        </span>
                        <span class="float-right text-muted">
                          {{ Auth::user()->reg_number}}
                        </span>
                      </p>
                      <p class="clearfix">
                        <span class="float-left">
                          Birthday
                        </span>
                        <span class="float-right text-muted">
                          {{Auth::user()->dob}}
                        </span>
                      </p>
                      <p class="clearfix">
                        <span class="float-left">
                          Phone No.
                        </span>
                        <span class="float-right text-muted">
                          +91 {{Auth::user()->phone_no}}
                        </span>
                      </p>
                      <p class="clearfix">
                        <span class="float-left">
                          Mail
                        </span>
                        <span class="float-right text-muted">
                          {{Auth::user()->email}}
                        </span>
                      </p>
                      <p class="clearfix">
                        <span class="float-left">
                          Aadhar Card
                        </span>
                        <span class="float-right text-muted">
                         <a style="width: 170px;  height: 20px;  text-align: right;" class="gallery-item  text-muted" data-title="Image 1" href="{{asset('uplode/aadhar_card/'.$data['kyc']->aadhaar_proof_img)}}" title="Image 1">{{$data['kyc']->aadhaar_proof_no}} </a>
                        </span>
                      </p>
                      <p class="clearfix">
                        <span class="float-left">
                          Pan Card
                        </span>
                        <span class="float-right text-muted">
                         <a style="width: 170px;  height: 20px;  text-align: right;" class="gallery-item  text-muted" data-title="Image 1" href="{{asset('uplode/pan/'.$data['kyc']->pan_img)}}" title="Image 1">{{$data['kyc']->pan_no}} </a>
                        </span>
                      </p>
                      <p class="clearfix">
                        <span class="float-left">
                          Other Id Card
                        </span>
                        <span class="float-right text-muted">
                          <a style="width: 170px; height: 20px; text-align: right;" class="gallery-item  text-muted" data-title="Image 1" href="{{asset('uplode/idcard/'.$data['kyc']->id_proof_img)}}" title="Image 1">{{$data['kyc']->id_proof_no}} </a>
                        </span>
                      </p>
                    </div>
                  </div>
                </div>
                <div class="card">
                  <div class="card-header">
                    <h4>Address</h4>
                  </div>
                  <div class="card-body">
                    <ul class="list-unstyled user-progress list-unstyled-border list-unstyled-noborder">
                      @foreach($data['address'] as $value)
                      <li class="media">
                        <div class="media-body">
                          <div class="media-title"><i class="fas @if($value->type == 'Home') {{'fa-home'}} @elseif($value->type == 'Office') {{'fa-hotel'}} @else {{'fa-map-marked-alt'}} @endif fa-md"></i></div>
                        </div>
                        <div class="p-l-30">
                          {{$value->address. $value->street.' '. $value->city.' '. $value->country.' '. $value->zip}}
                        </div>
                      </li>
                      @endforeach
                    </ul>
                  </div>
                </div>
              </div>
              <div class="col-12 col-md-12 col-lg-8">
                <div class="card">
                  <div class="padding-20">
                    <ul class="nav nav-tabs" id="myTab2" role="tablist">
                      <li class="nav-item">
                        <a class="nav-link @if($name == 'Setting') @else active @endif" id="home-tab2" data-toggle="tab" href="#about" role="tab"
                          aria-selected="true">About</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" id="profile-tab2" data-toggle="tab" href="#settings" role="tab"
                          aria-selected="false">Profile Setting</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" id="document-tab2" data-toggle="tab" href="#document" role="tab"
                          aria-selected="false">Document</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" id="profile-image-tab2" data-toggle="tab" href="#profile-image" role="tab"
                          aria-selected="false">Profile Image</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" id="address-tab2" data-toggle="tab" href="#address" role="tab"
                          aria-selected="false">Address</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" id="education-tab2" data-toggle="tab" href="#education" role="tab"
                          aria-selected="false">Education</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" id="experience-tab2" data-toggle="tab" href="#experience" role="tab"
                          aria-selected="false">Experience</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link @if($name == 'Setting') active @else @endif" id="setting-tab2" data-toggle="tab" href="#setting" role="tab"
                          aria-selected="false">Setting</a>
                      </li>
                    </ul>
                    <div class="tab-content tab-bordered" id="myTab3Content">
                      <div class="tab-pane fade @if($name == 'Setting') @else show active table-responsive @endif" id="about" role="tabpanel" aria-labelledby="home-tab2">
                        <div class="row">
                          <div class="col-md-3 col-6 b-r">
                            <strong>Father's Name</strong>
                            <br>
                            <p class="text-muted">{{ucwords(Auth::user()->father_name)}}</p>
                          </div>
                          <div class="col-md-3 col-6 b-r">
                            <strong>Mother's Name</strong>
                            <br>
                            <p class="text-muted">{{Auth::user()->mother_name}}</p>
                          </div>
                          <div class="col-md-3 col-6 b-r">
                            <strong>Mobile</strong>
                            <br>
                            <p class="text-muted">{{Auth::user()->other_phone_no}}</p>
                          </div>
                          <div class="col-md-3 col-6">
                            <strong>Location</strong>
                            <br>
                            <p class="text-muted">India</p>
                          </div>
                        </div>
                        <div class="section-title">Bio</div>
                        <p class="m-t-30">{!! Auth::user()->bio !!}</p>
                        <div class="section-title">Education</div>
                        <table class="table table-sm">
                          <tr>
                            <th>Education</th>
                            <th>Duration</th>
                            <th>%</th>
                            <th>Passing Year</th>
                            <th>School/College</th>
                          </tr>
                          @foreach($data['qualification'] as $value)
                          <tr>
                            <td>{{$value->qulification}}</td>
                            <td>{{$value->duration}}</td>
                            <td>{{$value->percentage}}</td>
                            <td>{{$value->pass_year}}</td>
                            <td>{{$value->collage}}</td>
                            <td>{{$value->desc}}</td>
                          </tr>
                          @endforeach
                        </table>
                        <div class="section-title">Experience</div>
                        <table class="table table-sm">
                          <tr>
                            <th>Company</th>
                            <th>Role</th>
                            <th>Duration</th>
                            <th>Job Note</th>
                            <th>Date Of Employment</th>
                          </tr>
                          @foreach($data['experience'] as $value)
                          <tr>
                            <td>{{$value->company}}</td>
                            <td>{{$value->role}}</td>
                            <td>{{$value->time}} Year</td>
                            <td>{{$value->job_note}}</td>
                            <td>{{$value->date_of_employment}}</td>
                          </tr>
                          @endforeach
                        </table>
                      </div>
                      <div class="tab-pane fade" id="settings" role="tabpanel" aria-labelledby="profile-tab2">
                        <form method="post" action="{{URL('update-profile')}}" class="needs-validation" id="form_pro">
                          @csrf
                          <div class="card-header">
                            <h4>Edit Profile</h4>
                          </div>
                          <div class="card-body">
                            <div class="row">
                              <div class="form-group col-md-6 col-12">
                                <label>Name</label>
                                <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{Auth::user()->name}}" required>
                                @error('name')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                              </div>

                              <div class="form-group col-md-6 col-12">
                                <label>Father's Name</label>
                                <input type="text" class="form-control @error('father_name') is-invalid @enderror" name="father_name" value="{{Auth::user()->father_name}}">
                                @error('father_name')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                              </div>

                              <div class="form-group col-md-6 col-12">
                                <label>Mother's Name</label>
                                <input type="text" class="form-control @error('mother_name') is-invalid @enderror" name="mother_name" value="{{Auth::user()->mother_name}}">
                                @error('mother_name')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                              </div>
                              <div class="form-group col-md-6 col-12">
                                <label>Gender</label>
                                    <div class="selectgroup w-100">
                                      <label class="selectgroup-item">
                                        <input type="radio" name="gender" value="1" class="selectgroup-input-radio @error('gender') is-invalid @enderror" {{Auth::user()->gander == 'Male'? 'checked':''}} required>
                                        <span class="selectgroup-button">Male</span>
                                      </label>
                                      <label class="selectgroup-item">
                                        <input type="radio" name="gender" value="2" class="selectgroup-input-radio @error('gender') is-invalid @enderror" {{Auth::user()->gander == 'Female'? 'checked':''}}>
                                        <span class="selectgroup-button">Female</span>
                                      </label>
                                      <label class="selectgroup-item">
                                        <input type="radio" name="gender" value="3" class="selectgroup-input-radio @error('gender') is-invalid @enderror" {{Auth::user()->gander == 'Other'? 'checked':''}}>
                                        <span class="selectgroup-button" >Other</span>
                                      </label>
                                    </div>
                                @error('gender')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                              </div>
                              <div class="form-group col-md-6 col-12">
                                <label>Birthdate</label>
                                <input type="date" class="form-control @error('birthdate') is-invalid @enderror" name="birthdate" value="{{Auth::user()->dob}}" required>
                                @error('birthdate')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                              </div>
                            </div>
                            <div class="row">
                              <div class="form-group col-md-6 col-12">
                                <label>Phone</label>
                                <input type="tel" class="form-control @error('phone_number') is-invalid @enderror" name="phone_number" value="{{Auth::user()->phone_no}}">
                                @error('phone_number')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                              </div>
                              <div class="form-group col-md-6 col-12">
                                <label>Other Phone</label>
                                <input type="tel" class="form-control @error('other_phone_number') is-invalid @enderror" name="other_phone_number" value="{{Auth::user()->other_phone_no}}">
                              </div>
                                @error('other_phone_number')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="row">
                              <div class="form-group col-md-6 col-12">
                                <label>Facebook</label>
                                <input type="text" class="form-control @error('facebook') is-invalid @enderror" name="facebook" value="{{Auth::user()->facebook}}">
                                @error('facebook')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                              </div>
                              <div class="form-group col-md-6 col-12">
                                <label>Twitter</label>
                                <input type="tel" class="form-control @error('twitter') is-invalid @enderror" name="twitter" value="{{Auth::user()->twitter}}">
                                @error('twitter')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                              </div>
                              <div class="form-group col-md-6 col-12">
                                <label>Instagram</label>
                                <input type="tel" class="form-control @error('instagram') is-invalid @enderror" name="instagram" value="{{Auth::user()->instagram}}">
                                @error('instagram')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                              </div>
                              <div class="form-group col-md-6 col-12">
                                <label>Linkedin</label>
                                <input type="tel" class="form-control @error('linkedin') is-invalid @enderror" name="linkedin" value="{{Auth::user()->linkedin}}">
                                @error('linkedin')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                              </div>
                            </div>
                            <div class="row">
                              <div class="form-group col-12" >
                                <label>Bio</label>
                                <textarea class="summernote-simple @error('bio') is-invalid @enderror" name="bio">{{Auth::user()->bio}}</textarea>
                                @error('bio')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                              </div>
                            </div>
                            <div class="row">
                              <div class="form-group mb-0 col-12">
                                <div class="custom-control custom-checkbox">
                                  <input type="checkbox" name="remember" class="custom-control-input @error('remember') is-invalid @enderror" id="newsletter" checked disabled>
                                  <label class="custom-control-label" for="newsletter">I agree with <a href="">App_Name</a> <a href="">Terms Conditions</a>  and <a href="">Privacy Policy</a> </label>
                                  <div class="text-muted form-text">
                                    You will get new information about products, offers and promotions
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="card-footer text-right">
                            <button class="btn btn-primary" onclick="SubmitForm('form_pro');" type="button"><i class="fas fa-save"></i> Save Changes</button>
                          </div>
                        </form>
                      </div>   
                      <div class="tab-pane fade" id="document" role="tabpanel" aria-labelledby="document-tab2">
                        <form method="post" class="needs-validation" id="form_Docum">
                          <div class="card-header">
                            <h4>Edit Document</h4>
                          </div>
                          <div class="card-body" id="aniimated-thumbnials">
                            <div class="row">
                              <div class="form-group col-md-3 col-12">
                                <div class="list-unstyled clearfix">
                                  @if($data['kyc']->aadhaar_proof_veryfy == 'Yes')
                                  <div class="verify-green badge-outline col-green"><i class="far fa-check-circle"> </i> Verify</div>
                                  @else
                                  <div class="verify-red badge-outline col-red"><i class="far fa-times-circle"> </i> {{$data['kyc']->aadhaar_proof_veryfy}} Verify</div>
                                  @endif
                                    <a href="{{asset('uplode/aadhar_card/'.$data['kyc']->aadhaar_proof_img)}}" data-sub-html="Demo Description" style="margin-top: -14px;">
                                      <img style="width: 150px;" class="img-responsive thumbnail img-thumbnail img-responsive" src="{{asset('uplode/aadhar_card/'.$data['kyc']->aadhaar_proof_img)}}" alt="">
                                    </a>
                                </div>
                              </div>
                              <div class="form-group col-md-5 col-12">
                                <label>Aadhar Card Number</label>
                                <input type="number" class="form-control" value="{{$data['kyc']->aadhaar_proof_no}}" {{ $data['kyc']->aadhaar_proof_veryfy == 'Yes'? 'disabled':'' }} placeholder="*******">
                                <div class="invalid-feedback">
                                  Please fill in the email
                                </div>
                              </div>
                              <div class="form-group col-md-4 col-12">
                                <label>Aadhar Card Front</label>
                                <div class="custom-file">
                                <input type="file" class="custom-file-input" id="customAadhar" {{ $data['kyc']->aadhaar_proof_veryfy == 'Yes'? 'disabled':'' }}>
                                <label class="custom-file-label" for="customAadhar">Choose Aadhar Card</label>
                              </div>
                              </div>
                            </div> 
                            <div class="row">
                              <div class="form-group col-md-3 col-12">
                                <div class="list-unstyled clearfix" >
                                  @if($data['kyc']->aadhaar_proof_veryfy == 'Yes')
                                  <div class="verify-green badge-outline col-green"><i class="far fa-check-circle"> </i> Verify</div>
                                  @else
                                  <div class="verify-red badge-outline col-red"><i class="far fa-times-circle"> </i> {{$data['kyc']->aadhaar_proof_veryfy}} Verify</div>
                                  @endif
                                    <a href="{{asset('uplode/aadhar_card/'.$data['kyc']->aadhaar_proof_img_back)}}" data-sub-html="Demo Description" style="margin-top: -14px;">
                                      <img style="width: 150px;" class="img-responsive thumbnail img-thumbnail img-responsive" src="{{asset('uplode/aadhar_card/'.$data['kyc']->aadhaar_proof_img_back)}}" alt="">
                                    </a>
                                </div>
                              </div>
                              <div class="form-group col-md-5 col-12">
                                <label>VID Number</label>
                                <input type="number" class="form-control" value="{{$data['kyc']->aadhaar_proof_vid}}" placeholder="" {{ $data['kyc']->aadhaar_proof_veryfy == 'Yes'? 'disabled':'' }}>
                                <div class="invalid-feedback">
                                  Please fill in the email
                                </div>
                              </div>
                              <div class="form-group col-md-4 col-12">
                                <label>Aadhar Card Back</label>
                                <div class="custom-file">
                                <input type="file" class="custom-file-input" id="customAa" {{ $data['kyc']->aadhaar_proof_veryfy == 'Yes'? 'disabled':'' }}>
                                <label class="custom-file-label" for="customAa">Choose Aadhar Card</label>
                              </div>
                              </div>
                            </div> 
                            <div class="row">
                              <div class="form-group col-md-3 col-12">
                                <div class="list-unstyled clearfix" >
                                  @if($data['kyc']->pan_veryfy == 'Yes')
                                  <div class="verify-green badge-outline col-green"><i class="far fa-check-circle"> </i> Verify</div>
                                  @else
                                  <div class="verify-red badge-outline col-red"><i class="far fa-times-circle"> </i> {{$data['kyc']->pan_veryfy}} Verify</div>
                                  @endif
                                    <a href="{{asset('uplode/pan/'.$data['kyc']->pan_img)}}" data-sub-html="Demo Description" style="margin-top: -14px;">
                                      <img style="width: 150px;" class="img-responsive thumbnail img-thumbnail img-responsive" src="{{asset('uplode/pan/'.$data['kyc']->pan_img)}}" alt="">
                                    </a>
                                </div>
                              </div>
                              <div class="form-group col-md-5 col-12">
                                <label>Pan Card Number</label>
                                <input type="text" class="form-control" value="{{$data['kyc']->pan_no}}" placeholder="" {{ $data['kyc']->pan_veryfy == 'Yes'? 'disabled':'' }}>
                                <div class="invalid-feedback">
                                  Please fill in the email
                                </div>
                              </div>
                              <div class="form-group col-md-4 col-12">
                                <label>Pan Card</label>
                                <div class="custom-file">
                                <input type="file" class="custom-file-input" id="customPan"  {{ $data['kyc']->pan_veryfy == 'Yes'? 'disabled':'' }}>
                                <label class="custom-file-label" for="customPan">Choose Pan Card</label>
                              </div>
                              </div>
                            </div> 
                            <div class="row">
                              <div class="form-group col-md-3 col-12">
                                <div class="list-unstyled clearfix" >
                                  @if($data['kyc']->id_proof_veryfy == 'Yes')
                                  <div class="verify-green badge-outline col-green"><i class="far fa-check-circle"> </i> Verify</div>
                                  @else
                                  <div class="verify-red badge-outline col-red"><i class="far fa-times-circle"> </i> {{$data['kyc']->id_proof_veryfy}} Verify</div>
                                  @endif
                                    <a href="{{asset('uplode/idcard/'.$data['kyc']->id_proof_img)}}" data-sub-html="Demo Description" style="margin-top: -14px;">
                                      <img style="width: 150px;" class="img-responsive thumbnail img-thumbnail img-responsive" src="{{asset('uplode/idcard/'.$data['kyc']->id_proof_img)}}" alt="">
                                    </a>
                                </div>
                              </div>
                              <div class="form-group col-md-5 col-12">
                                <label>Choose ID Type And Enter ID No</label>
                                <div class="input-group">
                                <select class="custom-select" id="inputGroupSelect05">
                                  <option {{ $data['kyc']->id_proof_veryfy == 'Yes'? 'disabled':'' }}>Choose...</option>

                                  <option value="1" {{ $data['kyc']->id_proof_type == "Indian passport"? 'selected':'' }} {{ $data['kyc']->id_proof_veryfy == 'Yes'? 'disabled':'' }}>Indian passport</option>

                                  <option value="2" {{ $data['kyc']->id_proof_type == "Bank/ Kisan/ Post Office Passbooks"? 'selected':'' }} {{ $data['kyc']->id_proof_veryfy == 'Yes'? 'disabled':'' }}>Bank/ Kisan/ Post Office Passbooks</option>

                                  <option value="3" {{ $data['kyc']->id_proof_type == "Gas Connection Bill"? 'selected':'' }} {{ $data['kyc']->id_proof_veryfy == 'Yes'? 'disabled':'' }}>Gas Connection Bill</option>

                                  <option value="4" {{ $data['kyc']->id_proof_type == "Driving license"? 'selected':'' }} {{ $data['kyc']->id_proof_veryfy == 'Yes'? 'disabled':'' }}>Driving license</option>

                                  <option value="5" {{ $data['kyc']->id_proof_type == "Service Identity Card issued by State/Central Government"? 'selected':'' }} {{ $data['kyc']->id_proof_veryfy == 'Yes'? 'disabled':'' }}>Service Identity Card issued by State/Central Government</option>

                                  <option value="6" {{ $data['kyc']->id_proof_type == "Electoral Photo Identity Card (EPIC)"? 'selected':'' }} {{ $data['kyc']->id_proof_veryfy == 'Yes'? 'disabled':'' }}>Electoral Photo Identity Card (EPIC)</option>
                                </select>
                                <input type="text" value="{{$data['kyc']->id_proof_no}}" class="form-control" {{ $data['kyc']->id_proof_veryfy == 'Yes'? 'disabled':'' }}>
                              </div>
                              </div>
                              <div class="form-group col-md-4 col-12">
                                <label>Id Card</label>
                                <div class="custom-file">
                                <input type="file" class="custom-file-input" id="customCard" {{ $data['kyc']->id_proof_veryfy == 'Yes'? 'disabled':'' }}>
                                <label class="custom-file-label" for="customCard">Choose ID Card</label>
                              </div>
                              </div>
                            </div>
                          </div>
                          <div class="card-footer text-right">
                            <button class="btn btn-primary" type="button" onclick="SubmitForm('form_Docum');"><i class="fas fa-save"></i> Save Changes</button>
                          </div>
                        </form>
                      </div>

                      <div class="tab-pane fade" id="profile-image" role="tabpanel" aria-labelledby="profile-image-tab2">
                        <form method="post" action="{{URL('user-profile-uplode')}}" class="needs-validation" enctype="multipart/form-data" id="form_uplode">
                          @csrf
                          <div class="card-header">
                            <h4>Edit Profile</h4>
                          </div>
                          <div class="card-body">
                              <div class="row">
                                <div class="col-sm-2 col-lg-4 col-mb-2 mb-md-0"></div>
                                <div class="col-sm-8 col-lg-4 col-mb-6 mb-md-0">
                                <div class="avatar-item">
                                  <img alt="image" src="{{asset('uplode/users/'.Auth::user()->image)}}" class="img-fluid img-thumbnail img-responsive" data-toggle="tooltip" title="{{Auth::user()->nmae}}" data-original-title="{{Auth::user()->nmae}}">
                                </div>
                              </div>
                              <div class="col-md-4 col-lg-4"></div>
                              <div class="col-md-4 col-lg-4"></div>
                              <div class="form-group col-sm-12 col-md-4 col-lg-4">
                                <div class="custom-file">
                                <input type="file" class="custom-file-input" name="profile-photo" id="customFiles">
                                <label class="custom-file-label" for="customFiles">Choose Image</label>
                              </div>
                              </div>
                            </div> 
                          </div>
                          <div class="card-footer text-right">
                            <button class="btn btn-primary" type="button" onclick="SubmitForm('form_uplode');"><i class="fas fa-save"></i> Save Changes</button>
                          </div>
                        </form>
                      </div> 

                      <div class="tab-pane fade" id="address" role="tabpanel" aria-labelledby="address-tab2">
                        <form method="post" class="needs-validation">
                          <div class="card-header">
                            <h4>Manage Address</h4>
                          </div>
                          <div class="card-body">
                            <ul class="list-unstyled user-progress list-unstyled-border list-unstyled-noborder">
                              @foreach($data['address'] as $value)
                              <li class="media">
                                <div>
                                  <div class="media-title"><i class="fas @if($value->type == 'Home') {{'fa-home'}} @elseif($value->type == 'Office') {{'fa-hotel'}} @else {{'fa-map-marked-alt'}} @endif fa-md"></i></div>
                                </div>
                                <div class="p-l-30 text-left">
                                  {{$value->address. $value->street.' '. $value->city.' '. $value->country.' '. $value->zip}} &emsp;&emsp;
                                <a href=""> <i class="far fa-edit text-info"></i></a>
                                </div>
                              </li>
                              @endforeach
                            </ul>
                          </div>
                          <div class="card-footer text-right">
                            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#exampleModal"><i class="fas fa-plus" ></i> New Address</button>
                          </div>
                        </form>
                      </div> 

                      <div class="tab-pane fade" id="education" role="tabpanel" aria-labelledby="education-tab2">
                        <form method="post" class="needs-validation">
                          <div class="card-header">
                            <h4>Manage Education Info</h4>
                          </div>
                          <div class="card-body">
                            <ul class="list-unstyled user-progress list-unstyled-border list-unstyled-noborder">
                              @foreach($data['address'] as $value)
                              <li class="media">
                                <div>
                                  <div class="media-title"><i class="fas @if($value->type == 'Home') {{'fa-home'}} @elseif($value->type == 'Office') {{'fa-hotel'}} @else {{'fa-map-marked-alt'}} @endif fa-md"></i></div>
                                </div>
                                <div class="p-l-30 text-left">
                                  {{$value->address. $value->street.' '. $value->city.' '. $value->country.' '. $value->zip}} &emsp;&emsp;
                                <a href=""> <i class="far fa-edit text-info"></i></a>
                                </div>
                              </li>
                              @endforeach
                            </ul>
                          </div>
                          <div class="card-footer text-right">
                            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#educationModal"><i class="fas fa-plus" ></i> New Address</button>
                          </div>
                        </form>
                      </div> 

                      <div class="tab-pane fade" id="experience" role="tabpanel" aria-labelledby="experience-tab2">
                        <form method="post" class="needs-validation">
                          <div class="card-header">
                            <h4>Manage Address</h4>
                          </div>
                          <div class="card-body">
                            <ul class="list-unstyled user-progress list-unstyled-border list-unstyled-noborder">
                              @foreach($data['address'] as $value)
                              <li class="media">
                                <div>
                                  <div class="media-title"><i class="fas @if($value->type == 'Home') {{'fa-home'}} @elseif($value->type == 'Office') {{'fa-hotel'}} @else {{'fa-map-marked-alt'}} @endif fa-md"></i></div>
                                </div>
                                <div class="p-l-30 text-left">
                                  {{$value->address. $value->street.' '. $value->city.' '. $value->country.' '. $value->zip}} &emsp;&emsp;
                                <a href=""> <i class="far fa-edit text-info"></i></a>
                                </div>
                              </li>
                              @endforeach
                            </ul>
                          </div>
                          <div class="card-footer text-right">
                            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#experienceModal"><i class="fas fa-plus" ></i> New Address</button>
                          </div>
                        </form>
                      </div>

                      <div class="tab-pane fade  @if($name == 'Setting') show active table-responsive @else @endif" id="setting" role="tabpanel" aria-labelledby="setting-tab2">
                        <form method="post" class="needs-validation" id="form_Manage">
                          <div class="card-header">
                            <h4>Manage Setting</h4>
                          </div>
                          <div class="card-body">
                            <div class="row">
                              <div class="form-group col-md-6 col-lg-6 col-sm-12">
                                <div class="pretty p-switch p-fill">
                                  <input type="checkbox" id="ShowProfileEveryone" {{ Auth::user()->show_profile_everyone == 'Yes'? 'checked':''}}>
                                  <div class="state p-success">
                                    <label><i class="fas fa-eye"> </i> Show Profile Everyone</label>
                                  </div>
                                </div>
                              </div>
                              <div class="form-group col-md-6 col-lg-6 col-sm-12">
                                <div class="pretty p-switch p-fill">
                                  <input type="checkbox"  id="ActivitiesLog" {{ Auth::user()->activities_recorder == 'Yes'? 'checked':''}}>
                                  <div class="state p-success">
                                    <label><i class="fas fa-eye"> </i> Activities Log</label>
                                  </div>
                                </div>
                              </div>
                              <div class="form-group col-md-6 col-lg-6 col-sm-12">
                                <div class="pretty p-switch p-fill">
                                  <input type="checkbox" name="two_step_verification" id="two_step_verification" {{ $data['TSV'] == 'Yes' ? 'checked':''}}>
                                  <div class="state p-success">
                                    <label><i class="fa fa-lock" aria-hidden="true"></i> Two Step Verification</label>
                                  </div>
                                </div>
                              </div>
                              <div class="form-group col-md-6 col-lg-6 col-sm-12">
                                <div class="pretty p-switch p-fill">
                                  <input type="checkbox" id="Notification" {{ Auth::user()->notification == 'Yes'? 'checked':''}}>
                                  <div class="state p-success">
                                    <label> <i class="fa fa-bell" aria-hidden="true"></i> Notification </label>
                                  </div>
                                </div>
                              </div>
                           
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>

        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="formModal" aria-hidden="true" style="display: none;">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="formModal">Add New Address</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>
              <div class="modal-body">
                <form action="" method="post" id="form_Address">
                            <div class="row">
                            <div class="form-group col-md-12 col-12">
                              <div class="form-group">
                              <label class="form-label">Icon input</label>
                              <div class="selectgroup selectgroup-pills">
                                <label class="selectgroup-item">
                                  <input type="radio" name="icon-input" value="1" class="selectgroup-input">
                                  <span class="selectgroup-button selectgroup-button-icon"><i class="fas fa-home"></i></span>
                                </label>
                                <label class="selectgroup-item">
                                  <input type="radio" name="icon-input" value="3" class="selectgroup-input">
                                  <span class="selectgroup-button selectgroup-button-icon"><i class="fas fa-hotel"></i></span>
                                </label>
                                <label class="selectgroup-item">
                                  <input type="radio" name="icon-input" value="4" class="selectgroup-input">
                                  <span class="selectgroup-button selectgroup-button-icon"><i class="fas fa-map-marked-alt"></i></span>
                                </label>
                              </div>
                            </div>
                                <label>Address</label>
                                <input type="text" class="form-control" value="{{Auth::user()->name}}">
                                <div class="invalid-feedback">
                                  Please fill in the name
                                </div>
                              </div>
                              <div class="form-group col-md-12 col-12">
                                <label>Street Address</label>
                                <input type="text" class="form-control" value="{{Auth::user()->father_name}}">
                                <div class="invalid-feedback">
                                  Please fill in the father's name
                                </div>
                              </div>
                              <div class="form-group col-md-6 col-12">
                                <label>City</label>
                                <select class="form-control">
                                  <option>Please Select</option>
                                </select>
                                <div class="invalid-feedback">
                                  Please fill in the mother's name
                                </div>
                              </div>
                              <div class="form-group col-md-6 col-12">
                                <label>State</label>
                                <select class="form-control">
                                  <option>Please Select</option>
                                </select>
                                <div class="invalid-feedback">
                                  Please fill in the birthdate name
                                </div>
                              </div>
                              <div class="form-group col-md-6 col-12">
                                <label>Postal / Zip Code</label>
                                <input type="text" class="form-control" value="{{Auth::user()->mother_name}}">
                                <div class="invalid-feedback">
                                  Please fill in the mother's name
                                </div>
                              </div>
                              <div class="form-group col-md-6 col-12">
                                <label>Country</label>
                                <select class="form-control">
                                  <option>Please Select</option>
                                </select>
                                <div class="invalid-feedback">
                                  Please fill in the birthdate name
                                </div>
                              </div>
                            </div>
                  <button class="btn btn-primary" type="button" onclick="SubmitForm('form_Address');"> <i class="fas fa-save"></i> Save Changes</button>
                  <button type="button" class="btn btn-danger" data-dismiss="modal" aria-label="Close">
                  <i class="fas fa-times"> </i> Cancel</button>
                </form>
              </div>
            </div>
          </div>
        </div>

        <div class="modal fade" id="educationModal" tabindex="-1" role="dialog" aria-labelledby="formModal" aria-hidden="true" style="display: none;">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="formModal">Add New Education Info</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>
              <div class="modal-body">
                <form action="" method="post" id="form_Education">
                            <div class="row">
                            <div class="form-group col-md-12 col-12">
                              <div class="form-group">
                              <label class="form-label">Icon input</label>
                              <div class="selectgroup selectgroup-pills">
                                <label class="selectgroup-item">
                                  <input type="radio" name="icon-input" value="1" class="selectgroup-input">
                                  <span class="selectgroup-button selectgroup-button-icon"><i class="fas fa-home"></i></span>
                                </label>
                                <label class="selectgroup-item">
                                  <input type="radio" name="icon-input" value="3" class="selectgroup-input">
                                  <span class="selectgroup-button selectgroup-button-icon"><i class="fas fa-hotel"></i></span>
                                </label>
                                <label class="selectgroup-item">
                                  <input type="radio" name="icon-input" value="4" class="selectgroup-input">
                                  <span class="selectgroup-button selectgroup-button-icon"><i class="fas fa-map-marked-alt"></i></span>
                                </label>
                              </div>
                            </div>
                                <label>Address</label>
                                <input type="text" class="form-control" value="{{Auth::user()->name}}">
                                <div class="invalid-feedback">
                                  Please fill in the name
                                </div>
                              </div>
                              <div class="form-group col-md-12 col-12">
                                <label>Street Address</label>
                                <input type="text" class="form-control" value="{{Auth::user()->father_name}}">
                                <div class="invalid-feedback">
                                  Please fill in the father's name
                                </div>
                              </div>
                              <div class="form-group col-md-6 col-12">
                                <label>City</label>
                                <select class="form-control">
                                  <option>Please Select</option>
                                </select>
                                <div class="invalid-feedback">
                                  Please fill in the mother's name
                                </div>
                              </div>
                              <div class="form-group col-md-6 col-12">
                                <label>State</label>
                                <select class="form-control">
                                  <option>Please Select</option>
                                </select>
                                <div class="invalid-feedback">
                                  Please fill in the birthdate name
                                </div>
                              </div>
                              <div class="form-group col-md-6 col-12">
                                <label>Postal / Zip Code</label>
                                <input type="text" class="form-control" value="{{Auth::user()->mother_name}}">
                                <div class="invalid-feedback">
                                  Please fill in the mother's name
                                </div>
                              </div>
                              <div class="form-group col-md-6 col-12">
                                <label>Country</label>
                                <select class="form-control">
                                  <option>Please Select</option>
                                </select>
                                <div class="invalid-feedback">
                                  Please fill in the birthdate name
                                </div>
                              </div>
                            </div>
                  <button class="btn btn-primary" type="button" onclick="SubmitForm('form_Education');"> <i class="fas fa-save"></i> Save Changes</button>
                  <button type="button" class="btn btn-danger" data-dismiss="modal" aria-label="Close">
                  <i class="fas fa-times"> </i> Cancel</button>
                </form>
              </div>
            </div>
          </div>
        </div>

        <div class="modal fade" id="experienceModal" tabindex="-1" role="dialog" aria-labelledby="formModal" aria-hidden="true" style="display: none;">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="formModal">Add New Experience Info</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>
              <div class="modal-body">
                <form action="" method="post" id="form">
                            <div class="row">
                            <div class="form-group col-md-12 col-12">
                              <div class="form-group">
                              <label class="form-label">Icon input</label>
                              <div class="selectgroup selectgroup-pills">
                                <label class="selectgroup-item">
                                  <input type="radio" name="icon-input" value="1" class="selectgroup-input">
                                  <span class="selectgroup-button selectgroup-button-icon"><i class="fas fa-home"></i></span>
                                </label>
                                <label class="selectgroup-item">
                                  <input type="radio" name="icon-input" value="3" class="selectgroup-input">
                                  <span class="selectgroup-button selectgroup-button-icon"><i class="fas fa-hotel"></i></span>
                                </label>
                                <label class="selectgroup-item">
                                  <input type="radio" name="icon-input" value="4" class="selectgroup-input">
                                  <span class="selectgroup-button selectgroup-button-icon"><i class="fas fa-map-marked-alt"></i></span>
                                </label>
                              </div>
                            </div>
                                <label>Address</label>
                                <input type="text" class="form-control" value="{{Auth::user()->name}}">
                                <div class="invalid-feedback">
                                  Please fill in the name
                                </div>
                              </div>
                              <div class="form-group col-md-12 col-12">
                                <label>Street Address</label>
                                <input type="text" class="form-control" value="{{Auth::user()->father_name}}">
                                <div class="invalid-feedback">
                                  Please fill in the father's name
                                </div>
                              </div>
                              <div class="form-group col-md-6 col-12">
                                <label>City</label>
                                <select class="form-control">
                                  <option>Please Select</option>
                                </select>
                                <div class="invalid-feedback">
                                  Please fill in the mother's name
                                </div>
                              </div>
                              <div class="form-group col-md-6 col-12">
                                <label>State</label>
                                <select class="form-control">
                                  <option>Please Select</option>
                                </select>
                                <div class="invalid-feedback">
                                  Please fill in the birthdate name
                                </div>
                              </div>
                              <div class="form-group col-md-6 col-12">
                                <label>Postal / Zip Code</label>
                                <input type="text" class="form-control" value="{{Auth::user()->mother_name}}">
                                <div class="invalid-feedback">
                                  Please fill in the mother's name
                                </div>
                              </div>
                              <div class="form-group col-md-6 col-12">
                                <label>Country</label>
                                <select class="form-control">
                                  <option>Please Select</option>
                                </select>
                                <div class="invalid-feedback">
                                  Please fill in the birthdate name
                                </div>
                              </div>
                            </div>
                  <button class="btn btn-primary" type="button" onclick="SubmitForm('form');"> <i class="fas fa-save"></i> Save Changes</button>
                  <button type="button" class="btn btn-danger" data-dismiss="modal" aria-label="Close">
                  <i class="fas fa-times"> </i> Cancel</button>
                </form>
              </div>
            </div>
          </div>
        </div>

<script type="text/javascript">

$("#ShowProfileEveryone").on("change", function() {
  var show_p = $("#"+this.id).is(":checked");
  $.ajax({
       type:"POST",
       url:"{{url('Change-show-profile')}}",
       data:{"_token": "{{ csrf_token() }}", "id":show_p }, 
       success:function(result)
       {  

       }
    });
});



$("#ActivitiesLog").on("change", function() {
  var show_p = $("#"+this.id).is(":checked");
  $.ajax({
       type:"POST",
       url:"{{url('Change-Activities-Log')}}",
       data:{"_token": "{{ csrf_token() }}", "id":show_p }, 
       success:function(result)
       {    
       }
    });
});

$("#two_step_verification").on("change", function() {
    var id = $('#'+this.id).is(":checked");
  $.ajax({
       type:"POST",
       url:"{{ url('/change-type-tsv') }}",
       data:{"_token": "{{ csrf_token() }}", "id": id}, 
       success:function(result)
       {     
       }
    });
});


$("#Notification").on("change", function() {
  var show_p = $("#"+this.id).is(":checked");
  $.ajax({
       type:"POST",
       url:"{{url('Change-Notification-on')}}",
       data:{"_token": "{{ csrf_token() }}", "id":show_p }, 
       success:function(result)
       {     
       }
    });
});
</script>
@endsection