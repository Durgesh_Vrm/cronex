@extends('layouts.app')
@section('content')

      <!-- Main Content -->
      <div class="main-content">
        <section class="section">
          <ul class="breadcrumb breadcrumb-style">
            <li class="breadcrumb-item">
              <a href="{{url('home')}}">
                <i data-feather="home"></i></a>
            </li>
            <li class="breadcrumb-item">Users</li>
            <li class="breadcrumb-item">User Registration</li>
          </ul>
          <div class="section-body">
            <div class="row">
              <div class="col-12 col-md-12 col-lg-12">
                <div class="card">
                  <div class="card-header">
                    <h4>User Registration</h4>
                  </div>
                  <form method="post" action="{{url('registration')}}" id="form_reg" autocomplete="off" enctype="multipart/form-data">
                    @csrf
                  <div class="card-body">
                      <div class="form-row">
                      <div class="form-group col-md-3">
                        <label for="inputFormNumber">Form Number</label>
                        <input type="email" class="form-control" id="inputFormNumber" value="#{{$data['FNUMBER']}}" readonly>
                      </div>
                      <div class="form-group col-md-3">
                        <label for="inputRegistrationNumber">Registration Number </label>
                        <input type="text" class="form-control" id="inputRegistrationNumber" name="registration_number" value="{{$data['RNUMBER']}}" readonly>
                      </div>
                      <div class="form-group col-md-3">
                        <label for="referral_code">Referral Code </label>
                        <input type="text" class="form-control" id="referral_code" name="referral_code" value="" placeholder="Enter Referral Code">
                                                @error('referral_code')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                      </div>
                      <div class="form-group col-md-3">
                      <label>Registration Date</label>
                      <input type="text" class="form-control" value="{{date('d/M/Y')}}" readonly>
                    </div>

                      <div class="form-group col-md-3">
                        <label>Select Role</label>
                        <select class="custom-select select2 input-sm  @error('role') is-invalid @enderror"  name="role" id="role">
                          <option value="">Select User Role</option>
                          @foreach($data['all'] as $value)
                          @if($value->role_name != 'Administrator')
                          <option value="{{Crypt::encrypt($value->role_id)}}"  {{ old('role') == $value->role_id ? 'selected':''}}>{{$value->role_name}}</option>
                          @endif
                          @endforeach
                        </select>
                        @error('role')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                      </div>

                    </div>
                    <div class="form-row">
                      <div class="form-group col-md-3">
                        <label for="name">Name <code>*</code></label>
                        <input type="text" class="form-control @error('name') is-invalid @enderror" id="name" name="name" value="{{old('name')}}" placeholder="User Name" required>
                      @error('name')
                          <div class="invalid-feedback">{{ $message }}</div>
                      @enderror
                      </div>
                      <div class="form-group col-md-3">
                        <label for="father_name">Father's Name</label>
                        <input type="text" class="form-control  @error('father_name') is-invalid @enderror" id="father_name" name="father_name" value="{{old('father_name')}}" placeholder="Father's Name">
                      @error('father_name')
                          <div class="invalid-feedback">{{ $message }}</div>
                      @enderror
                      </div>
                      <div class="form-group col-md-3">
                        <label for="mother_name">Mother's Name</label>
                        <input type="text" class="form-control  @error('mother_name') is-invalid @enderror" id="mother_name" name="mother_name" value="{{old('mother_name')}}" placeholder="Mother's Name">
                      @error('mother_name')
                          <div class="invalid-feedback">{{ $message }}</div>
                      @enderror
                      </div>
                
                    <div class="form-group col-md-3">
                      <label class="form-label">Gender {{old('gender')}}</label>
                      <div class="selectgroup w-100">
                        <label class="selectgroup-item">
                          <input type="radio" name="gender" id="gender1" value="1" class="selectgroup-input-radio  @error('gender') is-invalid @enderror" {{ old('gender') == 1 ? 'checked':''}} required>
                          <span class="selectgroup-button ">Male</span>
                        </label>
                        <label class="selectgroup-item">
                          <input type="radio" name="gender" id="gender2" value="2" class="selectgroup-input-radio  @error('gender') is-invalid @enderror" {{ old('gender') == 2 ? 'checked':''}}>
                          <span class="selectgroup-button ">Female</span>
                        </label>
                        <label class="selectgroup-item" >
                          <input type="radio" name="gender" id="gender3" value="3" class="selectgroup-input-radio  @error('gender') is-invalid @enderror" {{ old('gender') == 3 ? 'checked':''}}>
                          <span class="selectgroup-button">Other</span>
                        </label>
                      </div>
                      @error('gender')
                          <div class="invalid-feedback">{{ $message }}</div>
                      @enderror
                    </div>
                    <div class="form-group col-md-3 col-6">
                      <label>Date of birth</label>
                      <input type="date" name="date_of_birth" id="date_of_birth" class="form-control  @error('date_of_birth') is-invalid @enderror" value="{{ old('date_of_birth', date('Y-m-d')) }}" required>
                      @error('date_of_birth')
                          <div class="invalid-feedback">{{ $message }}</div>
                      @enderror
                    </div>
                    <div class="form-group  col-md-3 col-6">
                      <label>Phone Number <code>*</code></label>
                      <div class="input-group">
                        <input type="tel" name="phone_number" id="phone_number" class="form-control phone-number  @error('phone_number') is-invalid @enderror" maxlength="10" minlength="10" value="{{old('phone_number')}}" required>
                      @error('phone_number')
                          <div class="invalid-feedback">{{ $message }}</div>
                      @enderror
                      </div>
                    </div>
                    <div class="form-group  col-md-3">
                      <label>Email <code>*</code></label>
                      <div class="input-group">
                        <input type="Email" name="email" id="email" class="form-control phone-number  @error('email') is-invalid @enderror" value="{{old('email')}}" required>
                      @error('email')
                          <div class="invalid-feedback">{{ $message }}</div>
                      @enderror
                      </div>
                    </div>
                    <div class="form-group col-md-6">
                      <label>Address 1</label>
                      <textarea class="form-control @error('Address_1') is-invalid @enderror" name="Address_1" id="Address_1" placeholder="Apartment, studio, or floor ">{{old('Address_1')}}</textarea>
                      @error('Address_1')
                          <div class="invalid-feedback">{{ $message }}</div>
                      @enderror
                    </div>                    
                    <div class="form-group col-md-6">
                      <label>Address 2</label>
                      <textarea class="form-control @error('Address_2') is-invalid @enderror" name="Address_2" id="Address_2"  placeholder="Street">{{old('Address_2')}}</textarea>
                      @error('Address_2')
                          <div class="invalid-feedback">{{ $message }}</div>
                      @enderror
                    </div>
                    
                    </div>
                    <div class="form-row">
                    <div class="form-group col-md-3 col-6">
                      <label>Country</label>
                      <select class="custom-select select2 input-sm  @error('country') is-invalid @enderror"  name="country" id="country">
                        <option value="">Select Country</option>
                        @foreach($data['countries'] as $value)
                        <option value="{{$value->id}}"  {{ old('country') == $value->id ? 'selected':''}}>{{$value->name}}</option>
                        @endforeach
                      </select>
                      @error('country')
                          <div class="invalid-feedback">{{ $message }}</div>
                      @enderror
                    </div>

                    <div class="form-group col-md-3  col-6">
                      <label>State</label>
                      <select class="custom-select select2 input-sm  @error('state') is-invalid @enderror" name="state" id="state">
                        <option value="">Select State</option>
                        @foreach($data['states'] as $value)
                        <option value="{{$value->id}}" {{ old('state') == $value->id ? 'selected':''}}>{{$value->name}}</option>
                        @endforeach
                        <option value="other">Other</option>
                      </select>
                      @error('state')
                          <div class="invalid-feedback">{{ $message }}</div>
                      @enderror
                    </div>

                    <div class="form-group col-md-3  col-6">
                      <label>City</label>
                      <select class="custom-select select2 input-sm  @error('city') is-invalid @enderror" name="city" id="city">
                        <option value="">Select City</option> 
                        @foreach($data['cities'] as $value)
                        <option value="{{$value->id}}" {{ old('city') == $value->id ? 'selected':''}}>{{$value->name}}</option>
                        @endforeach
                        <option value="other">Other</option>
                      </select>
                      @error('city')
                          <div class="invalid-feedback">{{ $message }}</div>
                      @enderror
                    </div>
                      <div class="form-group col-md-1  col-6">
                        <label for="inputZip">Zip</label>
                        <input type="text" class="form-control  @error('zip') is-invalid @enderror" name="zip" id="zip" maxlength="6" minlength="6"  value="{{old('zip')}}">
                      @error('zip')
                          <div class="invalid-feedback">{{ $message }}</div>
                      @enderror
                      </div>
                    </div>
                    <div class="form-row">
                    <div class="form-group col-md-3">
                      <label>Photo <code>Max Size:3MB</code></label>
                      <input type="file" name="profile_photo" id="profile_photo" class="form-control   @error('profile_photo') is-invalid @enderror"  value="{{old('profile_photo')}}">
                      @error('profile_photo')
                          <div class="invalid-feedback">{{ $message }}</div>
                      @enderror
                    </div>
                    <div class="form-group col-md-3">
                      <label>Signature <code>Max Size:3MB</code></label>
                      <input type="file" name="signature" id="signature" class="form-control   @error('signature') is-invalid @enderror"  value="{{old('signature')}}">
                      @error('signature')
                          <div class="invalid-feedback">{{ $message }}</div>
                      @enderror
                    </div>

                    <div class="form-group col-md-3">
                      <label>Password <code>*</code></label>
                      <input type="password" name="password" id="password" class="form-control  @error('password') is-invalid @enderror" autocomplete="on">
                      @error('password')
                          <div class="invalid-feedback">{{ $message }}</div>
                      @enderror
                    </div>
                    <div class="form-group col-md-3">
                      <label>Confirm Password <code>*</code></label>
                      <input type="password" name="password_confirmation" id="password_confirmation" class="form-control  @error('password_confirmation') is-invalid @enderror" autocomplete="on">
                      @error('password_confirmation')
                          <div class="invalid-feedback">{{ $message }}</div>
                      @enderror
                    </div>
                    </div>
                  </div>
                  <center id="c_code">
                  </center>               
                  <div class="card-footer text-center">
                      @error('captcha')
                          <div class="invalid-feedback">{{ $message }}</div>
                      @enderror
                    <button class="btn btn-primary mr-1" type="button"  onclick="SubmitForm('form_reg');"><i class="fas fa-save"></i> Submit</button>
                    <button class="btn btn-danger"  type="reset"><i class="fas fa-sync-alt"></i> Reset</button>
                  </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
<script type="text/javascript">
    $(document).ready(function() {
    captcha_code();
  });
    function captcha_code(){
    var id = this.id;
          $.ajax({
       type:"POST",
       url:"{{ url('/get-captcha-code') }}",
       data:{"_token": "{{ csrf_token() }}"},
       success:function(result)
       {  
          if(result != '')
          {
            $('#c_code').html(result);
          } 
          else
          {
            swal({
              title: "Captcha Code Not Found",
              icon: "warning",
              buttons: true,
              dangerMode: true,
             });
          }    
       }
    });
  }

</script>

@endsection