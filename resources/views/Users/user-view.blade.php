@extends('layouts.app')
@section('content')
  
      <!-- Main Content -->
      <div class="main-content">
        <section class="section">
          <ul class="breadcrumb breadcrumb-style ">
            <li class="breadcrumb-item">
              <a href="{{url('home')}}">
                <i data-feather="home"></i></a>
            </li>
            <li class="breadcrumb-item">Tables</li>
            <li class="breadcrumb-item">Export Tables</li>
          </ul>
          <div class="section-body">
            <div class="row">
              <div class="col-12 col-md-12 col-lg-12">
                <div class="card">
                  <form method="post" action="{{url('search-user')}}" id="form_reg" autocomplete="off" enctype="multipart/form-data">
                    @csrf
                  <div class="card-body">
                    <div class="form-row">
                      <div class="form-group col-md-3">
                        <label for="name">Name <code>*</code></label>
                        <input type="text" class="form-control @error('name') is-invalid @enderror" id="name" name="name" value="{{old('name')}}" placeholder="User Name">
                      @error('name')
                          <div class="invalid-feedback">{{ $message }}</div>
                      @enderror
                      </div>
                      <div class="form-group col-md-3">
                        <label for="father_name">Father's Name</label>
                        <input type="text" class="form-control  @error('father_name') is-invalid @enderror" id="father_name" name="father_name" value="{{old('father_name')}}" placeholder="Father's Name">
                      @error('father_name')
                          <div class="invalid-feedback">{{ $message }}</div>
                      @enderror
                      </div>

                    <div class="form-group col-md-3">
                      <label>From Date</label>
                      <input type="date" name="from_date" id="from_date" class="form-control  @error('from_date') is-invalid @enderror" value="" >
                      @error('from_date')
                          <div class="invalid-feedback">{{ $message }}</div>
                      @enderror
                    </div>
                    <div class="form-group col-md-3">
                      <label>To Date</label>
                      <input type="date" name="to_date" id="to_date" class="form-control  @error('to_date') is-invalid @enderror" value="" >
                      @error('to_date')
                          <div class="invalid-feedback">{{ $message }}</div>
                      @enderror
                    </div>
                    </div>
                  </div>
                  <div class="card-footer text-center">
                    <button class="btn btn-primary" type="submit" ><i class="fas fa-search"></i> Submit</button>
                  </div>
                  </form>
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-12">
                <div class="card">
                  <div class="card-header">
                    <h4>Export Table</h4>
                  </div>
                  <div class="card-body">
                    <div class="table-responsive">
                      <table class="table table-striped table-hover" id="tableExport" style="width:100%;">
                        <thead>
                          <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Father's Name</th>
                            <th>Email</th>
                            <th>Image</th>
                            <th>Gander</th>
                            <th>Date of birth</th>
                            <th>Phone No</th>
                            <th>Created</th>
                            <th>Updated</th>
                            <th>Info</th>
                          </tr>
                        </thead>
                        <tbody>
                          @php $sn=0; @endphp
                          @foreach($data['users'] as $value)
                          <tr>
                            <td>{{$value->reg_number}}</td>
                            <td>{{$value->name}}</td>
                            <td>{{$value->father_name}}</td>
                            <td>{{$value->email}}</a></td>
                            <td>       
                              @if(!$value->image)
                              <img alt="image" src="{{asset('assets/img/user.png')}}"  width="35"> 
                              @else 
                              <img alt="image" src="{{asset('uplode/users/'.$value->image)}}" width="35"> 
                              @endif
                            </td>
                            <td>{{$value->gander}}</td>
                            <td>{{$value->dob}}</td>
                            <td>{{$value->phone_no}}</td>
                            <td>{{$value->created_at}}</td>
                            <td>{{$value->updated_at}}</td>
                            <td><div class="buttons"><a href="{{url('user-information/'.Crypt::encrypt($value->id))}}" class="btn btn-success btn-sm"><i class="fas fa-info-circle"></i></a></div></td>
                          </tr>
                          @endforeach
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      
      </div>

@endsection