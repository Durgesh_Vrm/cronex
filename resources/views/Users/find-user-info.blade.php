@extends('layouts.app')
@section('content')
  
      <!-- Main Content -->
      <div class="main-content">
        <section class="section">
          <ul class="breadcrumb breadcrumb-style ">
            <li class="breadcrumb-item">
              <a href="{{url('home')}}">
                <i data-feather="home"></i></a>
            </li>
            <li class="breadcrumb-item">Users</li>
            <li class="breadcrumb-item">Find Users</li>
          </ul>
          <div class="section-body">
              <div class="row">
              <div class="col-12 col-md-12 col-lg-12">
                <div class="card">
                  <div class="card-body">
                  <div class="col-12 col-md-12 col-lg-12">
                      <form class="form-inline" method="post" action="{{url('find-user')}}">
                        @csrf
                        <input class="form-control" type="search" name="search" value="@if(!empty($data['user'])) {{ $data['user']->reg_number }} @endif" placeholder="Search Users ID" aria-label="Search">
                        <button class="btn btn-default" type="submit"><i class="fas fa-search"></i></button>
                      </form>
                  </div>
                </div>
              </div>
            </div>
           @if(!empty($data['user'])) 
            <div class="row">
              <div class="col-12 col-md-12 col-lg-12">
                <div class="card">
                  <div class="card-header">
                    <h4>User Registration</h4>
                  </div>
                  <div class="card-body">
                      <div class="form-row">
                      <div class="form-group col-md-2">
                        <label for="inputFormNumber">Form Number</label>
                        <input type="email" class="form-control" id="inputFormNumber" value="#{{$data['user']->reg_number}}" readonly>
                      </div>
                      <div class="form-group col-md-2">
                        <label for="inputRegistrationNumber">Registration Number </label>
                        <input type="text" class="form-control" id="inputRegistrationNumber" value="{{$data['user']->reg_number}}" readonly>
                      </div>
                      <div class="form-group col-md-2">
                        <label for="inputRegistrationNumber">Referral Code </label>
                        <input type="text" class="form-control" id="inputRegistrationNumber" value="{{$data['user']->referral_code}}" readonly>
                      </div>
                      <div class="form-group col-md-2">
                      <label>Registration Date</label>
                      <input type="text" class="form-control" value="{{date('d-m-Y' , strtotime($data['user']->created_at))}}" readonly>
                    </div>
                    </div>
                    <div class="form-row">
                      <div class="form-group col-md-3">
                        <label for="name">Name <code>*</code></label>
                        <input type="text" class="form-control @error('name') is-invalid @enderror" id="name" name="name" value="{{$data['user']->name}}" placeholder="User Name" readonly>
                      @error('name')
                          <div class="invalid-feedback">{{ $message }}</div>
                      @enderror
                      </div>
                      <div class="form-group col-md-3">
                        <label for="father_name">Father's Name</label>
                        <input type="text" class="form-control  @error('father_name') is-invalid @enderror" id="father_name" name="father_name" value="{{$data['user']->father_name}}" placeholder="Father's Name" readonly>
                      @error('father_name')
                          <div class="invalid-feedback">{{ $message }}</div>
                      @enderror
                      </div>
                      <div class="form-group col-md-3">
                        <label for="mother_name">Mother's Name</label>
                        <input type="text" class="form-control  @error('mother_name') is-invalid @enderror" id="mother_name" name="mother_name" value="{{$data['user']->mother_name}}" placeholder="Mother's Name" readonly>
                      @error('mother_name')
                          <div class="invalid-feedback">{{ $message }}</div>
                      @enderror
                      </div>
                
                    <div class="form-group col-md-3">
                      <label class="form-label">Gender</label>
                      <div class="selectgroup w-100">
                        <label class="selectgroup-item">
                          <input type="radio" name="gender" id="gender1" value="1" class="selectgroup-input-radio  @error('gender') is-invalid @enderror" {{ $data['user']->gander == 'Male' ? 'checked':''}} disabled>
                          <span class="selectgroup-button ">Male</span>
                        </label>
                        <label class="selectgroup-item">
                          <input type="radio" name="gender" id="gender2" value="2" class="selectgroup-input-radio  @error('gender') is-invalid @enderror" {{ $data['user']->gander == 'Female' ? 'checked':''}} disabled>
                          <span class="selectgroup-button ">Female</span>
                        </label>
                        <label class="selectgroup-item" >
                          <input type="radio" name="gender" id="gender3" value="3" class="selectgroup-input-radio  @error('gender') is-invalid @enderror" {{ $data['user']->gander == 'Other' ? 'checked':''}} disabled>
                          <span class="selectgroup-button">Other</span>
                        </label>
                      </div>
                      @error('gender')
                          <div class="invalid-feedback">{{ $message }}</div>
                      @enderror
                    </div>
                    <div class="form-group col-md-3 col-6">
                      <label>Date of birth</label>
                      <input type="date" name="date_of_birth" id="date_of_birth" class="form-control  @error('date_of_birth') is-invalid @enderror" value="{{$data['user']->dob}}" readonly>
                      @error('date_of_birth')
                          <div class="invalid-feedback">{{ $message }}</div>
                      @enderror
                    </div>
                    <div class="form-group  col-md-3 col-6">
                      <label>Phone Number</label>
                      <div class="input-group">
                        <input type="tel" name="phone_number" id="phone_number" class="form-control phone-number  @error('phone_number') is-invalid @enderror" maxlength="10" minlength="10" value="{{$data['user']->phone_no}}" readonly>
                      @error('phone_number')
                          <div class="invalid-feedback">{{ $message }}</div>
                      @enderror
                      </div>
                    </div>
                    <div class="form-group  col-md-3">
                      <label>Email</label>
                      <div class="input-group">
                        <input type="Email" name="email" id="email" class="form-control phone-number  @error('email') is-invalid @enderror" value="{{$data['user']->email}}" readonly>
                      @error('email')
                          <div class="invalid-feedback">{{ $message }}</div>
                      @enderror
                      </div>
                    </div>
                    <div class="form-group col-md-6">
                      <label>Address 1</label>
                      <textarea class="form-control @error('Address_1') is-invalid @enderror" name="Address_1" id="Address_1" placeholder="Apartment, studio, or floor" readonly>{{$data['add'][0]->USRA06_address}}</textarea>
                      @error('Address_1')
                          <div class="invalid-feedback">{{ $message }}</div>
                      @enderror
                    </div>                    
                    <div class="form-group col-md-6">
                      <label>Address 2 <code>Street</code></label>
                      <textarea class="form-control @error('Address_2') is-invalid @enderror" name="Address_2" id="Address_2"  placeholder="Street" readonly>{{$data['add'][0]->USRA06_street}}</textarea>
                      @error('Address_2')
                          <div class="invalid-feedback">{{ $message }}</div>
                      @enderror
                    </div>
                    
                    </div>
                    <div class="form-row">
                    <div class="form-group col-md-3 col-6">
                      <label>Country</label>
                      <select class="custom-select select2 input-sm  @error('country') is-invalid @enderror"  name="country" id="country" disabled>
                        <option value="">Select Country</option>
                        @foreach($data['countries'] as $value)
                        <option value="{{$value->id}}"  {{ $data['add'][0]->USRA06_country == $value->id ? 'selected':''}}>{{$value->name}}</option>
                        @endforeach
                      </select>
                      @error('country')
                          <div class="invalid-feedback">{{ $message }}</div>
                      @enderror
                    </div>

                    <div class="form-group col-md-3  col-6">
                      <label>State</label>
                      <select class="custom-select select2 input-sm  @error('state') is-invalid @enderror" name="state" id="state" disabled>
                        <option value="">Select State</option>
                        @foreach($data['states'] as $value)
                        <option value="{{$value->id}}" {{ $data['add'][0]->USRA06_state == $value->id ? 'selected':''}}>{{$value->name}}</option>
                        @endforeach
                        <option value="other">Other</option>
                      </select>
                      @error('state')
                          <div class="invalid-feedback">{{ $message }}</div>
                      @enderror
                    </div>

                    <div class="form-group col-md-3  col-6">
                      <label>City</label>
                      <select class="custom-select select2 input-sm  @error('city') is-invalid @enderror" name="city" id="city" disabled>
                        <option value="">Select City</option> 
                        @foreach($data['cities'] as $value)
                        <option value="{{$value->id}}" {{ $data['add'][0]->USRA06_city == $value->id ? 'selected':''}}>{{$value->name}}</option>
                        @endforeach
                        <option value="other">Other</option>
                      </select>
                      @error('city')
                          <div class="invalid-feedback">{{ $message }}</div>
                      @enderror
                    </div>
                      <div class="form-group col-md-1  col-6">
                        <label for="inputZip">Zip</label>
                        <input type="text" class="form-control  @error('zip') is-invalid @enderror" name="zip" id="zip" maxlength="6" minlength="6"  value="{{$data['add'][0]->USRA06_zip}}" readonly>
                      @error('zip')
                          <div class="invalid-feedback">{{ $message }}</div>
                      @enderror
                      </div>
                    </div>
                    <div class="form-row">
                    <div class="form-group col-md-3">
                      <label>Signature <code>Max Size:3MB</code></label><br>
                      <img src="{{asset('uplode/users/'.$data['user']->image)}}" class="author-box-picture img-thumbnail img-responsive" width="150">
                    </div>
                    <div class="form-group col-md-3">
                      <label>Signature <code>Max Size:3MB</code></label>
                      <img src="{{asset('uplode/signature/'.$data['user']->signature)}}" class="author-box-picture img-thumbnail img-responsive" width="330">
                    </div>
                    </div>
                  </div>
                  <div class="card-footer text-center">
                  <form method="get" action="{{url('user-edit-information/'.Crypt::encrypt($data['user']->id))}}" id="form_reg">
                    <button class="btn btn-primary mr-1" type="button"  onclick="EditForm('form_reg');"><i class="fas fa-edit"></i> Edit</button>
                    <a class="btn btn-danger" href="{{url('view-users')}}"  type="reset"><i class="fas fa-arrow-alt-circle-left"></i> Back</a>
                  </form>
                  </div>
                </div>
              </div>
            </div>
            @endif
          </div>
        </section>
      
      </div>

@endsection