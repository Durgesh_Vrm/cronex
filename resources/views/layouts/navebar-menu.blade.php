      <nav class="navbar navbar-secondary navbar-expand-lg">
        <div class="container-fluid">
          <ul class="navbar-nav init-sidebar">
            <li class="nav-item dropdown active">
              <a href="#" data-toggle="dropdown" class="nav-link has-dropdown"><i
                  data-feather="airplay"></i><span>Dashboard</span></a>
              <ul class="dropdown-menu">
                <li class="nav-item"><a class="nav-link active" href="index.html">Dashboard 1</a></li>
                <li class="nav-item"><a class="nav-link" href="index2.html">Dashboard 2</a></li>
              </ul>
            </li>
            <li class="nav-item dropdown">
              <a href="#" data-toggle="dropdown" class="nav-link has-dropdown"><i
                  data-feather="box"></i><span>Widgets</span></a>
              <ul class="dropdown-menu">
                <li class="nav-item"><a class="nav-link" href="widget-chart.html">Chart Widgets</a></li>
                <li class="nav-item"><a class="nav-link" href="widget-data.html">Data Widgets</a></li>
              </ul>
            </li>
            <li class="nav-item dropdown">
              <a href="#" data-toggle="dropdown" class="nav-link has-dropdown"><i
                  data-feather="command"></i><span>Apps</span></a>
              <ul class="dropdown-menu">
                <li class="nav-item"><a class="nav-link" href="chat.html">Chat</a></li>
                <li class="nav-item"><a class="nav-link" href="portfolio.html">Portfolio</a></li>
                <li class="nav-item"><a class="nav-link" href="blog.html">Blog</a></li>
                <li class="nav-item"><a class="nav-link" href="calendar.html">Calendar</a></li>
                <li class="nav-item"><a class="nav-link" href="drag-drop.html">Drag & Drop</a></li>
                <li class="nav-item dropdown"><a href="#" class="nav-link has-dropdown">Email</a>
                  <ul class="dropdown-menu">
                    <li class="nav-item"><a class="nav-link" href="email-inbox.html">Inbox</a></li>
                    <li class="nav-item"><a class="nav-link" href="email-compose.html">Compose</a></li>
                    <li class="nav-item"><a class="nav-link" href="email-read.html">read</a></li>
                  </ul>
                </li>
              </ul>
            </li>
            <li class="nav-item dropdown">
              <a href="#" data-toggle="dropdown" class="nav-link has-dropdown"><i data-feather="copy"></i><span>UI
                  Elements</span></a>
              <ul class="dropdown-menu">
                <li class="nav-item dropdown"><a href="#" class="nav-link has-dropdown">Basic
                    Components</a>
                  <ul class="dropdown-menu">
                    <li class="nav-item"><a class="nav-link" href="alert.html">Alert</a></li>
                    <li class="nav-item"><a class="nav-link" href="badge.html">Badge</a></li>
                    <li class="nav-item"><a class="nav-link" href="breadcrumb.html">Breadcrumb</a></li>
                    <li class="nav-item"><a class="nav-link" href="buttons.html">Buttons</a></li>
                    <li class="nav-item"><a class="nav-link" href="collapse.html">Collapse</a></li>
                    <li class="nav-item"><a class="nav-link" href="dropdown.html">Dropdown</a></li>
                    <li class="nav-item"><a class="nav-link" href="checkbox-and-radio.html">Checkbox &amp; Radios</a>
                    </li>
                    <li class="nav-item"><a class="nav-link" href="list-group.html">List Group</a></li>
                    <li class="nav-item"><a class="nav-link" href="media-object.html">Media Object</a></li>
                    <li class="nav-item"><a class="nav-link" href="navbar.html">Navbar</a></li>
                    <li class="nav-item"><a class="nav-link" href="pagination.html">Pagination</a></li>
                    <li class="nav-item"><a class="nav-link" href="popover.html">Popover</a></li>
                    <li class="nav-item"><a class="nav-link" href="progress.html">Progress</a></li>
                    <li class="nav-item"><a class="nav-link" href="tooltip.html">Tooltip</a></li>
                    <li class="nav-item"><a class="nav-link" href="flags.html">Flag</a></li>
                    <li class="nav-item"><a class="nav-link" href="typography.html">Typography</a></li>
                  </ul>
                </li>
                <li class="nav-item dropdown"><a href="#" class="nav-link has-dropdown">Advanced
                    Components</a>
                  <ul class="dropdown-menu">
                    <li class="nav-item"><a class="nav-link" href="avatar.html">Avatar</a></li>
                    <li class="nav-item"><a class="nav-link" href="card.html">Card</a></li>
                    <li class="nav-item"><a class="nav-link" href="modal.html">Modal</a></li>
                    <li class="nav-item"><a class="nav-link" href="sweet-alert.html">Sweet Alert</a></li>
                    <li class="nav-item"><a class="nav-link" href="toastr.html">Toastr</a></li>
                    <li class="nav-item"><a class="nav-link" href="empty-state.html">Empty State</a></li>
                    <li class="nav-item"><a class="nav-link" href="multiple-upload.html">Multiple Upload</a></li>
                    <li class="nav-item"><a class="nav-link" href="pricing.html">Pricing</a></li>
                    <li class="nav-item"><a class="nav-link" href="tabs.html">Tab</a></li>
                  </ul>
                </li>
                <li class="nav-item"><a class="nav-link" href="blank.html">Blank
                    Page</a></li>
              </ul>
            </li>
            <li class="nav-item dropdown">
              <a href="#" data-toggle="dropdown" class="nav-link has-dropdown"><i
                  data-feather="layout"></i><span>Forms</span></a>
              <ul class="dropdown-menu">
                <li class="nav-item"><a class="nav-link" href="basic-form.html">Basic Form</a></li>
                <li class="nav-item"><a class="nav-link" href="forms-advanced-form.html">Advanced Form</a></li>
                <li class="nav-item"><a class="nav-link" href="forms-editor.html">Editor</a></li>
                <li class="nav-item"><a class="nav-link" href="forms-validation.html">Validation</a></li>
                <li class="nav-item"><a class="nav-link" href="form-wizard.html">Form Wizard</a></li>
              </ul>
            </li>
            <li class="nav-item dropdown">
              <a href="#" data-toggle="dropdown" class="nav-link has-dropdown"><i
                  data-feather="grid"></i><span>Tables</span></a>
              <ul class="dropdown-menu">
                <li class="nav-item"><a class="nav-link" href="basic-table.html">Basic Tables</a></li>
                <li class="nav-item"><a class="nav-link" href="advance-table.html">Advanced Table</a></li>
                <li class="nav-item"><a class="nav-link" href="datatables.html">Datatable</a></li>
                <li class="nav-item"><a class="nav-link" href="export-table.html">Export Table</a></li>
                <li class="nav-item"><a class="nav-link" href="footable.html">Footable</a></li>
                <li class="nav-item"><a class="nav-link" href="editable-table.html">Editable Table</a></li>
              </ul>
            </li>
            <li class="nav-item dropdown">
              <a href="#" data-toggle="dropdown" class="nav-link has-dropdown"><i
                  data-feather="pie-chart"></i><span>Charts</span></a>
              <ul class="dropdown-menu">
                <li class="nav-item"><a class="nav-link" href="chart-amchart.html">amChart</a></li>
                <li class="nav-item"><a class="nav-link" href="chart-apexchart.html">apexchart</a></li>
                <li class="nav-item"><a class="nav-link" href="chart-echart.html">eChart</a></li>
                <li class="nav-item"><a class="nav-link" href="chart-chartjs.html">Chartjs</a></li>
                <li class="nav-item"><a class="nav-link" href="chart-sparkline.html">Sparkline</a></li>
                <li class="nav-item"><a class="nav-link" href="chart-morris.html">Morris</a></li>
              </ul>
            </li>
            <li class="nav-item dropdown">
              <a href="#" data-toggle="dropdown" class="nav-link has-dropdown"><i
                  data-feather="image"></i><span>Media</span></a>
              <ul class="dropdown-menu">
                <li class="nav-item dropdown"><a href="#" class="nav-link has-dropdown">Gallery</a>
                  <ul class="dropdown-menu">
                    <li class="nav-item"><a class="nav-link" href="light-gallery.html">Light Gallery</a></li>
                    <li class="nav-item"><a class="nav-link" href="gallery1.html">Gallery 2</a></li>
                  </ul>
                </li>
                <li class="nav-item dropdown"><a href="#" class="nav-link has-dropdown">Sliders</a>
                  <ul class="dropdown-menu">
                    <li class="nav-item"><a class="nav-link" href="carousel.html">Bootstrap Carousel.html</a></li>
                    <li class="nav-item"><a class="nav-link" href="owl-carousel.html">Owl Carousel</a></li>
                  </ul>
                </li>
                <li class="nav-item dropdown"><a href="#" class="nav-link has-dropdown">Icons</a>
                  <ul class="dropdown-menu">
                    <li class="nav-item"><a class="nav-link" href="icon-font-awesome.html">Font Awesome</a></li>
                    <li class="nav-item"><a class="nav-link" href="icon-material.html">Material Design</a></li>
                    <li class="nav-item"><a class="nav-link" href="icon-ionicons.html">Ion Icons</a></li>
                    <li class="nav-item"><a class="nav-link" href="icon-feather.html">Feather Icons</a></li>
                    <li class="nav-item"><a class="nav-link" href="icon-weather-icon.html">Weather Icon</a></li>
                  </ul>
                </li>
                <li class="nav-item dropdown">
                  <a href="#" data-toggle="dropdown" class="nav-link has-dropdown"><span>Maps</span></a>
                  <ul class="dropdown-menu">

                    <li class="nav-item dropdown"><a href="#" class="nav-link has-dropdown">Google
                        Maps</a>
                      <ul class="dropdown-menu">
                        <li class="nav-item"><a class="nav-link" href="gmaps-advanced-route.html">Advanced Route</a>
                        </li>
                        <li class="nav-item"><a class="nav-link" href="gmaps-draggable-marker.html">Draggable Marker</a>
                        </li>
                        <li class="nav-item"><a class="nav-link" href="gmaps-geocoding.html">Geocoding</a></li>
                        <li class="nav-item"><a class="nav-link" href="gmaps-geolocation.html">Geolocation</a></li>
                        <li class="nav-item"><a class="nav-link" href="gmaps-marker.html">Marker</a></li>
                        <li class="nav-item"><a class="nav-link" href="gmaps-multiple-marker.html">Multiple Marker</a>
                        </li>
                        <li class="nav-item"><a class="nav-link" href="gmaps-route.html">Route</a></li>
                        <li class="nav-item"><a class="nav-link" href="gmaps-simple.html">Simple</a></li>
                      </ul>
                    </li>
                    <li class="nav-item"><a href="vector-map.html" class="nav-link">Vector
                        Map</a></li>
                  </ul>
                </li>
                <li class="nav-item"><a class="nav-link" href="timeline.html">Timeline</a></li>
              </ul>
            </li>
            <li class="nav-item dropdown">
              <a href="#" data-toggle="dropdown" class="nav-link has-dropdown"><i
                  data-feather="plus-circle"></i><span>More</span></a>
              <ul class="dropdown-menu">
                <li class="nav-item dropdown"><a href="#" class="nav-link has-dropdown">Authentication</a>
                  <ul class="dropdown-menu">
                    <li class="nav-item"><a class="nav-link" href="auth-login.html">Login</a></li>
                    <li class="nav-item"><a class="nav-link" href="auth-register.html">Register</a></li>
                    <li class="nav-item"><a class="nav-link" href="auth-forgot-password.html">Forgot Password</a></li>
                    <li class="nav-item"><a class="nav-link" href="auth-reset-password.html">Reset Password</a></li>
                    <li class="nav-item"><a class="nav-link" href="subscribe.html">Subscribe</a></li>
                  </ul>
                </li>
                <li class="nav-item dropdown"><a href="#" class="nav-link has-dropdown">Errors</a>
                  <ul class="dropdown-menu">
                    <li class="nav-item"><a class="nav-link" href="errors-503.html">503</a></li>
                    <li class="nav-item"><a class="nav-link" href="errors-403.html">403</a></li>
                    <li class="nav-item"><a class="nav-link" href="errors-404.html">404</a></li>
                    <li class="nav-item"><a class="nav-link" href="errors-500.html">500</a></li>
                  </ul>
                </li>
                <li class="nav-item dropdown"><a href="#" class="nav-link has-dropdown">Other Pages</a>
                  <ul class="dropdown-menu">
                    <li class="nav-item"><a class="nav-link" href="create-post.html">Create Post</a></li>
                    <li class="nav-item"><a class="nav-link" href="posts.html">Posts</a></li>
                    <li class="nav-item"><a class="nav-link" href="profile.html">Profile</a></li>
                    <li class="nav-item"><a class="nav-link" href="contact.html">Contact</a></li>
                    <li class="nav-item"><a class="nav-link" href="invoice.html">Invoice</a></li>
                  </ul>
                </li>
              </ul>
            </li>
          </ul>
        </div>
      </nav>
