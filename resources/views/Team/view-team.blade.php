@extends('layouts.app')
@section('content')
  
      <!-- Main Content -->
      <div class="main-content">
        <section class="section">
          <ul class="breadcrumb breadcrumb-style ">
            <li class="breadcrumb-item">
              <h4 class="page-title m-b-0">Export Tables</h4>
            </li>
            <li class="breadcrumb-item">
              <a href="index.html">
                <i data-feather="home"></i></a>
            </li>
            <li class="breadcrumb-item">Tables</li>
            <li class="breadcrumb-item">Export Tables</li>
          </ul>
          <div class="section-body">
            <div class="row">
              <div class="col-12 col-md-12 col-lg-12">
                <div class="card">
                  <div class="card-header">
                    <h4>User Registration</h4>
                  </div>
                  <form method="post" action="{{url('registration')}}" id="form_reg" autocomplete="off" enctype="multipart/form-data">
                    @csrf
                  <div class="card-body">
                    <div class="form-row">
                      <div class="form-group col-md-2">
                        <label for="name">Name <code>*</code></label>
                        <input type="text" class="form-control @error('name') is-invalid @enderror" id="name" name="name" value="{{old('name')}}" placeholder="User Name" required>
                      @error('name')
                          <div class="invalid-feedback">{{ $message }}</div>
                      @enderror
                      </div>
                      <div class="form-group col-md-3">
                        <label for="father_name">Father's Name</label>
                        <input type="text" class="form-control  @error('father_name') is-invalid @enderror" id="father_name" name="father_name" value="{{old('father_name')}}" placeholder="Father's Name">
                      @error('father_name')
                          <div class="invalid-feedback">{{ $message }}</div>
                      @enderror
                      </div>
                      <div class="form-group  col-md-3">
                      <label>Phone Number</label>
                      <div class="input-group">
                        <input type="tel" name="phone_number" id="phone_number" class="form-control phone-number  @error('phone_number') is-invalid @enderror" maxlength="10" minlength="10" value="{{old('phone_number')}}" required>
                      @error('phone_number')
                          <div class="invalid-feedback">{{ $message }}</div>
                      @enderror
                      </div>
                    </div>
          
                    <div class="form-group col-md-2">
                      <label>Date of birth</label>
                      <input type="date" name="date_of_birth" id="date_of_birth" class="form-control  @error('date_of_birth') is-invalid @enderror" value="{{ old('date_of_birth', date('Y-m-d')) }}" required>
                      @error('date_of_birth')
                          <div class="invalid-feedback">{{ $message }}</div>
                      @enderror
                    </div>
                    <div class="form-group col-md-2">
                      <label>Date of birth</label>
                      <input type="date" name="date_of_birth" id="date_of_birth" class="form-control  @error('date_of_birth') is-invalid @enderror" value="{{ old('date_of_birth', date('Y-m-d')) }}" required>
                      @error('date_of_birth')
                          <div class="invalid-feedback">{{ $message }}</div>
                      @enderror
                    </div>
                    </div>
                  </div>
                  <div class="card-footer text-center">
                    <button class="btn btn-primary mr-1" type="button"  onclick="SubmitForm('form_reg');"><i class="fas fa-search"></i> Submit</button>
                    <button class="btn btn-danger"  type="reset"><i class="fas fa-sync-alt"></i> Reset</button>
                  </div>
                  </form>
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-12">
                <div class="card">
                  <div class="card-header">
                    <h4>Export Table</h4>
                  </div>
                  <div class="card-body">
                    <div class="table-responsive">
                      <table class="table table-striped table-hover" id="tableExport" style="width:100%;">
                        <thead>
                          <tr>
                            <th>ID</th>
                            <th>Team's</th>
                            <th>Team's Leader</th>
                            <th>Type</th>
                            <th>Created</th>
                            <th>Updated</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody>
                          @php $sn=0; @endphp
                          @foreach($data['teams'] as $value)
                          <tr>
                            <td>{{++$sn}}</td>
                            <td>{{$value->team_name}}</td>
                            <td>{{$value->name}}</td>
                            <td>{{$value->personal_team}}</td>
                            <td>{{$value->created_at}}</td>
                            <td>{{$value->updated_at}}</td>
                            <td>
                              <div class="buttons">
                                <a href="" class="btn btn-success btn-sm"><i class="far fa-edit"></i></a>
                                <a href="" class="btn btn-danger btn-sm"><i class="fas fa-trash-alt"></i></a>
                              </div>
                            </td>
                          </tr>
                          @endforeach
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      
      </div>

@endsection