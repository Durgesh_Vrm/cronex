@extends('layouts.app')
@section('content')
  
      <!-- Main Content -->
      <div class="main-content">
        <section class="section">
          <ul class="breadcrumb breadcrumb-style ">
            <!-- <li class="breadcrumb-item">
              <h4 class="page-title m-b-0">Setting</h4>
            </li> -->
            <li class="breadcrumb-item">
              <a href="index.html">
                <i data-feather="home"></i></a>
            </li>
            <li class="breadcrumb-item">Setting</li>
            <li class="breadcrumb-item">Center Percentage</li>
            <li class="breadcrumb-item">View Percentage</li>
          </ul>
          <div class="section-body">
            

            <div class="row">
              <div class="col-12">
                <div class="card">
                  <div class="card-header">
                    <h4>Export Table</h4>
                  </div>
                  <div class="card-body">
                    <div class="table-responsive">
                      <table class="table table-striped table-hover" id="tableExport" style="width:100%;">
                        <thead>
                          <tr>
                            <th>ID</th>
                            <th>Role</th>
                            <th>Margin Value</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody>
                        @php $sn=0; @endphp
                          @if(isset($data))
                          @foreach($data['data'] as $value)
                          <tr>
                            <td>{{++$sn}}</td>
                            <td>{{ $value->role }}</td>
                            <form action="{{url('update-center-percentage')}}" method="post">
                            @csrf
                            <input type="hidden" value="{{ $value->id }}" name="id" />
                            <td><input type="text" id="marginvalue" value="{{ $value->margin }}" name="marginvalue" class="form-control @error('marginvalue') is-invalid @enderror" placeholder="Margin Value" required="" autocomplete="off"></td>
                            <td>
                              <div class="buttons">
                                <button class="btn btn-success btn-lg"> Update</button>
                                <!-- <a href="" class="btn btn-success btn-sm"><i class="far fa-edit"></i></a> -->
                                </div>
                            </td>
                            </form>
                          </tr>
                          @endforeach
                          @endif

                          
                          
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      
      </div>

@endsection