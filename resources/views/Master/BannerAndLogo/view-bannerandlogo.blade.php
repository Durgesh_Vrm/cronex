@extends('layouts.app')
@section('content')
  
      <!-- Main Content -->
      <div class="main-content">
        <section class="section">
          <ul class="breadcrumb breadcrumb-style ">
            <!-- <li class="breadcrumb-item">
              <h4 class="page-title m-b-0">Setting</h4>
            </li> -->
            <li class="breadcrumb-item">
              <a href="index.html">
                <i data-feather="home"></i></a>
            </li>
            <li class="breadcrumb-item">Setting</li>
            <li class="breadcrumb-item active">Logo & Banner <i>
        <li class="breadcrumb-item active">View Logo & Banner</li>
          </ul>
          <div class="section-body">
            

            <div class="row">
              <div class="col-12">
                <div class="card">
                  <div class="card-header">
                    <h4>Export Table</h4>
                  </div>
                  <div class="card-body">
                    <div class="table-responsive">
                      <table class="table table-striped table-hover" id="tableExport" style="width:100%;">
                        <thead>
                          <tr>
                            <th>ID</th>
                            <th>Banner/Logo For</th>
                            <th>Banner/Logo Type</th>
                            <th>Banner/Logo Name</th>
                            <th>Banner/Logo Description</th>
                            <th>Banner/Logo Title</th>
                            <th>Banner/Logo Image</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody>
                          @php $sn=0; @endphp
                          @if(isset($data))
                          @foreach($data['data'] as $value)
                          <tr>
                            <td>{{++$sn}}</td>
                            <td>{{$value->bl_for}}</td>
                            <td>{{$value->bl_type}}</td>
                            <td>{{$value->bl_name}}</td>
                            <td>{{$value->bl_desc}}</td>
                            <td>{{$value->bl_title}}</td>
                            <td><img src="{{ asset('/uplode/BannerAndLogo/'.$value->bl_value) }}" style="height:100px;width:150px;" /></td>
                            <td>
                              <div class="buttons">
                                <a href="{{url('/edit-bannerandlogo/'.encrypt($value->id))}}" class="btn btn-success btn-sm"><i class="far fa-edit"></i></a>
                                <a href="{{url('/delete-bannerandlogo/'.encrypt($value->id))}}" class="btn btn-danger btn-sm"><i class="fas fa-trash-alt"></i></a>
                              </div>
                            </td>
                          </tr>
                          @endforeach
                          @endif
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      
      </div>

@endsection