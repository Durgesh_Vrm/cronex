@extends('layouts.app')
@section('content')
  <!-- Main Content -->
  <div class="main-content">
    <section class="section">
      <ul class="breadcrumb breadcrumb-style ">
        <li class="breadcrumb-item">
          <a href="{{url('dashboard')}}">
            <i data-feather="home"></i></a>
        </li>
        <li class="breadcrumb-item">Setting</li>
        <li class="breadcrumb-item active">Logo & Banner <i>
        <li class="breadcrumb-item active">Edit Logo & Banner</li>
      </ul>
      <div class="section-body">
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="card">
              <div class="boxs mail_listing">
              <form class="composeForm" action="{{url('update-banner-logo')}}" method="post" id="form" enctype="multipart/form-data">
                @csrf
                <input type="hidden" value="{{$data->id}}" name="id" />
                <div class="row">
                  <div class="col-lg-6">
                      <div class="form-group">
                        <div class="form-line">
                          <select name="for" id="for" class="form-control form-control-sm custom-select @error('type') is-invalid @enderror">
                            <option value="">Select for</option>
                            <option value="1" {{$data->bl_for == 'Website'? 'selected':''}}>Website</option>
                            <option value="2" {{$data->bl_for == 'Android'? 'selected':''}}>Android</option>
                            <option value="3" {{$data->bl_for == 'Both'? 'selected':''}}>Both</option>
                          </select>
                          @error('for')
                              <div class="invalid-feedback">{{ $message }}</div>
                          @enderror
                        </div>
                      </div>
                  </div>
                  <div class="col-lg-6">
                      <div class="form-group">
                        <div class="form-line">
                          <select name="type" id="type" class="form-control form-control-sm custom-select @error('type') is-invalid @enderror">
                            <option value="">Select Type</option>
                            <option value="1" {{$data->bl_type == 'Login Banner'? 'selected':''}}>Login Banner</option>
                            <option value="2" {{$data->bl_type == 'Home Slider'? 'selected':''}}>Home Slider</option>
                            <option value="3" {{$data->bl_type == 'About Us Banner'? 'selected':''}}>About Us Banner</option>
                            <option value="4" {{$data->bl_type == 'Our Best Services'? 'selected':''}}>Our Best Services</option>
                            <option value="5" {{$data->bl_type == 'Our Partners'? 'selected':''}}>Our Partners</option>
                          </select>
                          @error('type')
                              <div class="invalid-feedback">{{ $message }}</div>
                          @enderror
                        </div>
                      </div>
                  </div>
                  <div class="col-lg-6">
                      <div class="form-group">
                        <div class="form-line">
                          <input type="text" id="name" name="name" value="{{$data->bl_name}}" class="form-control @error('name') is-invalid @enderror" placeholder="name" autocomplete="off" readonly="readonly">
                        </div>
                        @error('name')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                      </div>
                  </div>
                  <div class="col-lg-6">
                      <div class="form-group">
                        <div class="form-line">
                          <input type="text" id="description" name="description" value="{{$data->bl_desc}}" class="form-control @error('description') is-invalid @enderror" placeholder="description" autocomplete="off">
                        </div>
                        @error('description')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                      </div>
                  </div>
                  <div class="col-lg-12">
                      <div class="form-group">
                        <div class="form-line">
                          <input type="text" id="title" name="title" value="{{$data->bl_title}}" class="form-control @error('title') is-invalid @enderror" placeholder="title" autocomplete="off">
                        </div>
                        @error('description')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                      </div>
                  </div>
                  <div class="col-lg-12">
                      <!-- <textarea class="summernote-simple" id="content" name="content">{{$data->bl_value}}</textarea> -->
                      <input type="file" id="img" name="content" class="form-control @error('content') is-invalid @enderror" autocomplete="off">
                        @error('content')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                  </div>
                  <div class="col-lg-12">
                    <div class="m-l-25 m-b-20">
                      <button type="button" class="btn btn-info btn-border-radius waves-effect" onclick="SubmitForm('form');">Send</button>
                      <button type="reset" class="btn btn-danger btn-border-radius waves-effect">Reset</button>
                    </div>
                  </div>
                </div>
              </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
@endsection