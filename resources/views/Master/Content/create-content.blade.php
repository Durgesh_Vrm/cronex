@extends('layouts.app')
@section('content')
  <!-- Main Content -->
  <div class="main-content">
    <section class="section">
      <ul class="breadcrumb breadcrumb-style ">
        <li class="breadcrumb-item">
          <a href="{{url('dashboard')}}">
            <i data-feather="home"></i></a>
        </li>
        <li class="breadcrumb-item">Setting</li>
        <li class="breadcrumb-item active">Website & App Content</li>
        <li class="breadcrumb-item active">Create Content</li>
      </ul>
      <div class="section-body">
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="card">
              <div class="boxs mail_listing">
              <form class="composeForm" action="{{url('add-content')}}" method="post" id="form">
                @csrf
                <div class="row">
                  <div class="col-lg-6">
                      <div class="form-group">
                        <div class="form-line">
                          <select name="for" id="for" required="" class="form-control form-control-sm custom-select @error('for') is-invalid @enderror">
                            <option value="" selected>Select For</option>
                            <option value="1">Website</option>
                            <option value="2">Android</option>
                            <option value="3">Both</option>
                          </select>
                          @error('for')
                              <div class="invalid-feedback">{{ $message }}</div>
                          @enderror
                        </div>
                      </div>
                  </div>
                  <div class="col-lg-6">
                      <div class="form-group">
                        <div class="form-line">
                          <select name="type" id="type" required="" class="form-control form-control-sm custom-select @error('type') is-invalid @enderror">
                            <option value="" selected>Select Type</option>
                            <option value="1">Home Slider</option>
                            <option value="2">About Us</option>
                            <option value="3">Our Best Services</option>
                            <option value="4">What We Offer</option>
                            <option value="5">Why Choose Us</option>
                            <option value="6">FAQ</option>
                            <option value="7">Our Policies</option>
                            <option value="8">Terms & Conditions</option>
                            <option value="9">Footer Content</option>
                          </select>
                          @error('type')
                              <div class="invalid-feedback">{{ $message }}</div>
                          @enderror
                        </div>
                      </div>
                  </div>
                  <div class="col-lg-6">
                      <div class="form-group">
                        <div class="form-line">
                          <input type="text" id="name" name="name" required="" class="form-control @error('name') is-invalid @enderror" placeholder="name" autocomplete="off">
                        </div>
                        @error('name')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                      </div>
                  </div>
                  <div class="col-lg-6">
                      <div class="form-group">
                        <div class="form-line">
                          <input type="text" id="description" name="description" class="form-control @error('description') is-invalid @enderror" placeholder="description" autocomplete="off">
                        </div>
                        @error('description')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                      </div>
                  </div>
                  <div class="col-lg-12">
                      <div class="form-group">
                        <div class="form-line">
                          <input type="text" id="title" name="title" class="form-control @error('title') is-invalid @enderror" placeholder="title" autocomplete="off">
                        </div>
                        @error('description')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                      </div>
                  </div>
                  <div class="col-lg-12">
                      <textarea class="summernote-simple" id="content" name="content"></textarea>
                        @error('content')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                  </div>
                  <div class="col-lg-12">
                    <div class="m-l-25 m-b-20">
                      <button type="button" class="btn btn-info btn-border-radius waves-effect" onclick="SubmitForm('form');">Send</button>
                      <button type="reset" class="btn btn-danger btn-border-radius waves-effect">Reset</button>
                    </div>
                  </div>
                </div>
              </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
@endsection