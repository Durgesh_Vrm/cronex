@extends('layouts.app')
@section('content')
  <!-- Main Content -->
  <div class="main-content">
    <section class="section">
      <ul class="breadcrumb breadcrumb-style ">
        <li class="breadcrumb-item">
          <a href="{{url('dashboard')}}">
            <i data-feather="home"></i></a>
        </li>
        <li class="breadcrumb-item">Setting</li>
        <li class="breadcrumb-item active">Website & App Content</li>
        <li class="breadcrumb-item active">Create Content</li>
      </ul>
      <div class="section-body">
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="card">
              <div class="boxs mail_listing">
              <form class="composeForm" action="{{url('update-content')}}" method="post" id="form">
                @csrf
                <input type="hidden" id="iid" name="id" required="" value="{{$data->id}}">
                <div class="row">
                  <div class="col-lg-6">
                      <div class="form-group">
                        <div class="form-line">
                          <select name="for" id="for" class="form-control form-control-sm custom-select @error('for') is-invalid @enderror">
                            <option value="">Select For</option>
                            <option value="1" {{$data->sc_for == 'Website'? 'selected':''}}>Website</option>
                            <option value="2" {{$data->sc_for == 'Android'? 'selected':''}}>Android</option>
                            <option value="3" {{$data->sc_for == 'Both'? 'selected':''}}>Both</option>
                          </select>
                          @error('for')
                              <div class="invalid-feedback">{{ $message }}</div>
                          @enderror
                        </div>
                      </div>
                  </div>
                  <div class="col-lg-6">
                      <div class="form-group">
                        <div class="form-line">
                          <select name="type" id="type" class="form-control form-control-sm custom-select @error('type') is-invalid @enderror">
                            <option value="">Select Type</option>
                            <option value="1" {{$data->sc_type == 'Home Slider'? 'selected':''}}>Home Slider</option>
                            <option value="2" {{$data->sc_type == 'About Us'? 'selected':''}}>About Us</option>
                            <option value="3" {{$data->sc_type == 'Our Best Services'? 'selected':''}}>Our Best Services</option>
                            <option value="4" {{$data->sc_type == 'What We Offer'? 'selected':''}}>What We Offer</option>
                            <option value="5" {{$data->sc_type == 'Why Choose Us'? 'selected':''}}>Why Choose Us</option>
                            <option value="6" {{$data->sc_type == 'FAQ'? 'selected':''}}>FAQ</option>
                            <option value="7" {{$data->sc_type == 'Our Policies'? 'selected':''}}>Our Policies</option>
                            <option value="8" {{$data->sc_type == 'Terms & Conditions'? 'selected':''}}>Terms & Conditions</option>
                            <option value="9" {{$data->sc_type == 'Footer Content'? 'selected':''}}>Footer Content</option>
                          </select>
                          @error('type')
                              <div class="invalid-feedback">{{ $message }}</div>
                          @enderror
                        </div>
                      </div>
                  </div>
                  <div class="col-lg-6">
                      <div class="form-group">
                        <div class="form-line">
                          <input type="text" id="name" name="name" value="{{$data->sc_name}}" class="form-control @error('name') is-invalid @enderror" placeholder="name" autocomplete="off" readonly="readonly">
                        @error('name')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                        </div>
                      </div>
                  </div>
                  <div class="col-lg-6">
                      <div class="form-group">
                        <div class="form-line">
                          <input type="text" id="description" name="description" value="{{$data->sc_desc}}" class="form-control @error('description') is-invalid @enderror" placeholder="description" autocomplete="off">
                        @error('description')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                        </div>
                      </div>
                  </div>
                  <div class="col-lg-12">
                      <div class="form-group">
                        <div class="form-line">
                          <input type="text" id="title" name="title" value="{{$data->sc_title}}" class="form-control @error('title') is-invalid @enderror" placeholder="title" autocomplete="off">
                        @error('title')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                        </div>
                      </div>
                  </div>
                  <div class="col-lg-12">
                      <textarea class="summernote-simple" id="content" name="content">{{$data->sc_value}}</textarea>
                        @error('content')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                  </div>
                  <div class="col-lg-12">
                    <div class="m-l-25 m-b-20">
                      <button type="button" class="btn btn-info btn-border-radius waves-effect" onclick="SubmitForm('form');">Send</button>
                      <button type="reset" class="btn btn-danger btn-border-radius waves-effect">Reset</button>
                    </div>
                  </div>
                </div>
              </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
@endsection