@extends('layouts.app')
@section('content')
  
      <!-- Main Content -->
      <div class="main-content">
        <section class="section">
          <ul class="breadcrumb breadcrumb-style ">
            <!-- <li class="breadcrumb-item">
              <h4 class="page-title m-b-0">Setting</h4>
            </li> -->
            <li class="breadcrumb-item">
              <a href="index.html">
                <i data-feather="home"></i></a>
            </li>
            <li class="breadcrumb-item">Setting</li>
            <li class="breadcrumb-item">Website & App Content</li>
            <li class="breadcrumb-item">View Content</li>
          </ul>
          <div class="section-body">
            

            <div class="row">
              <div class="col-12">
                <div class="card">
                  <div class="card-header">
                    <h4>Export Table</h4>
                  </div>
                  <div class="card-body">
                    <div class="table-responsive">
                      <table class="table table-striped table-hover" id="tableExport" style="width:100%;">
                        <thead>
                          <tr>
                            <th>ID</th>
                            <th>Content For</th>
                            <th>Content Type</th>
                            <th>Content Name</th>
                            <th>Description</th>
                            <th>Title</th>
                            <th>Long Description</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody>
                          @php $sn=0; @endphp
                          @if(isset($data))
                          @foreach($data['data'] as $value)
                          <tr>
                            <td>{{++$sn}}</td>
                            <td>{{$value->sc_for}}</td>
                            <td>{{$value->sc_type}}</td>
                            <td>{{$value->sc_name}}</td>
                            <td>{{$value->sc_desc}}</td>
                            <td>{{$value->sc_title}}</td>
                            <td>{!!$value->sc_value!!}</td>
                            <td>
                              <div class="buttons">
                                <a href="{{url('/edit-content/'.encrypt($value->id))}}" class="btn btn-success btn-sm"><i class="far fa-edit"></i></a>
                                <form action="{{url('/delete-content/'.encrypt($value->id))}}" method="get" id="form"> <button type="button"  class="btn {{$value->sc_status =='Active'? 'btn-success':'btn-danger' }}  btn-sm"  onclick="DeleteForm('form');">{{$value->sc_status}}</button></form>
                              </div>
                            </td>
                          </tr>
                          @endforeach
                          @endif
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      
      </div>

@endsection