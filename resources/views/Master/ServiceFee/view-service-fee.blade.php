@extends('layouts.app')
@section('content')
  
      <!-- Main Content -->
      <div class="main-content">
        <section class="section">
          <ul class="breadcrumb breadcrumb-style ">
            <!-- <li class="breadcrumb-item">
              <h4 class="page-title m-b-0">Setting</h4>
            </li> -->
            <li class="breadcrumb-item">
              <a href="index.html">
                <i data-feather="home"></i></a>
            </li>
            <li class="breadcrumb-item">Setting</li>
            <li class="breadcrumb-item">Services Fee</li>
            <li class="breadcrumb-item">View Service Fee</li>
          </ul>
          <div class="section-body">
            

            <div class="row">
              <div class="col-12">
                <div class="card">
                  <div class="card-header">
                    <h4>Export Table</h4>
                  </div>
                  <div class="card-body">
                    <div class="table-responsive">
                      <table class="table table-striped table-hover" id="tableExport" style="width:100%;">
                        <thead>
                          <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Rupee</th>
                            <th>Description</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody>
                          @php $sn=0; @endphp
                          @if(isset($data))
                          @foreach($data['data'] as $value)
                          <tr>
                            <td>{{++$sn}}</td>
                            <td>{{$value->name}}</td>
                            <td>{{$value->rs}}</td>
                            <td>{{$value->desc}}</td>
                            <td>
                              <div class="buttons">
                                <a href="{{url('/edit-service/'.encrypt($value->id))}}" class="btn btn-success btn-sm"><i class="far fa-edit"></i></a>
                                @if($value->status == 'Active')
                                <a href="{{url('/delete-service/'.encrypt($value->id))}}" class="btn btn-danger btn-sm"><i class="far fa-trash-alt"></i></a>
                                @else
                                <a href="#" class="btn btn-succcess btn-sm">Deleted</a>
                                @endif
                                </div>
                            </td>
                          </tr>
                          @endforeach
                          @endif
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      
      </div>

@endsection