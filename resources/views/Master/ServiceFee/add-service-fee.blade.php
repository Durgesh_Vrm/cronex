@extends('layouts.app')
@section('content')
  <!-- Main Content -->
  <div class="main-content">
    <section class="section">
      <ul class="breadcrumb breadcrumb-style ">
        <li class="breadcrumb-item">
          <a href="{{url('dashboard')}}">
            <i data-feather="home"></i></a>
        </li>
        <li class="breadcrumb-item">Setting</li>
        <li class="breadcrumb-item active">Services Fee<i>
        <li class="breadcrumb-item active">Add Service Fee</li>
      </ul>
      <div class="section-body">
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="card">
              <div class="boxs mail_listing">
              <form class="composeForm" action="{{url('insert-service-fee')}}" method="post" id="form" enctype="multipart/form-data">
                @csrf
                
                <div class="row">

                <div class="col-lg-6">
                      <div class="form-group">
                        <div class="form-line">
                          <input type="text" id="name" name="name" required="" class="form-control @error('name') is-invalid @enderror" placeholder="Name" autocomplete="off">
                        </div>
                        @error('name')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                      </div>
                </div>
                <div class="col-lg-6">
                      <div class="form-group">
                        <div class="form-line">
                          <input type="text" id="price" name="price" class="form-control @error('price') is-invalid @enderror" placeholder="Rupees" required="" autocomplete="off">
                        </div>
                        @error('price')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                      </div>
                </div>
               

               
                <div class="col-lg-12">
                      <div class="form-group">
                        <div class="form-line">
                          <input type="text" id="desc" name="desc" class="form-control @error('desc') is-invalid @enderror" placeholder="Description" required="" autocomplete="off">
                        </div>
                        @error('desc')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                      </div>
                </div>


                
                
                  
                 
                 
                  <div class="col-lg-12" style="margin-top:20px;">
                    <div class="m-l-25 m-b-20">
                      <button type="button" class="btn btn-info btn-border-radius waves-effect" onclick="SubmitForm('form');">Send</button>
                      <button type="reset" class="btn btn-danger btn-border-radius waves-effect">Reset</button>
                    </div>
                  </div>
                </div>
              </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
@endsection