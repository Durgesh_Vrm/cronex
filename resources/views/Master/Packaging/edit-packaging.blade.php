@extends('layouts.app')
@section('content')
  <!-- Main Content -->
  <div class="main-content">
    <section class="section">
      <ul class="breadcrumb breadcrumb-style ">
        <li class="breadcrumb-item">
          <a href="{{url('dashboard')}}">
            <i data-feather="home"></i></a>
        </li>
        <li class="breadcrumb-item">Setting</li>
        <li class="breadcrumb-item active">Packages <i>
        <li class="breadcrumb-item active">Edit Package</li>
      </ul>
      <div class="section-body">
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="card">
              <div class="boxs mail_listing">
              <form class="composeForm" action="{{url('update-package')}}" method="post" id="form" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="id" value="{{$respons->id}}" />
                <div class="row">

                <div class="col-lg-4">
                      <div class="form-group">
                        <div class="form-line">
                          <input type="text" id="name" name="name" value="{{ $respons->name }}" required="" class="form-control @error('name') is-invalid @enderror" placeholder="Name" autocomplete="off">
                        </div>
                        @error('name')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                      </div>
                </div>
                <div class="col-lg-4">
                      <div class="form-group">
                        <div class="form-line">
                          <input type="text" id="height" name="height" value="{{ $respons->height }}" class="form-control @error('height') is-invalid @enderror" placeholder="Height in cm" required="" autocomplete="off">
                        </div>
                        @error('height')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                      </div>
                </div>
                <div class="col-lg-4">
                      <div class="form-group">
                        <div class="form-line">
                          <input type="text" id="width" name="width" value="{{ $respons->width }}" class="form-control @error('width') is-invalid @enderror" placeholder="Width in cm" required="" autocomplete="off">
                        </div>
                        @error('width')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                      </div>
                </div>

                <div class="col-lg-4">
                      <div class="form-group">
                        <div class="form-line">
                          <input type="file" id="img" name="img" class="form-control @error('img') is-invalid @enderror" required="" autocomplete="off">
                        </div>
                        @error('img')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                      </div>
                </div>
                <div class="col-lg-8">
                      <div class="form-group">
                        <div class="form-line">
                          <input type="text" id="desc" name="desc" value="{{ $respons->desc }}" class="form-control @error('desc') is-invalid @enderror" placeholder="Description" required="" autocomplete="off">
                        </div>
                        @error('desc')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                      </div>
                </div>


                
                
                  
                 
                 
                  <div class="col-lg-12" style="margin-top:20px;">
                    <div class="m-l-25 m-b-20">
                      <button type="button" class="btn btn-info btn-border-radius waves-effect" onclick="SubmitForm('form');">Send</button>
                      <button type="reset" class="btn btn-danger btn-border-radius waves-effect">Reset</button>
                    </div>
                  </div>
                </div>
              </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
@endsection