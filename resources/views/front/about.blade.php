@extends('front.layout')
@section('content')

<!--Breadcrumb start-->
<div class="ed_pagetitle">
<div class="ed_img_overlay"></div>
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12">
				<div class="page_title">
					<h2>About Us</h2>
				</div>
			</div>
			<div class="col-lg-12 col-md-12 col-sm-12">
				<ul class="breadcrumb">
					<li><a href="{{url('/')}}">home</a></li>
					<li>//</li>
					<li><a href="{{url('about-us')}}">About us</a></li>
				</ul>
			</div>
		</div>
	</div>
</div>
<!--Breadcrumb end-->
<!--Our expertise section one start -->
<div class="ed_transprentbg">
	<div class="container">
		<div class="row">
			<div class="col-lg-6 col-md-6 col-sm-12">
				<div class="ed_video_section_discription_img">
					<img src="front/images/content/about_dummy1.jpg" style="cursor:pointer"  alt="1" />
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-12">
				<div class="ed_video_section_discription ed_toppadder90">
					<h4>@if(isset($data['ABOUT_T'])) {{$data['ABOUT_T']}} @endif</h4>
					<p>@if(isset($data['ABOUT_T'])) {!!$data['ABOUT_C']!!} @endif</p>
				</div>
			</div>
		</div>
    </div><!-- /.container -->
</div>
<!--Our expertise section one end -->
<!--skill section start -->
<div class="ed_graysection ed_toppadder90 ed_bottompadder60">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="ed_heading_top ed_bottompadder50">
					<h3>what we offer</h3>
				</div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
				<div class="skill_section">
					<h4>@if(isset($data['WHAT_WE_OFFER-1_T'])) {{$data['WHAT_WE_OFFER-1_T']}} @endif</h4>
					<p>@if(isset($data['WHAT_WE_OFFER-1_C'])) {!!$data['WHAT_WE_OFFER-1_C']!!} @endif</p>
					<span><i class="fa fa-handshake-o" aria-hidden="true"></i></span>
				</div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
				<div class="skill_section">
					<h4>@if(isset($data['WHAT_WE_OFFER-2_T'])) {{$data['WHAT_WE_OFFER-2_T']}} @endif</h4>
					<p>@if(isset($data['WHAT_WE_OFFER-2_C'])) {!!$data['WHAT_WE_OFFER-2_C']!!} @endif</p>
					<span><i class="fa fa-users" aria-hidden="true"></i></span>
				</div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
				<div class="skill_section">
					<h4>@if(isset($data['WHAT_WE_OFFER-3_T'])) {{$data['WHAT_WE_OFFER-3_T']}} @endif</h4>
					<p>@if(isset($data['WHAT_WE_OFFER-3_C'])) {!!$data['WHAT_WE_OFFER-3_C']!!} @endif</p>
					<span><i class="fa fa-gift" aria-hidden="true"></i></span>
				</div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
				<div class="skill_section">
					<h4>@if(isset($data['WHAT_WE_OFFER-4_T'])) {{$data['WHAT_WE_OFFER-4_T']}} @endif</h4>
					<p>@if(isset($data['WHAT_WE_OFFER-4_C'])) {!!$data['WHAT_WE_OFFER-4_C']!!} @endif</p>
					<span><i class="fa fa-money" aria-hidden="true"></i></span>
				</div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
				<div class="skill_section">
					<h4>@if(isset($data['WHAT_WE_OFFER-5_T'])) {{$data['WHAT_WE_OFFER-5_T']}} @endif</h4>
					<p>@if(isset($data['WHAT_WE_OFFER-5_C'])) {!!$data['WHAT_WE_OFFER-5_C']!!} @endif</p>
					<span><i class="fa fa-child" aria-hidden="true"></i></span>
				</div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
				<div class="skill_section">
					<h4>@if(isset($data['WHAT_WE_OFFER-6_T'])) {{$data['WHAT_WE_OFFER-6_T']}} @endif</h4>
					<p>@if(isset($data['WHAT_WE_OFFER-6_C'])) {!!$data['WHAT_WE_OFFER-6_C']!!} @endif</p>
					<span><i class="fa fa-tags" aria-hidden="true"></i></span>
				</div>
			</div>
        </div>
	</div>
</div>
<!--skill section end -->
<!--chart section start -->
<div class="ed_transprentbg ed_toppadder100">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="ed_counter_wrapper">
					<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
						<div class="ed_chart_ratio">
							<i class="fa fa-bus" aria-hidden="true"></i>
							<h4>@if(isset($data['WHAT_WE_OFFER-7_T'])) {{$data['WHAT_WE_OFFER-7_T']}} @endif</h4>
							<p>@if(isset($data['WHAT_WE_OFFER-7_C'])) {!!$data['WHAT_WE_OFFER-7_C']!!} @endif</p>
						</div>
					</div>
					<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
						<div class="ed_chart_ratio">
							<i class="fa fa-plane" aria-hidden="true"></i>
							<h4>@if(isset($data['WHAT_WE_OFFER-8_T'])) {{$data['WHAT_WE_OFFER-8_T']}} @endif</h4>
							<p>@if(isset($data['WHAT_WE_OFFER-8_C'])) {!!$data['WHAT_WE_OFFER-8_C']!!} @endif</p>
						</div>
					</div>
					<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
						<div class="ed_chart_ratio">
							<i class="fa fa-ship" aria-hidden="true"></i>
							<h4>@if(isset($data['WHAT_WE_OFFER-9_T'])) {{$data['WHAT_WE_OFFER-9_T']}} @endif</h4>
							<p>@if(isset($data['WHAT_WE_OFFER-9_C'])) {!!$data['WHAT_WE_OFFER-9_C']!!} @endif</p>
						</div>
					</div>
				</div>
			</div>
        </div>
	</div>
</div>
<!-- chart Section end -->
<!-- Services start -->
<div class="ed_transprentbg ed_toppadder90 ed_bottompadder60">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="ed_heading_top ed_bottompadder50">
					<h3>our best services</h3>
				</div>
			</div>
			<div class="ed_mostrecomeded_course_slider">
				<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
					<div class="ed_mostrecomeded_course style_2">
						<div class="ed_item_img">
							<img src="@if(isset($data['Our Best Services_Ground Transport'])){{url('uplode/BannerAndLogo/'.$data['Our Best Services_Ground Transport'])}} @endif" alt="item1" class="img-responsive">
						</div>
						<div class="ed_item_description ed_most_recomended_data">
							<h4><a href="#">@if(isset($data['OUR_BEST_SERVICES-1_T'])){{$data['OUR_BEST_SERVICES-1_T']}} @endif</a></h4>
							<p>@if(isset($data['OUR_BEST_SERVICES-1_C'])) {!!$data['OUR_BEST_SERVICES-1_C']!!} @endif</p>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
					<div class="ed_mostrecomeded_course style_2">
						<div class="ed_item_img">
							<img  src="@if(isset($data['Our Best Services_Warehousing'])){{url('uplode/BannerAndLogo/'.$data['Our Best Services_Warehousing'])}} @endif" alt="item1" class="img-responsive">
						</div>
						<div class="ed_item_description ed_most_recomended_data">
							<h4><a href="#">@if(isset($data['OUR_BEST_SERVICES-2_T'])){{$data['OUR_BEST_SERVICES-2_T']}} @endif</a></h4>
							<p>@if(isset($data['OUR_BEST_SERVICES-2_C'])) {!!$data['OUR_BEST_SERVICES-2_C']!!} @endif</p>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
					<div class="ed_mostrecomeded_course style_2">
						<div class="ed_item_img">
							<img  src="@if(isset($data['Our Best Services_Packaging & Storage'])){{url('uplode/BannerAndLogo/'.$data['Our Best Services_Packaging & Storage'])}} @endif" alt="item1" class="img-responsive">
						</div>
						<div class="ed_item_description ed_most_recomended_data">
							<h4><a href="#">@if(isset($data['OUR_BEST_SERVICES-3_T'])){{$data['OUR_BEST_SERVICES-3_T']}} @endif</a></h4>
							<p>@if(isset($data['OUR_BEST_SERVICES-3_C'])) {!!$data['OUR_BEST_SERVICES-3_C']!!} @endif</p>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
					<div class="ed_mostrecomeded_course style_2">
						<div class="ed_item_img">
							<img src="@if(isset($data['Our Best Services_Door-To-Door Delivery'])){{url('uplode/BannerAndLogo/'.$data['Our Best Services_Door-To-Door Delivery'])}} @endif" alt="item1" class="img-responsive">
						</div>
						<div class="ed_item_description ed_most_recomended_data">
							<h4><a href="#">@if(isset($data['OUR_BEST_SERVICES-4_T'])){{$data['OUR_BEST_SERVICES-4_T']}} @endif</a></h4>
							<p>@if(isset($data['OUR_BEST_SERVICES-4_C'])) {!!$data['OUR_BEST_SERVICES-4_C']!!} @endif</p>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
					<div class="ed_mostrecomeded_course style_2">
						<div class="ed_item_img">
							<img  src="@if(isset($data['Our Best Services_Cargo'])){{url('uplode/BannerAndLogo/'.$data['Our Best Services_Cargo'])}} @endif" alt="item1" class="img-responsive">
						</div>
						<div class="ed_item_description ed_most_recomended_data">
							<h4><a href="#">@if(isset($data['OUR_BEST_SERVICES-5_T'])){{$data['OUR_BEST_SERVICES-5_T']}} @endif</a></h4>
							<p>@if(isset($data['OUR_BEST_SERVICES-5_C'])) {!!$data['OUR_BEST_SERVICES-5_C']!!} @endif</p>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
					<div class="ed_mostrecomeded_course style_2">
						<div class="ed_item_img">
							<img  src="@if(isset($data['Our Best Services_Logistic'])){{url('uplode/BannerAndLogo/'.$data['Our Best Services_Logistic'])}} @endif" alt="item1" class="img-responsive">
						</div>
						<div class="ed_item_description ed_most_recomended_data">
							<h4><a href="#">@if(isset($data['OUR_BEST_SERVICES-6_T'])){{$data['OUR_BEST_SERVICES-6_T']}} @endif</a></h4>
							<p>@if(isset($data['OUR_BEST_SERVICES-6_C'])) {!!$data['OUR_BEST_SERVICES-6_C']!!} @endif</p>
						</div>
					</div>
				</div>
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center ed_toppadder30">
					<a href="{{url('service')}}" class="btn ed_btn ed_orange">view  more</a>
				</div>
			</div>
		</div>
    </div><!-- /.container -->
</div>
<!-- Services end -->
<!--Timer Section three start -->
<div class="ed_timer_section ed_toppadder90 ed_bottompadder60">
<div class="ed_img_overlay"></div>
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="ed_heading_top ed_bottompadder50">
					<h3>Why Choose Us</h3>
				</div>
			</div>
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="ed_counter_wrapper">
					<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
						<div class="ed_counter">
							<h2 class="timer" data-from="0" data-to="@if(isset($data['WHY_CHOOSE_US-1_T'])) {!!$data['WHY_CHOOSE_US-1_T']!!} @endif" data-speed="5000"></h2>
							<h4>@if(isset($data['WHY_CHOOSE_US-1_C'])) {!!$data['WHY_CHOOSE_US-1_C']!!} @endif</h4>
						</div>
					</div>
					<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
						<div class="ed_counter">
							<h2 class="timer" data-from="0" data-to="@if(isset($data['WHY_CHOOSE_US-2_T'])) {!!$data['WHY_CHOOSE_US-2_T']!!} @endif" data-speed="5000"></h2>
							<h4>@if(isset($data['WHY_CHOOSE_US-2_C'])) {!!$data['WHY_CHOOSE_US-2_C']!!} @endif</h4>
						</div>
					</div>
					<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
						<div class="ed_counter">
							<h2 class="timer" data-from="0" data-to="@if(isset($data['WHY_CHOOSE_US-3_T'])) {!!$data['WHY_CHOOSE_US-3_T']!!} @endif" data-speed="5000"></h2>
							<h4>@if(isset($data['WHY_CHOOSE_US-3_C'])) {!!$data['WHY_CHOOSE_US-3_C']!!} @endif</h4>
						</div>
					</div>
					<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
						<div class="ed_counter">
							<h2 class="timer" data-from="0" data-to="@if(isset($data['WHY_CHOOSE_US-4_T'])) {!!$data['WHY_CHOOSE_US-4_T']!!} @endif" data-speed="5000"></h2>
							<h4>@if(isset($data['WHY_CHOOSE_US-4_C'])) {!!$data['WHY_CHOOSE_US-4_C']!!} @endif</h4>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!--Timer Section three end -->

<!-- Testimonial start -->
<div class="ed_graysection ed_toppadder90 ed_bottompadder90">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ed_bottompadder80">
				<div class="ed_heading_top">
					<h3>what our client say</h3>
				</div>
			</div>
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="ed_latest_news_slider">
					<div id="owl-demo2" class="owl-carousel owl-theme">
						<div class="item">
							<div class="ed_item_description">
								<img src="front/images/content/t1.jpg" alt="1" title="1"/>
								<h4>Carolyn Ayala</h4>
								<span>CEO</span>
								<p>Just in case there is anyone looking for it, new expertise to our knowledge base to make you happy as well.</p>
							</div>
						</div>
						<div class="item">
							<div class="ed_item_description">
								<img src="front/images/content/t2.jpg" alt="1" title="1"/>
								<h4>Ronnie Parker</h4>
								<span>manager</span>
								<p>Just in case there is anyone looking for it, new expertise to our knowledge base to make you happy as well.</p>
							</div>
						</div>
						<div class="item">
							<div class="ed_item_description">
								<img src="front/images/content/t3.jpg" alt="1" title="1"/>
								<h4>Kim Hiatt</h4>
								<span>director</span>
								<p>Just in case there is anyone looking for it, new expertise to our knowledge base to make you happy as well.</p>
							</div>
						</div>
						<div class="item">
							<div class="ed_item_description">
								<img src="front/images/content/t4.jpg" alt="1" title="1"/>
								<h4>Michael Garza</h4>
								<span>Employee</span>
								<p>Just in case there is anyone looking for it, new expertise to our knowledge base to make you happy as well.</p>
							</div>
						</div>
						<div class="item">
							<div class="ed_item_description">
								<img src="front/images/content/t5.jpg" alt="1" title="1"/>
								<h4>Mary J. Cole</h4>
								<span>receptionist</span>
								<p>Just in case there is anyone looking for it, new expertise to our knowledge base to make you happy as well.</p>
							</div>
						</div> 
					</div>
				</div>
			</div>
		</div>
    </div><!-- /.container -->
</div>
<!--Testimonial end -->
<!--client section start -->
<div class="ed_transprentbg ed_toppadder90 ed_bottompadder90">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="ed_heading_top ed_bottompadder50">
					<h3>our partners</h3>
				</div>
			</div>
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="ed_clientslider">
					<div id="owl-demo5" class="owl-carousel owl-theme">
						@if(isset($data['Our Partners']))
						@foreach($data['Our Partners']  as $key => $value)
						<div class="item">
							<a href="#">
								<img src="{{url('uplode/BannerAndLogo/'.$value)}}" alt="Partner Img" />
							</a>
						</div>
						@endforeach
						@endif
					</div>
				</div>
			</div>
		</div>
    </div><!-- /.container -->
</div>
<!--client section end -->
@endsection