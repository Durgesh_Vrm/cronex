@extends('front.layout')
@section('content')
<!--Breadcrumb start-->
<div class="ed_pagetitle">
<div class="ed_img_overlay"></div>
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12">
				<div class="page_title">
					<h2>Hello, how can we help?</h2>
				</div>
			</div>
			<div class="col-lg-12 col-md-12 col-sm-12">
				<ul class="breadcrumb">
					<li><a href="{{url('/')}}">home</a></li>
					<li>//</li>
					<li><a href="{{url('faq')}}">FAQ</a></li>
				</ul>
			</div>
		</div>
	</div>
</div>
<!--Breadcrumb end-->
<!--FAQ content start-->
<div class="ed_transprentbg ed_toppadder90 ed_bottompadder60">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12">
				<div class="ed_faq_section">
					<ul>
						<li>
							@if(isset($data['FAQ']))
							@foreach($data['FAQ'] as $key => $value)
							@if ($key % 2 == 0)
							<div class="ed_faq_que">
								<h3>{{$value['FAQ_T']}}</h3>
								<p>{!!$value['FAQ_T']!!}</p>
							</div>
							@endif
							@endforeach
							@endif

						</li>
						<li>
							@if(isset($data['FAQ']))
							@foreach($data['FAQ'] as $key => $value)
							@if ($key % 2 == 1)
							<div class="ed_faq_que">
								<h3>{{$value['FAQ_T']}}</h3>
								<p>{!!$value['FAQ_T']!!}</p>
							</div>
							@endif
							@endforeach
							@endif
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>  
</div>
<!--FAQ content end-->
@endsection