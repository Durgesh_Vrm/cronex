@extends('front.layout')
@section('content')
<!--Breadcrumb start-->
<div class="ed_pagetitle">
<div class="ed_img_overlay"></div>
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12">
				<div class="page_title">
					<h2>Terms & Conditions</h2>
				</div>
			</div>
			<div class="col-lg-12 col-md-12 col-sm-12">
				<ul class="breadcrumb">
					<li><a href="{{url('/')}}">home</a></li>
					<li>//</li>
					<li><a href="{{url('t&c')}}">Terms & Conditions</a></li>
				</ul>
			</div>
		</div>
	</div>
</div>
<!--Breadcrumb end-->
<!--Blog content start-->

<div class="ed_transprentbg ed_toppadder90 ed_bottompadder90">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="ed_policy_content">
					@if(isset($data['hone'][0])){!!$data['hone'][0]->sc_value!!} @endif
				</div>
			</div>
		</div>
	</div>
</div>
<!--Blog content end-->
@endsection