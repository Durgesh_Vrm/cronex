@extends('front.layout')
@section('content')
<!--Breadcrumb start-->
<div class="ed_pagetitle">
<div class="ed_img_overlay"></div>
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12">
				<div class="page_title">
					<h2>Our services</h2>
				</div>
			</div>
			<div class="col-lg-12 col-md-12 col-sm-12">
				<ul class="breadcrumb">
					<li><a href="{{url('/')}}">home</a></li>
					<li>//</li>
					<li><a href="{{url('service')}}">Our services</a></li>
				</ul>
			</div>
		</div>
	</div>
</div>
<!--Breadcrumb end-->
<!-- Services start -->
<div class="ed_transprentbg ed_toppadder90 ed_bottompadder60">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="ed_heading_top ed_bottompadder50">
					<h3>our best services</h3>
				</div>
			</div>
			<div class="ed_mostrecomeded_course_slider">
				<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
					<div class="ed_mostrecomeded_course style_2">
						<div class="ed_item_img">
							<img src="@if(isset($data['Our Best Services_Ground Transport'])){{url('uplode/BannerAndLogo/'.$data['Our Best Services_Ground Transport'])}} @endif" alt="item1" class="img-responsive">
						</div>
						<div class="ed_item_description ed_most_recomended_data">
							<h4><a href="#">@if(isset($data['OUR_BEST_SERVICES-1_T'])){{$data['OUR_BEST_SERVICES-1_T']}} @endif</a></h4>
							<p>@if(isset($data['OUR_BEST_SERVICES-1_C'])) {!!$data['OUR_BEST_SERVICES-1_C']!!} @endif</p>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
					<div class="ed_mostrecomeded_course style_2">
						<div class="ed_item_img">
							<img  src="@if(isset($data['Our Best Services_Warehousing'])){{url('uplode/BannerAndLogo/'.$data['Our Best Services_Warehousing'])}} @endif" alt="item1" class="img-responsive">
						</div>
						<div class="ed_item_description ed_most_recomended_data">
							<h4><a href="#">@if(isset($data['OUR_BEST_SERVICES-2_T'])){{$data['OUR_BEST_SERVICES-2_T']}} @endif</a></h4>
							<p>@if(isset($data['OUR_BEST_SERVICES-2_C'])) {!!$data['OUR_BEST_SERVICES-2_C']!!} @endif</p>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
					<div class="ed_mostrecomeded_course style_2">
						<div class="ed_item_img">
							<img  src="@if(isset($data['Our Best Services_Packaging & Storage'])){{url('uplode/BannerAndLogo/'.$data['Our Best Services_Packaging & Storage'])}} @endif" alt="item1" class="img-responsive">
						</div>
						<div class="ed_item_description ed_most_recomended_data">
							<h4><a href="#">@if(isset($data['OUR_BEST_SERVICES-3_T'])){{$data['OUR_BEST_SERVICES-3_T']}} @endif</a></h4>
							<p>@if(isset($data['OUR_BEST_SERVICES-3_C'])) {!!$data['OUR_BEST_SERVICES-3_C']!!} @endif</p>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
					<div class="ed_mostrecomeded_course style_2">
						<div class="ed_item_img">
							<img src="@if(isset($data['Our Best Services_Door-To-Door Delivery'])){{url('uplode/BannerAndLogo/'.$data['Our Best Services_Door-To-Door Delivery'])}} @endif" alt="item1" class="img-responsive">
						</div>
						<div class="ed_item_description ed_most_recomended_data">
							<h4><a href="#">@if(isset($data['OUR_BEST_SERVICES-4_T'])){{$data['OUR_BEST_SERVICES-4_T']}} @endif</a></h4>
							<p>@if(isset($data['OUR_BEST_SERVICES-4_C'])) {!!$data['OUR_BEST_SERVICES-4_C']!!} @endif</p>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
					<div class="ed_mostrecomeded_course style_2">
						<div class="ed_item_img">
							<img  src="@if(isset($data['Our Best Services_Cargo'])){{url('uplode/BannerAndLogo/'.$data['Our Best Services_Cargo'])}} @endif" alt="item1" class="img-responsive">
						</div>
						<div class="ed_item_description ed_most_recomended_data">
							<h4><a href="#">@if(isset($data['OUR_BEST_SERVICES-5_T'])){{$data['OUR_BEST_SERVICES-5_T']}} @endif</a></h4>
							<p>@if(isset($data['OUR_BEST_SERVICES-5_C'])) {!!$data['OUR_BEST_SERVICES-5_C']!!} @endif</p>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
					<div class="ed_mostrecomeded_course style_2">
						<div class="ed_item_img">
							<img  src="@if(isset($data['Our Best Services_Logistic'])){{url('uplode/BannerAndLogo/'.$data['Our Best Services_Logistic'])}} @endif" alt="item1" class="img-responsive">
						</div>
						<div class="ed_item_description ed_most_recomended_data">
							<h4><a href="#">@if(isset($data['OUR_BEST_SERVICES-6_T'])){{$data['OUR_BEST_SERVICES-6_T']}} @endif</a></h4>
							<p>@if(isset($data['OUR_BEST_SERVICES-6_C'])) {!!$data['OUR_BEST_SERVICES-6_C']!!} @endif</p>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
					<div class="ed_mostrecomeded_course style_2">
						<div class="ed_item_img">
							<img src="@if(isset($data['Our Best Services_Air Shipping'])){{url('uplode/BannerAndLogo/'.$data['Our Best Services_Air Shipping'])}} @endif" alt="item1" class="img-responsive">
						</div>
						<div class="ed_item_description ed_most_recomended_data">
							<h4><a href="#">@if(isset($data['OUR_BEST_SERVICES-7_T'])){{$data['OUR_BEST_SERVICES-7_T']}} @endif</a></h4>
							<p>@if(isset($data['OUR_BEST_SERVICES-7_C'])) {!!$data['OUR_BEST_SERVICES-7_C']!!} @endif</p>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
					<div class="ed_mostrecomeded_course style_2">
						<div class="ed_item_img">
							<img src="@if(isset($data['Our Best Services_Ship Delivery'])){{url('uplode/BannerAndLogo/'.$data['Our Best Services_Ship Delivery'])}} @endif" alt="item1" class="img-responsive">
						</div>
						<div class="ed_item_description ed_most_recomended_data">
							<h4><a href="#">@if(isset($data['OUR_BEST_SERVICES-8_T'])){{$data['OUR_BEST_SERVICES-8_T']}} @endif</a></h4>
							<p>@if(isset($data['OUR_BEST_SERVICES-8_C'])) {!!$data['OUR_BEST_SERVICES-8_C']!!} @endif</p>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
					<div class="ed_mostrecomeded_course style_2">
						<div class="ed_item_img">
							<img src="@if(isset($data['Our Best Services_Passenger Transport'])){{url('uplode/BannerAndLogo/'.$data['Our Best Services_Passenger Transport'])}} @endif" alt="item1" class="img-responsive">
						</div>
						<div class="ed_item_description ed_most_recomended_data">
							<h4><a href="#">@if(isset($data['OUR_BEST_SERVICES-9_T'])){{$data['OUR_BEST_SERVICES-9_T']}} @endif</a></h4>
							<p>@if(isset($data['OUR_BEST_SERVICES-9_C'])) {!!$data['OUR_BEST_SERVICES-9_C']!!} @endif</p>
						</div>
					</div>
				</div>
			</div>
		</div>
    </div><!-- /.container -->
</div>
<!-- Services end -->
<!-- Services start -->
<div class="ed_transprentbg ed_toppadder90 ed_bottompadder60">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="ed_heading_top ed_bottompadder50">
					<h3>@if(isset($data['OUR_BEST_SERVICES-10_T'])){{$data['OUR_BEST_SERVICES-10_T']}} @endif</h3>
				</div>
						<div class="ed_item_description ed_most_recomended_data">
							<p>@if(isset($data['OUR_BEST_SERVICES-10_C'])){!!$data['OUR_BEST_SERVICES-10_C']!!} @endif</p>
						</div>
			</div>
		</div>
    </div><!-- /.container -->
</div>
<!-- Services end -->
@endsection