<table border="0" cellspacing="0" cellpadding="0" align="center" width="520" bgcolor="#ffffff" style="background:#ffffff;min-width:520px">
  <tbody><tr>
    <td width="20" bgcolor="#eeeeee" style="background:#eeeeee"></td>
      <td width="480">
      <table border="0" cellspacing="0" cellpadding="0" width="100%">
				<tbody><tr>
					<td height="20" bgcolor="#eeeeee" style="background:#eeeeee"></td>
				</tr>
				<tr>
					<td>
  				 <table border="0" cellspacing="0" cellpadding="0" align="center" width="100%" style="border-bottom:1px solid #eeeeee">
            <tbody><tr>
    			    <td height="49"></td>
             </tr>
             <tr>
             <td align="center" style="color:#4285f4;font-family:&quot;Roboto&quot;,OpenSans,&quot;Open Sans&quot;,Arial,sans-serif;font-size:32px;font-weight:normal;line-height:46px;margin:0;padding:0 25px 0 25px;text-align:center">Hi {{$data['name']}},</td>
             </tr>
             
              <tr>
        	      <td height="20"></td>
              </tr>
             <tr>
              <td  style="color:#757575;font-family:&quot;Roboto&quot;,OpenSans,&quot;Open Sans&quot;,Arial,sans-serif;font-size:17px;font-weight:normal;line-height:24px;margin:0;padding:0 25px 0 25px;text-align:justify"> We have received your order but your payment confirmation is awaited.</td>
             </tr><td height="20"></td>
            <tr>
              <td  style="color:#757575;font-family:&quot;Roboto&quot;,OpenSans,&quot;Open Sans&quot;,Arial,sans-serif;font-size:17px;font-weight:normal;line-height:24px;margin:0;padding:0 25px 0 25px;text-align:justify"> If your payment is deducted, your order will either be confirmed and you will receive a confirmation message within next 15 minutes or the amount will be refunded within 5-7 workings days.</td>
             </tr><td height="20"></td>
            <tr>
              <td  style="color:#757575;font-family:&quot;Roboto&quot;,OpenSans,&quot;Open Sans&quot;,Arial,sans-serif;font-size:17px;font-weight:normal;line-height:24px;margin:0;padding:0 25px 0 25px;text-align:justify"> If have haven't completed payment yet, you can either retry payment or choose cash on delivery to complete your order.</td>
             </tr>
            
            
           
             <tr>
      		    <td height="30">

              </td>
             </tr>
             
  				 </tbody></table>
					</td>
				</tr>
        
        
        
        <tr>
          <td>
            <table border="0" cellspacing="0" cellpadding="0" align="center" width="100%">
            <tbody>
             
             
             
             <tr>
            
             </tr>
             <tr>
              <td height="35"></td>
             </tr>
             
             
             
             <tr>
               <td width="" height="46" align="center" style="font-size:8px">
               <a href="#" style="color:#4285f4;text-decoration:underline" target="_blank" >
               <img width="" height="46" border="0" src="{{asset('/images/shopping-cart.png')}}" alt="Google" style="height:46px;text-align:center;border:none" class="CToWUd">
               </a>
               </td>
             </tr>
             <tr>
              <td height="31"></td>
             </tr>
             <tr>
              <td align="center">
                <table border="0" cellspacing="0" cellpadding="0" align="center" width="405" style="padding-left:4px"> 
                  <tbody><tr>
                    <td width="32" height="32" style="margin:0;padding:0 0 0 0">
                      <a href="#" style="color:#4285f4;text-decoration:underline" target="_blank" >
                        <img width="32" height="32" border="0" src="{{asset('/images/cat_images/grocery.png')}}" alt="Google Search" style="font-size:4px;width:32px;height:32px;text-align:center;border:none" class="CToWUd">
                      </a>
                    </td>
                    
                    <td width="20" style="margin:0;padding:0"></td>
                    <td width="32" height="32" style="margin:0;padding:0 0 0 0">
                    <a href="#" style="color:#4285f4;text-decoration:underline" target="_blank" >
                    <img src="{{asset('/images/cat_images/veg.png')}}" width="32" height="32" alt="Gmail" style="font-size:8px;width:32px;height:32px" class="CToWUd">
                      </a>
                    </td>
                    <td width="18" style="margin:0;padding:0"></td>
                    <td width="32" height="32" style="margin:0;padding:0 0 0 0">
                    <a href="#" style="color:#4285f4;text-decoration:underline" target="_blank" >
                    <img src="{{asset('/images/cat_images/personal.png')}}" width="32" height="32" alt="Google Assistant" style="font-size:3px;width:32px;height:32px" class="CToWUd">
                      </a>
                    </td>
                    <td width="18" style="margin:0;padding:0"></td>
                    <td width="32" height="32" style="margin:0;padding:0 0 0 0">
                    <a href="#" style="color:#4285f4;text-decoration:underline" target="_blank" >
                    <img width="32" height="32" border="0" src="{{asset('/images/cat_images/household-item.png')}}" alt="Maps" style="font-size:8px;width:32px;height:32px;text-align:center;border:none" class="CToWUd">
                      </a>
                    </td>
                    <td width="22" style="margin:0;padding:0"></td>
                    <td width="32" height="32" style="margin:0;padding:0 0 0 0">
                    <a href="#" style="color:#4285f4;text-decoration:underline" target="_blank" >
                    <img width="32" height="32" border="0" src="{{asset('/images/cat_images/home.png')}}" alt="YouTube" style="font-size:7px;width:32px;height:32px;text-align:center;border:none" class="CToWUd">
                      </a>
                    </td>
                    
                    <td width="22" style="margin:0;padding:0"></td>
                    <td width="32" height="32" style="margin:0;padding:0 0 0 0">
                   <a href="#" style="color:#4285f4;text-decoration:underline" target="_blank" >
                    <img width="32" height="32" border="0" src="{{asset('/images/cat_images/biscuits.png')}}" alt="Drive" style="font-size:8px;width:32px;height:32px;text-align:center;border:none" class="CToWUd">
                      </a>
                    </td>
                    <td width="17" style="margin:0;padding:0"></td>
                    <td width="32" height="32" style="margin:0;padding:0 0 0 0">
                    <a href="#" style="color:#4285f4;text-decoration:underline" target="_blank" >
                      <img width="32" height="32" border="0" src="{{asset('/images/cat_images/beverages.png')}}" alt="Photos" style="font-size:8px;width:32px;height:32px;text-align:center;border:none" class="CToWUd"> 
                    </a>
                    </td> 
                    <td width="16" style="margin:0;padding:0"></td>
                    <td width="32" height="32" style="margin:0;padding:0 0 0 0">
                      <a href="#" style="color:#4285f4;text-decoration:underline" target="_blank" >
                        <img width="32" height="32" border="0" src="{{asset('/images/cat_images/dairy.png')}}" alt="Play" style="width:32px;height:32px;text-align:center;border:none" class="CToWUd">
                      </a>
                    </td>                    
                    <td width="18" style="margin:0;padding:0"></td>
                    <td width="32" height="32" style="margin:0;padding:0 0 0 0">
                     <a href="#" style="color:#4285f4;text-decoration:underline" target="_blank" >
                       <img width="32" height="32" border="0" src="{{asset('/images/cat_images/baby-care.png')}}" alt="Chrome" style="font-size:8px;width:32px;height:32px;text-align:center;border:none" class="CToWUd">
                      </a>
                    </td>
                  </tr>
                </tbody></table>
              </td>
             </tr>
             <tr>
  				<td height="28"></td>
				</tr>
        <tr>
          <td height="19" bgcolor="#eeeeee" style="background:#eeeeee"></td>
          </tr>
             <tr>
             <td valign="middle" style="background:#eee;color:#777;font-family:&quot;Roboto&quot;,OpenSans,&quot;Open Sans&quot;,Arial,sans-serif;font-size:10px;font-weight:normal;line-height:14px;margin:0;padding:0 6px 0 6px;text-align:center" align="center">
                      <span style="font-size:inherit;color:inherit;font-weight:inherit;line-height:inherit;font-family:inherit">© 2020 {{ config('APP_NAME') }} | Designed & Developed by <a href="https://appworkstechnologies.in" style="color:#4285f4;text-decoration:underline" target="_blank" >AppWorks Technologies Pvt. Ltd.</a></span>
   
   
<br>This email was sent to you because you created a {{ config('APP_NAME') }} Account.
            </td>
             </tr>
           </tbody></table>
          </td>
        </tr>
        
				<tr>
					<td height="18" bgcolor="#eeeeee" style="background:#eeeeee"></td>
				</tr>
			</tbody></table>
		</td>
		<td width="20" bgcolor="#eeeeee" style="background:#eeeeee"></td>
	</tr>
</tbody></table>