@extends('layouts.app')
@section('content')
<style>
/* width */
::-webkit-scrollbar {
  width: 3px;
}
/* Track */
::-webkit-scrollbar-track {
  background: #f1f1f1; 
}
/* Handle */
::-webkit-scrollbar-thumb {
  background: #888; 
}
/* Handle on hover */
::-webkit-scrollbar-thumb:hover {
  background: #555; 
}
</style>
<?php $color_arrar = array('col-red','col-orange','col-blue-grey','col-yellow','col-pink','col-grey','col-blue','col-green','col-purple','col-cyan','col-lime','col-teal','col-black');?>
      <!-- Main Content -->
      <div class="main-content">
        <section class="section">
          <ul class="breadcrumb breadcrumb-style ">
            <li class="breadcrumb-item">
              <a href="{{url('dashboard')}}">
                <i data-feather="home"></i></a>
            </li>
            <li class="breadcrumb-item">Email</li>
            <li class="breadcrumb-item active">Compose</li>
          </ul>
          <div class="section-body">
            <div class="row">
              <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                <div class="card">
                  <div class="body">
                    <div id="mail-nav">
                     <a href="{{url('email-compose')}}"> <button type="button" class="btn btn-danger waves-effect btn-compose m-b-15">COMPOSE</button></a>
                      <ul class="" id="mail-folders">
                        <li>
                          <a href="{{url('mail-inbox')}}" title="Inbox">Inbox ({{$data['count']}})
                          </a>
                        </li>
                        <li>
                          <a href="{{url('mail-send')}}" title="Sent">Sent</a>
                        </li>
                      </ul>
                      <h5 class="b-b p-10 text-strong">Templates</h5>
                      <ul class="" id="mail-labels" style="height: 441px; overflow-y: scroll;">
                        @foreach($data['all'] as $key => $value)
                        <li>
                          <a href="javascript:;" id="{{$value->id}}" onclick="get_temp(this.id)">
                            <i class="material-icons {{$color_arrar[$key]}}">local_offer</i>{{$value->subject}}</a>
                        </li>
                        @endforeach    
                      </ul>
                    </div>
                  </div>
                </div>
              </div>

              <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                <div class="card">
                  <div class="boxs mail_listing">
                    <div class="inbox-center table-responsive">
                      <table class="table table-hover">
                        <thead>
                          <tr>
                            <th colspan="1">
                              <div class="inbox-header">
                                Compose New Message

                                <div class="btn-group btn-group-md float-right" role="group" aria-label="Basic example" style="margin-top: -3px;">
                                  <button type="button" class="btn btn-outline-primary" id="BCC">BCC</button>
                                  <button type="button" class="btn btn-outline-primary"  id="CC">CC</button>
                                </div>

                            </div>
                            </th>
                          </tr>
                        </thead>
                      </table>
                    </div>
                    <div class="row">
                      <div class="col-lg-12">
                        <form class="composeForm">
                          <div class="form-group">
                            <div class="form-line">
                              <input type="text" id="email_address_to" name="email_address_to" class="form-control" placeholder="TO" autocomplete="off">
                            </div>
                            <div class="list-group" id="TO" style="display: none; height: 200px; overflow-y: scroll;">
                            </div>
                          </div>
                        <div class="form-group" id="IBCC" style="display: none;">
                            <div class="form-line">
                              <input type="text" id="email_address_bcc" name="email_address_bcc" class="form-control" placeholder="BCC" autocomplete="off">
                            </div>
                            <div class="list-group" id="SBCC"  style="display: none;  height: 200px; overflow-y: scroll;">
                            </div>
                          </div>
                        <div class="form-group"  id="ICC"  style="display: none;">
                            <div class="form-line">
                              <input type="text" id="email_address_cc" name="email_address_cc" class="form-control" placeholder="CC" autocomplete="off">
                            </div>
                            <div class="list-group" id="SCC"  style="display: none; height: 200px; overflow-y: scroll;">
                            </div>
                          </div>
                          <div class="form-group">
                            <div class="form-line">
                              <input type="text" id="subject" name="subject"  class="form-control" placeholder="Subject">
                            </div>
                          </div>
                          <textarea class="summernote" id="content" name="content"> </textarea>
                          <div class="compose-editor m-t-20">
                            <input type="file" class="default" multiple>
                          </div>
                        </form>
                      </div>
                      <div class="col-lg-12">
                        <div class="m-l-25 m-b-20">
                          <button type="button" class="btn btn-info btn-border-radius waves-effect">Send</button>
                          <button type="button" class="btn btn-danger btn-border-radius waves-effect">Discard</button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>


<script type="text/javascript">

$("#email_address_to").on("input", function() {
  var emails = $("#"+this.id).val();
  var show_emails='';
  $.ajax({
       type:"POST",
       url:"{{url('Get-to-emails')}}",
       data:{"_token": "{{ csrf_token() }}", "emails":emails }, 
       success:function(result)
       {  
          if(result != 'error')
          {     $('#TO').show();
                $('#TO').empty();
                 show_emails += ('<a class="list-group-item list-group-item-action"> <button class="btn btn-primary btn-sm" onclick="select_hi_email();">Hidden Filter</button> <button type="button" class="btn btn-primary btn-sm" onclick="select_email();">Select Selected Email</button> <button class="btn btn-primary btn-sm" onclick="select_all_email();">Select All Email</button> </a>');
                $.each(result, function (key, val) {
                 show_emails += ('<a href="#" class="list-group-item list-group-item-action ">&ensp; <input type="checkbox" name="email_add" id="email_add" value="'+val.email+'"> <span onclick="select_this_to(this.id);" id="'+val.email+'">'+val.email+'</span> </a>');
                });
                $('#TO').append(show_emails);
          }     
       }
    });
});

function select_email() {
   var SelectedEmail = $("input[name=email_add]:checked").map(function () {
    return this.value;
  }).get().join(",");
   if(SelectedEmail != '')
   {
      $('#email_address_to').val(SelectedEmail);
      $('#TO').empty();
      $('#TO').hide();
   }
   else
   {
        swal({
          title: "Select At Least One Email",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        });
   }
}



function select_all_email() {
   var SelectedEmail = $("input[name=email_add]").map(function () {
    return this.value;
  }).get().join(",");
   if(SelectedEmail != '')
   {
      $('#email_address_to').val(SelectedEmail);
      $('#TO').empty();
      $('#TO').hide();
   }
}



function select_hi_email() {
      $('#TO').empty();
      $('#TO').hide();
}


function select_this_to(em) {
  $('#email_address_to').val(em);
  $('#TO').empty();
  $('#TO').hide();
}





$("#email_address_bcc").on("input", function() {
  var emails = $("#"+this.id).val();
  var show_emails='';
  $.ajax({
       type:"POST",
       url:"{{url('Get-to-emails')}}",
       data:{"_token": "{{ csrf_token() }}", "emails":emails }, 
       success:function(result)
       {  
          if(result != 'error')
          {     $('#SBCC').show();
                $('#SBCC').empty();
                 show_emails += ('<a class="list-group-item list-group-item-action"> <button  type="button"  class="btn btn-primary btn-sm" onclick="select_bhi_email();">Hidden Filter</button> <button type="button" class="btn btn-primary btn-sm" onclick="select_bemail();">Select Selected Email</button> <button  type="button" class="btn btn-primary btn-sm" onclick="select_ball_email();">Select All Email</button> </a>');
                $.each(result, function (key, val) {
                 show_emails += ('<a href="#" class="list-group-item list-group-item-action ">&ensp; <input type="checkbox" name="bemail_add" id="bemail_add" value="'+val.email+'">  <span onclick="select_this_bcc(this.id);" id="'+val.email+'">'+val.email+'</span></a>');
                });
                $('#SBCC').append(show_emails);

          }     
       }
    });
});



function select_bemail() {
   var SelectedEmail = $("input[name=bemail_add]:checked").map(function () {
    return this.value;
  }).get().join(",");
   if(SelectedEmail != '')
   {
      $('#email_address_bcc').val(SelectedEmail);
      $('#SBCC').empty();
      $('#SBCC').hide();
   }
   else
   {
        swal({
          title: "Select At Least One Email",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        });
   }
}



function select_ball_email() {
   var SelectedEmail = $("input[name=bemail_add]").map(function () {
    return this.value;
  }).get().join(",");
   if(SelectedEmail != '')
   {
      $('#email_address_bcc').val(SelectedEmail);
      $('#SBCC').empty();
      $('#SBCC').hide();
   }
}



function select_bhi_email() {
      $('#SBCC').empty();
      $('#SBCC').hide();
}


function select_this_bcc(email) {
      $('#email_address_bcc').val(email);
      $('#SBCC').empty();
      $('#SBCC').hide();
}



$("#email_address_cc").on("input", function() {
  var emails = $("#"+this.id).val();
  var show_emails='';
  $.ajax({
       type:"POST",
       url:"{{url('Get-to-emails')}}",
       data:{"_token": "{{ csrf_token() }}", "emails":emails }, 
       success:function(result)
       {  
          if(result != 'error')
          {     $('#SCC').show();
                $('#SCC').empty();
                 show_emails += ('<a class="list-group-item list-group-item-action"> <button  type="button"  class="btn btn-primary btn-sm" onclick="select_bcchi_email();">Hidden Filter</button> <button type="button" class="btn btn-primary btn-sm" onclick="select_bccemail();">Select Selected Email</button> <button  type="button" class="btn btn-primary btn-sm" onclick="select_bccall_email();">Select All Email</button> </a>');
                $.each(result, function (key, val) {
                 show_emails += ('<a href="#" class="list-group-item list-group-item-action ">&ensp; <input type="checkbox" name="bccemail_add" id="bccemail_add" value="'+val.email+'"> <span onclick="select_this_cc(this.id);" id="'+val.email+'">'+val.email+' </span></a>');
                });
                $('#SCC').append(show_emails);

          }     
       }
    });
});



function select_bccemail() {
   var SelectedEmail = $("input[name=bccemail_add]:checked").map(function () {
    return this.value;
  }).get().join(",");
   if(SelectedEmail != '')
   {
      $('#email_address_cc').val(SelectedEmail);
      $('#SCC').empty();
      $('#SCC').hide();
   }
   else
   {
        swal({
          title: "Select At Least One Email",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        });
   }
}



function select_bccall_email() {
   var SelectedEmail = $("input[name=bccemail_add]").map(function () {
    return this.value;
  }).get().join(",");
   if(SelectedEmail != '')
   {
      $('#email_address_cc').val(SelectedEmail);
      $('#SCC').empty();
      $('#SCC').hide();
   }
}



function select_bcchi_email() {
      $('#SCC').empty();
      $('#SCC').hide();
}



function select_this_cc(email) {
      $('#email_address_cc').val(email);
      $('#SCC').empty();
      $('#SCC').hide();
}


function get_temp(id) {
    $.ajax({
       type:"POST",
       url:"{{url('get-email-templ')}}",
       data:{"_token": "{{ csrf_token() }}", "id":id }, 
       success:function(result)
       {  
        if(result != 'error')
        {
        $('#subject').val('');
        $('textarea').val('');
        $('#subject').val(result.subject);
        $(".summernote").summernote("code", result.content);
        }
        else
        {
          swal({
            title: "Message Template Not Found",
            icon: "warning",
            buttons: true,
            dangerMode: true,
          });
        }
       }
     });
}
</script>
@endsection