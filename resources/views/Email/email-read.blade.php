@extends('layouts.app')
@section('content')
<style>
/* width */
::-webkit-scrollbar {
  width: 3px;
}
/* Track */
::-webkit-scrollbar-track {
  background: #f1f1f1; 
}
/* Handle */
::-webkit-scrollbar-thumb {
  background: #888; 
}
/* Handle on hover */
::-webkit-scrollbar-thumb:hover {
  background: #555; 
}
</style>
<?php $color_arrar = array('col-red','col-orange','col-blue-grey','col-yellow','col-pink','col-grey','col-blue','col-green','col-purple','col-cyan','col-lime','col-teal','col-black');?>
      <!-- Main Content -->
      <div class="main-content">
        <section class="section">
          <ul class="breadcrumb breadcrumb-style ">
            <li class="breadcrumb-item">
              <h4 class="page-title m-b-0">Read</h4>
            </li>
            <li class="breadcrumb-item">
              <a href="{{url('dashboard')}}">
                <i data-feather="home"></i></a>
            </li>
            <li class="breadcrumb-item">Email</li>
            <li class="breadcrumb-item">Read</li>
          </ul>
          <div class="section-body">
            <div class="row">
              <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                <div class="card">
                  <div class="body">
                    <div id="mail-nav">
                     <a href="{{url('email-compose')}}"> <button type="button" class="btn btn-danger waves-effect btn-compose m-b-15">COMPOSE</button></a>
                      <ul class="" id="mail-folders">
                        <li>
                          <a href="{{url('mail-inbox')}}" title="Inbox">Inbox ({{$data['count']}})
                          </a>
                        </li>
                        <li>
                          <a href="{{url('mail-send')}}" title="Sent">Sent</a>
                        </li>
                      </ul>
                      <h5 class="b-b p-10 text-strong">Templates</h5>
                      <ul class="" id="mail-labels" style="height: 441px; overflow-y: scroll;">
                        @foreach($data['all'] as $key => $value)
                        <li>
                          <a href="javascript:;" id="{{$value->id}}" onclick="get_temp(this.id)">
                            <i class="material-icons {{$color_arrar[$key]}}">local_offer</i>{{$value->subject}}</a>
                        </li>
                        @endforeach    
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                <div class="card">
                  <div class="boxs mail_listing">
                    <div class="inbox-body no-pad">
                      <section class="mail-list">
                        <div class="mail-sender">
                          <div class="mail-heading">
                            <h4 class="vew-mail-header">
                              <b>Hi Dear, How are you?</b>
                            </h4>
                          </div>
                          <hr>
                          <div class="media">
                            <a href="#" class="table-img m-r-15">
                              <img alt="image" src="assets/img/users/user-2.png" class="rounded-circle" width="35"
                                data-toggle="tooltip" title="Sachin Pandit">
                            </a>
                            <div class="media-body">
                              <span class="date pull-right">4:15AM 04 April 2017</span>
                              <h5 class="col-black">Sarah Smith</h5>
                              <small class="text-muted">From: sarah@example.com</small>
                            </div>
                          </div>
                        </div>
                        <div class="view-mail p-t-20">
                          <p>Donec ultrices faucibus rutrum. Phasellus sodales vulputate urna, vel
                            accumsan augue
                            egestas ac. Donec vitae leo at sem lobortis porttitor eu consequat risus.
                            Mauris
                            sed congue orci. Donec ultrices faucibus rutrum. Phasellus sodales
                            vulputate urna,
                            vel accumsan augue egestas ac. Donec vitae leo at sem lobortis porttitor eu
                            consequat
                            risus. Mauris sed congue orci. Donec ultrices faucibus rutrum. Phasellus
                            sodales
                            vulputate urna, vel accumsan augue egestas ac. Donec vitae leo at sem
                            lobortis porttitor
                            eu consequat risus. Mauris sed congue orci.</p>
                          <p>
                            Porttitor eu consequat risus.
                            <a href="#">Mauris sed congue orci. Donec ultrices faucibus rutrum</a>.
                            Phasellus sodales vulputate
                            urna, vel accumsan augue egestas ac. Donec vitae leo at sem lobortis
                            porttitor eu
                            consequat risus. Mauris sed congue orci. Donec ultrices faucibus rutrum.
                            Phasellus
                            sodales vulputate urna, vel accumsan augue egestas ac. Donec vitae leo at
                            sem lobortis
                            porttitor eu consequat risus. Mauris sed congue orci.
                          </p>
                          <p>
                            Sodales
                            <a href="#">vulputate urna, vel accumsan augue egestas ac</a>. Donec vitae
                            leo at sem lobortis
                            porttitor eu consequat risus. Mauris sed congue orci. Donec ultrices
                            faucibus rutrum.
                            Phasellus sodales vulputate urna, vel accumsan augue egestas ac. Donec
                            vitae leo
                            at sem lobortis porttitor eu consequat risus. Mauris sed congue orci.
                          </p>
                        </div>
                        <div class="attachment-mail">
                          <p>
                            <span>
                              <i class="fa fa-paperclip"></i> 3 attachments — </span>
                            <a href="#">Download all attachments</a> |
                            <a href="#">View all images</a>
                          </p>
                          <div class="row">
                            <div class="col-md-2">
                              <a href="#">
                                <img class="img-thumbnail img-responsive" alt="attachment"
                                  src="assets/img/users/user-1.png">
                              </a>
                              <a class="name" href="#"> IMG_001.png
                                <span>20KB</span>
                              </a>
                            </div>
                            <div class="col-md-2">
                              <a href="#">
                                <img class="img-thumbnail img-responsive" alt="attachment"
                                  src="assets/img/users/user-3.png">
                              </a>
                              <a class="name" href="#"> IMG_002.png
                                <span>22KB</span>
                              </a>
                            </div>
                            <div class="col-md-2">
                              <a href="#">
                                <img class="img-thumbnail img-responsive" alt="attachment"
                                  src="assets/img/users/user-4.png">
                              </a>
                              <a class="name" href="#"> IMG_003.png
                                <span>28KB</span>
                              </a>
                            </div>
                          </div>
                        </div>
                        <div class="replyBox m-t-20">
                          <p class="p-b-20">click here to
                            <a href="#">Reply</a> or
                            <a href="#">Forward</a>
                          </p>
                        </div>
                      </section>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
@endsection