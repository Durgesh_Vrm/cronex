@extends('mainlayout')
@section('content')
<!--CONTENT CONTAINER-->
<!--===================================================-->
<div id="content-container">
    <div id="page-head">
        
        <!--Page Title-->
        <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
        <div id="page-title">
            <h1 class="page-header text-overflow">Master Panel</h1>
        </div>
        <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
        <!--End page title-->


        <!--Breadcrumb-->
        <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
        <ol class="breadcrumb">
        <li><a href="{{ url('/') }}"><i class="demo-pli-home"></i></a></li>
        <li><a href="{{ url('/') }}">Dashboard</a></li>
        <li class="active">Send Mail</li>
        </ol>
        <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
        <!--End breadcrumb-->

    </div>

    
    <!--Page content-->
    <!--===================================================-->
    <div id="page-content">

        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif

        @if ($message = Session::get('message'))
          <div class="alert alert-info alert-dismissible alert-cstm center mt30">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <span class="center">{{ $message }}</span>
          </div>
        @endif

        <div class="row">
            <div class="col-md-12">
            
                <div class="panel">
                    <div class="panel-heading">
                        <div class="panel-control">
                            <button class="btn btn-default" data-panel="minmax"><i class="demo-psi-chevron-up"></i></button>
                        </div>
                        <h3 class="panel-title">Send Mail</h3>
                    </div>

                    <div class="collapse in">
                        <form class="form-horizontal" action="{{url('send_mail')}}" method="POST">
                            {{ csrf_field() }}
                            <div class="panel-body">
                                <div class="row">    

                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Name</label>
                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                          <input type="text" id="txt_name" name="txt_name" class="form-control" placeholder="Enter Name">
                                        </div>
                                    </div> 

                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Email</label>
                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                          <input type="text" id="txt_email" name="txt_email" class="form-control" placeholder="Enter Email">
                                        </div>
                                    </div>    

                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Subject</label>
                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                          <input type="text" id="txt_sub" name="txt_sub" class="form-control" placeholder="Enter Subject">
                                        </div>
                                    </div>                          
                                
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Message</label>
                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                          <textarea id="txt_msg" name="txt_msg" class="form-control" placeholder="Enter Message." rows="3"></textarea>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="panel-footer text-center">
                                <button type="submit" class="btn btn-success">Save</button>
                                <button class="btn btn-primary" type="reset">Reset</button>                            
                            </div>
                        </form>
                        
                    </div>
        
                </div>
            </div>
            
        </div>
            
    </div>
    <!--===================================================-->
    <!--End page content-->

</div>
<!--===================================================-->
<!--END CONTENT CONTAINER-->
@endsection

