@extends('layouts.app')
@section('content')
      <!-- Main Content -->
      <div class="main-content">
        <section class="section">
          <ul class="breadcrumb breadcrumb-style ">
            <li class="breadcrumb-item">
              <a href="{{url('home')}}">
                <i data-feather="home"></i></a>
            </li>
            <li class="breadcrumb-item">Setting</li>
            <li class="breadcrumb-item">User Role & Permissions</li>
          </ul>
          <div class="section-body">
            <div class="row">
              <div class="col-12 col-sm-12 col-lg-12">
                <div class="card">
                          <div class="card-header">
                            <h4>User Role & Permissions</h4>
                          </div>
                          <div class="card-body">
                              <div class="row">
                              <table class="table table-sm">
                                <thead>
                                  <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Role</th>
                                    <th scope="col">View</th>
                                    <th scope="col">Create</th>
                                    <th scope="col">Update</th>
                                    <th scope="col">Delete</th>
                                    <th scope="col">Action</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  @php $sn=0; @endphp 
                                  @foreach($data['all'] as $value)
                                  <tr>
                                    <th scope="row">{{++$sn}}</th>
                                    <th scope="row"><span  id="a_div{{$value->id}}"> {{$value->role_name}} </span>
                              
                                      <input  class="form-control form-control-sm col-sm-8"  style="display: none" type="text" name="role_name{{$value->id}}" id="role_name{{$value->id}}" value="{{$value->role_name}}" required> 
                                    </th>
                                    <td>
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" @if($value->role_name == 'Administrator' || $value->role_name == 'User') disabled @else onclick="change_view_role({{$value->id}})" @endif value="{{$value->view}}" id="View{{$value->id}}" {{$value->view == 'Yes' ? 'checked':''}}>
                                        <label class="custom-control-label" for="View{{$value->id}}"></label>
                                      </div>
                                    </td>
                                    <td>
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" @if($value->role_name == 'Administrator' || $value->role_name == 'User') disabled @else onclick="change_create_role({{$value->id}})" @endif value="{{$value->create}}" id="Create{{$value->id}}" {{$value->create == 'Yes' ? 'checked':''}}>
                                        <label class="custom-control-label" for="Create{{$value->id}}"></label>
                                      </div>
                                    </td>
                                    <td>
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" @if($value->role_name == 'Administrator' || $value->role_name == 'User') disabled @else onclick="change_update_role({{$value->id}})" @endif value="{{$value->update}}" id="Update{{$value->id}}" {{$value->update == 'Yes' ? 'checked':''}}>
                                        <label class="custom-control-label" for="Update{{$value->id}}"></label>
                                      </div>
                                    </td>
                                    <td>
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" @if($value->role_name == 'Administrator' || $value->role_name == 'User') disabled @else onclick="change_delete_role({{$value->id}})" @endif value="{{$value->delete}}" id="Delete{{$value->id}}" {{$value->delete == 'Yes' ? 'checked':''}}>
                                        <label class="custom-control-label" for="Delete{{$value->id}}"></label>
                                      </div>
                                    </td>
                                    <td>
                                        <button class="btn btn-primary btn-sm" type="button" id="bt{{$value->id}}"  @if($value->role_name == 'Administrator' || $value->role_name == 'User') disabled @else  onclick="EditRole('{{$value->id}}');"  @endif><i class="fas fa-edit"></i> Edit </button>
                                        <button class="btn btn-success btn-sm" style="display: none"  onclick="Save('{{$value->id}}');" id="btn{{$value->id}}" type="button"><i class="fas fa-save"></i>  Save </button>
                                        <button class="btn btn-danger btn-sm" style="display: none"  onclick="CHidde('{{$value->id}}');" id="btns{{$value->id}}" type="button"><i class="fas fa-times"></i>  Cancel </button>
                                    </td>
                                  </tr>
                                  @endforeach
                                </tbody>
                              </table>
                            </div>
                          </div>
                </div>
              </div>
            </div>

          </div>
        </section>
      </div>


<script type="text/javascript">
  //edit form confirmation
  function EditRole(id)
  {
    swal({
      title: "Are you sure you want to edit?",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
        
        $('#a_div'+id).hide();
        $('#role_name'+id).show("slow");        
        $('#bt'+id).hide();
        $('#btn'+id).show("slow");
        $('#btns'+id).show("slow");
      } 
    });
  }



  function CHidde(id) {
        $('#a_div'+id).show("slow");
        $('#role_name'+id).hide();        
        $('#bt'+id).show("slow");
        $('#btn'+id).hide();
        $('#btns'+id).hide();
  }



  function Save(id)
  {
      var nam = $('#role_name'+id).val();

        $.ajax({
         type:"POST",
         url:"{{ url('/change-user-role-name') }}",
         data:{"_token": "{{ csrf_token() }}", "id": id, "name": nam},
         success:function(result)
         { 
              location.reload();
         }
        });      
  }

  



  function change_view_role(id) {
    var valu = $('#View'+id).val();
      $.ajax({
         type:"POST",
         url:"{{ url('/change-user-role-view-p') }}",
         data:{"_token": "{{ csrf_token() }}", "id": id, "valu": valu},
         success:function(result)
         { 
              location.reload();
         }
      }); 
  }

  function change_create_role(id) {
    var valu = $('#Create'+id).val();
      $.ajax({
         type:"POST",
         url:"{{ url('/change-user-role-create-p') }}",
         data:{"_token": "{{ csrf_token() }}", "id": id, "valu": valu},
         success:function(result)
         { 
              location.reload();
         }
      }); 
  }

  function change_update_role(id) {
    var valu = $('#Update'+id).val();
      $.ajax({
         type:"POST",
         url:"{{ url('/change-user-role-update-p') }}",
         data:{"_token": "{{ csrf_token() }}", "id": id, "valu": valu},
         success:function(result)
         { 
              location.reload();
         }
      }); 
  }

  function change_delete_role(id) {
    var valu = $('#Delete'+id).val();
      $.ajax({
         type:"POST",
         url:"{{ url('/change-user-role-delete-p') }}",
         data:{"_token": "{{ csrf_token() }}", "id": id, "valu": valu},
         success:function(result)
         { 
              location.reload();
         }
      }); 
  }

  function show_message() {
    swal({
      title: "Are you sure you want to edit?",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
  }
   
</script>
@endsection