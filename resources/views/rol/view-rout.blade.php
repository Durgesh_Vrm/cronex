@extends('layouts.app')
@section('content')
      <!-- Main Content -->
      <div class="main-content">
        <section class="section">
          <ul class="breadcrumb breadcrumb-style ">
            <li class="breadcrumb-item">
              <a href="index.html">
                <i data-feather="home"></i></a>
            </li>
            <li class="breadcrumb-item">Setting</li>
            <li class="breadcrumb-item">User Permissions</li>
          </ul>
          <div class="section-body">
            <div class="row">
              <div class="col-12 col-sm-12 col-lg-12">
                <div class="card">
                          <div class="card-header">
                            <h4>User Permissions</h4>
                          </div>
                          <div class="card-body">
                              <div class="row">
                              <table class="table table-striped table-hover table-responsive"  id="tableExport" style="width:100%;">
                                <thead>
                                  <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Route Name</th>
                                    @foreach($data['role'] as $value)
                                    <th scope="col">{{$value->role_name}}</th>
                                    @endforeach
                                  </tr>
                                </thead>
                                <tbody>
                                  <tr>
                                    <th >0</th>
                                    <th >Select All Routes</th>
                                    @foreach($data['role'] as $value)
                                    @php $ids = $value->role_id; @endphp
                                    <th>
                                        <div class="preview text-success"  onclick="route_all_permissions(this.id)" value="{{Crypt::encrypt($value->role_id)}}" id="customChecks{{$value->id}}"><i class="material-icons">done_all</i> <span class="icon-name"></span></div>
                                    </th>
                                  @endforeach
                                  </tr>
                                  @php $sn=0; @endphp
                                  @foreach($data['all'] as $values)
                                  <tr>
                                    <th scope="row">{{++$sn}}</th>
                                    <th>{{$values->route}}</th>
                                  @foreach($data['role'] as $value)
                                  @if($value->role_id != $values)
                                    @php $ids = $value->role_id; @endphp
                                    <td>
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" onclick="route_permissions(this.id)" permission="{{$values->$ids}}" data="{{$values->id}}" value="{{Crypt::encrypt($value->role_id)}}" id="customCheck{{$value->id}}{{$values->id}}" {{ $values->$ids == 'Yes'? 'checked':''}} >
                                        <label class="custom-control-label" for="customCheck{{$value->id}}{{$values->id}}"></label>
                                      </div>
                                    </td>
                                  @endif
                                  @endforeach
                                  </tr>
                                  @endforeach
                                </tbody>
                              </table>

                            </div>
                          </div>
                </div>
              </div>
            </div>

          </div>
        </section>
      </div>


<script type="text/javascript">
      
  function route_permissions(id) {
     var roleID = $('#'+id).val();
     var routeID = $('#'+id).attr('data');
     var permissionID = $('#'+id).attr('permission');

        swal({
          title: "Are you sure you want to change user role",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {
              $.ajax({
               type:"POST",
               url:"{{ url('/change-route-permissions') }}",
               data:{"_token": "{{ csrf_token() }}", "roleID": roleID, "routeID": routeID, "permissionID": permissionID},
               success:function(result)
               {  
                  swal({
                        title: "Successfully change role",
                        icon: "success",
                        button: "OK",
                      }).then((willDelete) => { if (willDelete) { location.reload();  }});      
               }
            });
          } 
        });
  }

  function route_all_permissions(id) {
     var roleID = $('#'+id).attr('value');

        swal({
          title: "Are you sure you want to change user role",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {
              $.ajax({
               type:"POST",
               url:"{{ url('/all-route-permissions') }}",
               data:{"_token": "{{ csrf_token() }}", "roleID": roleID},
               success:function(result)
               {  
                  swal({
                        title: "Successfully change role",
                        icon: "success",
                        button: "OK",
                      }).then((willDelete) => { if (willDelete) { location.reload();  }});      
               }
            });
          } 
        });
  }
   
</script>
@endsection