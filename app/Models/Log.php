<?php

namespace App\Models;
use Auth;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class Log extends Model
{
    use HasFactory;

    protected $table = 'user_log';

    protected $fillable = [
        'ip_address',
        'user_agent',
        'payload',
        'login',
        'last_activity',
    ];

    protected $hidden = [
        'email',
    ]; 

	public function Create($data)
	{
		return $this->insert($data);
	}

	public function Last()
	{
		return Log::orderBy('id', 'desc')->where('email', Auth::user()->email )->first();
	}

	public function UpdateLog($id, $data)
	{
		return $this->where('id',$id)->update($data);
	}
 
}
