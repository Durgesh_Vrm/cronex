<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CenterPercentage extends Model
{
    use HasFactory;
    protected $table = 'center_margin';

    protected $fillable = [
        'Hub Center',
        'State Center',
        'Out of State Center',
        'Branch Center',
        'type',
        'created_at',
        'updated_at',
    ];

    protected $hidden = [
        'id',
    ];

    public function UpdateData($id, $data)
    {
        return $this->where('id',$id)->update($data);
    }
    public function GetById($id)
    {
        return CenterPercentage::where('id', $id)->first();
    } 
}
