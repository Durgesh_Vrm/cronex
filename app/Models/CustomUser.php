<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CustomUser extends Model
{
    use HasFactory;

    protected $table = 'usr02_old_user_master_authorizations';

    protected $fillable = [
        'USR02_name',
        'USR02_email',
        'USR02_role',
        'USR02_image',
        'USR02_signature',
        'USR02_gander',
        'USR02_dob',
        'USR02_father_name',
        'USR02_mother_name',
        'USR02_phone_no',
        'USR02_other_phone_no',
        'USR02_bio',
        'USR02_facebook',
        'USR02_twitter',
        'USR02_instagram',
        'USR02_linkedin',
    ];

    protected $hidden = [
        'USR02_remember_token',
        'USR02_password',
    ];

    protected $casts = [
        'USR02_email_verified_at' => 'datetime',
    ];


}
