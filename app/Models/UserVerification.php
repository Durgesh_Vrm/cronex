<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;

class UserVerification extends Model
{
    use HasFactory;

    protected $table = 'user_verification';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_otp',
        'device_info',
        'device_ip',
        'opt_active',
        'user_active',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'id',
        'user_id',
    ];



    public function Create($data)
    {   
        return $this->insert($data);
    }

    public function UpdateAt($id, $data)
    {   
        return UserVerification::where('user_id',$id)->update($data);
    }

    public function GetById($id)
	{
		return UserVerification::where('user_id', $id)->first();
	}  
}
