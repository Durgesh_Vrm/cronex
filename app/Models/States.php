<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class States extends Model
{
    use HasFactory;

    protected $table = 'states';

    protected $fillable = [
        'id',
        'name',
        'iso2',
        'country_id',
        'country_code',
        'fips_code',
        'latitude',
        'longitude',
        'image',
    ];

    protected $hidden = [
       
    ]; 

	public function Create($data)
	{
		return States::insert($data);
	}

	public function GetById($id)
	{
		return States::where('id', $id)->first();
	}		


	public function GetData()
	{
		return States::get();
	}

	public function GetSelectData()
    {
        return States::select('id','country_id','image','name')->get();
    }

	public function UpdateData($id, $data)
	{
		return States::where('id',$id)->update($data);
	}

}
