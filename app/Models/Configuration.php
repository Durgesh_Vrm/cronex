<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Configuration extends Model
{
    use HasFactory;

    protected $table = 'configuration';

    protected $fillable = [
        'name',
        'value',
        'desc',
        'status',
    ];

    protected $hidden = [
        'id',
    ];

    public function GetConfig()
	{
		return Configuration::get();
	} 

    public function Create($data)
    {
        return $this->insert($data);
    }

    public function UpdateData($id, $data)
    {
        return $this->where('id',$id)->update($data);
    }

    public function GetById($id)
    {
        return Configuration::where('id', $id)->first();
    } 

}
