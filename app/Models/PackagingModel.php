<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PackagingModel extends Model
{
    use HasFactory;
    protected $table = 'packaging_modal';

    protected $fillable = [
        'id',
        'name',
        'width',
        'height',
        'image',
        'desc',
        'created_at',
        'updated_at',
    ];

    protected $hidden = [
        'user_id',
    ];

    public function GetAll()
	{
		return PackagingModel::get();
	} 

    public function Create($data)
    {
        return $this->insert($data);
    }
    public function UpdateData($id, $data)
    {
        return $this->where('id',$id)->update($data);
    }

    public function GetPack($id)
    {
        return PackagingModel::whereIn('user_id', [0, $id])->get();
    }

    public function GetById($id)
    {
        return PackagingModel::where('id', $id)->first();
    }

    public function deletePack($id)
    {
        return PackagingModel::where('id', $id)->whereNotIn('user_id', 0)->delete();
    }

    public function GetByUserId($id)
    {
        return PackagingModel::whereIn('user_id', [0, $id])->first();
    }  
}
