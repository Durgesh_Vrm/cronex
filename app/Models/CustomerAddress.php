<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;

class CustomerAddress extends Model
{
    use HasFactory;

    protected $table = 'customer_address';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'phone_number',
        'type',
        'address',
        'street',
        'city',
        'state',
        'country',
        'zip',
        'default',
        'longitude',
        'latitude',
        'status',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'user_id',
    ];



    public function Create($data)
    {   
        return $this->insert($data);
    }

    public function UpdateAt($id, $uid, $data)
    {   
        return CustomerAddress::where('id', $id)->where('user_id', $uid)->update($data);
    }

    public function GetByUId($id)
	{
		return CustomerAddress::where('customer_address.user_id', $id)->leftJoin('countries', 'customer_address.country', '=', 'countries.id')->leftJoin('states', 'customer_address.state', '=', 'states.id')->leftJoin('cities', 'customer_address.city', '=', 'cities.id')->select('customer_address.*','countries.id as countrieID','countries.name as country','countries.phonecode','states.id as stateID','states.name as state','cities.id as cityID','cities.name as city')->get();
	}  

    public function GetById($id, $uid)
	{
		return CustomerAddress::where('customer_address.id', $id)->where('customer_address.user_id', $uid)->leftJoin('countries', 'customer_address.country', '=', 'countries.id')->leftJoin('states', 'customer_address.state', '=', 'states.id')->leftJoin('cities', 'customer_address.city', '=', 'cities.id')->select('customer_address.*','countries.id as countrieID','countries.name as country','countries.phonecode','states.id as stateID','states.name as state','cities.id as cityID','cities.name as city')->first();
	} 

    public function deleteAdd($id, $uid)
	{
		return CustomerAddress::where('id', $id)->where('user_id', $uid)->delete();
	} 

}
