<?php

namespace App\Models;
use Auth;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    use HasFactory;

    protected $table = 'teams';

    protected $fillable = [
        'name',
        'personal_team',
    ];

    protected $hidden = [
        'id',
        'user_id',
    ]; 

	public function Create($data)
	{
		return $this->insert($data);
	}

	public function GetData()
	{
		return Team::leftJoin('users', 'teams.user_id', '=', 'users.id')->select('teams.id','teams.team_name','users.name','teams.personal_team','teams.created_at','teams.updated_at')->get();
	}

    public function GetByRol($id)
    {
        return Team::where('role_id', $id)->first();
    }

	public function UpdateLog($id, $data)
	{
		return $this->where('id',$id)->update($data);
	}
 
}
