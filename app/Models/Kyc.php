<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kyc extends Model
{
    use HasFactory;

    protected $table = 'kyc';

    protected $fillable = [
        'id_proof_type',
        'id_proof_img',
        'id_proof_no',
        'id_proof_veryfy',
        'aadhaar_proof_img',
        'aadhaar_proof_img_back',
        'aadhaar_proof_no',
        'aadhaar_proof_veryfy',
        'pan_img',
        'pan_no',
        'pan_veryfy',
        'status',
    ];

    protected $hidden = [
        'user_id',
    ];

    public function Create($data)
	{
		return $this->insert($data);
	}      

	public function GetByUser($id)
	{
		return Kyc::where('user_id', $id)->first();
	}  

}
