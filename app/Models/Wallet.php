<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Wallet extends Model
{
    use HasFactory;

    protected $table = 'wallet';


    protected $fillable = [
        'main_balance', 
        'wallet_balance', 
    ];


    protected $hidden = [
        'id',
        'user_id',
    ];


   	protected $casts = [
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
    ];

    public function Create($data)
	{
		return $this->insert($data);
	}    

    public function Update($id, $data)
    {
        return $this->where('id' ,$id)->update($data);
    } 
 
    public function GetAll()
    {   
        return $this->where('user_id', $id)->first();
    }   

    public function GetByid($id)
    {   
        return $this->where('user_id', $id)->first();
    }   

}
