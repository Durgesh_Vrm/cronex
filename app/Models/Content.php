<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Content extends Model
{
    use HasFactory;

    protected $table = 'static_content';

    protected $fillable = [
        'sc_for',
        'sc_type',
        'sc_name',
        'sc_title',
        'sc_value',
        'sc_desc',
        'sc_status',
        'created_at',
        'updated_at',
    ];

    protected $hidden = [
        'id',
        'affiliate_id',
    ];

    public function GetConfig()
	{
		return Content::get();
	} 

    public function Create($data)
    {
        return $this->insert($data);
    }

    public function UpdateData($id, $data)
    {
        return $this->where('id',$id)->update($data);
    }

    public function GetById($id)
    {
        return Content::where('id', $id)->first();
    } 

}
