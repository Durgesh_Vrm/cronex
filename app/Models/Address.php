<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    use HasFactory;

    protected $table = 'address';

    protected $fillable = [
        'type',
        'address',
        'street',
        'city',
        'country',
        'zip',
        'default',
        'map',
        'status',
    ];

    protected $hidden = [
        'user_id',
    ];

    public function Create($data)
	{
		return $this->insert($data);
	}      

	public function GetByUser($id)
	{
		return Address::where('user_id', $id)->get();
	}  
}
