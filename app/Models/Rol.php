<?php

namespace App\Models;
use Auth;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Rol extends Model
{
    use HasFactory;

    protected $table = 'user_role';

    protected $fillable = [
        'role_id',
        'create',
        'edit',
        'update',
        'delete',
    ];

    protected $hidden = [
        'id',
        'role_name',
    ]; 

	public function Create($data)
	{
		return $this->insert($data);
	}

	public function GetData()
	{
		return Rol::get();
	}

    
	public function GetDataReg()
	{
		return Rol::where('show_reg_form', 'Yes')->get();
	}

    public function GetByRol($id)
    {
        return Rol::where('role_id', $id)->first();
    }

	public function UpdateLog($id, $data)
	{
		return $this->where('id',$id)->update($data);
	}
 
}
