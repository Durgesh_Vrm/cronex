<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable implements MustVerifyEmail
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'bio',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public function Create($data)
    {
        return User::insertGetId($data);
    } 

    public function UpdateData($id, $data)
    {
        return User::where('reg_number', $id)->update($data);
    } 


    public function GetById($id)
    {   
        return User::find($id);
    } 


    public function SearchByReg($id)
    {   
        return User::where('reg_number', $id)->first();
    } 


    public function SearchByName($name)
    {   
        return User::where('name', 'LIKE', '%'.$name.'%')->get();
    } 


    public function SearchByFName($fname)
    {   
        return User::where('father_name', 'LIKE', '%'.$fname.'%')->get();
    }


    public function SearchByFromDate($date)
    {   
        return User::where('created_at', 'LIKE', '%'.$date.'%')->get();
    }


    public function SearchByFromToDate($from, $to)
    {   
        return User::whereBetween('created_at', [$from, $to])->get();
    }


    public function SearchByNFName($name, $fname)
    {   
        return User::where('name', 'LIKE', '%'.$name.'%')->orwhere('father_name', 'LIKE', '%'.$fname.'%')->get();
    } 


    public function SearchByNameOrDate($name, $from, $to)
    {   
        return User::where('name', 'LIKE', '%'.$name.'%')->whereBetween('created_at', [$from, $to])->get();
    } 


    public function SearchByFNameOrDate($fname, $from, $to)
    {   
        return User::where('father_name', 'LIKE', '%'.$fname.'%')->whereBetween('created_at', [$from, $to])->get();
    } 


    public function SearchByFNNameOrDate($name, $fname, $from, $to)
    {   
        return User::where('name', 'LIKE', '%'.$name.'%')->orwhere('father_name', 'LIKE', '%'.$fname.'%')->whereBetween('created_at', [$from, $to])->get();
    } 


    public function GetUsers()
    {   
        return User::get();
    } 

    public function GetByEmail($id)
    {   
        return $this->where('email', $id)->select('id','email')->first();
    } 


    public function GetByLast()
    {   
        return User::OrderBy('id', 'DESC')->select('id')->first();
    }

}
