<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Qualification extends Model
{
    use HasFactory;

    protected $table = 'qualification';

    protected $fillable = [
        'qulification',
        'duration',
        'percentage',
        'pass_year',
        'desc',
        'collage',
        'status',
    ];

    protected $hidden = [
        'user_id',
    ];

    public function Create($data)
	{
		return $this->insert($data);
	}      

	public function GetByUser($id)
	{
		return Qualification::where('user_id', $id)->get();
	} 

}
