<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Experience extends Model
{
    use HasFactory;

    protected $table = 'experience';

    protected $fillable = [
        'date_of_employment',
        'company',
        'address',
        'role',
        'time',
        'job_note',
        'status',
    ];

    protected $hidden = [
        'user_id',
    ];

    public function Create($data)
	{
		return $this->insert($data);
	}      

	public function GetByUser($id)
	{
		return Experience::where('user_id', $id)->get();
	}      
}
