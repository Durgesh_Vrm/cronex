<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Countries extends Model
{
    use HasFactory;

    protected $table = 'countries';

    protected $fillable = [
        'id',
        'name',
        'iso3',
        'iso2',
        'phonecode',
        'capital',
        'currency',
        'native',
        'region',
        'subregion',
        'timezones',
        'longitude',
        'latitude',
        'image',
    ];

    protected $hidden = [
        
    ]; 

	public function Create($data)
	{
		return Countries::insert($data);
	}

	public function GetById($id)
	{
		return Countries::where('id', $id)->first();
	}	

	public function GetData()
	{
		return Countries::get();
	}  

    public function GetSelectData()
    {
        return Countries::select('id','iso2','image','name')->get();
    }	

	public function UpdateData($id, $data)
	{
		return Countries::where('id',$id)->update($data);
	}

}
