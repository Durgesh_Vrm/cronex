<?php

namespace App\Models;
use Auth;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StaticContent extends Model
{
    use HasFactory;

    protected $table = 'static_content';

    protected $fillable = [
        'sc_for',
        'sc_type',
        'sc_name',
        'sc_title',
        'sc_value',
        'sc_desc',
        'sc_status',
        'created_at',
        'updated_at',
    ];

    protected $hidden = [
        'id',
        'affiliate_id',
    ]; 

	public function Create($data)
	{
		return $this->insert($data);
	}

	public function GetData()
	{
		return StaticContent::where('sc_status', 'Active')->whereIn('sc_for', ['Both', 'Android'])->get();
	}

    public function GetId($id)
    {
        return StaticContent::where('id', $id)->where('sc_status', 'Active')->first();
    }

	public function UpdateLog($id, $data)
	{
		return $this->where('id',$id)->update($data);
	}
 
}
