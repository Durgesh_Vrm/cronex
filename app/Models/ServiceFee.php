<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ServiceFee extends Model
{
    use HasFactory;
    protected $table = 'service_fee';

    protected $fillable = [
        'name',
        'rs',
        'desc',
        'created_at',
        'updated_at',
        'status',
    ];

    protected $hidden = [
        'id',
    ];


    public function GetConfig()
	{
		return ServiceFee::get();
	} 

    public function Create($data)
    {
        return $this->insert($data);
    }
    public function UpdateData($id, $data)
    {
        return $this->where('id',$id)->update($data);
    }

    public function GetById($id)
    {
        return ServiceFee::where('id', $id)->first();
    } 
    
}
