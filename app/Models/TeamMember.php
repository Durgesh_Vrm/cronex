<?php

namespace App\Models;
use Auth;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TeamMember extends Model
{
    use HasFactory;

    protected $table = 'team_user';

    protected $fillable = [
        'role',
    ];

    protected $hidden = [
        'id',
        'user_id',
        'team_id',
    ]; 

	public function Create($data)
	{
		return $this->insert($data);
	}

	public function GetData()
	{
		return TeamMember::get();
	}

    public function GetByRol($id)
    {
        return TeamMember::where('role_id', $id)->first();
    }

	public function UpdateLog($id, $data)
	{
		return $this->where('id',$id)->update($data);
	}
 
}
