<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MessageTemplates extends Model
{
    use HasFactory;

    protected $table = 'message_templates';


    protected $fillable = [
        'subject', 
        'content', 
        'variable', 
        'tem_type', 
        'type', 
        'status ', 
    ];


    protected $hidden = [
        'id',
        'value',
    ];


   	protected $casts = [
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
    ];

    public function Create($data)
	{
		return $this->insert($data);
	}    

    public function Updates($id, $data)
    {
        return $this->where('id' ,$id)->update($data);
    } 
 
    public function GetAll()
    {   
        return $this->get();
    }   

    public function GetByid($id)
    {   
        return $this->where('id', $id)->first();
    }   

}
