<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
//use Illuminate\Foundation\Auth\Customer as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Customer extends Model
{
    use HasFactory;

    protected $table = 'customer';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'reg_number',
        'referral_code',
        'affiliate_id',
        'role',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public function Create($data)
    {
        return Customer::insertGetId($data);
    } 

    public function UpdateData($id, $data)
    {
        return Customer::where('reg_number', $id)->update($data);
    } 


    public function GetById($id)
    {   
        return Customer::find($id);
    } 

    public function GetByPhone($number)
    {   
        return Customer::where('phone_no', $number)->first();
    } 

    public function GetByReg($id)
    {   
        return Customer::where('reg_number', $id)->first();
    } 


    public function SearchByName($name)
    {   
        return Customer::where('name', 'LIKE', '%'.$name.'%')->get();
    } 


    public function SearchByFName($fname)
    {   
        return Customer::where('father_name', 'LIKE', '%'.$fname.'%')->get();
    }


    public function SearchByFromDate($date)
    {   
        return Customer::where('created_at', 'LIKE', '%'.$date.'%')->get();
    }


    public function SearchByFromToDate($from, $to)
    {   
        return Customer::whereBetween('created_at', [$from, $to])->get();
    }


    public function SearchByNFName($name, $fname)
    {   
        return Customer::where('name', 'LIKE', '%'.$name.'%')->orwhere('father_name', 'LIKE', '%'.$fname.'%')->get();
    } 


    public function SearchByNameOrDate($name, $from, $to)
    {   
        return Customer::where('name', 'LIKE', '%'.$name.'%')->whereBetween('created_at', [$from, $to])->get();
    } 


    public function SearchByFNameOrDate($fname, $from, $to)
    {   
        return Customer::where('father_name', 'LIKE', '%'.$fname.'%')->whereBetween('created_at', [$from, $to])->get();
    } 


    public function SearchByFNNameOrDate($name, $fname, $from, $to)
    {   
        return Customer::where('name', 'LIKE', '%'.$name.'%')->orwhere('father_name', 'LIKE', '%'.$fname.'%')->whereBetween('created_at', [$from, $to])->get();
    } 


    public function GetCustomers()
    {   
        return Customer::get();
    } 

    public function GetByEmail($id)
    {   
        return $this->where('email', $id)->select('id','email')->first();
    } 


    public function GetByLast()
    {   
        return Customer::OrderBy('id', 'DESC')->select('id')->first();
    }

}
