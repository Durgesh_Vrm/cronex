<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cities extends Model
{
    use HasFactory;

    protected $table = 'cities';

    protected $fillable = [
        'id',
        'name',
        'state_id',
        'state_code',
        'country_id',
        'country_code',
        'latitude',
        'longitude',
        'image',
    ];

    protected $hidden = [
      
    ]; 

	public function Create($data)
	{
		return Cities::insert($data);
	}

	public function GetById($id)
	{
		return Cities::where('id', $id)->first();
	}

    public function GetByState($id)
    {
        return Cities::where('state_id', $id)->get();
    }   

	public function GetData()
	{
		return Cities::get();
	}	

	public function GetSelectData()
    {
        return Cities::select('id','state_id','country_id','image','name')->get();
    }

	public function UpdateData($id, $data)
	{
		return Cities::where('id',$id)->update($data);
	}

}
