<?php

namespace App\Models;
use Auth;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Rout extends Model
{
    use HasFactory;

    protected $table = 'user_route';

    protected $fillable = [
        'route',
        'Administrator',
        'User',
        'Editor',
        'Member',
        'Employee',
        'Manager',
    ];

    protected $hidden = [
        'id',
    ]; 

	public function Create($data)
	{
		return $this->insert($data);
	}

	public function GetData()
	{
		return Rout::get();
	}

    public function GetByUrl($url)
    {
        return Rout::where('route', $url)->first();
    }

	public function UpdateLog($id, $data)
	{
		return $this->where('id',$id)->update($data);
	}
 
}
