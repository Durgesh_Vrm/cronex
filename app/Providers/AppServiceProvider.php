<?php

namespace App\Providers;
use Illuminate\Support\ServiceProvider;
use App\Models\Configuration;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $Configuration = new Configuration();
        foreach ($Configuration->GetConfig() as $key => $value){
        $data[$value->name] = $value->value; } 
        config($data);
    }
}
