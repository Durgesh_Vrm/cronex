<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use App\Models\Rol;
use App\Models\Rout;
use App\Models;
use Auth;
use DB;

class RoutePermisan
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $Rout = new Rout();
        $Rol = new Rol();
   
        if(Auth::user()->role === 'ADMIN469785')
        {
            $RouteUrl = request()->segment(count(request()->segments()));
            $rdata = $Rout->GetByUrl($RouteUrl);
            if($rdata)
            {
                if($rdata->Administrator === "Yes")
                {
                     return $next($request);
                }
                else
                {
                    return redirect('home')->with('error','Can`t Permission !');
                }
            }
            else
            {
                return redirect('home')->with('error','Can`t Permission !');
            }

        }        
        elseif(Auth::user()->role === 'USER58428D3')
        {
            $RouteUrl = request()->segment(count(request()->segments()));
            $rdata = $Rout->GetByUrl($RouteUrl);
            if($rdata)
            {
                if($rdata->User === "Yes")
                {
                     return $next($request);
                }
                else
                {
                    return redirect('home')->with('error','Can`t Permission !');
                }
            }
            else
            {
                return redirect('home')->with('error','Can`t Permission !');
            }

        }        
        elseif(Auth::user()->role === 'EDITOR32238')
        {
            $RouteUrl = request()->segment(count(request()->segments()));
            $rdata = $Rout->GetByUrl($RouteUrl);
            if($rdata)
            {
                if($rdata->Editor === "Yes")
                {
                     return $next($request);
                }
                else
                {
                    return redirect('home')->with('error','Can`t Permission !');
                }
            }
            else
            {
                return redirect('home')->with('error','Can`t Permission !');
            }

        }        
        elseif(Auth::user()->role === 'MEMBER55228')
        {
            $RouteUrl = request()->segment(count(request()->segments()));
            $rdata = $Rout->GetByUrl($RouteUrl);
            if($rdata)
            {
                if($rdata->Member === "Yes")
                {
                     return $next($request);
                }
                else
                {
                    return redirect('home')->with('error','Can`t Permission !');
                }
            }
            else
            {
                return redirect('home')->with('error','Can`t Permission !');
            }

        }        
        elseif(Auth::user()->role === 'EMPLOY15472')
        {
            $RouteUrl = request()->segment(count(request()->segments()));
            $rdata = $Rout->GetByUrl($RouteUrl);
            if($rdata)
            {
                if($rdata->Employee === "Yes")
                {
                     return $next($request);
                }
                else
                {
                    return redirect('home')->with('error','Can`t Permission !');
                }
            }
            else
            {
                return redirect('home')->with('error','Can`t Permission !');
            }

        }        
        elseif(Auth::user()->role === 'MANAGER0739')
        {
            $RouteUrl = request()->segment(count(request()->segments()));
            $rdata = $Rout->GetByUrl($RouteUrl);
            if($rdata)
            {
                if($rdata->Manager === "Yes")
                {
                     return $next($request);
                }
                else
                {
                    return redirect('home')->with('error','Can`t Permission !');
                }
            }
            else
            {
                return redirect('home')->with('error','Can`t Permission !');
            }

        }
        elseif (Auth::user()->role === 'SUPPERADMINC60C3421A4D4B2152B430') 
        {
            return $next($request);
        }
        else
        {
            return redirect('home')->with('error','Can`t Permission !');
        }
    }
}
