<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use App\Models\Rol;
use App\Models\Rout;
use App\Models;
use Auth;
use DB;

class UserPermisanDelete
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $Rout = new Rout();
        $Rol = new Rol();
   
        if(Auth::user()->role === 'ADMIN469785' || Auth::user()->role === 'USER58428D3' || Auth::user()->role === 'EDITOR32238' || Auth::user()->role === 'MEMBER55228' || Auth::user()->role === 'EMPLOY15472' || Auth::user()->role === 'MANAGER0739')
        {
            $rdata = $Rol->GetByRol(Auth::user()->role);
        
            if($rdata->delete == "Yes")
            {
                return $next($request);
            }
            else
            {
                return redirect('home')->with('error','Can`t Permission !');  
            }
 
        }
        elseif (Auth::user()->role === 'SUPPERADMINC60C3421A4D4B2152B430') 
        {
            return $next($request);
        }
        else
        {
            return redirect('home')->with('error','Can`t Permission !'); 
        }
    }
}
