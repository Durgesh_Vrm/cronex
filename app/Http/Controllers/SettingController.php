<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Configuration;
use App\Models\Content;
use App\Models\PackagingModel;
use App\Models\ServiceFee;
use App\Models\CenterPercentage;
use App\Models\Countries;
use App\Models\States;
use App\Models\Cities;
use App\Models\Rol;
use App\Models\User;
use App\Models\Rout;
use Carbon\Carbon;
use App\Models;
use DateTime;
use Image;
use Cache;
use Crypt;
use Auth;
use Hash;
use DB;
use File;
use Illuminate\Support\Facades\Http;


class SettingController extends Controller
{
	public function error_page()
	{
		Auth::logout();
    	Session()->flush();
		return view('errors');
	}




	public function web_configuration_settings_value()
	{
		$Configuration = new Configuration();
		$data['config'] = $Configuration->GetConfig();
		return view('Setting.config-setting',compact('data'));
	}





	public function web_configuration_settings_logo()
	{
		$Configuration = new Configuration();
		$data['config'] = $Configuration->GetConfig();
		return view('Setting.config-setting2',compact('data'));
	}






	public function web_configuration_settings_banner()
	{
		$Configuration = new Configuration();
		$data['config'] = $Configuration->GetConfig();
		return view('Setting.config-setting3',compact('data'));
	}




	public function web_configuration_settings_color()
	{
		$Configuration = new Configuration();
		$data['config'] = $Configuration->GetConfig();
		return view('Setting.config-setting4',compact('data'));
	}




	public function web_configuration_settings_loader()
	{
		$Configuration = new Configuration();
		$data['config'] = $Configuration->GetConfig();

		$files = File::allFiles(public_path().'/uplode/loder/'); 
		$loaders = [];
		foreach ($files as $value) {
			$loaders[] = $value->getFilename();
		}

		return view('Setting.config-setting5',compact('data','loaders'));
	}







	public function create_configuration_settings_value(request $request, Configuration $Configuration)
	{ 	
		$request->validate([
	        'type' => 'string|required|max:200',
	        'config_for' => 'string|required|max:200',
	        'config_variable' => '|string|required|min:5|max:200|alpha_dash|unique:configuration,name',
	        'config_value' => 'required|min:2|max:250',
	        'config_desc' => 'nullable|string|min:5|max:2000',
	      ]);

		$Configuration->Create([
			'for' => $request->Input('config_for'),
			'name' => $request->Input('config_variable'),
			'value' =>  $request->Input('config_value'),
			'desc' => $request->Input('config_desc'),
		]);

		return back();
	}

	public function create_configuration_settings_logo(request $request, Configuration $Configuration)
	{ 	
		$request->validate([
	        'type' => 'string|required|max:200',
	        'config_for' => 'string|required|max:200',
	        'config_variable' => '|string|required|min:5|max:200|alpha_dash|unique:configuration,name',
	        'config_logo' => 'required|image|mimes:jpeg,jpg,png|max:2048',
	        'config_desc' => 'nullable|string|min:5|max:2000',
	      ]);

		if($request->hasFile('config_logo'))
        {
            $image = $request->file('config_logo');
            $name = time().'.'.$image->getClientOriginalExtension();
            $thumbnailpath = public_path('/uplode/config/logo/');
           /* $img = Image::make($image->getRealPath())->resize(100, 100);
            $img->move($thumbnailpath.'\\'.$name);*/
            $request->config_logo->move($thumbnailpath,$image);  
        }		

		$Configuration->Create([
			'for' => $request->Input('config_for'),
			'name' => $request->Input('config_variable'),
			'value' => $name,
			'desc' => $request->Input('config_desc'),
		]);

		return back();
	}

	public function create_configuration_settings_banner(request $request, Configuration $Configuration)
	{ 	

		$request->validate([
	        'type' => 'string|required|max:200',
	        'config_for' => 'string|required|max:200',
	        'config_variable' => '|string|required|min:5|max:200|alpha_dash|unique:configuration,name',
	        'config_banner' => 'required|image|mimes:jpeg,jpg,png|max:10048',
	        'config_desc' => 'nullable|string|min:5|max:2000',
	      ]);

	
        if($request->hasFile('config_banner'))
        {
            $image = $request->file('config_banner');
            $name = 'BN'.time().'.'.$image->getClientOriginalExtension();
            $thumbnailpath = public_path('/uplode/config/banner/');
            $img = Image::make($image->getRealPath())->resize(1500, 800);
            $img->save($thumbnailpath.'\\'.$name); 
        }


		$Configuration->Create([
			'for' => $request->Input('config_for'),
			'name' => $request->Input('config_variable'),
			'value' => $name,
			'desc' => $request->Input('config_desc'),
		]);
	
		return back();
	}


	public function create_configuration_settings_color(request $request, Configuration $Configuration)
	{ 	

		$request->validate([
	        'type' => 'string|required|max:200',
	        'config_for' => 'string|required|max:200',
	        'config_variable' => '|string|required|min:5|max:200|alpha_dash|unique:configuration,name',
	        'config_color' => 'string|required|min:7|max:7',
	        'config_desc' => 'nullable|string|min:5|max:2000',
	      ]);

		$Configuration->Create([
			'for' => $request->Input('config_for'),
			'name' => $request->Input('config_variable'),
			'value' => $request->Input('config_color'),
			'desc' => $request->Input('config_desc'),
		]);
		
		return back();
	}


	public function edit_configuration_settings_value($id)
	{ 	
		$Configuration = new Configuration();
		$data['config'] = $Configuration->GetConfig();
		$data['SingleData'] = $Configuration->GetById(Crypt::decrypt($id));
		return view('Setting.edit-config-setting',compact('data'));
	}
	public function edit_configuration_settings_logo($id)
	{
		$Configuration = new Configuration();
		$data['config'] = $Configuration->GetConfig();
		$data['SingleData'] = $Configuration->GetById(Crypt::decrypt($id));
		return view('Setting.edit-config-setting2',compact('data'));
	}
	public function edit_configuration_settings_banner($id)
	{
		$Configuration = new Configuration();
		$data['config'] = $Configuration->GetConfig();
		$data['SingleData'] = $Configuration->GetById(Crypt::decrypt($id));
		return view('Setting.edit-config-setting3',compact('data'));
	}
	public function edit_configuration_settings_color($id)
	{
		$Configuration = new Configuration();
		$data['config'] = $Configuration->GetConfig();
		$data['SingleData'] = $Configuration->GetById(Crypt::decrypt($id));
		return view('Setting.edit-config-setting4',compact('data'));
	}
	public function edit_configuration_settings_loader($id)
	{
		$Configuration = new Configuration();
		$data['config'] = $Configuration->GetConfig();
		$data['SingleData'] = $Configuration->GetById(Crypt::decrypt($id));

		$files = File::allFiles(public_path().'/uplode/loder/'); 
		$loaders = [];
		foreach ($files as $value) {
			$loaders[] = $value->getFilename();
		}

		return view('Setting.edit-config-setting5',compact('data','loaders'));
	}



	public function update_configuration_settings_value(request $request, Configuration $Configuration)
	{ 
		$name = $request->Input('config_variable');

		$request->validate([
	        'type' => 'string|required|max:200',
	        'config_for' => 'string|required|max:200',
	        'config_variable' => 'sometimes|min:5|max:200|alpha_dash|unique:configuration,name,'. Crypt::decrypt($request->Input('config_id')),
	        'config_value' => 'required|min:2|max:250',
	        'config_desc' => 'nullable|string|min:5|max:2000',
	      ]);

		$Configuration->UpdateData(Crypt::decrypt($request->Input('config_id')), [
			'for' => $request->Input('config_for'),
			'name' => $request->Input('config_variable'),
			'value' =>  $request->Input('config_value'),
			'desc' => $request->Input('config_desc'),
		]);
		
		return back();
	}

	public function update_configuration_settings_logo(request $request, Configuration $Configuration)
	{ 	

		$request->validate([
	        'type' => 'string|required|max:200',
	        'config_for' => 'string|required|max:200',
	        'config_variable' => 'required|min:5|max:200|alpha_dash|unique:configuration,name,'.$request->Input('config_id'),
	        'config_value' => 'required|min:2|max:250',
	        'config_desc' => 'nullable|string|min:5|max:2000',
	      ]);

		if($request->hasFile('config_logo'))
        {
            $image = $request->file('config_logo');
            $name = time().'.'.$image->getClientOriginalExtension();
            $thumbnailpath = public_path('/uplode/config/logo/');
            /*$img = Image::make($image->getRealPath())->resize(100, 100);
            $img->save($thumbnailpath.'\\'.$name);*/
            $request->config_logo->move($thumbnailpath,$image); 
        }		

		$Configuration->UpdateData([
			'for' => $request->Input('config_for'),
			'name' => $request->Input('config_variable'),
			'value' => $name,
			'desc' => $request->Input('config_desc'),
		]);
		
		return back();
	}

	public function update_configuration_settings_banner(request $request, Configuration $Configuration)
	{ 	

		$request->validate([
	        'type' => 'string|required|max:200',
	        'config_for' => 'string|required|max:200',
	        'config_variable' => 'required|min:5|max:200|alpha_dash|unique:configuration,name,'.$request->Input('config_id'),
	        'config_value' => 'required|min:2|max:250',
	        'config_desc' => 'nullable|string|min:5|max:2000',
	      ]);

	
        if($request->hasFile('config_banner'))
        {
            $image = $request->file('config_banner');
            $name = 'BN'.time().'.'.$image->getClientOriginalExtension();
            $thumbnailpath = public_path('/uplode/config/banner/');
            $img = Image::make($image->getRealPath())->resize(1500, 800);
            $img->save($thumbnailpath.'\\'.$name); 
        }


		$Configuration->Create([
			'for' => $request->Input('config_for'),
			'name' => $request->Input('config_variable'),
			'value' => $name,
			'desc' => $request->Input('config_desc'),
		]);

		return back();
	}


	public function update_configuration_settings_color(request $request, Configuration $Configuration)
	{ 	

		$request->validate([
	        'type' => 'string|required|max:200',
	        'config_for' => 'string|required|max:200',
	        'config_variable' => 'required|min:5|max:200|alpha_dash|unique:configuration,name,'.$request->Input('config_id'),
	        'config_value' => 'required|min:2|max:250',
	        'config_desc' => 'nullable|string|min:5|max:2000',
	      ]);

		$Configuration->Create([
			'for' => $request->Input('config_for'),
			'name' => $request->Input('config_variable'),
			'value' => $request->Input('config_color'),
			'desc' => $request->Input('config_desc'),
		]);
		
		return back();
	}






	public function ActivateThemeMode()
	{	
		Cache::get('mode');
		if(Cache::get('mode') != 'DarkTheme'){
			Cache::put('mode', 'DarkTheme');
		}
		else{
			Cache::put('mode', 'LightTheme');
		}
		return back();
	}






	public function ChangeMenuStyle()
	{	
		Cache::get('menustyle');
		if(Cache::get('menustyle') != 'TopMenuStyle'){
			Cache::put('menustyle', 'TopMenuStyle');
		}
		else{
			Cache::put('menustyle', 'SideMenuStyle');
		}
		return back();
	}





	public function view_countries(Countries $Countries)
	{
		$data['all'] = $Countries->GetData();
		return view('location.view-countries',compact('data'));
	}	


	public function view_states(States $States)
	{
		$data['all'] = $States->GetData();
		return view('location.view-states',compact('data'));
	}

	public function view_cities(Cities $Cities)
	{
		$data['all'] = $Cities->GetData();
		return view('location.view-cities',compact('data'));
	}

	public function select_cities_name(request $request, Cities $Cities)
	{
		$city = $Cities->GetByState($request->state);
		$data = '';
		foreach ($city as $value) {

			$data .= '<option value="'.$value->id.'"'; 
					if(old('city') == $value->id)
					{ 
			$data .= 'selected'; 
					}
			$data .= '>'.$value->name.'</option>';
		}
			$data .= '<option value="other">Other</option>';
		return $data;
	}


	protected function user_permission(User $User, Rol $Rol)
	{
		$data['users'] = $User->GetUsers();
		$data['role'] = $Rol->GetData();
		//dd($data);
		return view('Users.role-user-info', compact('data'));
	}

	public function view_user_rout(Rout $Rout, Rol $Rol)
	{
		$data['all'] = $Rout->GetData();
		$data['role'] = $Rol->GetData();
		return view('rol.view-rout',compact('data'));
	}

	public function view_send_updates(Rout $Rout)
	{
		$data['all'] = $Rout->GetData();
		return view('Master.send-user-updates',compact('data'));
	}





	public function create_content()
	{
		return view('Master.Content.create-content');
	}



	public function view_content()
	{
		$data['data'] = DB::table('static_content')->get();
		
		return view('Master.Content.view-content',compact('data'));
	}



	public function edit_content($id)
	{
		$id = decrypt($id);
		

		$data = DB::table('static_content')->where('id',$id)->first();
		
		return view('Master.Content.edit-content',compact('data'));
	}




	public function add_content(request $request, Content $Content)
	{
		$request->validate([
	        'type' => 'required',
	        'for' => 'required',
	        'title' => 'string|required|min:3|max:200',
	        'name' => 'string|required|max:200|unique:static_content,sc_name',
	        'content' => 'required',
	      ]);


		$data = $Content->Create([
			'sc_for' => $request->Input('type'),
			'sc_name' => $request->Input('name'),
			'sc_title' => $request->Input('title'),
			'sc_value' => $request->Input('content'),
			'sc_desc' => $request->Input('description'),
			'sc_status' => 1,
			'affiliate_id' => Auth::user()->affiliate_id,
		]);
		
	  if($data){  
      return back()->with('success','Content Insert successfully');}
      else{
      return back()->with('warning','Can`t Insert');}  
	}




	public function update_content(request $request, Content $Content)
	{

		
		$request->validate([
	        'type' => 'required',
	        'title' => 'string|required|min:3|max:200',
	        'name' => 'string|required|max:200',
	        'content' => 'required',
	      ]);

		
		
			$data = $Content->UpdateData($request->id,[
			'sc_for' => $request->Input('for'),
			'sc_type' => $request->Input('type'),
			'sc_name' => $request->Input('name'),
			'sc_title' => $request->Input('title'),
			'sc_value' => $request->Input('content'),
			'sc_desc' => $request->Input('description')
			]);

	  if($data){
		  return redirect('view-content')->with('success','Content Update successfully');}  
    //   return back()->with('success','Content Update successfully');}
      else{
      return back()->with('warning','Can`t Insert');}  
	}




	public function deletecontent($id, Content $Content)
	{
		$id = decrypt($id);
		$respons = $Content->GetById($id);

		if($respons->sc_status == "Active")
		{
		
			$data = $Content->UpdateData($id,['sc_status' => 2]);

		}
		else
		{
			$data = $Content->UpdateData($id,['sc_status' => 1]);
		}


		if($data){
			  return back()->with('success','Content States Change');
		}else{
			return back()->with('warning','Can`t Insert');
		}
	}



	public function add_banner_logo()
	{
		return view('Master.BannerAndLogo.add-bannerandlogo');
	}



	public function insert_banner_logo(request $request)
	{
		$request->validate([
	        'for' => 'required',
	        'type' => 'required',
	        'title' => 'string|required|min:3|max:200',
	        'name' => 'string|required|max:200|unique:banner_and_logo,bl_name',
	        'content' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048',
	      ]); 

		  $cimg=$request->type.time().''.$request->file('content')->getClientOriginalName();


		if($request->type == 2)
		{	
			$image = $request->file('content');
			$thumbnailpath = public_path('/uplode/BannerAndLogo/');
            $img = Image::make($image->getRealPath())->resize(1920, 1080);
            $img->save($thumbnailpath.'\\'.$cimg);
		}
		elseif ($request->type == 3) 
		{
			$image = $request->file('content');
			$thumbnailpath = public_path('/uplode/BannerAndLogo/');
            $img = Image::make($image->getRealPath())->resize(555, 555);
            $img->save($thumbnailpath.'\\'.$cimg);
		}
		elseif ($request->type == 4) 
		{
			$image = $request->file('content');
			$thumbnailpath = public_path('/uplode/BannerAndLogo/');
            $img = Image::make($image->getRealPath())->resize(128, 128);
            $img->save($thumbnailpath.'\\'.$cimg);
		}
		elseif ($request->type == 5) 
		{
			$image = $request->file('content');
			$thumbnailpath = public_path('/uplode/BannerAndLogo/');
            $img = Image::make($image->getRealPath())->resize(126, 80);
            $img->save($thumbnailpath.'\\'.$cimg);
		}
		else
		{
			$request->content->move(public_path('uplode/BannerAndLogo/'),$cimg);
		}

			$bannerlogodata = array(
				'bl_for' => $request->for,
				'bl_type' => $request->type,
				'bl_name' => $request->name,
				'bl_title' => $request->title,
				'bl_value' => $cimg,
				'bl_desc' => $request->description,
				'bl_status' => 1,
				'affiliate_id' => Auth::user()->affiliate_id, 
			);

			// dd($bannerlogodata);

			$data = DB::table('banner_and_logo')->insert($bannerlogodata);
		

		  if($data)
		  {
	      return back()->with('success','Content Update successfully');
	  	  }
	      else
	      {
	      return back()->with('warning','Can`t Insert');
	  	  } 
	}


	

	public function update_banner_logo(request $request)
	{
		if($request->hasFile('content')){

			$request->validate([
				'for' => 'required',
				'type' => 'required',
				'title' => 'string|required|min:3|max:200',
				'name' => 'string|required|max:200',
				'content' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048',
			  ]);

		  	  $cimg=$request->type.time().''.$request->file('content')->getClientOriginalName();


			if($request->type == 2)
			{	
				$image = $request->file('content');
				$thumbnailpath = public_path('/uplode/BannerAndLogo/');
	            $img = Image::make($image->getRealPath())->resize(1920, 1080);
	            $img->save($thumbnailpath.'\\'.$cimg);
			}
			elseif ($request->type == 3) 
			{
				$image = $request->file('content');
				$thumbnailpath = public_path('/uplode/BannerAndLogo/');
	            $img = Image::make($image->getRealPath())->resize(555, 555);
	            $img->save($thumbnailpath.'\\'.$cimg);
			}
			elseif ($request->type == 4) 
			{
				$image = $request->file('content');
				$thumbnailpath = public_path('/uplode/BannerAndLogo/');
	            $img = Image::make($image->getRealPath())->resize(128, 128);
	            $img->save($thumbnailpath.'\\'.$cimg);
			}
			elseif ($request->type == 5) 
			{
				$image = $request->file('content');
				$thumbnailpath = public_path('/uplode/BannerAndLogo/');
	            $img = Image::make($image->getRealPath())->resize(126, 80);
	            $img->save($thumbnailpath.'\\'.$cimg);
			}
			else
			{
				$request->content->move(public_path('uplode/BannerAndLogo/'),$cimg);
			}

			$bannerlogodata = array(
				'bl_for' => $request->for,
				'bl_type' => $request->type,
				'bl_name' => $request->name,
				'bl_title' => $request->title,
				'bl_value' => $cimg,
				'bl_desc' => $request->description,
				'bl_status' => 1,
				'affiliate_id' => Auth::user()->affiliate_id, 
			);


		}else{


			$request->validate([
				'for' => 'required',
				'type' => 'required',
				'title' => 'string|required|min:3|max:200',
				'name' => 'string|required|max:200',
				'description' => 'sometimes|min:5|max:200',
			  ]);

			  $tt = DB::table('banner_and_logo')->where('id',$request->id)->first();

			  $bannerlogodata = array(
				'bl_for' => $request->for,
				'bl_type' => $request->type,
				'bl_name' => $request->name,
				'bl_title' => $request->title,
				'bl_value' => $tt->bl_value,
				'bl_desc' => $request->description,
				'bl_status' => 1,
				'affiliate_id' => Auth::user()->affiliate_id, 
			);

		}
		

			

			

			$data = DB::table('banner_and_logo')->where('id',$request->id)->update($bannerlogodata);
		

	  if($data){
		  return redirect('view-banner-logo')->with('success','Content Update successfully');}  
    //   return back()->with('success','Content Update successfully');}
      else{
      return back()->with('warning','Can`t Insert');} 
	}
	

	public function view_banner_logo()
	{
		$data['data'] = DB::table('banner_and_logo')->get();
		return view('Master.BannerAndLogo.view-bannerandlogo',compact('data'));
	}
	public function edit_banner_logo($id)
	{
		$id = decrypt($id);
		$data = DB::table('banner_and_logo')->where('id',$id)->first();
		return view('Master.BannerAndLogo.edit-bannerandlogo',compact('data'));
	}

	public function deletebannerlogo($id)
	{
		$id = decrypt($id);
		
		
		$data = DB::table('banner_and_logo')->where('id',$id)->delete();
		if($data){
			  return back()->with('success','Content Deleted successfully');
		}else{
			return back()->with('warning','Can`t Insert');
		}
	}


	public function add_packaging()
	{
		return view("Master.Packaging.add-packaging");
	}

	public function insert_package(Request $request, PackagingModel $package)
	{
		
		
		$request->validate([
	        'name' => 'required',
	        'height' => 'required',
	        'width' => 'required',
	        'img' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048',
	        'desc' => 'required',
	      ]);

		
		  $cimg= time().''.$request->file('img')->getClientOriginalName();

			$data = $package->Create([
			'name' => $request->Input('name'),
			'height' => $request->Input('height'),
			'width' => $request->Input('width'),
			'image' => $cimg,
			'desc' => $request->Input('desc'),
			]);

			$request->img->move(public_path('uplode/PackageImage/'),$cimg);

	  if($data){
		  return back()->with('success','Package Added successfully');}  
    //   return back()->with('success','Content Update successfully');}
      else{
      return back()->with('warning','Can`t Insert');}  
	}


	public function view_packaging()
	{
		$data['data'] = PackagingModel::get();
		return view("Master.Packaging.view-packaging",compact('data'));
	}

	public function delete_packaging($id, PackagingModel $package)
	{
		$id = decrypt($id);
		$respons = $package->GetById($id);

		if($respons->status == "Active")
		{
		
			$data = $package->UpdateData($id,['status' => 2]);

		}
		else
		{
			$data = $package->UpdateData($id,['status' => 1]);
		}


		if($data){
			  return back()->with('success','Package Status Change');
		}else{
			return back()->with('warning','Can`t Insert');
		}
	}

	public function edit_packaging($id,PackagingModel $package)
	{
		$id = decrypt($id);
		$respons = $package->GetById($id);

		return view('Master.Packaging.edit-packaging',compact('respons'));
	}

	public function update_packaging(Request $request, PackagingModel $package)
	{
		
		$request->validate([
	        'name' => 'required',
	        'height' => 'required',
	        'width' => 'required',
	        'desc' => 'required',
	      ]);

		  if($request->hasFile('img')){
			$cimg= time().''.$request->file('img')->getClientOriginalName();

			$data = $package->UpdateData($request->id,[
				'name' => $request->Input('name'),
				'height' => $request->Input('height'),
				'width' => $request->Input('width'),
				'image' => $cimg,
				'desc' => $request->Input('desc'),
				]);

				$request->img->move(public_path('uplode/PackageImage/'),$cimg);
				
		  }else{

			$respon = $package->GetById($request->id);

			$data = $package->UpdateData($request->id,[
				'name' => $request->Input('name'),
				'height' => $request->Input('height'),
				'width' => $request->Input('width'),
				'image' => $respon->image,
				'desc' => $request->Input('desc'),
				]);
		  }
		
		  

			
			

	  if($data){
		  return redirect('view-package')->with('success','Package Update successfully');}  
    //   return back()->with('success','Content Update successfully');}
      else{
      return back()->with('warning','Can`t Insert');}  
	}


	public function add_service_fee()
	{
		return view('Master.ServiceFee.add-service-fee');
	}

	public function insert_service_fee(Request $request,ServiceFee $servicefee)
	{
		$request->validate([
	        'name' => 'required',
	        'price' => 'required',
	        'desc' => 'required',
	      ]);

		  $data = $servicefee->Create([
			'name' => $request->Input('name'),
			'rs' => $request->Input('price'),
			'desc' => $request->Input('desc'),
			'status' => 'Active',
			]);

			if($data){
				return back()->with('success','Service Fee Added successfully');}  
		  //   return back()->with('success','Content Update successfully');}
			else{
			return back()->with('warning','Can`t Insert');}  
	}

	public function view_service_fee(ServiceFee $servicefee)
	{
		$data['data'] = ServiceFee::get();
		return view('Master.ServiceFee.view-service-fee',compact('data'));
	}

	
	public function delete_service_fee($id, ServiceFee $servicefee)
	{
		$id = decrypt($id);
		$respons = $servicefee->GetById($id);

		if($respons->status == "Active")
		{
		
			$data = $servicefee->UpdateData($id,['status' => 2]);

		}
		else
		{
			$data = $servicefee->UpdateData($id,['status' => 1]);
		}


		if($data){
			  return back()->with('success','Service Status Change');
		}else{
			return back()->with('warning','Can`t Insert');
		}
	}


	public function edit_service_fee($id, ServiceFee $servicefee)
	{
		$id = decrypt($id);
		$respons = $servicefee->GetById($id);

		return view('Master.ServiceFee.edit-service-fee',compact('respons'));
	}

	public function update_service_fee(Request $request,ServiceFee $servicefee)
	{
		$request->validate([
	        'name' => 'required',
	        'price' => 'required',
	        'desc' => 'required',
	      ]);

		  $data = $servicefee->UpdateData($request->id,[
			'name' => $request->Input('name'),
			'rs' => $request->Input('price'),
			'desc' => $request->Input('desc'),
			]);

			if($data){
				return redirect('view-service-fee')->with('success','Service Fee Update successfully');}  
		  //   return back()->with('success','Content Update successfully');}
			else{
			return back()->with('warning','Can`t Insert');}  
	}


	public function view_center_percentage(CenterPercentage $centerpercentage)
	{
		$data['data'] = CenterPercentage::get();
		return view('Master.CenterPercentage.view-center-percentage',compact('data'));
	}

	public function update_center_percentage(Request $request, CenterPercentage $centerpercentage)
	{
		$arrayName = array(
			'margin' => $request->marginvalue,
		);

		$centerpercentage->UpdateData($request->id,$arrayName);
		return back()->with('success','Percentage Update successfully');
	}

	
}
