<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Content;
use DB;

class FrontController extends Controller
{
    //	

    public function index(Content $Content)
	{
		$datas['hone'] = DB::select("SELECT `id`,`sc_for`,`sc_type`,`sc_name`,`sc_title`,`sc_value`,`sc_status`,`affiliate_id` FROM `static_content` WHERE id BETWEEN 1 AND 26 AND `sc_status` = 'Active' AND `sc_for` = 'Website' OR `sc_for` = 'Both'");

		$datas['banner'] = DB::select("SELECT `id`,`bl_for`,`bl_type`,`bl_name`,`bl_title`,`bl_value`,`bl_status`,`affiliate_id` FROM `banner_and_logo` WHERE  id BETWEEN 1 AND 26 AND `bl_status` = 'Active' AND `bl_for` = 'Website' OR `bl_for` = 'Both'");

		foreach($datas['hone'] as $key => $value)
		{
        $data[$value->sc_name.'_C'] = $value->sc_value; 
        $data[$value->sc_name.'_T'] = $value->sc_title; 
    	} 	

    	$data['Our Partners']=[];

    	foreach($datas['banner'] as $key => $value)
		{
        	if($value->bl_type == 'Our Partners')
        	{
				array_push($data['Our Partners'],$value->bl_value);
        	}
        	$data[$value->bl_type.'_'.$value->bl_name] = $value->bl_value; 
    	} 

		return view('front.index',compact('data'));
	}


    public function about(Content $Content)
	{
		$datas['hone'] = DB::select("SELECT `id`,`sc_for`,`sc_type`,`sc_name`,`sc_title`,`sc_value`,`sc_status`,`affiliate_id` FROM `static_content`  WHERE id BETWEEN 1 AND 26 AND `sc_status` = 'Active' AND `sc_for` = 'Website' OR `sc_for` = 'Both'");

		$datas['banner'] = DB::select("SELECT `id`,`bl_for`,`bl_type`,`bl_name`,`bl_title`,`bl_value`,`bl_status`,`affiliate_id` FROM `banner_and_logo` WHERE  id BETWEEN 1 AND 26 AND `bl_status` = 'Active' AND `bl_for` = 'Website' OR `bl_for` = 'Both'");

		foreach($datas['hone'] as $key => $value)
		{
        $data[$value->sc_name.'_C'] = $value->sc_value; 
        $data[$value->sc_name.'_T'] = $value->sc_title; 
    	} 

    	$data['Our Partners']=[];

    	foreach($datas['banner'] as $key => $value)
		{
        	if($value->bl_type == 'Our Partners')
        	{
				array_push($data['Our Partners'],$value->bl_value);
        	}
        	$data[$value->bl_type.'_'.$value->bl_name] = $value->bl_value; 
    	} 
		return view('front.about',compact('data'));
	}


    public function contact(Content $Content)
	{
		$data['hone'] = DB::select("SELECT `id`,`sc_for`,`sc_type`,`sc_name`,`sc_title`,`sc_value`,`sc_status`,`affiliate_id` FROM `static_content`  WHERE id BETWEEN 1 AND 26 AND `sc_status` = 'Active' AND `sc_for` = 'Website' OR `sc_for` = 'Both'");

		foreach($data['hone'] as $key => $value)
		{
        $data[$value->sc_name.'_C'] = $value->sc_value; 
        $data[$value->sc_name.'_T'] = $value->sc_title; 
    	} 

		return view('front.contact',compact('data'));
	}

    public function Service(Content $Content)
	{
		$datas['hone'] = DB::select("SELECT `id`,`sc_for`,`sc_type`,`sc_name`,`sc_title`,`sc_value`,`sc_status`,`affiliate_id` FROM `static_content`  WHERE id BETWEEN 1 AND 32 AND `sc_status` = 'Active' AND `sc_for` = 'Website' OR `sc_for` = 'Both'");

		$datas['banner'] = DB::select("SELECT `id`,`bl_for`,`bl_type`,`bl_name`,`bl_title`,`bl_value`,`bl_status`,`affiliate_id` FROM `banner_and_logo` WHERE  id BETWEEN 1 AND 26 AND `bl_status` = 'Active' AND `bl_for` = 'Website' OR `bl_for` = 'Both'");

		foreach($datas['hone'] as $key => $value)
		{
        $data[$value->sc_name.'_C'] = $value->sc_value; 
        $data[$value->sc_name.'_T'] = $value->sc_title; 
    	} 

    	$data['Our Partners']=[];

    	foreach($datas['banner'] as $key => $value)
		{
        	if($value->bl_type == 'Our Partners')
        	{
				array_push($data['Our Partners'],$value->bl_value);
        	}
        	$data[$value->bl_type.'_'.$value->bl_name] = $value->bl_value; 
    	}  

		return view('front.services',compact('data'));
	}

    public function private_policy(Content $Content)
	{
		$data['hone'] = DB::select("SELECT `id`,`sc_for`,`sc_type`,`sc_name`,`sc_title`,`sc_value`,`sc_status`,`affiliate_id` FROM `static_content`  WHERE `sc_type` = 'Our Policies' AND `sc_status` = 'Active' AND `sc_for` = 'Website' OR `sc_type` = 'Our Policies' AND `sc_status` = 'Active' AND `sc_for` = 'Both'");

		return view('front.private_policy',compact('data'));
	}

    public function tc(Content $Content)
	{
		$data['hone'] = DB::select("SELECT `id`,`sc_for`,`sc_type`,`sc_name`,`sc_title`,`sc_value`,`sc_status`,`affiliate_id` FROM `static_content`  WHERE `sc_type` = 'Our Policies' AND `sc_status` = 'Active' AND `sc_for` = 'Website' OR `sc_type` = 'Our Policies' AND `sc_status` = 'Active' AND `sc_for` = 'Both'");

		return view('front.tc',compact('data'));
	}


    public function faq(Content $Content)
	{
		$datas['hone'] = DB::select("SELECT `id`,`sc_for`,`sc_type`,`sc_name`,`sc_title`,`sc_value`,`sc_status`,`affiliate_id` FROM `static_content`  WHERE id BETWEEN 1 AND 26 AND `sc_status` = 'Active' AND `sc_for` = 'Website' OR `sc_for` = 'Both'");


    	$data['FAQ']=[];

    	foreach($datas['hone'] as $key => $value)
		{
        	if($value->sc_type == 'FAQ')
        	{	
        		$array = array('FAQ_T' => $value->sc_title,'FAQ_C' => $value->sc_value);
				array_push($data['FAQ'],$array);
        	}
        	$data[$value->sc_type.'_'.$value->sc_name] = $value->sc_value; 
    	} 

		return view('front.faq',compact('data'));
	}


}
