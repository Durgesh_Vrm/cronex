<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\CustomerVerification;
use App\Models\CustomerAddress;
use App\Models\StaticContent;
use App\Models\PackagingModel;
use App\Models\Customer;
use App\Models\Countries;
use App\Models\States;
use App\Models\Cities;
use App\Models\Rol;
use Validator;
use DB;
use Hash;
use App\Http\Requests\PostFormRequest;

class ApiController extends Controller
{
    public function static_content(StaticContent $StaticContent)
    {
    	$data = $StaticContent->GetData();
   		return response()->json($data);
    }

	
	public function static_content_id($id, StaticContent $StaticContent)
    {
    	if($id != '')
		{
    		$data = $StaticContent->GetId($id);
		}
		else
		{
    		$data = '';
		}
   		return response()->json($data);
    }



	public function state(States $States)
    {
    	$state = $States->GetData();
    	$data = ["status" => "200","state" => $state];
   		return response()->json($data);
    }

	
	public function state_id($id, States $States)
    {
    	if($id != '')
		{
    		$data = $States->GetById($id);
		}
		else
		{
    		$data = '';
		}
   		return response()->json($data);
    }
	

	public function city(Cities $Cities)
    {
    	$city = $Cities->GetData();
    	$data = ["status" => "200","city" => $city];
   		return response()->json($data);
    }

	
	public function city_id($id, Cities $Cities)
    {
    	if($id != '')
		{
    		$data = $Cities->GetById($id);
		}
		else
		{
    		$data = '';
		}
   		return response()->json($data);
    }
	
	public function countries(Countries $Countries)
    {
    	$country = $Countries->GetData();
    	$data = ["status" => "200","country" => $country];
   		return response()->json($data);
    }

	
	public function countries_id($id, Countries $Countries)
    {
    	if($id != '')
		{
    		$data = $Countries->GetById($id);
		}
		else
		{
    		$data = '';
		}
   		return response()->json($data);
    }


    public function user_reg_code()
    {
        $ccode = 'CONXU'.mt_rand(10000000,99999999);
        $data = ["status" => "200","regcode" => $ccode];
    	return response()->json($data);;
    }
	

    public function user_registration(request $request, Customer $Customer, CustomerVerification $CustomerVerification)
    {
    	$validation = [
    		'registration_number' => 'required|unique:customer,reg_number',
    		'referral_code' => 'nullable|exists:customer,reg_number',
    		'name' => 'required|regex:/^[\pL\s\-]+$/u|string|min:2|max:250',
    		'gender' => 'nullable|digits_between:1,3.',
    		'date_of_birth' => 'nullable|date_format:Y-m-d|before:10 years ago',
    		'phone_number' => 'required|numeric|digits:10|unique:customer,phone_no', //regex:/^[7896]\d{9}$/
    		'country_code' => 'required|numeric|max:9999|min:1',
			'device_information' => 'required',
    	];

    	$validator=Validator::make($request->all(),$validation);

    	if($validator->fails())
        {
            $data=$validator->messages();
        }
        else
        {
	        $response = $Customer->Create([
				'reg_number' => $request->Input('registration_number'),
				'name' => ucwords($request->Input('name')),
				'image' => 'user_image.png',
				'phone_no' => $request->Input('phone_number'),
				'role' => 'USER58428D3',
				'country_code' => $request->Input('country_code'),
			]);	

        	if($response)
        	{

				$CustomerVerification->Create([
					'user_id' => $response,
					'user_otp' => Hash::make('1234'),
					'device_info' => $request->Input('device_information'),
				]);	

        		$data = ['success' =>'User register successfully', 'nmae' => ucwords($request->Input('name')), 'register_number' => $request->Input('registration_number')];
        	}
        	else
        	{
        		$data = ['error' =>'User Not register'];
        	}
        }

   		return response()->json($data);
    }



    public function otp_verification(request $request, Customer $Customer, CustomerVerification $CustomerVerification)
    {
    	$validation = [
    		'otp' => 'required',
    		'phone_number' => 'required|numeric|digits:10|exists:customer,phone_no', //regex:/^[7896]\d{9}$/
    		'country_code' => 'required|numeric',
    	];

    	$validator=Validator::make($request->all(),$validation);

    	if($validator->fails())
        {
            $data=$validator->messages();
        }
        else
        {
	        $response = $Customer->GetByPhone($request->Input('phone_number'));	
			
        	if($response)
        	{
				$respo = $CustomerVerification->GetById($response->id);
				if($respo)  
				{
					if(Hash::check($request->Input('otp'), $respo->user_otp))
					{
						$data = $response;
						$Customer->UpdateData($response->reg_number,['country_code' => $request->Input('country_code'), 'password' => Hash::make($request->Input('otp'))]);
					}
					else
					{
						$data = ['error' =>'Enter Valid OTP'];
					}
				}
				else
				{
					$data = ['error' =>'User Not Found'];
				}
        	}
        	else
        	{
        		$data = ['error' =>'User Not Found'];
        	}
        }

   		return response()->json($data);
    }




	
    public function user_login(request $request, Customer $Customer, CustomerVerification $CustomerVerification)
    {
    	$validation = [
			'phone_number' => 'required|exists:customer,phone_no',
			'device_information' => 'required',
    	];

    	$validator=Validator::make($request->all(),$validation);

    	if($validator->fails())
        {
            $data=$validator->messages();
        }
        else
        {
	        $response = $Customer->GetByPhone($request->Input('phone_number'));	
        	if($response)
        	{
				$respo = $CustomerVerification->GetById($response->id);
				if($respo)  
				{
					$CustomerVerification->UpdateAt($response->id, [
						'user_otp' => Hash::make('1234'),
						'device_info' => $request->Input('device_information'),
					]);	
					$data = ['success' =>'OTP Send successfully', 'registration_number' => $request->Input('registration_number')];
				}
				else
				{
					$data = ['error' =>'User Not Found'];
				}
        	}
        	else
        	{
        		$data = ['error' =>'User Not Found'];
        	}
        }

   		return response()->json($data);
    }




	
    public function login_otp_verification(request $request, Customer $Customer, CustomerVerification $CustomerVerification)
    {
    	$validation = [
    		'otp' => 'required',
    		'registration_number' => 'required|exists:customer,reg_number',
    	];

    	$validator=Validator::make($request->all(),$validation);

    	if($validator->fails())
        {
            $data=$validator->messages();
        }
        else
        {
	        $response = $Customer->GetByReg($request->Input('registration_number'));
			
        	if($response)
        	{
				$respo = $CustomerVerification->GetById($response->id);
				if($respo)  
				{
					if(Hash::check($request->Input('otp'), $respo->user_otp))
					{
						$data = $response;
					}
					else
					{
						$data = ['error' =>'Enter Valid OTP'];
					}
				}
				else
				{
					$data = ['error' =>'User Not Found'];
				}
        	}
        	else
        	{
        		$data = ['error' =>'User Not Found'];
        	}
        }

   		return response()->json($data);
    }




	
    public function add_address(request $request, Customer $Customer, CustomerAddress $CustomerAddress)
    {
    	$validation = [
    		'registration_number' => 'required|exists:customer,reg_number',
    		'phone_number' => 'required|numeric|digits:10', //regex:/^[7896]\d{9}$/
    		'type' => 'required',
    		'address' => 'required',
    		'street' => 'required',
			'city' => 'required',
			'state' => 'required',
			'country' => 'required',
			'zip' => 'required',
    	];

    	$validator=Validator::make($request->all(),$validation);

    	if($validator->fails())
        {
            $data=$validator->messages();
        }
        else
        {

			$response = $Customer->GetByReg($request->Input('registration_number'));
			
        	if($response)
        	{
				$data = $CustomerAddress->Create([
					'user_id' => $response->id,
					'phone_number' => $request->Input('phone_number'),
					'type' => $request->Input('type'),
					'address' => ucwords($request->Input('address')),
					'street' => ucwords($request->Input('street')),
					'city' => $request->Input('city'),
					'state' => $request->Input('state'),
					'country' => $request->Input('country'),
					'zip' => $request->Input('zip'),
					'longitude' => $request->Input('longitude'),
					'latitude' => $request->Input('latitude'),
				]);	
	
				if($data)
				{
					$data = ['success' =>'Add Address successfully'];
				}
				else
				{
					$data = ['error' =>'Address Not Add'];
				}
        	}
        	else
        	{
        		$data = ['error' =>'User Not Found'];
        	}
        }

   		return response()->json($data);
    }




	
    public function get_address(request $request, Customer $Customer, CustomerAddress $CustomerAddress)
    {
    	$validation = [
    		'registration_number' => 'required|exists:customer,reg_number',
    	];

    	$validator=Validator::make($request->all(),$validation);

    	if($validator->fails())
        {
            $data=$validator->messages();
        }
        else
        {
			$response = $Customer->GetByReg($request->Input('registration_number'));

        	if($response)
        	{
				$data = $CustomerAddress->GetByUId($response->id);	
        	}
        	else
        	{
        		$data = ['error' =>'User Not Found'];
        	}
        }

        $arr = ["status" => "200","addresslist" => $data];
   		return response()->json($arr);
    }




	public function get_address_id(request $request, Customer $Customer, CustomerAddress $CustomerAddress)
    {
    	$validation = [
    	    'registration_number' => 'required|exists:customer,reg_number',
			'address_id' => 'required',
    	];

    	$validator=Validator::make($request->all(),$validation);

    	if($validator->fails())
        {
            $data=$validator->messages();
        }
        else
        {
			$response = $Customer->GetByReg($request->Input('registration_number'));

        	if($response)
        	{
				$data = $CustomerAddress->GetById($request->Input('address_id'), $response->id);	
        	}
        	else
        	{
        		$data = ['error' =>'User Not Found'];
        	}
        }

   		return response()->json($data);
    }




	
	
    public function update_address(request $request, Customer $Customer, CustomerAddress $CustomerAddress)
    {
    	$validation = [
    		'registration_number' => 'required|exists:customer,reg_number',
    		'phone_number' => 'required|numeric|digits:10', //regex:/^[7896]\d{9}$/
    		'type' => 'required',
    		'address' => 'required',
    		'street' => 'required',
			'city' => 'required',
			'state' => 'required',
			'country' => 'required',
			'zip' => 'required',
			'address_id' => 'required',
    	];

    	$validator=Validator::make($request->all(),$validation);

    	if($validator->fails())
        {
            $data=$validator->messages();
        }
        else
        {

			$response = $Customer->GetByReg($request->Input('registration_number'));
			
        	if($response)
        	{
				$data = $CustomerAddress->UpdateAt($request->Input('address_id'), $response->id, [
					'phone_number' => $request->Input('phone_number'),
					'type' => $request->Input('type'),
					'address' => ucwords($request->Input('address')),
					'street' => ucwords($request->Input('street')),
					'city' => $request->Input('city'),
					'state' => $request->Input('state'),
					'country' => $request->Input('country'),
					'zip' => $request->Input('zip'),
					'longitude' => $request->Input('longitude'),
					'latitude' => $request->Input('latitude'),
				]);	
	
				if($data)
				{
					$data = ['success' =>'Update Address successfully'];
				}
				else
				{
					$data = ['error' =>'Address Not Update'];
				}
        	}
        	else
        	{
        		$data = ['error' =>'User Not Found'];
        	}
        }

   		return response()->json($data);
    }


	
		
    public function delete_address(request $request, Customer $Customer, CustomerAddress $CustomerAddress)
    {
    	$validation = [
    		'registration_number' => 'required|exists:customer,reg_number',
			'address_id' => 'required',
    	];

    	$validator=Validator::make($request->all(),$validation);

    	if($validator->fails())
        {
            $data=$validator->messages();
        }
        else
        {
			$response = $Customer->GetByReg($request->Input('registration_number'));
			
        	if($response)
        	{
				$data = $CustomerAddress->deleteAdd($request->Input('address_id'), $response->id);	
	
				if($data)
				{
					$data = ['success' =>'Delete Address successfully'];
				}
				else
				{
					$data = ['error' =>'Address Not Delete'];
				}
        	}
        	else
        	{
        		$data = ['error' =>'User Not Found'];
        	}
        }

   		return response()->json($data);
    }

	
	
    public function add_package(request $request, Customer $Customer, PackagingModel $PackagingModel)
    {
    	$validation = [
    		'registration_number' => 'required|exists:customer,reg_number',
    		'name' => 'required',
    		'width' => 'required',
    		'height' => 'required',
// 			'length' => 'required',
    	];

    	$validator=Validator::make($request->all(),$validation);

    	if($validator->fails())
        {
            $data=$validator->messages();
        }
        else
        {
			$response = $Customer->GetByReg($request->Input('registration_number'));
			
        	if($response)
        	{
				$data = $PackagingModel->Create([
					'user_id' => $response->id,
					'name' => $request->Input('name'),
					'width' => $request->Input('width'),
					'height' => $request->Input('height'),
				// 	'length' => $request->Input('length'),
					'desc' => $request->Input('desc'),
				]);	
	
				if($data)
				{
					$data = ['success' =>'Add Package successfully'];
				}
				else
				{
					$data = ['error' =>'Package Not Add'];
				}
        	}
        	else
        	{
        		$data = ['error' =>'User Not Found'];
        	}
        }

   		return response()->json($data);
    }



	public function get_package(request $request, Customer $Customer, PackagingModel $PackagingModel)
    {
    	$validation = [
    		'registration_number' => 'required|exists:customer,reg_number',
    	];

    	$validator=Validator::make($request->all(),$validation);

    	if($validator->fails())
        {
            $data=$validator->messages();
        }
        else
        {
			$response = $Customer->GetByReg($request->Input('registration_number'));
			
        	if($response)
        	{
				$pkg = $PackagingModel->GetPack($response->id);
				$data = ['success' =>'200','package' =>$pkg];
        	}
        	else
        	{
        		$data = ['error' =>'User Not Found'];
        	}
        }

   		return response()->json($data);
    }



	public function get_package_id(request $request, Customer $Customer, PackagingModel $PackagingModel)
    {
    	$validation = [
    		'registration_number' => 'required|exists:customer,reg_number',
    		'package_id' => 'required',
    	];

    	$validator=Validator::make($request->all(),$validation);

    	if($validator->fails())
        {
            $data=$validator->messages();
        }
        else
        {
			$response = $Customer->GetByReg($request->Input('registration_number'));
			
        	if($response)
        	{
				$data = $PackagingModel->GetById($request->Input('package_id'));	
        	}
        	else
        	{
        		$data = ['error' =>'User Not Found'];
        	}
        }

   		return response()->json($data);
    }



    public function update_package(request $request, Customer $Customer, PackagingModel $PackagingModel)
    {
    	$validation = [
    		'registration_number' => 'required|exists:customer,reg_number',
    		'name' => 'required',
    		'width' => 'required',
    		'height' => 'required',
			'length' => 'required',
			'package_id' => 'required',
    	];

    	$validator=Validator::make($request->all(),$validation);

    	if($validator->fails())
        {
            $data=$validator->messages();
        }
        else
        {
			$response = $Customer->GetByReg($request->Input('registration_number'));
			
        	if($response)
        	{
				$data = $PackagingModel->UpdateData($request->Input('package_id'), [
					'name' => $request->Input('name'),
					'width' => $request->Input('width'),
					'height' => $request->Input('height'),
					'length' => $request->Input('length'),
					'desc' => $request->Input('desc'),
				]);	
	
				if($data)
				{
					$data = ['success' =>'Update Package successfully'];
				}
				else
				{
					$data = ['error' =>'Package Not Update'];
				}
        	}
        	else
        	{
        		$data = ['error' =>'User Not Found'];
        	}
        }

   		return response()->json($data);
    }




	
    public function delete_package(request $request, Customer $Customer, PackagingModel $PackagingModel)
    {
    	$validation = [
    		'registration_number' => 'required|exists:customer,reg_number',
			'package_id' => 'required',
    	];

    	$validator=Validator::make($request->all(),$validation);

    	if($validator->fails())
        {
            $data=$validator->messages();
        }
        else
        {
			$response = $Customer->GetByReg($request->Input('registration_number'));
			
        	if($response)
        	{
				// $data = $PackagingModel->deletePack($request->Input('package_id'));	
				$data = DB::table('packaging_modal')->where('id',$request->package_id)->delete();	
	
				if($data)
				{
					$data = ['success' =>'Delete Package successfully'];
				}
				else
				{
					$data = ['error' =>'Package Not Delete'];
				}
        	}
        	else
        	{
        		$data = ['error' =>'User Not Found'];
        	}
        }
   		return response()->json($data);
    }


    public function get_service_fee(){
        $serv = DB::table('service_fee')->get();
        $data = ['service' =>$serv];
        return response()->json($data);
    }

	

	public function order_booking(request $request, Customer $Customer, PackagingModel $PackagingModel)
    {
		$validation = [
    		'registration_number' => 'required|exists:customer,reg_number',
    		'packag_info' => 'required',
    		'longitude' => 'required',
    		'latitude' => 'required',
			'address_pickup' => 'required',
			'address_drop' => 'required',
			'status' => 'required',
			'amount' => 'required',
			'service_day' => 'required',
			'service_day_info' => 'required',
			'hub_info' => 'required',
			'hub_id' => 'required',
    	];

    	$validator=Validator::make($request->all(),$validation);

    	if($validator->fails())
        {
            $data=$validator->messages();
        }
        else
        {
			$response = $Customer->GetByReg($request->Input('registration_number'));
			
        	if($response)
        	{	
				$orderid = 'CONX'.date("Ymd-His-u");
				$data = DB::table('service_booking')->insert([
					'user_id' => $response->id,
					'trans_id' => $orderid,
					'packag_info' => $request->Input('packag_info'),
					'longitude' => $request->Input('longitude'),
					'latitude' => $request->Input('latitude'),
					'address_pickup' => $request->Input('address_pickup'),
					'address_drop' => $request->Input('address_drop'),
					'status' => $request->Input('status'),
					'amount' => $request->Input('amount'),
					'service_day' => $request->Input('service_day'),
					'service_day_info' => $request->Input('service_day_info'),
					'barcode' => '123',
					'hub_info' => $request->Input('hub_info'),
					'otp' => Hash::make('1234'),
					'hub_id' => $request->Input('hub_id'),
				]);	

				if($data)
				{
					$data = ['success' =>'Order successfully', 'booking_id' => $orderid ];
				}
				else
				{
					$data = ['error' =>'Order Not successfully'];
				}
        	}
        	else
        	{
        		$data = ['error' =>'User Not Found'];
        	}
        }
   		return response()->json($data);
    }






		

	public function get_order_booking(request $request, Customer $Customer, PackagingModel $PackagingModel)
    {
		$validation = [
    		'registration_number' => 'required|exists:customer,reg_number',
    		'status' => 'required',
    	];

    	$validator=Validator::make($request->all(),$validation);

    	if($validator->fails())
        {
            $data=$validator->messages();
        }
        else
        {
			$response = $Customer->GetByReg($request->Input('registration_number'));
			
        	if($response)
        	{	
				$status = strtolower($request->Input('status'));

				if($status == 'pending' && $request->Input('id') == '')
				{
					$booking = DB::table('service_booking')->where('driver_assing_date', null)->where('user_id', $response->id)->get();
				}
				elseif($status == 'success' && $request->Input('id') == '')
				{
					$booking = DB::table('service_booking')->where('delivered','!=' , null)->where('user_id', $response->id)->get();
				}
				elseif($status == 'under process'  && $request->Input('id') == '')
				{
					$booking = DB::table('service_booking')->where('driver_assing_date', '!=', null)->where('user_id', $response->id)->get();
				}
				else
				{
					$booking = DB::table('service_booking')->where('user_id', $response->id)->get();
				}	

				if(count($booking) > 0)
				{
					$data = $booking;
				}
				else
				{
					$data = ['error' =>'Order Not Found'];
				}
        	}
        	else
        	{
        		$data = ['error' =>'User Not Found'];
        	}
        }
   		return response()->json($data);
    }




	
		

	public function getid_order_booking(request $request, Customer $Customer, PackagingModel $PackagingModel)
    {
		$validation = [
    		'registration_number' => 'required|exists:customer,reg_number',
    		'booking_id' => 'required',
    	];

    	$validator=Validator::make($request->all(),$validation);

    	if($validator->fails())
        {
            $data=$validator->messages();
        }
        else
        {
			$response = $Customer->GetByReg($request->Input('registration_number'));
			
        	if($response)
        	{	
				$booking = DB::table('service_booking')->where('id', $request->booking_id)->where('user_id', $response->id)->first();

				if($booking)
				{
					$data = $booking;
				}
				else
				{
					$data = ['error' =>'Order Not Found'];
				}
        	}
        	else
        	{
        		$data = ['error' =>'User Not Found'];
        	}
        }
   		return response()->json($data);
    }




	
	
    public function get_near_hub(request $request, Customer $Customer, PackagingModel $PackagingModel)
    {
    	$validation = [
    		'registration_number' => 'required|exists:customer,reg_number',
			'city' => 'required',
    	];

    	$validator=Validator::make($request->all(),$validation);

    	if($validator->fails())
        {
            $data=$validator->messages();
        }
        else
        {
			$response = $Customer->GetByReg($request->Input('registration_number'));
			
        	if($response)
        	{	
				$cityid = DB::table('cities')->where('name',$request->city)->first();
				if($cityid)
				{	
					$vaender = DB::table('users')->leftJoin('address', 'users.id', '=', 'address.user_id')->leftJoin('cities', 'address.city', '=', 'cities.id')->select('users.name','users.reg_number','users.email','users.phone_no','users.other_phone_no','users.affiliate_id','users.referral_code','address.*','cities.name as city','address.city as cityId')->where('address.city', $cityid->id)->get();
					if(count($vaender) > 0)
					{
						$data = $vaender;
					}
					else
					{
						$data = ['error' =>'Hub Not Found'];
					}
				}
				else
				{
					$data = ['error' =>'City Not Found'];
				}	
        	}
        	else
        	{
        		$data = ['error' =>'User Not Found'];
        	}
        }
   		return response()->json($data);
    }




}
