<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\MessageTemplates;
use App\Models\InboxEmails;
use DB;

class EmailController extends Controller
{
    public function view_email_compose(MessageTemplates $MessageTemplates,InboxEmails $InboxEmails)
    {
    	$data['all'] = $MessageTemplates->GetAll();
        $data['count'] = count($InboxEmails->GetAll());
    	return view('Email.email-compose-page',compact('data'));
    }



    public function view_email_inbox(MessageTemplates $MessageTemplates,InboxEmails $InboxEmails)
    {
        $data['all'] = $MessageTemplates->GetAll();
        $data['emails'] = DB::table('email_inbox')->orderby('id','desc')->simplePaginate(10);
        $data['count'] = count($InboxEmails->GetAll());
        return view('Email.email-inbox',compact('data'));
    }




    public function view_email_send(MessageTemplates $MessageTemplates,InboxEmails $InboxEmails)
    {
            $data['all'] = $MessageTemplates->GetAll();
            $data['count'] = count($InboxEmails->GetAll());
        return view('Email.email-send',compact('data'));
    }



    public function view_email_read($id ,MessageTemplates $MessageTemplates,InboxEmails $InboxEmails)
    {
            $data['all'] = $MessageTemplates->GetAll();
            $data['count'] = count($InboxEmails->GetAll());
        return view('Email.email-read',compact('data'));
    }   



    public function get_email_templ(request $request ,MessageTemplates $MessageTemplates)
    {
            $data = $MessageTemplates->GetByid($request->id);
            if($data)
            {
                return $data;
            }
            else
            {
                return 'error';
            }
            
        return view('Email.email-read',compact('data'));
    }


    public function get_to_emails(request $request)
    {
           $email = DB::table('users')->leftJoin('user_role', 'users.role', '=', 'user_role.role_id')
                ->where('users.name', 'LIKE', '%'.$request->emails.'%')
                ->orwhere('users.email', 'LIKE', '%'.$request->emails.'%')
                ->orwhere('user_role.role_name', 'LIKE', '%'.$request->emails.'%')->select('users.email','users.name')->get();
            if(count($email) > 0)
            {
                return $email;
            }
            else
            {
                return 'error';
            }
    }


    public function get_inbox_email(InboxEmails $InboxEmails)
    {
        //dd(set_time_limit(500));
        $hostname = '{imap.gmail.com:993/imap/ssl}INBOX';
        $username = 'durgeshvrm010@gmail.com';
        $password = 'Durgesh@verma@9918829937';
        $inbox = imap_open($hostname,$username,$password);
        $emails = imap_search($inbox,'UNSEEN');
        $id = 1;
        foreach($emails as $e)
        {
            $overview = imap_fetch_overview($inbox,$e,0);
            $message = imap_fetchbody($inbox,$e,2);
            // the body of the message is in $message
            $details = $overview[0];

        if(DB::table('email_inbox')->where('uid', $details->uid)->where('msgno', $details->msgno)->first()) 
        {
        }
        else
        {   
            $data= ['from' =>  $details->from,'subject' =>  $details->subject,'message' => $message,'uid' =>  $details->uid,'msgno' =>  $details->msgno,'date' =>  $details->date]; 
            $InboxEmails->Create($data);
        }
            // you can do a var_dump($details) to see which parts you need
            //then do whatever to insert them into your DB
        }

        return redirect('mail-inbox');
    }



}

