<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Models\UserVerification;
use App\Models\Qualification;
use App\Models\Experience;
use App\Models\Profile;
use App\Models\Address;
use App\Models\User;
use App\Models\log;
use App\Models\Rol;
use App\Models\Kyc;
use Carbon\Carbon;
use App\Models;
use DateTime;
use Image;
use Auth;
use DB;
class ProfileController extends Controller
{
    public $timestamps = false;
    
    public function user_log_time(Log $Log)
    {	  
      $Log->UpdateLog($Log->Last()->id, ['last_activity' => Carbon::now()->toDateTimeString()]);
    }

    public function UserProfile($name='')
    {	
      $Rol = new Rol();
      $Kyc = new Kyc();
    	$User = new User();
      $Address = new Address();
      $Experience = new Experience();
      $Qualification = new Qualification();
      $UserVerification = new UserVerification();

      $data['Rol'] = $Rol->GetByRol(Auth::user()->role);

        if($data['Rol'] != null)
        {
          $data['Rol'] = $data['Rol']->role_name;
        }
        else
        {
          $data['Rol'] = 'User';
        }

   
      $data['kyc'] = $Kyc->GetByUser(Auth::user()->id);
      $data['address'] = $Address->GetByUser(Auth::user()->id);
      $data['experience'] = $Experience->GetByUser(Auth::user()->id);
      $data['qualification'] = $Qualification->GetByUser(Auth::user()->id);
      $data['TSV'] = $UserVerification->GetById(Auth::user()->email);
      $data['TSV'] = $data['TSV']->opt_active;

    	if(Auth::user())
    	return view('Users.profile-page',compact('data','name'));
    	else	
	    return abort('404');
    }


    public function user_profile_uplode(request $request, Profile $Profile)
    {
      if(Auth::user()->id){
        if($request->hasFile('image_logo')){
        $image = $request->file('profile-photo');
        $imagename = time().'.'.$image->getClientOriginalExtension();
        $path = public_path('/uplode/users/');
        $img = Image::make($image->getRealPath())->resize(300, 300);
        $img->save($path.'\\'.$imagename);
        }
        else{ 
            $imagename = Auth::user()->image;
        }
        $data = $Profile->UpdatePro(Auth::user()->id, ['USR01_image' => $imagename]);

      if($data){  
      return back()->with('success','Update successfully');}
      else{
      return back()->with('warning','Can`t update');}  
          }
      else{ return back()->with('warning','Can`t update'); }        
    }


    public function update_profile(request $request, User $User)
    { 
      if(Auth::user()->id){
        
      $request->validate([
        'name' => 'required|regex:/^[\pL\s\-]+$/u|string|min:2|max:250',
        'father_name' => 'nullable|regex:/^[\pL\s\-]+$/u|string|min:3|max:250',
        'mother_name' => 'nullable|regex:/^[\pL\s\-]+$/u|string|min:3|max:250',
        'phone_number' => 'required|numeric|digits:10|regex:/^[7896]\d{9}$/|unique:users,phone_no,'.Auth::user()->id,
        'birthdate' => 'required|date_format:Y-m-d|before:10 years ago',
      ]);


      $data = $User->UpdateData(Auth::user()->reg_number, [
        'name' => $request->Input('name'),
        'gander' => $request->Input('gender'),
        'dob' => $request->Input('birthdate'),
        'father_name' => $request->Input('father_name'),
        'mother_name' => $request->Input('mother_name'),
        'phone_no' => $request->Input('phone_number'),
        'other_phone_no' => $request->Input('other_phone_number'),
        'bio' => $request->Input('bio'),
        'facebook' => $request->Input('facebook'),
        'twitter' => $request->Input('twitter'),
        'instagram' => $request->Input('instagram'),
        'linkedin' => $request->Input('linkedin')
        ]);

      if($data){  
      return back()->with('success','Update successfully');}
      else{
      return back()->with('warning','Can`t update');}  
          }
      else{ return back()->with('warning','Can`t update'); }

    }

 


  public function change_type_tsvs(request $request, UserVerification $UserVerification)
  {
    if($request->id == "true")
    {
      $UserVerification->UpdateAt(Auth::user()->email, [ 'opt_active' => 1 ]);
    }
    else
    {
      $UserVerification->UpdateAt(Auth::user()->email, [ 'opt_active' => 2 ]);
    }
  }




  public function change_show_profile(request $request, User $User)
  {
    if($request->id == "true")
    {
      $User->UpdateData(Auth::user()->reg_number, [ 'show_profile_everyone' => 1 ]);
    }
    else
    {
      $User->UpdateData(Auth::user()->reg_number, [ 'show_profile_everyone' => 2 ]);
    }
  }




  public function Change_Activities_Log(request $request, User $User)
  {
    if($request->id == "true")
    {
      $User->UpdateData(Auth::user()->reg_number, [ 'activities_recorder' => 1 ]);
    }
    else
    {
      $User->UpdateData(Auth::user()->reg_number, [ 'activities_recorder' => 2 ]);
    }
  }




  public function change_notification_on(request $request, User $User)
  {
    if($request->id == "true")
    {
      $User->UpdateData(Auth::user()->reg_number, [ 'notification' => 1 ]);
    }
    else
    {
      $User->UpdateData(Auth::user()->reg_number, [ 'notification' => 2 ]);
    }
  }
















}
