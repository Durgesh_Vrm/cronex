<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Models\UserVerification;
use App\Models\Qualification;
use App\Models\Experience;
use App\Models\Wallet;
use App\Models\Address;
use App\Models\Countries;
use App\Models\States;
use App\Models\Cities;
use App\Models\User;
use App\Models\Team;
use App\Models\Rout;
use App\Models\TeamMember;
use App\Models\log;
use App\Models\Rol;
use App\Models\Kyc;
use Carbon\Carbon;
use App\Models;
use DateTime;
use Crypt;
use Image;
use Auth;
use DB;
use Hash;

class UserController extends Controller
{

	protected function get_captcha_code(request $request)
	{
		if (request()->getMethod() == 'POST') 
		{ 
        	$form =  captcha_img() . ' &ensp; <img onclick="captcha_code()" src="'.url('assets/img/relod_icon.png').'" width="35">
        			<div class="form-group pt-2">
                    <label>Captcha Code <code>*</code></label>
                    <input style="width: 150px;" type="text" name="captcha" id="captcha" class="form-control form-control-sm">
                    </div>';
        	return $form;
        }
        else
        {
        	return '';
        }
	}
	

    protected function user_registration(User $User, Countries $Countries, States $States, Cities $Cities, Rol $Rol)
    {	
    	$data['countries'] = $Countries->GetSelectData();
     	$data['states'] = $States->GetSelectData();
     	$data['cities'] = $Cities->GetSelectData();
     	$data['all'] = $Rol->GetDataReg();
     	$fno = $User->GetByLast();
    	$data['FNUMBER'] = ++$fno->id;
    	$data['RNUMBER'] = 'CONXV'.mt_rand(10000000,99999999);
    	session()->put('RNUMBER', $data['RNUMBER']);
    	return view('Users.user-registration',compact('data'));
    }


    protected function registration(request $request, User $User, UserVerification $UserVerification, Kyc $Kyc, Address $Address, Wallet $Wallet)
    {
    	$rules = ['captcha' => 'required|captcha'];
        $validator = validator()->make(request()->all(), $rules);

            if ($validator->fails()){
                session()->flash('warning','Wrong captcha code.');
            }


    	$request->validate([
    		'registration_number' => 'required|unique:users,reg_number',
    		'referral_code' => 'nullable|exists:users,reg_number',
    		'name' => 'required|regex:/^[\pL\s\-]+$/u|string|min:2|max:250',
    		'father_name' => 'nullable|regex:/^[\pL\s\-]+$/u|string|min:3|max:250',
		   	'mother_name' => 'nullable|regex:/^[\pL\s\-]+$/u|string|min:3|max:250',
    		'gender' => 'nullable|digits_between:1,3.',
    		'date_of_birth' => 'nullable|date_format:Y-m-d|before:10 years ago',
    		'phone_number' => 'required|numeric|digits:10|regex:/^[7896]\d{9}$/|unique:users,phone_no',
		   	'email' => 'required|email|min:6|max:250|unique:users,email',
			'Address_1' => 'nullable|string|max:2050',
    		'Address_2' => 'nullable|string|max:2050',
    		'country' => 'nullable|numeric',
    		'state' => 'nullable|numeric',
    		'city' => 'nullable|numeric',
    		'zip' => 'nullable|numeric|digits:6',
		    'profile_photo' => 'nullable|mimes:jpg,jpeg,png|max:10840',
		    'signature' => 'nullable|mimes:jpg,jpeg,png|max:10840',
    		'password' => 'required|string|min:8|max:250',
    		'password_confirmation' => 'required|min:8|same:password',
    	]);


    	if($request->hasFile('profile_photo'))
    	{
        	$image       = $request->file('profile_photo');
		    $profile_img    = $image->getClientOriginalName();
		    $image_resize = Image::make($image->getRealPath());              
		    $image_resize->resize(300, 140,);
		    $image_resize->save(public_path('uplode/users/' .$profile_img));
    	}
    	else
    	{
    		$profile_img = '';
    	}


    	if($request->hasFile('signature'))
    	{
		    $image       = $request->file('signature');
		    $signature_img    = $image->getClientOriginalName();
		    $image_resize = Image::make($image->getRealPath());              
		    $image_resize->resize(300, 140,);
		    $image_resize->save(public_path('uplode/signature/' .$signature_img));
    	}
    	else
    	{
    		$signature_img = '';
    	}

    	if($request->Input('role') != null )
    	{
    		$role =  Crypt::decrypt($request->Input('role'));
    	}
    	else
    	{
    		$role = 'USER58428D3';
    	}

		$data1 = $User->Create([
			'reg_number' => session()->get('RNUMBER'),
			'name' => ucwords($request->Input('name')),
			'email' => ucwords($request->Input('email')),
			'image' => $profile_img,
			'signature' => $signature_img,
			'gander' => $request->Input('gender'),
			'dob' => $request->Input('date_of_birth'),
			'father_name' => ucwords($request->Input('father_name')),
			'mother_name' => ucwords($request->Input('mother_name')),
			'phone_no' => $request->Input('phone_number'),
			'password' => Hash::make($request->Input('password')),
			'role' => $role,
		]);	


		$data2 = $UserVerification->Create([
			'USR01_id' => $request->Input('email'),
			'USR08_otp' => 'otp_number',
		]);

				
		$data3 = $Kyc->Create([
			'USR01_id' => $data1,
	        'USRK03_id_proof_img' => 'idcard.png',
	        'USRK03_aadhaar_proof_img' => 'aadharfront.png',
	        'USRK03_aadhaar_proof_img_back' => 'aadharback.png',
	        'USRK03_pan_img' => 'pan.png',
		]);		

		$data4 = $Address->Create([
			'USR01_id' => $data1,
	        'USRA06_type' => 1,
	        'USRA06_address' => ucwords($request->Input('Address_1')),
	        'USRA06_street' => ucwords($request->Input('Address_2')),
	        'USRA06_city' => $request->Input('city'),
	        'USRA06_state' => $request->Input('state'),
	        'USRA06_country' => $request->Input('country'),
	        'USRA06_zip' => $request->Input('zip'),
	        'USRA06_default' => 1,
		]);


		$data4 = $Wallet->Create([
			'user_id' => $data1,
	        'main_wallet' => 0.00,
	        'main_wallet1' => 0.00,
	        'main_wallet2' => 0.00,
	        'main_wallet3' => 0.00
		]);


    	if($data1 && $data2 && $data3 && $data4 && $data4)
    	{
    		return back()->with('success','Create user successfully');
    	}
    	else
    	{
    		return back()->with('warning','Can`t create user ');
    	}


    }


	protected function view_user_all(User $User)
	{
		$data['users'] = $User->GetUsers();

		return view('Users.user-view', compact('data'));
	}

	protected function search_user(request $request, User $User)
	{
		$name = $request->Input('name');
		$fname = $request->Input('father_name');
		$from = $request->Input('from_date');
		$to = $request->Input('to_date');
		
		if($name != null && $fname != null && $from != null && $to != null)
		{
			$data['users'] = $User->SearchByFNNameOrDate($name, $fname, $from, $to);
		}		
		elseif($name != null && $from != null && $to != null)
		{
			$data['users'] = $User->SearchByNameOrDate($name, $from, $to);
		}		
		elseif($fname != null && $from != null && $to != null)
		{
			$data['users'] = $User->SearchByFNameOrDate($fname, $from, $to);
		}		
		elseif($from != null && $to != null)
		{
			$data['users'] = $User->SearchByFromToDate($from, $to);
		}		
		elseif($from != null || $to != null)
		{
			if($from != null){ $date = $from; }
			if($to != null){ $date = $to; }
			$data['users'] = $User->SearchByFromDate($date);
		}
		elseif($name != null)
		{
			$data['users'] = $User->SearchByName($name);
		}		
		elseif($fname != null)
		{
			$data['users'] = $User->SearchByFName($fname);
		}		
		else
		{
			$data['users'] = $User->GetUsers();
		}

		return view('Users.user-view', compact('data'));
	}


	protected function edit_delete_users_all(User $User)
	{
		$data['users'] = $User->GetUsers();

		return view('Users.user-edit-delete', compact('data'));
	}


	protected function search_users(request $request, User $User)
	{
		$name = $request->Input('name');
		$fname = $request->Input('father_name');
		$from = $request->Input('from_date');
		$to = $request->Input('to_date');
		

		if($name != null && $fname != null && $from != null && $to != null)
		{
			$data['users'] = $User->SearchByFNNameOrDate($name, $fname, $from, $to);
		}		
		elseif($name != null && $from != null && $to != null)
		{
			$data['users'] = $User->SearchByNameOrDate($name, $from, $to);
		}		
		elseif($fname != null && $from != null && $to != null)
		{
			$data['users'] = $User->SearchByFNameOrDate($fname, $from, $to);
		}		
		elseif($from != null && $to != null)
		{
			$data['users'] = $User->SearchByFromToDate($from, $to);
		}		
		elseif($from != null || $to != null)
		{
			if($from != null){ $date = $from; }
			if($to != null){ $date = $to; }
			$data['users'] = $User->SearchByFromDate($date);
		}
		elseif($name != null)
		{
			$data['users'] = $User->SearchByName($name);
		}		
		elseif($fname != null)
		{
			$data['users'] = $User->SearchByFName($fname);
		}		
		else
		{
			$data['users'] = $User->GetUsers();
		}

		return view('Users.user-edit-delete', compact('data'));
	}




	protected function view_user_info(request $request, User $User, Address $Address, Countries $Countries, States $States, Cities $Cities)
	{
		$data['user'] = $User->SearchByReg($request->Input('search'));
		if($data['user'])
		{
			$data['add'] = $Address->GetByUser($data['user']->id);
		}
		else
		{
			$data['add'] = '';
		}
		$data['countries'] = $Countries->GetSelectData();
     	$data['states'] = $States->GetSelectData();
     	$data['cities'] = $Cities->GetSelectData();	
		return view('Users.find-user-info', compact('data'));
	}




	protected function user_information($id, User $User, Address $Address, Countries $Countries, States $States, Cities $Cities)
	{
		$id = Crypt::decrypt($id);

		$data['user'] = $User->GetById($id);
		$data['add'] = $Address->GetByUser($id);
		$data['countries'] = $Countries->GetSelectData();
     	$data['states'] = $States->GetSelectData();
     	$data['cities'] = $Cities->GetSelectData();

		return view('Users.single-user-info', compact('data'));
	}


	protected function user_edit_information($id, User $User, Address $Address, Countries $Countries, States $States, Cities $Cities)
	{
		$id = Crypt::decrypt($id);
		$data['user'] = $User->GetById($id);
		$data['add'] = $Address->GetByUser($id);
		$data['countries'] = $Countries->GetSelectData();
     	$data['states'] = $States->GetSelectData();
     	$data['cities'] = $Cities->GetSelectData();
		return view('Users.edit-user-info', compact('data'));
	}



	public function view_user_rols(Rol $Rol)
	{
		$data['all'] = $Rol->GetData();
		return view('rol.view-rols',compact('data'));
	}


	protected function change_user_role(request $request, User $User)
	{
		$id = $request->id;
		$roleID = Crypt::decrypt($request->roleID);

		$data = $User->UpdateData($id, ['role' => $roleID]);
	}



	protected function change_user_role_name(request $request, Rol $Rol)
	{

		$data = $Rol->UpdateLog($request->id, ['role_name' => $request->name]);

		if($data)
		{
			session()->flash('success','Update Successfully');
			return '1';
		}
		else
		{
			session()->flash('warning','Can`t Update');
			return '2';
		}
	}



	protected function change_user_role_view_p(request $request, Rol $Rol)
	{

		if($request->valu == 'Yes')
		{
			$data = $Rol->UpdateLog($request->id, ['view' => 2]);
		}
		if($request->valu == 'No')
		{
			$data = $Rol->UpdateLog($request->id, ['view' => 1]);
		}

		if($data)
		{
			session()->flash('success','Update Successfully');
			return '1';
		}
		else
		{
			session()->flash('warning','Can`t Update');
			return '2';
		}
	}



	protected function change_user_role_create_p(request $request, Rol $Rol)
	{
		if($request->valu == 'Yes')
		{
			$data = $Rol->UpdateLog($request->id, ['create' => 2]);
		}
		if($request->valu == 'No')
		{
			$data = $Rol->UpdateLog($request->id, ['create' => 1]);
		}

		if($data)
		{
			session()->flash('success','Update Successfully');
			return '1';
		}
		else
		{
			session()->flash('warning','Can`t Update');
			return '2';
		}
	}
	

	protected function change_user_role_update_p(request $request, Rol $Rol)
	{
		if($request->valu == 'Yes')
		{
			$data = $Rol->UpdateLog($request->id, ['update' => 2]);
		}
		if($request->valu == 'No')
		{
			$data = $Rol->UpdateLog($request->id, ['update' => 1]);
		}

		if($data)
		{
			session()->flash('success','Update Successfully');
			return '1';
		}
		else
		{
			session()->flash('warning','Can`t Update');
			return '2';
		}
	}


	protected function change_user_role_delete_p(request $request, Rol $Rol)
	{
		if($request->valu == 'Yes')
		{
			$data = $Rol->UpdateLog($request->id, ['delete' => 2]);
		}
		if($request->valu == 'No')
		{
			$data = $Rol->UpdateLog($request->id, ['delete' => 1]);
		}

		if($data)
		{
			session()->flash('success','Update Successfully');
			return '1';
		}
		else
		{
			session()->flash('warning','Can`t Update');
			return '2';
		}
	}



	protected function view_all_teams(Team $Team)
	{
		$data['teams'] = $Team->GetData();
		return view('Team.view-team', compact('data'));
	}




	protected function view_team_members(TeamMember $TeamMember, Team $Team)
	{
		$data['users'] = $TeamMember->GetData();
		$data['teams'] = $Team->GetData();
		return view('Team.view-team-member', compact('data'));
	}



	protected function change_route_permission(request $request, Rout $Rout)
	{

		$roleID = Crypt::decrypt($request->roleID);

		if($request->permissionID == 'Yes')
		{
			$data = $Rout->UpdateLog($request->routeID, [$roleID => 2]);
		}
		if($request->permissionID == 'No')
		{
			$data = $Rout->UpdateLog($request->routeID, [$roleID => 1]);
		}

		if($data)
		{
			session()->flash('success','Update Successfully');
			return '1';
		}
		else
		{
			session()->flash('warning','Can`t Update');
			return '2';
		}
	}




	protected function all_route_permissions(request $request, Rout $Rout)
	{

		$roleID = Crypt::decrypt($request->roleID);

		$data = $Rout->GetData();

		if($data[0]->$roleID == 'Yes')
		{		
			foreach($data as $value)
			{
			$data = $Rout->UpdateLog($value->id, [$roleID => 2]);
			}
		}
		else
		{	
			foreach($data as $value)
			{
			$data = $Rout->UpdateLog($value->id, [$roleID => 1]);
			}
		}

		if($data)
		{
			session()->flash('success','Update Successfully');
			return '1';
		}
		else
		{
			session()->flash('warning','Can`t Update');
			return '2';
		}
	}






	public function view_timeline()
	{
		return view('Users.timeline-page');
	}









}
